import React, { useState, useEffect, useMemo, useCallback } from "react";

import { useLoading } from "hooks/useLoading";
import { usePage } from "hooks/usePage";
import { useBreadcrumb } from "hooks/useBreadcrumb";
import useAuth from "hooks/useAuth";

import Table, { IColumns } from "designs/Table";
import TableLayout from "layouts/Table";
import AlertDialog from "components/AlertDialog";
import SVG from "designs/SVG";
import ListDataLayout from "layouts/ListDataLayout";

import { RouteComponentProps } from "react-router";
import {
  CustomerReportContainer,
  SearchBox,
  RightButtonWrapper,
  FilterButton,
} from "./styles";

import { PATH } from "common/constants/routes";
import { getQueryFromLocation } from "common/functions";
import { t } from "language";

interface ICustomerReportProps extends RouteComponentProps {}

const SIZE_PER_PAGE = 10;
const LOAD_DATA = "LOAD_DATA";

const CustomerReport: React.FC<ICustomerReportProps> = ({ location }) => {
  const [page, setPage] = usePage(getQueryFromLocation(location)?.page);
  const { startLoading, stopLoading } = useLoading();
  const [search, setSearch] = useState<string>("");
  // const [listReport, setListReport] = useState<CustomerReport[]>([]);
  const { accountInfo } = useAuth();

  useBreadcrumb([
    {
      name: t("report.report"),
      href: PATH.CONFIGURATION.SELF,
    },
  ]);

  useEffect(() => {
    invokeGetAllDataAPI();
  }, [page, search]);

  const invokeGetAllDataAPI = async () => {
    try {
      startLoading(LOAD_DATA);
      // const result = await getCustomerReportAPI({
      //   branch: accountInfo?.userInfo?._id as string,
      //   filterDayReport: "",
      // });
      // setListReport(result);
    } catch (error) {
      console.log(error);
    } finally {
      stopLoading(LOAD_DATA);
    }
  };

  const onSearch = (text: string) => setSearch(text);

  const handleChangePage = useCallback((nextPage: number) => {
    setPage(nextPage);
  }, []);

  // const handleDelete = async (record: User) => {
  //   try {
  //     const payload: IById = {
  //       id: record?._id as string,
  //     };
  //     // await deleteUser(payload);
  //     startLoading(LOAD_DATA);
  //     invokeGetAllDataAPI();
  //   } catch (error) {
  //     console.log(error);
  //   } finally {
  //     stopLoading(LOAD_DATA);
  //   }
  // };

  // const renderAction = (record: User) => {
  //   return (
  //     <div className="flex justify-end space-x-1">
  //       <AlertDialog
  //         tooltip={t("report.customer-report.delete-customer")}
  //         ButtonMenu={<SVG name="common/delete" width={20} height={20} />}
  //         title={t("report.customer-report.delete-customer")}
  //         message={t("report.customer-report.confirm-delete-customer")}
  //         onConfirm={() => handleDelete(record)}
  //       />
  //     </div>
  //   );
  // };

  const columns: IColumns = useMemo(
    () => [
      {
        text: t("report.customer-report.code"),
        dataField: "code",
      },
      {
        text: t("report.customer-report.name"),
        dataField: "name",
      },
      {
        text: t("report.customer-report.revenue"),
        dataField: "totalMoney",
      },
      {
        text: "",
        dataField: "actions",
        headerStyle: () => ({ display: "none" }),
        formatter: (_: string) => <div className="w-0"></div>,
      },
    ],
    [page],
  );

  return (
    <CustomerReportContainer>
      <ListDataLayout
        rightTopBar={
          <RightButtonWrapper>
            <FilterButton>{t("common.this-month")}</FilterButton>
            <FilterButton>{t("common.this-week")} </FilterButton>
          </RightButtonWrapper>
        }
        leftTopBar={
          <SearchBox
            onFetchData={onSearch}
            placeholder={t("report.customer-report.search-by-customer-name")}
          ></SearchBox>
        }
        title={t("report.customer-report.customer-report")}
      >
        <Table
          columns={columns}
          // data={listReport}
          data={[]}
          totalSize={10}
          sizePerPage={SIZE_PER_PAGE}
          onPageChange={handleChangePage}
          page={page}
          isRemote
        />
      </ListDataLayout>
    </CustomerReportContainer>
  );
};

export default CustomerReport;
