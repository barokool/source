import styled, { css } from "styled-components/macro";
import tw from "twin.macro";
import _SVG from "designs/SVG";

export const Container = styled.div`
  ${tw`relative`}
`;

export const Icon = styled.div`
  transform: translateY(-50%);
  ${tw`absolute z-10 leading-none left-2 top-1/2 `}
`;

export const InputWrapper = styled.div<{ error: boolean }>`
  ${tw`flex min-h-[48px] items-center rounded-sm overflow-hidden bg-white border border-solid border-gray focus:border-primary px-1.5`}
  ${({ error }) => error && tw`border-danger`}
`;

export const Input = styled.input<{
  error?: boolean;
  icon?: boolean;
  disabled: boolean;
}>(
  ({ icon, disabled }) => css`
    ${tw`flex-1 py-1 text-14 text-black placeholder-gray outline-none`}
    ${icon && tw`pl-5`}
    ${disabled && tw`cursor-not-allowed`}
  `,
);

export const Error = styled.p`
  ${tw`flex items-center mt-1 font-regular text-14 text-danger`}
`;

export const PasswordIcon = styled(_SVG)`
  ${tw`w-2.5 h-2.5 cursor-pointer`}
`;
