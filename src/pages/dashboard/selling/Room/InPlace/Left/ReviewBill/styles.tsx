import styled from "styled-components/macro";
import tw from "twin.macro";
import _Button from "designs/Button";

export const LeftReviewBillContainer = styled.div`
  ${tw`w-full h-screen relative shadow-lg`}
`;

export const ProductThumbnail = styled.img`
  ${tw`rounded-full w-3 h-3`}
`;

export const CodeTag = styled.div`
  ${tw`p-1 text-black text-16 font-semibold rounded-sm border-2 border-black w-max`}
`;

export const CodeTagWrapper = styled.div`
  ${tw`mt-2 px-3 py-2.5`}
`;
