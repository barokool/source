import { IIconSVGProps } from "typings";

const ReportIcon: React.FC<IIconSVGProps> = props => {
  return (
    <svg
      width={20}
      height={21}
      viewBox="0 0 20 21"
      fill="currentColor"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M20 19.3281H18.8281V7.53125H15.2734V19.3281H14.1016V12.2188H10.5859V19.3281H9.41406V9.875H5.89844V19.3281H4.72656V13.3906H1.17188V19.3281H0V20.5H20V19.3281Z"
        fill="#currentColor"
      />
      <path
        d="M20.0002 5.1875V0.5H15.2736V1.67188H17.9998L12.3439 7.28863L7.65645 2.60113L0.171875 10.0467L1.00039 10.8752L7.65645 4.25824L12.3439 8.94574L18.8283 2.50043V5.1875H20.0002Z"
        fill="#currentColor"
      />
    </svg>
  );
};

export default ReportIcon;
