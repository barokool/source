import styled from "styled-components/macro";
import tw from "twin.macro";
import _Button from "designs/Button";
import { Form as _Form } from "formik";

export const CaptchaWrapper = styled.div`
  ${tw``}
`;

export const Form = styled(_Form)`
  ${tw`flex flex-col gap-1`}
`;

export const ResetPasswordContainer = styled.div`
  ${tw`grid laptop:grid-cols-2 gap-2 w-full h-full bg-background my-auto`}
`;

export const ResetPasswordForm = styled.div`
  ${tw`w-full phone:w-50 p-1 phone:p-4 bg-white rounded-sm`}
`;

export const OptionWrapper = styled.div`
  ${tw`flex gap-x-1 items-center w-full justify-between`}
`;

export const PhoneInputWrapper = styled.div`
  ${tw`grid grid-cols-12 gap-x-0.5`}
`;

export const OptionTitle = styled.h3`
  ${tw`text-14 text-gray`}
`;

export const Heading = {
  Wrapper: styled.div`
    ${tw`flex flex-col gap-[15px] mb-3`}
  `,
  Title: styled.p`
    ${tw`font-semibold text-26 text-black`}
  `,
  SubTitle: styled.p`
    ${tw`font-regular text-16 text-black`}
  `,
};

export const Button = styled(_Button)`
  ${tw`w-full flex items-center bg-primary justify-center text-16 font-medium text-white py-1 mt-2`}
`;

export const LogoWrapper = styled.div`
  ${tw`hidden laptop:flex items-center justify-center w-full h-full bg-primary`}
`;

export const ElementWrapper = styled.div``;

export const Other = styled.div`
  ${tw`flex justify-between`}
`;
export const TextDesc = styled.p`
  ${tw` text-16 text-primary hover:text-primary hover:underline  cursor-pointer`}
`;

export const Title = styled.h1`
  ${tw`text-20 phone:text-26 font-semibold text-black text-center mb-2 phone:mb-4`}
`;

export const ButtonWrapper = styled.div`
  ${tw`w-full flex`}
`;
