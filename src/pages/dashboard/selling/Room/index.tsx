import React, { useState } from "react";

//language
import { t } from "language";

//locals
import { GroupTabWrapper, SellingRoomContainer } from "./styles";
import GroupTab from "designs/TabsAction";
import TakeAwayTab from "./TakeAway";
import InPlaceTab from "./InPlace";
import DeliveryTab from "./Delivery";
import BookingTable from "./BookingTable";

interface ISellingRoomProps {}

const SellingRoom: React.FC<ISellingRoomProps> = () => {
  const listSellingTypes: string[] = [
    t("selling.in-place"),
    t("selling.take-away"),
    t("selling.delivery"),
    t("selling.booking-table"),
  ];
  return (
    <SellingRoomContainer>
      <GroupTabWrapper>
        <GroupTab
          size="lg"
          titles={listSellingTypes}
          content={[
            <InPlaceTab key={0} />,
            <TakeAwayTab key={1} />,
            <DeliveryTab key={2} />,
            <BookingTable key={3} />,
          ]}
        />
      </GroupTabWrapper>
    </SellingRoomContainer>
  );
};

export default SellingRoom;
