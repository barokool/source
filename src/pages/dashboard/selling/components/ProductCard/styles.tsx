import styled from "styled-components/macro";
import tw from "twin.macro";
import BaseButton from "designs/BaseButton";

export const ProductCardContainer = styled(BaseButton)`
  ${tw`flex flex-col gap-1 justify-center text-center `}
`;
export const TextWrapper = styled.div`
  ${tw`flex flex-col gap-0.5 py-1 text-center mx-auto`}
`;

export const TextName = styled.p`
  ${tw`font-medium text-13 text-black h-4`}
`;

export const TextPrice = styled.p`
  ${tw`font-bold text-14 text-primary`}
`;

export const Image = styled.img`
  ${tw`rounded-md`}
`;
