import styled from "styled-components/macro";
import tw from "twin.macro";
import { Form as _Form } from "formik";
import _Button from "designs/Button";

export const ElementWrapper = styled.div`
  ${tw`cursor-pointer `}
`;

export const ProductDialogContainer = styled.div`
  ${tw`bg-white p-1 phone:p-2 laptop:p-5 rounded-sm`}
`;

export const Form = styled(_Form)`
  ${tw`grid gap-2`}
`;

export const ButtonWrapper = styled.div`
  ${tw`flex justify-end mt-3`}
`;

export const Button = styled(_Button)`
  ${tw`py-1 px-2 w-max flex justify-center text-center ml-1`}
`;

export const FormLabel = {
  Container: styled.div`
    ${tw`py-1.5 px-2 bg-tertiary`}
  `,
  Content: styled.h3`
    ${tw`text-16 text-black font-semibold`}
  `,
};

export const FormGroup = {
  Container: styled.div`
    ${tw`grid gap-y-4 `}
  `,
};

export const SelectUnit = {
  Container: styled.div`
    ${tw`grid grid-cols-2 gap-x-2 gap-y-3.5 items-center`}
  `,
  Content: styled.h3`
    ${tw`text-black text-14 font-semibold`}
  `,
  Price: styled.h3`
    ${tw`text-black text-16`}
  `,
};

export const QuantityButtonsWrapper = styled.div`
  ${tw`flex justify-between items-center`}
`;

export const PriceItem = styled.p`
  ${tw`w-full text-right`}
`;

export const TableWrapper = styled.div`
  ${tw`w-full grid grid-cols-3 gap-x-6 gap-y-3 p-2`}
`;

export const TableHeaderItem = styled.div`
  ${tw`text-14 text-black font-semibold`}
`;

export const TableItem = styled.div`
  ${tw`text-14 text-black`}
`;

export const UpDownQuantityWrapper = styled.div`
  ${tw`flex w-full items-center justify-between`}
`;
