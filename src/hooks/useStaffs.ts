import { GetAllUserArgs, RoleEnum, useGetAllUser } from "apiCaller";
import { useCallback, useEffect } from "react";
import { fragmentGetAllUser } from "services/user";
import useAuth from "./useAuth";

export const useStaffs = () => {
  const { accountInfo } = useAuth();
  const [getAllUser, { data, called, loading, error }] =
    useGetAllUser(fragmentGetAllUser);
  useEffect(() => {
    invokeSetAllStaffs({
      filterUser: {
        role: RoleEnum.Staff,
        branches: accountInfo?.userInfo?.branches?.map(
          branch => branch._id,
        ) as string[],
      },
    });
  }, []);
  const invokeSetAllStaffs = useCallback((arg: GetAllUserArgs) => {
    getAllUser({
      variables: {
        filterUser: {
          role: arg.filterUser?.role,
          branches: arg.filterUser?.branches,
        },
      },
    });
  }, []);

  return {
    ListStaff: data,
    invokeSetAllStaffs,
  };
};
