import { FilterUser, useGetAllUser, User } from "apiCaller";
import { useState, useCallback, useEffect } from "react";
import { fragmentGetAllUser } from "services/user";

export const useUser = (filterUser: FilterUser) => {
  const [listUser, setListUser] = useState<User[]>([]);

  useEffect(() => {
    setAllUser();
  }, []);
  const setAllUser = useCallback(async () => {
    const [getAllUser, { data }] = useGetAllUser(fragmentGetAllUser);
    await getAllUser({
      variables: {
        page: 0,
        filterUser: filterUser,
      },
    });

    setListUser(data?.getAllUser?.results || []);
  }, []);

  return {
    listUser,
    setAllUser,
    setListUser,
  };
};
