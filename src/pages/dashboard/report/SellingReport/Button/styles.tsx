import styled, { css } from "styled-components/macro";
import tw from "twin.macro";
import { Listbox } from "@headlessui/react";

const formControlCommon = (
  isError: boolean | undefined,
  disabled: boolean | undefined,
) =>
  css`
    ${tw`
      w-full 
      bg-white
      px-1.5
      h-4
      focus-within:border-black
      text-14
      font-medium
      text-black
      placeholder-gray
    `}
    ${!isError
      ? tw`border-gray focus:border-black group-focus:border-black `
      : tw`border-danger focus:border-secondary group-focus:border-secondary`}
    ${disabled && tw`pointer-events-none opacity-60`}
  `;

export const SelectContainer = styled.div`
  ${tw`w-full relative`}
`;

export const HiddenInput = styled.input`
  ${tw`absolute w-1 h-1 opacity-0 `}
`;

export const ListboxButton = styled(Listbox.Button)`
  ${tw`relative w-full text-left cursor-pointer`}
`;

export const ListboxOptionsContainer = styled(Listbox.Options)`
  ${tw`absolute z-30 w-full py-1 mt-1 overflow-auto border rounded shadow-as-border bg-white max-h-60 focus:outline-none border-neutral-4`}
`;

export const MenuButton = styled.div<{
  isError: boolean;
  disabled: boolean;
}>`
  ${tw`grid items-center`}
  ${({ isError, disabled }) => formControlCommon(isError, disabled)}
  grid-template-columns: 1fr 25px;
`;

export const Text = styled.p`
  ${tw`truncate `}
`;

export const Placeholder = styled.p`
  ${tw`truncate text-neutral-3`}
`;

export const MenuItem = styled.div<{ active?: boolean }>`
  ${tw` w-full px-1.5 py-0.5 text-14 font-regular bg-white cursor-pointer`}
  ${({ active }) => active && tw`bg-neutral-4`}
`;

export const EmptyData = styled.div`
  ${tw`w-full flex items-center justify-center py-0.5 text-14 font-semibold text-neutral-3`}
`;

export const LabelWrapper = styled.div`
  ${tw`flex items-center justify-between absolute top-[-15px] left-[20px] z-10 bg-white rounded-sm px-[4px] `}
`;
