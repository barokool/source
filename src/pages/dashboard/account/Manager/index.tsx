import React from "react";
import { PATH } from "common/constants/routes";

//hooks
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { useBreadcrumb } from "hooks/useBreadcrumb";

//styles
import { ManagerContainer } from "./styles";

import { t } from "language";

interface IManagerProps {}

const Manager: React.FC<IManagerProps> = props => {
  const dispatch = useDispatch();

  useEffect(() => {
    setUpBreadcrumb();
  }, []);

  const setUpBreadcrumb = () => {
    dispatch(
      useBreadcrumb([
        {
          name: t("account.manager.account"),
          href: PATH.DASHBOARD,
        },
        {
          name: t("account.manager.system-manager"),
        },
      ]),
    );
  };
  return (
    <ManagerContainer>{t("account.manager.manager-page")}</ManagerContainer>
  );
};

export default Manager;
