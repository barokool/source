import { randomId } from "common/functions";
import FormControlErrorHelper from "common/styles/FormControlErrorHelper";
import SVG from "designs/SVG";
import { useField, useFormikContext } from "formik";
import CheckboxIcon from "icons/Checkbox";
import {
  CSSProperties,
  useCallback,
  useEffect,
  useLayoutEffect,
  useState,
} from "react";
import {
  CheckboxButtonBox,
  CheckboxButtonContainer,
  Input,
  Label,
  StyleRadioButton,
  Title,
  Wrapper,
} from "./styles";

interface IValue {
  _id?: string;
  name?: string;
  value?: string;
  [key: string]: any;
}

interface ICheckboxButtonProps {
  name: string;
  isChecks?: boolean[];
  label?: string;
  values: IValue[];
  fieldLabel?: string;
  fieldValue?: string;
  vertical?: boolean;
  colNumber?: number;
  classNameList?: string;
  classNameLabelBox?: string;
  styleLabelBox?: CSSProperties;
  onChange?: (value: IValue) => void;
  renderOption?: (props: { option: IValue; checked: boolean }) => JSX.Element;
}

const CheckboxButton: React.FC<ICheckboxButtonProps> = props => {
  const {
    values = [],
    isChecks = [],
    label,
    name,
    onChange,
    fieldLabel = "name",
    fieldValue = "_id",
    vertical = false,
    colNumber = 0,
    renderOption,
    classNameLabelBox,
    classNameList,
    styleLabelBox,
    ...restProps
  } = props;

  const [id, setId] = useState(randomId());
  const [field, meta] = useField(name);
  const [checkedList, setCheckedList] = useState<IValue[]>([]);

  const isError: boolean = Boolean(!!meta.error && !!meta.touched);

  const { setFieldValue } = useFormikContext();

  const handleChange = (value: IValue, index: number) => {
    console.log({ checkedList, value });

    const checkFound = checkedList.find(item => item.value === value.value);

    let checkedListNew: IValue[] = checkFound
      ? checkedList.filter(value => value.value !== checkFound.value)
      : [...checkedList, value];

    console.log({ checkedListNew });

    setCheckedList(checkedListNew);
    onChange && onChange(value);
    setFieldValue(name, checkedListNew);
  };

  useEffect(() => {
    if (values) {
      setCheckedList([...values]);
    }
  }, [values]);

  return (
    <CheckboxButtonContainer>
      {label && <Title>{label}</Title>}
      <CheckboxButtonBox
        role="group"
        aria-labelledby="checkbox-group"
        vertical={vertical}
        colNumber={colNumber}
        className={classNameList}
      >
        {values.map((value, index) => (
          <Wrapper key={`checkbox-button-${id}-${index}`}>
            <Input
              id={`option-${name}-${index}`}
              type="checkbox"
              value={value.value}
              defaultChecked={isChecks?.[index] || false}
              onChange={() => handleChange(value, index)}
              name={name}
              {...(restProps as any)}
            />
            <Label
              htmlFor={`option-${name}-${index}`}
              className={`${classNameLabelBox}`}
              style={styleLabelBox}
            >
              <StyleRadioButton
                checked={
                  isChecks?.[index] ||
                  !!checkedList.find(item => item.value === value.value)
                }
                isError={isError}
              >
                <CheckboxIcon className="text-white" />
              </StyleRadioButton>
              {renderOption
                ? renderOption({
                    option: value,
                    checked: !!checkedList.find(
                      item => item.value === value.value,
                    ),
                  })
                : value[fieldLabel]}
            </Label>
          </Wrapper>
        ))}
        {isError && (
          <FormControlErrorHelper>{meta?.error}</FormControlErrorHelper>
        )}
      </CheckboxButtonBox>
    </CheckboxButtonContainer>
  );
};

export default CheckboxButton;
