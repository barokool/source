import * as AWS from "aws-sdk";
import {
  AWS_ACCESS_KEY_ID,
  AWS_PUBLIC_BUCKET_NAME,
  AWS_REGION_NAME,
  AWS_SECRET_ACCESS_KEY,
} from "common/env";
import { randomId } from "common/functions";

AWS.config.update({
  accessKeyId: AWS_ACCESS_KEY_ID,
  secretAccessKey: AWS_SECRET_ACCESS_KEY,
});

const myBucket = new AWS.S3({
  params: { Bucket: AWS_PUBLIC_BUCKET_NAME },
  region: AWS_REGION_NAME,
});

interface IUploadImageToS3AWSArgs {
  file: File | Buffer;
  onProgress?: (progress: number, loaded: number, total: number) => void;
  ContentType?: string;
}

export const uploadImageToS3AWS = ({
  file,
  onProgress,
  ContentType = "image/jpg",
}: IUploadImageToS3AWSArgs): Promise<string> =>
  new Promise((resolve, reject) => {
    const params: AWS.S3.PutObjectRequest = {
      ACL: "public-read",
      Body: file,
      Bucket: AWS_PUBLIC_BUCKET_NAME || "",
      Key: `${randomId()}-${(file as any)?.name || "string.js"}`,
      ContentType: ContentType,
    };

    console.log({ params });

    myBucket
      .putObject(params)
      .on("httpUploadProgress", ({ loaded, total }) => {
        onProgress?.(loaded / total, loaded, total);
      })
      .send((err, data) => {
        if (err) reject?.(err);
        const url = `https://${params.Bucket}.s3-${AWS_REGION_NAME}.amazonaws.com/${params.Key}`;
        resolve(url);
      });
  });
