import React, { useEffect, useMemo, useState } from "react";
import { RouteComponentProps } from "react-router";
import { toast } from "react-toastify";
import i18n, { t } from "language";

//styles
import ListDataLayout from "layouts/ListDataLayout";
import Spinner from "icons/Spinner";
import Table, { IColumns } from "designs/Table";
import ActionButtons from "designs/ActionButton";

import AddPrintDialog from "./PrinterTemplate/AddPrintDialog";
import EmptyPrinter from "./PrinterTemplate/EmptyPrinter";

import { ButtonCustom, GeneralSettingContainer } from "./styles";

//api
import { usePage } from "hooks/usePage";
import { getQueryFromLocation } from "common/functions";
import { Printer, useDeletePrinter, useGetAllPrinter } from "apiCaller";
import useAuth from "hooks/useAuth";
import { fragmentGetAllPrinterTemplate } from "services/printer";
import { useBreadcrumb } from "hooks/useBreadcrumb";
import { PATH } from "common/constants/routes";

const GeneralSetting: React.FC<RouteComponentProps> = ({ location }) => {
  const { accountInfo } = useAuth();

  useBreadcrumb([
    {
      name: t("configuration.configuration"),
      href: PATH.CONFIGURATION.SELF,
    },
    { name: t("configuration.general-settings.general-settings") },
  ]);
  const [getAllPrinter, { data, loading }] = useGetAllPrinter(
    fragmentGetAllPrinterTemplate,
  );

  const [deletePrinter, { error: DeleteError, data: deleteData }] =
    useDeletePrinter();

  const [page, setPage] = usePage(getQueryFromLocation(location)?.page);

  const handleDelete = (record: Printer) => {
    deletePrinter({
      variables: {
        listId: [record._id as string],
      },
    });
  };

  useEffect(() => {
    invokeGettingPrinter();
  }, []);

  useEffect(() => {
    if (!deleteData && DeleteError)
      toast.error(t("configuration.general-settings.delete-error"));
    if (deleteData && !DeleteError) {
      toast.success(t("configuration.general-settings.delete-notify"));
    }
    invokeGettingPrinter();
  }, [DeleteError, deleteData]);

  const renderActions = (record: Printer) => {
    return (
      <ActionButtons
        buttons={{
          edit: {
            DialogContent: ({ onClose }) => {
              return (
                <AddPrintDialog
                  onClose={onClose}
                  open
                  editField={record}
                  onSuccess={() => {
                    invokeGettingPrinter();
                  }}
                />
              );
            },
          },
          delete: {
            title: t("configuration.general-settings.confirm-delete-printer"),
            message: t("configuration.general-settings.confirm-delete-printer"),
            onDelete: () => {
              handleDelete(record);
            },
          },
        }}
      />
    );
  };

  const columns: IColumns = useMemo(
    () => [
      {
        text: t("configuration.general-settings.ip-address"),
        dataField: "ipAddress",
        formatter: (ip: string) => ip,
      },
      {
        text: t("configuration.general-settings.printer-name"),
        dataField: "name",
        formatter: (name: string) => name,
      },
      {
        text: t("configuration.general-settings.actions"),
        dataField: "",
        formatter: (_: string, record: Printer) => renderActions(record),
      },
    ],
    [page, i18n.language],
  );

  const invokeGettingPrinter = () => {
    getAllPrinter({
      variables: {
        filter: {
          branch: accountInfo?.userInfo?.branches?.[0]?._id as string,
        },
      },
    });
  };

  return (
    <GeneralSettingContainer>
      <ListDataLayout
        title={t("configuration.general-settings.general-settings")}
        rightTopBar={
          <AddPrintDialog
            ButtonMenu={
              <ButtonCustom>
                {t("configuration.general-settings.add-new-printer")}
              </ButtonCustom>
            }
            onSuccess={() => {
              invokeGettingPrinter();
            }}
            title={t("configuration.general-settings.add-new-printer")}
          />
        }
      >
        {data?.getAllPrinter?.results?.length == 0 ? (
          <EmptyPrinter onClick={() => {}} />
        ) : (
          <>
            {loading ? (
              <div>
                <Spinner />
              </div>
            ) : (
              ""
            )}
            <Table
              columns={columns}
              data={data?.getAllPrinter?.results || []}
              page={page}
              totalSize={data?.getAllPrinter?.totalCount as number}
            />
          </>
        )}
      </ListDataLayout>
    </GeneralSettingContainer>
  );
};

export default GeneralSetting;
