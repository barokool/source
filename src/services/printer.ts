import { GenFields, Printer, PrinterResult } from "apiCaller";

export const fragmentGetAllPrinterTemplate: GenFields<PrinterResult> = [
  {
    results: [
      "_id",
      "branchLocation",
      "branchName",
      "branchNote",
      "config",
      "customerLocation",
      "customerName",
      "customerPhoneNumber",
      "dateOfPayment",
      "fontSize",
      "footerContent",
      "ipAddress",
      "method",
      "name",
      "title",
      { branch: ["_id", "name"] },
      { configBill: ["method", "numberOfPrints"] },
      {
        orderInfo: [
          "discountOrder",
          "excessCash",
          "shippingPrice",
          "tax",
          "totalAmount",
          "totalAmountReceived",
          "totalQuantity",
        ],
      },
      { productInfo: ["comboDetail", "discountProduct", "sku"] },
    ],
  },
  "totalCount",
];

export const fragmentCreatePrinter: GenFields<Printer> = [
  "_id",
  "branchLocation",
  "branchName",
  "branchNote",
  "config",
  "customerLocation",
  "customerName",
  "customerPhoneNumber",
  "dateOfPayment",
  "fontSize",
  "footerContent",
  "ipAddress",
  "method",
  "name",
  "title",
  { branch: ["_id", "name"] },
  { configBill: ["method", "numberOfPrints"] },
  {
    orderInfo: [
      "discountOrder",
      "excessCash",
      "shippingPrice",
      "tax",
      "totalAmount",
      "totalAmountReceived",
      "totalQuantity",
    ],
  },
  { productInfo: ["comboDetail", "discountProduct", "sku"] },
];

export const fragmentGetPrinterById: GenFields<Printer> = [
  "_id",
  "branchLocation",
  "branchName",
  "branchNote",
  "config",
  "customerLocation",
  "customerName",
  "customerPhoneNumber",
  "dateOfPayment",
  "fontSize",
  "footerContent",
  "ipAddress",
  "method",
  "name",
  "title",
  { branch: ["_id", "name"] },
  { configBill: ["method", "numberOfPrints"] },
  {
    orderInfo: [
      "discountOrder",
      "excessCash",
      "shippingPrice",
      "tax",
      "totalAmount",
      "totalAmountReceived",
      "totalQuantity",
    ],
  },
  { productInfo: ["comboDetail", "discountProduct", "sku"] },
];
