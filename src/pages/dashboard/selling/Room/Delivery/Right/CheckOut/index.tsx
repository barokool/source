import React, { useEffect, useMemo, useState } from "react";
import UserDefaultAvatar from "assets/images/selling/user-default-avatar.jpg";
import Table, { IColumns } from "designs/Table";
import SVG from "designs/SVG";
import SimpleSelect from "designs/SimpleSelect";

import AlertDialog from "components/AlertDialog";
import { toast } from "react-toastify";

import { IRootState } from "typings";
import { fragmentGetOrderById, fragmentUpdateOrder } from "services/order";
import { fragmentGetAllUser } from "services/user";
import {
  Order,
  OrderItem,
  Product,
  User,
  ProductUnit,
  useUpdateOrder,
  useDeleteOrderItem,
  useGetOrderById,
  OrderStatusEnum,
  useGetAllUser,
  RoleEnum,
  Branch,
} from "apiCaller";

//redux
import { useSelector, useDispatch } from "react-redux";
import { setOrderSelected } from "redux/slices/order";
import { setActionSuccess } from "redux/slices/common";

//hooks
import useAuth from "hooks/useAuth";

//languages
import { t } from "language";
import { setSellingPageSelected } from "redux/slices/sellingPage";
import OrderItemDialog from "../../../../components/OrderItemDialog";

//locals
import {
  FooterContainer,
  HeaderContainer,
  RightCheckOutContainer,
  OrderCodeTag,
  TemporaryCalculatedContainer,
  TemporaryCalculatedContent,
  TemporaryCalculatedPrice,
  ButtonWrapper,
  FooterItemsContainer,
  LeftButton,
  RightButton,
  FooterItemWrapper,
  RenderActionContainer,
  OptionAvatar,
  OptionLabel,
  OptionWrapper,
  ProductThumbnail,
} from "./styles";

interface IRightCheckOutProps {}

const RightCheckOut: React.FC<IRightCheckOutProps> = props => {
  //redux
  const { orderSelected } = useSelector((state: IRootState) => state.order);
  const dispatch = useDispatch();
  const { accountInfo } = useAuth();
  const { branches: listBranch } = accountInfo.userInfo;

  //local states
  const listOrderItem = (orderSelected && orderSelected.orderItems) || [];
  const [branchSelected, setBranchSelected] = useState<Branch | null>(
    listBranch?.[0] || null,
  );
  const [listStaff, setListStaff] = useState<User[]>([]);
  const [staffSelected, setStaffSelected] = useState<User | null>(null);
  const [listDriver, setListDriver] = useState<User[]>([]);
  const [driverSelected, setDriverSelected] = useState<User | null>(null);
  const [order, setOrder] = useState<Order | null>(null);
  const [page, setPage] = useState<number>(0);

  //api
  const [getOrderById, { data: orderReturned, error: getOrderByIdErrors }] =
    useGetOrderById(fragmentGetOrderById);
  const [
    deleteOrderItem,
    { loading: deleteOrderItemLoading, error: deleteOrderItemError },
  ] = useDeleteOrderItem();
  const [updateOrder, { data: orderUpdated, error: updateOrderError }] =
    useUpdateOrder(fragmentUpdateOrder);
  const [getAllStaff, { data: listStaffReturned, error: getAllStaffError }] =
    useGetAllUser(fragmentGetAllUser);
  const [getAllDriver, { data: listDriverReturned, error: getAllDriverError }] =
    useGetAllUser(fragmentGetAllUser);

  //Call API
  useEffect(() => {
    invokeGetAllDataAPI();
  }, []);

  //toast errors when they occur
  useEffect(() => {
    if (getOrderByIdErrors) {
      toast.error(getOrderByIdErrors.message);
    }
  }, [getOrderByIdErrors]);

  useEffect(() => {
    if (updateOrderError) {
      toast.error(updateOrderError.message);
    }
  }, [updateOrderError]);

  useEffect(() => {
    if (deleteOrderItemError) {
      toast.error(deleteOrderItemError?.message);
    }
  }, [deleteOrderItemError]);

  useEffect(() => {
    if (getAllStaffError) {
      toast.error(getAllStaffError.message);
    }
  }, [getAllStaffError]);

  useEffect(() => {
    if (getAllDriverError) {
      toast.error(getAllDriverError.message);
    }
  }, [getAllDriverError]);

  //Catch data returned from API
  useEffect(() => {
    orderReturned && setOrder(orderReturned?.getOrderById as Order);
    orderUpdated && setOrder(orderUpdated?.updateOrder as Order);
  }, [orderReturned, orderUpdated]);

  useEffect(() => {
    if (order) {
      dispatch(setOrderSelected(order));
    }
  }, [order]);

  useEffect(() => {
    if (listStaffReturned) {
      setListStaff(listStaffReturned.getAllUser?.results as User[]);
    }
  }, [listStaffReturned]);

  useEffect(() => {
    if (listDriverReturned) {
      setListDriver(listDriverReturned.getAllUser?.results as User[]);
    }
  }, [listDriverReturned]);

  useEffect(() => {
    if (orderUpdated) {
      toast.success(t("selling.save-order-success"));
      handleRefreshOrder();
      dispatch(setActionSuccess());
      dispatch(setOrderSelected(orderUpdated.updateOrder as Order));
      dispatch(setSellingPageSelected("Room"));
    }
  }, [orderUpdated]);

  //functions
  const handleRefreshOrder = () => {
    getOrderById({
      variables: { id: orderSelected?._id as string },
    });
  };

  const handleApprovingOrder = () => {
    updateOrder({
      variables: {
        id: orderSelected?._id as string,
        input: {
          status: OrderStatusEnum.Approving,
          fromBranch: accountInfo?.userInfo.branches?.[0]._id as string,
          driver: driverSelected?._id as string,
        },
      },
    });
  };

  const invokeGetAllDataAPI = () => {
    getAllStaff({
      variables: {
        filterUser: {
          role: RoleEnum.Staff,
          branches: [branchSelected?._id as string] as string[],
        },
      },
    });
    getAllDriver({
      variables: {
        filterUser: {
          role: RoleEnum.Driver,
          branches: [branchSelected?._id as string] as string[],
        },
      },
    });
  };

  const renderAction = (record: OrderItem) => {
    return (
      <RenderActionContainer>
        <OrderItemDialog
          onSuccess={() => {
            handleRefreshOrder();
          }}
          title={t("common.edit")}
          editFields={record}
          ButtonMenu={<SVG name="common/edit" width={20} height={20} />}
        />
        <AlertDialog
          tooltip={t("common.delete")}
          ButtonMenu={<SVG name="common/delete" width={20} height={20} />}
          title={t("product.product-category.delete-product-category")}
          message={t(
            "product.product-category.confirm-delete-product-category",
          )}
          loading={deleteOrderItemLoading}
          onConfirm={async () => {
            try {
              await deleteOrderItem({
                variables: {
                  idOrder: orderSelected?._id as string,
                  listIdOrderItem: [record._id as string],
                },
              });
              handleRefreshOrder();
              dispatch(setActionSuccess());
              toast.success(t("selling.delete-item-success"));
            } catch (error) {
              console.log(error);
            }
          }}
        />
      </RenderActionContainer>
    );
  };

  const columns: IColumns = useMemo(
    () => [
      {
        text: t("selling.product.code"),
        dataField: "product",
        formatter: (product: Product) => product.barcode,
      },
      {
        text: t("selling.product.name"),
        dataField: "product",
        formatter: (product: Product) => product.name,
      },
      {
        text: t("selling.product.image"),
        dataField: "product",
        formatter: (product: Product) => (
          <ProductThumbnail
            src={
              (product.thumbnail?.small as string) ||
              (product.thumbnail?.medium as string) ||
              (product.thumbnail?.default as string)
            }
          />
        ),
      },
      {
        text: t("selling.product.size"),
        dataField: "unit",
        formatter: (unit: ProductUnit) => unit?.name,
      },
      {
        text: t("selling.product.quantity"),
        dataField: "quantity",
      },
      {
        text: t("selling.product.price"),
        dataField: "product",
        formatter: (product: Product) =>
          product.price?.toString().prettyMoney(),
      },
      {
        text: t("selling.product.total-price"),
        dataField: "totalPrice",
        formatter: (totalPrice: number) => totalPrice?.toString().prettyMoney(),
      },
      {
        text: t("common.actions"),
        dataField: "actions",
        formatter: (_: string, record: OrderItem) => renderAction(record),
      },
    ],
    [page],
  );

  return (
    <>
      <RightCheckOutContainer>
        <HeaderContainer>
          <OrderCodeTag>#{orderSelected?.barcode}</OrderCodeTag>
        </HeaderContainer>
        <Table
          columns={columns}
          data={listOrderItem as OrderItem[]}
          totalSize={listOrderItem?.length as number}
          sizePerPage={10}
          onPageChange={() => {}}
          isRemote
          page={page}
          className="mt-0.5 h-35"
        />
        <FooterContainer>
          <TemporaryCalculatedContainer>
            <TemporaryCalculatedContent>
              {t("selling.total")}
            </TemporaryCalculatedContent>
            <TemporaryCalculatedPrice>
              {orderSelected?.price?.finalPrice}€
            </TemporaryCalculatedPrice>
          </TemporaryCalculatedContainer>
        </FooterContainer>
        <FooterItemsContainer>
          <FooterItemWrapper>
            <SimpleSelect
              options={listBranch as Branch[]}
              onSelect={branch => setBranchSelected(branch)}
              optionSelected={branchSelected}
              placeholder={t("selling.select-branch")}
            />
          </FooterItemWrapper>
          <FooterItemWrapper>
            <SimpleSelect
              options={listStaff}
              onSelect={staff => setStaffSelected(staff)}
              optionSelected={staffSelected}
              placeholder={t("selling.select-staff")}
              optionTarget="displayName"
              renderOption={staff => (
                <OptionWrapper>
                  <OptionAvatar
                    src={(staff?.avatar?.medium as string) || UserDefaultAvatar}
                  />
                  <OptionLabel>{staff?.displayName}</OptionLabel>
                </OptionWrapper>
              )}
            />
          </FooterItemWrapper>
          <FooterItemWrapper>
            <SimpleSelect
              options={listDriver}
              onSelect={driver => setDriverSelected(driver)}
              optionSelected={driverSelected}
              placeholder={t("selling.select-driver")}
              optionTarget="displayName"
              renderOption={driver => (
                <OptionWrapper>
                  <OptionAvatar
                    src={
                      (driver?.avatar?.medium as string) || UserDefaultAvatar
                    }
                  />
                  <OptionLabel>{driver?.displayName}</OptionLabel>
                </OptionWrapper>
              )}
            />
          </FooterItemWrapper>
        </FooterItemsContainer>
        <ButtonWrapper>
          <LeftButton onClick={() => dispatch(setOrderSelected(null))}>
            {t("common.cancel")}
          </LeftButton>
          <RightButton
            onClick={() => {
              handleApprovingOrder();
            }}
          >
            {t("selling.check-out")}
          </RightButton>
        </ButtonWrapper>
      </RightCheckOutContainer>
    </>
  );
};

export default RightCheckOut;
