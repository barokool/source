import React, { useEffect } from "react";
import OrderCompletedImage from "assets/images/selling/order-completed.png";

//api
import { useCreateOrder, OrderTypeEnum } from "apiCaller";
import { fragmentCreateOrder } from "services/order";

//redux
import { useDispatch, useSelector } from "react-redux";
import { setOrderSelected } from "redux/slices/order";
import { setActionSuccess } from "redux/slices/common";
import { IRootState } from "typings";

//languages
import { t } from "language";

//locals
import {
  RightCompletedContainer,
  RightCompleteWrapper,
  Image,
  Content,
  AddButton,
} from "./styles";

interface IRightCompletedProps {}

const RightCompleted: React.FC<IRightCompletedProps> = props => {
  const { branchSelected } = useSelector(
    (state: IRootState) => state.sellingPage,
  );
  const dispatch = useDispatch();
  const [createOrder, { data: orderCreated, loading, called }] =
    useCreateOrder(fragmentCreateOrder);

  useEffect(() => {
    if (orderCreated) {
      dispatch(setOrderSelected(orderCreated?.createOrder));
      dispatch(setActionSuccess());
    }
  }, [orderCreated?.createOrder]);

  const handleCreateOrder = async () => {
    await createOrder({
      variables: {
        input: {
          type: OrderTypeEnum.In_place,
          fromBranch: branchSelected?._id as string,
        },
      },
    });
  };

  return (
    <RightCompletedContainer>
      <RightCompleteWrapper>
        <Image src={OrderCompletedImage} alt="order-completed" />
        <Content.Wrapper>
          <Content.Title>{t("selling.order-completed")}</Content.Title>
          <Content.Description>
            {t("selling.order-completed-message")}
          </Content.Description>
        </Content.Wrapper>
        <AddButton primary onClick={() => handleCreateOrder()}>
          {t("selling.add-order")}
        </AddButton>
      </RightCompleteWrapper>
    </RightCompletedContainer>
  );
};

export default RightCompleted;
