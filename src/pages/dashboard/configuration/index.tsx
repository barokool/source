import React, { useMemo } from "react";
import {
  ConfigChildrenContainer,
  ConfigurationPageContainer,
  InfoLabel,
} from "./styles";

import { PATH } from "common/constants/routes";

import BranchIcon from "icons/Branch";
import LocationIcon from "icons/Location";
import SettingIcon from "icons/Setting";
import CustomerIcon from "icons/Customer";
import DecentralizedIcon from "icons/Decentralized";

import { Link } from "react-router-dom";
import i18n, { t } from "language";
import ConfigChildren from "components/ConfigChildren";
import { useBreadcrumb } from "hooks/useBreadcrumb";

export interface IChildrenConfig {
  Icon?: React.ReactNode;
  title?: string;
  description?: string;
  href?: string;
}

const ConfigurationPage: React.FC = () => {
  useBreadcrumb([
    {
      name: t("configuration.configuration"),
      href: "",
    },
  ]);

  const chilrens = useMemo<IChildrenConfig[]>(
    () => [
      {
        Icon: (
          <>
            <BranchIcon />
          </>
        ),
        title: t("configuration.branch-management.branch-management"),
        description: t("configuration.branch-management.detail"),
        href: PATH.CONFIGURATION.BRANCH_MANAGEMENT,
      },
      {
        Icon: (
          <>
            {/* <MultUsersIcon /> */}
            <BranchIcon />
          </>
        ),
        title: t("configuration.staff-management.staff-management"),
        description: t("configuration.staff-management.detail"),
        href: PATH.CONFIGURATION.EMPLOYEES_MANAGEMENT,
      },
      {
        Icon: (
          <>
            <LocationIcon />
          </>
        ),
        title: t("configuration.account-info.account-info"),
        description: t("configuration.account-info.detail"),
        href: PATH.CONFIGURATION.ACCOUNT_INFORMATION,
      },
      {
        Icon: (
          <>
            <SettingIcon />
          </>
        ),
        title: t("configuration.general-settings.general-settings"),
        description: t("configuration.general-settings.detail"),
        href: PATH.CONFIGURATION.GENERAL_SETTING.SELF,
      },
      {
        Icon: (
          <>
            <CustomerIcon />
          </>
        ),
        title: t("configuration.customer-management.customer-management"),
        description: t("configuration.customer-management.detail"),
        href: PATH.CONFIGURATION.CUSTOMERS_MANAGEMENT,
      },
      {
        Icon: (
          <>
            <DecentralizedIcon />
          </>
        ),
        title: t("configuration.role-delegation.role-delegation"),
        description: t("configuration.role-delegation.detail"),
        href: PATH.CONFIGURATION.ROLE_DELEGATION,
      },
    ],
    [i18n.language],
  );

  return (
    <ConfigurationPageContainer>
      {/*<TopFunctionContainer>
         <AddButton primary>
          + {t("configuration.generate-complete-report")}
        </AddButton>
      </TopFunctionContainer> */}
      <InfoLabel>{t("configuration.store-information")}</InfoLabel>
      <ConfigChildrenContainer>
        {chilrens.map(item => (
          <Link key={item.href} to={item.href as string}>
            <ConfigChildren
              Icon={item.Icon}
              title={item.title}
              description={item.description}
              href={item.href}
            />
          </Link>
        ))}
      </ConfigChildrenContainer>
    </ConfigurationPageContainer>
  );
};

export default ConfigurationPage;
