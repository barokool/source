import styled from "styled-components/macro";
import tw from "twin.macro";
import SVG from "designs/SVG";
import BaseButton from "designs/BaseButton";
import { TableWrapper } from "layouts/Table/styles";

export const OrderCardContainer = styled(BaseButton)<{
  active?: boolean;
}>`
  ${tw` p-1 rounded-sm shadow-md cursor-pointer hover:shadow-xl border-solid border-2`}
  ${({ active }) => (active ? tw`border-primary ` : tw`border-transparent`)}
`;

export const OrderCodeTag = styled.h3`
  ${tw`text-body-color text-12 text-left`}
`;

export const LineWrapper = styled.div`
  ${tw`mt-1 flex justify-between items-center`}
`;

export const GroupIcon = {
  Wrapper: styled.div`
    ${tw`flex items-center`}
  `,
  Icon: styled(SVG)`
    ${tw`w-1.5 h-1.5 mr-0.5`}
  `,
  Content: styled.h3`
    ${tw`text-12`}
  `,
};

export const TableCode = styled.h3`
  ${tw`text-black text-12`}
`;

export const Price = styled.h3`
  ${tw`text-black text-14`}
`;

export const LowSkeletonLine = styled.div`
  ${tw`bg-line h-1`}
`;

export const HighSkeletonLine = styled.div`
  ${tw`bg-line h-2`}
`;
