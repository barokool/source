import styled from "styled-components/macro";
import tw from "twin.macro";

export const TwoSideLayoutContainer = styled.div`
  ${tw`  `}
`;

export const Box = styled.div`
  ${tw`grid grid-cols-2 gap-5`}
`;

export const LeftSide = styled.div`
  ${tw`flex flex-col  h-screen gap-1 `}
`;

export const RightSide = styled.div`
  ${tw`  `}
`;

export const ListBox = styled.div`
  ${tw`gap-1 overflow-auto flex-1`}
`;

export const ButtonBox = styled.div`
  ${tw` w-full  gap-x-2 grid-cols-2 flex`}
`;
