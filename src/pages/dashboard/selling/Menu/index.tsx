import React from "react";

//redux
import { useSelector } from "react-redux";
import { IRootState } from "typings";

//api
import { OrderStatusEnum } from "apiCaller";

//locals
import MenuLeft from "./Left";
import MenuRight from "./Right";
import { NarrowerWrapper, SellingMenuContainer, WiderWrapper } from "./styles";

interface ISellingMenuProps {
  listBusinessProductType?: string[];
}

const SellingMenu: React.FC<ISellingMenuProps> = props => {
  const { orderSelected } = useSelector((state: IRootState) => state.order);
  return (
    <SellingMenuContainer>
      <NarrowerWrapper>
        <MenuLeft />
      </NarrowerWrapper>
      <WiderWrapper>
        {!(orderSelected?.status === OrderStatusEnum.Approving) && (
          <MenuRight />
        )}
      </WiderWrapper>
    </SellingMenuContainer>
  );
};

export default SellingMenu;
