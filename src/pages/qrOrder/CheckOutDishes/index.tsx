import React, { useEffect, useState } from "react";

import {
  CheckOutDishesContainer,
  HeaderContainer,
  HeaderLabel,
  ButtonsWrapper,
  ChangeQuantityButton,
  QuantityContent,
  BodyContainer,
  LeftButtonContent,
  RightButtonContent,
} from "./styles";
import SVG from "designs/SVG";
import DishCard from "components/DishCard";
import CheckOutButton from "components/CheckOutButton";

import Item1 from "assets/images/qr-order/food-image-1.png";
import Item2 from "assets/images/qr-order/food-image-2.png";
import Item3 from "assets/images/qr-order/food-image-3.png";
import Item4 from "assets/images/qr-order/food-image-4.png";
import Item5 from "assets/images/qr-order/food-image-5.png";
import Item6 from "assets/images/qr-order/food-image-6.png";

interface ICheckOutDishesProps {}
import { IDish } from "pages/qrOrder/Menu";

const CheckOutDishes: React.FC<ICheckOutDishesProps> = props => {
  const [datas, setData] = useState<IDish[]>([
    {
      name: "Mực chiên nước mắm",
      price: 155000,
      thumbnail: Item1,
      quantity: 0,
    },
    {
      name: "Tàu hũ nhồi thịt",
      price: 155000,
      thumbnail: Item2,
      quantity: 0,
    },
    {
      name: "Gà chiên nước mắm",
      price: 200000,
      thumbnail: Item3,
      quantity: 0,
    },
    {
      name: "Gà chiên nước mắm",
      price: 200000,
      thumbnail: Item4,
      quantity: 0,
    },
    {
      name: "Gà chiên nước mắm",
      price: 200000,
      thumbnail: Item5,
      quantity: 0,
    },
    {
      name: "Gà chiên nước mắm",
      price: 200000,
      thumbnail: Item6,
      quantity: 0,
    },
  ]);

  return (
    <CheckOutDishesContainer>
      <HeaderContainer>
        <SVG name="common/arrow-left" />
        <HeaderLabel>Danh sách món đã chọn</HeaderLabel>
      </HeaderContainer>
      <BodyContainer>
        {datas.map((item, index) => (
          <DishCard
            dish={item}
            RightButton={
              <ButtonsWrapper>
                <ChangeQuantityButton>-</ChangeQuantityButton>
                <QuantityContent>1</QuantityContent>
                <ChangeQuantityButton>+</ChangeQuantityButton>
              </ButtonsWrapper>
            }
          />
        ))}
      </BodyContainer>

      <CheckOutButton
        LeftElements={
          <>
            {" "}
            <LeftButtonContent>Gửi yêu cầu Đơn đặt hàng</LeftButtonContent>
          </>
        }
        RightElements={<RightButtonContent>đ 155,000</RightButtonContent>}
      />
    </CheckOutDishesContainer>
  );
};

export default CheckOutDishes;
