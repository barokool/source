import tw from "twin.macro";
import styled from "styled-components/macro";
import _Button from "designs/Button";

export const EmptyPrinterContainer = styled.div`
  ${tw`flex flex-col gap-2 justify-center items-center mt-6`}
`;
export const SVGBox = styled.div`
  ${tw`  `}
`;
export const Title = styled.span`
  ${tw`text-26 font-bold`}
`;
export const Description = styled.p`
  ${tw`text-20 text-gray `}
`;
export const ButtonWrapper = styled(_Button)`
  ${tw`py-1 px-2`}
`;
