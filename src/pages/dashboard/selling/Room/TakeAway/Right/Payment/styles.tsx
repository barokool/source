import styled from "styled-components/macro";
import tw from "twin.macro";
import _Button from "designs/Button";
import { Form as _Form } from "formik";

export const RightPaymentContainer = styled(_Form)`
  ${tw`h-screen shadow-lg`}
`;

export const Top = {
  Container: styled.div`
    ${tw`px-1 pb-2 pt-3 grid gap-y-2 bg-white`}
  `,
};

export const Mid = {
  Container: styled.div`
    ${tw`py-2 px-1 grid gap-y-2 bg-background`}
  `,
  PriceTag: styled.button`
    ${tw`text-black text-14 rounded-xl bg-white py-1.5 px-2`}
  `,
};

export const ItemWrapper = styled.div`
  ${tw`w-full flex justify-between items-center`}
`;

export const ItemLeft = styled.div`
  ${tw`text-14 text-black`}
`;

export const ItemRight = styled.div`
  ${tw`text-14 text-black`}
`;

export const Bottom = {
  Container: styled.div`
    ${tw`grid gap-y-2 py-2 px-1`}
  `,
  Button: styled(_Button)`
    ${tw`px-7 py-1.5 rounded-md`}
  `,
  ButtonWrapper: styled.div`
    ${tw`grid grid-cols-2 gap-x-2`}
  `,
};

export const RadioItem = {
  Container: styled.button`
    ${tw`flex items-center justify-between w-full`}
  `,
  LeftWrapper: styled.div`
    ${tw`flex items-center`}
  `,
  Label: styled.h3`
    ${tw`text-13 text-black`}
  `,
};

export const Point = styled.div`
  ${tw`p-[2px] mx-1 flex items-center justify-center w-2 h-2 overflow-hidden border border-solid rounded-full outline-none cursor-pointer  border-primary `}
`;

export const CustomRadio = styled.p<{ active: boolean }>`
  ${tw`w-full h-full duration-300 rounded-full cursor-pointer`}
  ${({ active }) => (active ? tw`bg-primary` : tw`bg-transparent`)}
`;

export const BoxPoint = styled.div`
  ${tw`w-max`}
`;

export const PhoneNumberFieldWrapper = styled.div`
  ${tw`grid grid-cols-2 gap-x-4 items-center`}
`;

export const CustomerPayFieldWrapper = styled.div`
  ${tw`grid grid-cols-2 gap-x-4 items-center`}
`;
