import { Dialog as DialogUI, Transition } from "@headlessui/react";
import React, { Fragment, PropsWithChildren } from "react";
import { DialogBox } from "./styles";

export type IDialogSize = "sm" | "md" | "lg";

interface ILoginDialogProps {
  size?: IDialogSize;
  open: boolean;
  onClose: () => void;
  className?: string;
}

const sizes: {
  [key in IDialogSize]: string;
} = {
  sm: "w-min",
  md: "w-full max-w-screen-phone",
  lg: "w-full max-w-screen-laptop",
};

const Dialog = (props: PropsWithChildren<ILoginDialogProps>): JSX.Element => {
  const { size = "md", children, className = "", open, onClose } = props;

  const handleClose = () => {
    onClose && onClose();
  };

  return (
    <Transition appear show={open} as={Fragment}>
      <DialogUI as="div" className="fixed inset-0 z-50" onClose={handleClose}>
        <div className="min-h-screen px-2 h-full flex items-center justify-center">
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <DialogUI.Overlay
              className="fixed inset-0 "
              style={{ backgroundColor: "#131313b3" }}
            />
          </Transition.Child>

          {/* This element is to trick the browser into centering the modal contents. */}
          <span
            className="inline-block h-screen align-middle"
            aria-hidden="true"
          >
            &#8203;
          </span>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300 transform"
            enterFrom="opacity-0 scale-90"
            enterTo="opacity-100 scale-100"
            leave="ease-in duration-200 transform"
            leaveFrom="opacity-100 scale-100"
            leaveTo="opacity-0 scale-90"
          >
            <DialogBox
              className={`
              fixed inline-block max-h-[96%] px-1 phone:px-0 rounded-sm scrollbar scrollbar-thumb-secondary scrollbar-thin 
              ${className}
              ${sizes[size]}
            `}
            >
              {children}
            </DialogBox>
          </Transition.Child>
        </div>
      </DialogUI>
    </Transition>
  );
};

Dialog.Header = ({ children }: PropsWithChildren<{}>) => {
  return (
    <div className="relative flex flex-row p-2 text-3xl font-bold text-neutral-1">
      {children}
      {/* <CloseButtonIcon /> */}
    </div>
  );
};

Dialog.Content = ({
  children,
  className = "",
}: PropsWithChildren<{ className?: string }>) => {
  return (
    <div className={`w-full h-full p-2 pt-0 ${className}`}>{children}</div>
  );
};

Dialog.Divider = ({
  children,
  className = "",
}: PropsWithChildren<{ className: string }>) => {
  return <div className="w-full h-full">{children}</div>;
};

export default Dialog;
