import React, { useState, useEffect } from "react";

//api
import { Order, OrderStatusEnum } from "apiCaller";

//languages
import { t } from "language";

//locals
import {
  GroupIcon,
  LineWrapper,
  OrderCardContainer,
  OrderCodeTag,
  Price,
  TableCode,
  LowSkeletonLine,
  HighSkeletonLine,
} from "./styles";

type OrderCardProps = {
  className?: string;
  isActive?: boolean;
  onClick?: () => void;
} & (
  | {
      loading?: false;
      data: Order;
    }
  | {
      loading: true;
      readonly data?: Order;
    }
);

const OrderCard: React.FC<OrderCardProps> = props => {
  const { data, onClick, className, loading, isActive } = props;
  const { barcode, table, price, status } = data || {};
  const [timeGap, setTimeGap] = useState<string>("");

  const getTimes = () => {
    const currentDate = new Date(Date.now());
    const createdAtDate = new Date(Date.parse(data?.createdAt as string));
    const gap = currentDate.getTime() - createdAtDate.getTime();

    const second = 1000;
    const minute = second * 60;
    const hour = minute * 60;
    const day = hour * 24;

    const textDay = Math.floor(gap / day);
    const textHour = Math.floor((gap % day) / hour);
    const textMinute = Math.floor((gap % hour) / minute);
    const textSecond = Math.floor((gap % minute) / second);

    if (textDay) {
      if (textDay < 2) setTimeGap(textDay + " " + t("selling.day"));
      if (textDay >= 2) setTimeGap(textDay + " " + t("selling.days"));
    } else if (!textDay && textHour) {
      if (textHour < 2) setTimeGap(textHour + " " + t("selling.hour"));
      if (textHour >= 2) setTimeGap(textHour + " " + t("selling.hours"));
    } else if (!textDay && !textHour && textMinute) {
      if (textMinute < 2) setTimeGap(textMinute + " " + t("selling.minute"));
      if (textMinute >= 2) setTimeGap(textMinute + " " + t("selling.minutes"));
    } else if (!textDay && !textHour && !textMinute && textSecond) {
      if (textSecond > 2) setTimeGap(textSecond + " " + t("selling.second"));
      if (textSecond <= 2) setTimeGap(textSecond + t("selling.seconds"));
    } else {
      setTimeGap("");
    }
  };

  useEffect(() => {
    getTimes();
  }, []);

  useEffect(() => {
    const interval = setInterval(() => {
      getTimes();
    }, 1000 * 60);

    return () => clearInterval(interval);
  }, []);

  if (loading) {
    return (
      <OrderCardContainer
        onClick={onClick}
        className={"animate-pulse space-y-1 " + className}
      >
        <></>
        <HighSkeletonLine></HighSkeletonLine>
        <HighSkeletonLine></HighSkeletonLine>
        <LowSkeletonLine></LowSkeletonLine>
      </OrderCardContainer>
    );
  }

  return (
    <OrderCardContainer
      onClick={onClick}
      className={className}
      active={isActive}
    >
      <OrderCodeTag>#{barcode}</OrderCodeTag>
      <LineWrapper>
        <TableCode>
          {t("selling.table")} {table?.code?.toString()}
        </TableCode>
        <Price>{price?.finalPrice?.toString().prettyMoney() || 0}</Price>
      </LineWrapper>
      <LineWrapper>
        <GroupIcon.Wrapper>
          <GroupIcon.Icon name="selling/clock" />
          <GroupIcon.Content className="text-body-color">
            {timeGap}
          </GroupIcon.Content>
        </GroupIcon.Wrapper>
        <GroupIcon.Wrapper>
          <GroupIcon.Icon name="selling/table" />
          <GroupIcon.Content className="text-body-color">
            {data?.table?.code?.toString()}
          </GroupIcon.Content>
        </GroupIcon.Wrapper>
      </LineWrapper>
      <LineWrapper>
        <GroupIcon.Wrapper>
          {status === OrderStatusEnum.Approving && (
            <>
              <GroupIcon.Icon name="selling/history" />
              <GroupIcon.Content className="text-warning">
                {t("selling.approving")}
              </GroupIcon.Content>
            </>
          )}
          {status === OrderStatusEnum.Processing && (
            <>
              <GroupIcon.Icon name="selling/clock" />
              <GroupIcon.Content className="text-successfull">
                {t("selling.processing")}
              </GroupIcon.Content>
            </>
          )}

          {status === OrderStatusEnum.New && (
            <>
              <GroupIcon.Content className="text-danger">
                {t("selling.new")}
              </GroupIcon.Content>
            </>
          )}

          {status === OrderStatusEnum.Complete && (
            <>
              <GroupIcon.Content className="text-primary">
                {t("selling.complete")}
              </GroupIcon.Content>
            </>
          )}

          {status === OrderStatusEnum.Shipping && (
            <>
              <GroupIcon.Icon name="selling/shipping" />
              <GroupIcon.Content className="text-successfull">
                {t("selling.shipping")}
              </GroupIcon.Content>
            </>
          )}
        </GroupIcon.Wrapper>
      </LineWrapper>
    </OrderCardContainer>
  );
};

export default OrderCard;
