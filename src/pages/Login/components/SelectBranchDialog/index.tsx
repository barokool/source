import React, { useEffect, useState, useRef } from "react";
import { toast } from "react-toastify";
import * as Yup from "yup";
import Dialog from "components/Dialog";
import { Formik } from "formik";
import { DialogTitle } from "common/styles/Title";
import AutoComplete from "designs/AutoComplete";

//api
import { Branch } from "apiCaller";

//hooks
import { useCurrentBranch } from "hooks/useCurrentBranch";

//styles
import { SelectBranchDialogContainer, ButtonWrapper, Button } from "./styles";

//language
import { t } from "language";

interface ITableListDialogProps {
  title: string;
  listBranch: Branch[];
  onClose?: () => void;
  onSuccess?: () => void;
  open: boolean;
}

interface IFormValue {
  branch: string;
}

const validationSchema = Yup.object().shape<{
  [key in keyof IFormValue]: any;
}>({
  branch: Yup.number().required(t("selling.please-enter-customer-pay")),
});

const SelectBranchDialog: React.FC<ITableListDialogProps> = ({
  title,
  onClose,
  open,
  onSuccess,
  listBranch,
}) => {
  const ref = useRef<any>();
  const [isOpen, setOpen] = useState<boolean>(open as boolean);
  const [loading, setLoading] = useState<boolean>(false);
  const [initialValues] = useState<IFormValue>({ branch: "" });
  const { branchSelected, setBranchSelected } = useCurrentBranch();

  const onHandleClose = () => {
    setOpen(false);
  };

  const handleSubmit = () => {
    //loading effect
    if (branchSelected) {
      setLoading(true);
      ref.current = setTimeout(() => {
        onSuccess?.();
        setLoading(false);
        toast.success(t("login.login-success.message-after"));
      }, 1000);
    } else {
      toast.error(t("login.please-select-branch"));
      setOpen(true);
    }
  };

  useEffect(() => {
    return clearTimeout(ref.current);
  }, []);

  return (
    <>
      <Formik
        enableReinitialize
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={handleSubmit}
      >
        <Dialog
          open={open}
          onClose={onHandleClose}
          className="overflow-auto"
          size="md"
        >
          <SelectBranchDialogContainer>
            <DialogTitle>{title}</DialogTitle>
            <AutoComplete
              name="branch"
              label={t("login.select-branch")}
              optionSelected={branchSelected}
              onSelect={branch => setBranchSelected(branch as Branch)}
              options={listBranch ? listBranch : []}
              required
            />
            <ButtonWrapper>
              <Button
                type="submit"
                primary
                loading={loading}
                onClick={() => {
                  handleSubmit();
                  setOpen(false);
                }}
              >
                {t("common.accept")}
              </Button>
            </ButtonWrapper>
          </SelectBranchDialogContainer>
        </Dialog>
      </Formik>
    </>
  );
};
export default SelectBranchDialog;
