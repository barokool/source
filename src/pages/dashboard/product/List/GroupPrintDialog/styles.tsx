import styled from "styled-components/macro";
import tw from "twin.macro";

export const OrderInfoPanelContainer = styled.main`
  ${tw`min-h-[400px]`}
`;

export const RenderAllBarcodeContainer = styled.div`
  ${tw`grid w-full grid-cols-4`}
`;

export const ProductBarcodeItem = styled.div`
  ${tw`p-0.5 w-full border border-solid border-neutral-4`}
`;

export const RefWrapper = styled.div`
  ${tw``}
`;
