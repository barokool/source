import { useEffect, useState } from "react";
import { Formik } from "formik";
import { toast } from "react-toastify";
import OTPInput from "components/OTPInput";

//language
import { t } from "language";

//redux
import { useDispatch } from "react-redux";

//locals
import {
  OTPFormContainer,
  Form,
  Notification,
  CountDown,
  Button,
  Instruction,
} from "./styles";
import { firebase } from "common/utils/firebase";
import { useSelector } from "react-redux";
import { IRootState } from "redux/storeToolkit";
import { setResetPasswordToken } from "redux/slices/resetPassword";

interface IOTPFormProps {
  onVerifySuccess?: () => void;
}

interface IFormValue {
  otp: string;
}

const COUNT_DOWN_TIME = 60;

const InputOTP: React.FC<IOTPFormProps> = ({ onVerifySuccess }) => {
  const { phoneNumber } = useSelector(
    (state: IRootState) => state.resetPassword,
  );
  const dispatch = useDispatch();

  //local states
  const [buttonLoading, setButtonLoading] = useState<boolean>(false);
  const [countDown, setCountDown] = useState(COUNT_DOWN_TIME);
  const [resend, setResend] = useState(false);
  const [initialValue] = useState<IFormValue>({
    otp: "",
  });

  const [result, setResult] = useState<firebase.auth.ConfirmationResult>();

  useEffect(() => {
    sendOTP();
  }, []);

  const sendOTP = async () => {
    try {
      setResend(false);
      setCountDown(COUNT_DOWN_TIME);
      if (!phoneNumber) return;
      let verify = new firebase.auth.RecaptchaVerifier("recaptcha-container", {
        size: "invisible",
      });
      const result = await firebase
        .auth()
        .signInWithPhoneNumber(phoneNumber, verify);
      setResult(result);
      setResend(false);
      setCountDown(COUNT_DOWN_TIME);
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    const timer = setTimeout(() => {
      setCountDown(countDown - 1);
    }, 1000);

    if (countDown === 0) {
      setResend(true);
    }
    return () => {
      clearTimeout(timer);
    };
  }, [countDown]);

  const hanldeVerify = async (values: IFormValue) => {
    try {
      setButtonLoading(true);
      const res = await result?.confirm(values.otp);
      if (!res) throw new Error("OTP is valid!");
      const token: string = (await res.user?.getIdToken(true)) || "";
      dispatch(setResetPasswordToken(token));
      toast.success(t("resetPassword.verify-otp-success"));
      onVerifySuccess?.();
    } catch (error) {
      toast.error(t("resetPassword.verify-otp-failed"));
      console.error(error);
    } finally {
      setButtonLoading(false);
    }
  };

  return (
    <OTPFormContainer>
      <div id="recaptcha-container" />
      <Formik
        initialValues={initialValue}
        onSubmit={hanldeVerify}
        enableReinitialize
      >
        {({ values, setFieldValue }) => (
          <Form>
            <Instruction>{t("resetPassword.otp-sent")}</Instruction>
            <OTPInput
              value={values.otp}
              onChange={value => setFieldValue("otp", value)}
            />

            {resend ? (
              <Notification>
                {t("resetPassword.not-recevied-otp-yet")}{" "}
                <button className="font-semibold" onClick={sendOTP}>
                  {t("resetPassword.resend")}
                </button>
              </Notification>
            ) : (
              <Notification>
                {t("resetPassword.not-recevied-otp-yet-please")}{" "}
                <CountDown>{countDown}s</CountDown>{" "}
                {t("resetPassword.to-resend-otp")}
              </Notification>
            )}
            <Button loading={buttonLoading} primary type="submit">
              {t("resetPassword.confirm")}
            </Button>
          </Form>
        )}
      </Formik>
    </OTPFormContainer>
  );
};

export default InputOTP;
