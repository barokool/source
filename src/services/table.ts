import {
  GenFields,
  Table,
  TableResult,
  TableType,
  TableTypeResult,
} from "apiCaller";

export const fragmentGetAllTable: GenFields<TableResult> = [
  "totalCount",
  {
    results: [
      "_id",
      "maxGuest",
      "quantity",
      "status",
      "bookingDate",
      "code",
      {
        tableType: [
          "_id",
          "name",
          "maxGuest",
          "quantity",
          { branch: ["_id", "name"] },
          "createdAt",
          "updatedAt",
        ],
      },
      "createdAt",
      "updatedAt",
    ],
  },
];
export const fragmentCreateTable: GenFields<Table> = [
  { tableType: ["name"] },
  "maxGuest",
  "quantity",
];
export const fragmentUpdateTable: GenFields<Table> = [
  "_id",
  { tableType: ["name"] },
  "maxGuest",
  "quantity",
  "status",
  "updatedAt",
];

export const fragmentGetAllTableType: GenFields<TableTypeResult> = [
  "totalCount",
  {
    results: [
      "_id",
      "name",
      "quantity",
      "maxGuest",
      {
        tables: [
          {
            tableType: [
              "name",
              "maxGuest",
              { tables: ["quantity", "maxGuest"] },
            ],
          },
          "status",
          "code",
        ],
      },
      { branch: ["_id", "name"] },
    ],
  },
];
export const fragmentCreateTableType: GenFields<TableType> = [
  "name",
  "quantity",
  "maxGuest",
];

export const fragmentUpdateTableType: GenFields<TableType> = [
  "maxGuest",
  "name",
  "quantity",
];
