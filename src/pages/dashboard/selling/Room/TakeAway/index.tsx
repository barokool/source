import React from "react";

//redux
import { useSelector } from "react-redux";
import { IRootState } from "typings";

//api
import { OrderTypeEnum, OrderStatusEnum } from "apiCaller";

//locals
import { TakeAwayTabContainer, NarrowerWrapper, WiderWrapper } from "./styles";
import InPlaceLeft from "./Left";
import InPlaceRight from "./Right";

interface IInPlaceTabProps {}

const TakeAwayTab: React.FC<IInPlaceTabProps> = props => {
  const { orderSelected } = useSelector((state: IRootState) => state.order);
  return (
    <>
      {orderSelected?.status === OrderStatusEnum.Approving &&
      orderSelected.type === OrderTypeEnum.Take_away ? (
        <TakeAwayTabContainer>
          <WiderWrapper>
            <InPlaceLeft />
          </WiderWrapper>
          <NarrowerWrapper>
            <InPlaceRight />
          </NarrowerWrapper>
        </TakeAwayTabContainer>
      ) : (
        <TakeAwayTabContainer>
          <NarrowerWrapper>
            <InPlaceLeft />
          </NarrowerWrapper>
          <WiderWrapper>
            <InPlaceRight />
          </WiderWrapper>
        </TakeAwayTabContainer>
      )}
    </>
  );
};

export default TakeAwayTab;
