import React, { useState, useEffect, Fragment } from "react";
import { PATH } from "common/constants/routes";

//locals
import { Heading, ResetPasswordForm } from "./styles";
import InputOTP from "./components/InputOTP";
import InputPhone from "./components/InputPhone";
import InputPassword from "./components/InputPassword";
import SVG from "designs/SVG";

//hooks
import { useRedirect } from "hooks/useRedirect";

//languages
import { t } from "language";
import AuthLayout from "layouts/Auth";
import { useSelector } from "react-redux";
import { IRootState } from "redux/storeToolkit";

interface IResetPasswordProps {}

type IScreenMode = "INPUT_PHONE" | "INPUT_PASSWORD" | "INPUT_OTP";

const ResetPassword: React.FC<IResetPasswordProps> = props => {
  const [screenMode, setScreenMode] = useState<IScreenMode>("INPUT_PHONE");
  const [body, setBody] = useState<React.ReactNode>(<></>);
  const redirect = useRedirect();
  const { languageSelected } = useSelector(
    (state: IRootState) => state.language,
  );

  useEffect(() => {
    switch (screenMode) {
      case "INPUT_OTP":
        setBody(
          <InputOTP onVerifySuccess={() => setScreenMode("INPUT_PASSWORD")} />,
        );
        break;
      case "INPUT_PASSWORD":
        setBody(<InputPassword />);
        break;
      case "INPUT_PHONE":
        setBody(
          <InputPhone
            onVerifyPhoneNumberSuccess={() => setScreenMode("INPUT_OTP")}
          />,
        );
    }
  }, [screenMode, languageSelected]);

  const handleBack = () => {
    switch (screenMode) {
      case "INPUT_OTP":
        setScreenMode("INPUT_PHONE");
        break;
      case "INPUT_PASSWORD":
        setScreenMode("INPUT_OTP");
        break;
      case "INPUT_PHONE":
        redirect(PATH.AUTH.LOGIN);
        break;
      default:
        redirect(PATH.AUTH.LOGIN);
    }
  };

  return (
    <AuthLayout>
      <ResetPasswordForm>
        <Heading.Wrapper>
          <SVG
            name="common/back"
            className="cursor-pointer"
            onClick={() => handleBack()}
          />
          <Heading.Title>{t("resetPassword.forget-password")}</Heading.Title>
          <Heading.SubTitle>
            {t("resetPassword.reset-password-with-phone-number")}
          </Heading.SubTitle>
        </Heading.Wrapper>
        {body}
      </ResetPasswordForm>
    </AuthLayout>
  );
};

export default ResetPassword;
