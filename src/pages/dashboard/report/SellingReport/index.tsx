import React, { useState, useEffect, useMemo, useCallback } from "react";
import { RouteComponentProps } from "react-router";
import i18n, { t } from "language";

import Table, { IColumns } from "designs/Table";
import ListDataLayout from "layouts/ListDataLayout";
import EmptyImage from "assets/images/product/ProductListEmpty.png";

import SimpleSelect from "./Button";
import DropdownMultiLevel, {
  IChildren,
} from "../components/DropdownMutilLevel";

import {
  SellingReportContainer,
  SearchBox,
  RightButtonWrapper,
} from "./styles";

import { PATH } from "common/constants/routes";
import { getQueryFromLocation } from "common/functions";

import {
  Branch,
  FilterReportDateEnum,
  OrderPrice,
  useGetAllReportRevenue,
} from "apiCaller";
import { useBranches } from "hooks/useBranches";
import { useLoading } from "hooks/useLoading";
import { usePage } from "hooks/usePage";
import { useBreadcrumb } from "hooks/useBreadcrumb";
import useAuth from "hooks/useAuth";
import { fragmentGetAllReport } from "services/report";
import { toast } from "react-toastify";
import Empty from "components/Empty";

interface ISellingReportProps extends RouteComponentProps {}

const SIZE_PER_PAGE = 10;
const LOAD_DATA = "LOAD_DATA";

const SellingReport: React.FC<ISellingReportProps> = ({ location }) => {
  const initData: IChildren[] = useMemo(
    () => [
      {
        key: 1,
        data: t("report.filter.by-day-and-week"),
        childrens: [
          {
            key: 4,
            data: t("report.filter.today"),
            value: FilterReportDateEnum.Today,
          },
          {
            key: 5,
            data: t("report.filter.yesterday"),
            value: FilterReportDateEnum.Yesterday,
          },
          {
            key: 6,
            data: t("report.filter.this-week"),
            value: FilterReportDateEnum.This_week,
          },
          {
            key: 7,
            data: t("report.filter.last-week"),
            value: FilterReportDateEnum.Last_week,
          },
        ],
      },
      {
        key: 2,
        data: t("report.filter.by-month-and-quarter"),
        childrens: [
          {
            key: 8,
            data: t("report.filter.this-month"),
            value: FilterReportDateEnum.This_month,
          },
          {
            key: 9,
            data: t("report.filter.last-month"),
            value: FilterReportDateEnum.Last_month,
          },
          {
            key: 10,
            data: t("report.filter.this-quarter"),
            value: FilterReportDateEnum.This_quarter,
          },
          {
            key: 11,
            data: t("report.filter.precious"),
            value: FilterReportDateEnum.Last_quarter,
          },
        ],
      },
      {
        key: 3,
        data: t("report.filter.by-year"),
        childrens: [
          {
            key: 12,
            data: t("report.filter.this-year"),
            value: FilterReportDateEnum.This_year,
          },
          {
            key: 13,
            data: t("report.filter.last-year"),
            value: FilterReportDateEnum.Last_year,
          },
        ],
      },
    ],
    [i18n.language],
  );

  const [page, setPage] = usePage(getQueryFromLocation(location)?.page);
  const { startLoading, stopLoading } = useLoading();
  const { accountInfo } = useAuth();
  const [branchSelected, setBranchSelected] = useState<Branch>(
    accountInfo?.userInfo?.branches?.[0] as Branch,
  );
  const { listBranch } = useBranches();
  const [filterDaySelected, setFilterDaySelected] = useState<IChildren>(
    initData?.[2]?.childrens?.[0] as IChildren,
  );
  //api
  const [
    getAllReportRevenue,
    { data: getData, error: getError, loading: getLoading },
  ] = useGetAllReportRevenue(fragmentGetAllReport);

  useBreadcrumb([
    {
      name: t("report.report"),
      href: PATH.REPORT.SELF,
    },
    {
      name: t("report.selling-report.selling-report"),
    },
  ]);

  useEffect(() => {
    invokeGetAllDataAPI();
  }, [page, filterDaySelected.value, branchSelected?._id]);

  useEffect(() => {
    if (!getData && getError) toast.error(getError.message);
  }, [getData, getError]);

  const invokeGetAllDataAPI = (text: string = "") => {
    try {
      startLoading(LOAD_DATA);
      getAllReportRevenue({
        variables: {
          filter: {
            filterDate: filterDaySelected.value as FilterReportDateEnum,
            idBranch: branchSelected?._id as string,
            orderCode: text,
          },
        },
      });
    } catch (error) {
    } finally {
      stopLoading(LOAD_DATA);
    }
  };

  const handleChangePage = useCallback((nextPage: number) => {
    setPage(nextPage);
  }, []);

  const columns: IColumns = useMemo(
    () => [
      {
        text: t("report.selling-report.trading-code"),
        dataField: "code",
      },
      {
        text: t("report.selling-report.time"),
        dataField: "shippingTime",
      },
      {
        text: t("report.selling-report.customer"),
        dataField: "customerName",
      },
      {
        text: t("report.selling-report.total-money"),

        dataField: "price",
        formatter: (price: OrderPrice) => price?.finalPrice,
      },
      {
        text: t("report.selling-report.discount"),
        dataField: "price",
        formatter: (price: OrderPrice) => price?.discount,
      },
      {
        text: t("report.selling-report.received-money"),
        dataField: "customerPay",
      },
      {
        text: "",
        dataField: "",
      },
    ],
    [page, i18n.language],
  );

  return getData?.getAllReportRevenue?.totalRevenue === 0 ? (
    <Empty
      SVG={<img src={EmptyImage} width={200} height={200} />}
      Title={t("configuration.branch-management.empty-title")}
    />
  ) : (
    <SellingReportContainer>
      <ListDataLayout
        rightTopBar={
          <RightButtonWrapper>
            <DropdownMultiLevel
              defaultValue={filterDaySelected}
              onSelected={branch => setFilterDaySelected(branch)}
              options={initData}
            />

            <SimpleSelect
              className=" font-medium bg-white rounded-sm  w-max border-2 border-secondary"
              label={t("common.branch")}
              optionSelected={branchSelected as Branch}
              options={listBranch?.getAllBranch?.results || []}
              onSelect={(option: Branch) => setBranchSelected(option)}
            />
          </RightButtonWrapper>
        }
        leftTopBar={
          <SearchBox
            onFetchData={invokeGetAllDataAPI}
            placeholder={t("report.customer-report.search-by-customer-name")}
          />
        }
        title={t("report.selling-report.selling-report")}
      >
        <Table
          columns={columns}
          data={getData?.getAllReportRevenue?.orderResult?.results || []}
          totalSize={getData?.getAllReportRevenue?.orderResult?.totalCount || 0}
          sizePerPage={SIZE_PER_PAGE}
          onPageChange={handleChangePage}
          page={page}
          isRemote
        />
      </ListDataLayout>
    </SellingReportContainer>
  );
};

export default SellingReport;
