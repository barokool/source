import { GetAllCityArgs, useGetAllCity } from "apiCaller";
import React, { useEffect } from "react";
import { fragmentGetAllCities } from "services/location";

export const useCities = (value?: GetAllCityArgs) => {
  const [getAllCity, { data }] = useGetAllCity(fragmentGetAllCities);

  useEffect(() => {
    value && invokeGetAllCities(value.filter?.district as string);
  }, []);

  const invokeGetAllCities = (districtValue: string) => {
    getAllCity({
      variables: {
        filter: {
          district: districtValue,
        },
      },
    });
  };

  return {
    listCities: data,
    invokeGetAllCities,
  };
};
