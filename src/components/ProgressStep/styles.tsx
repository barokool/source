import styled, { css } from "styled-components/macro";
import tw from "twin.macro";

export const ProgressStepsContainer = styled.ul`
  ${tw`flex flex-row items-center justify-between w-full`}
`;

export const Step = {
  Wrapper: styled.a<{ isLast: boolean }>`
    ${tw`flex`}
    ${({ isLast }) =>
      !isLast &&
      css`
        flex: 1 0 0;
        &::after {
          content: "";
          ${tw`mt-2 phone:mt-0 flex-grow min-w-max phone:self-center h-[1px] bg-primary mx-1`}
        }
      `}
  `,
  Container: styled.li<{ active: boolean; isPassed: boolean }>`
    ${tw`flex flex-col items-center gap-15 phone:flex-row`}
    ${({ active }) => (active ? tw`font-bold ` : tw`font-light`)}
      ${({ isPassed }) =>
      isPassed &&
      css`
        ${tw`cursor-pointer `}
        .step-number {
          ${tw`bg-neutral-4`}
        }
        p {
          ${tw`hover:underline`}
        }
      `}
  `,
  Click: styled.div<{ active: boolean }>`
    ${tw`flex items-center justify-center w-4 h-4 font-bold rounded-full text-16 `}
    ${tw`border-2 border-solid border-neutral-4`}
    ${({ active }) =>
      active ? tw`bg-primary text-white` : tw`bg-secondary text-white`}
    text-decoration: none !important;
  `,
};
