import { useState, useCallback, useEffect } from "react";
import { ProductCategory, useGetAllProductCategory } from "apiCaller";
import { fragmentGetAllProductCategory } from "services/product";

export const useProductCategory = (name?: string) => {
  const [getAllProductCategories, { data, loading, called }] =
    useGetAllProductCategory(fragmentGetAllProductCategory);

  useEffect(() => {
    invokeGetAllProductCategories(name || "");
  }, []);

  const invokeGetAllProductCategories = useCallback(async (name?: string) => {
    await getAllProductCategories({
      variables: {
        page: 0,
        filterProductCategory: {
          name: name,
        },
      },
    });
  }, []);

  return {
    listProductCategory: data,
    invokeGetAllProductCategories,
  };
};
