import { GenFields, Role, RoleResult } from "apiCaller";

export const fragmentGetAllRole: GenFields<RoleResult> = [
  { results: ["_id", "name", "position", "permissions"] },
];
export const fragmentCreateRole: GenFields<Role> = ["_id", "name"];

export const fragmentUpdateRole: GenFields<Role> = ["_id", "name"];
