import styled from "styled-components/macro";
import tw from "twin.macro";
import { Form as _Form } from "formik";
import _Button from "designs/Button";
import InputV2 from "designs/InputV2";

export const ElementWrapper = styled.div`
  ${tw`cursor-pointer `}
`;

export const UserDialogContainer = styled.div`
  ${tw`bg-white p-1 phone:p-2 laptop:p-5 rounded-sm`}
`;

export const Form = styled(_Form)`
  ${tw`grid gap-2`}
`;

export const ButtonWrapper = styled.div`
  ${tw`flex justify-end mt-3`}
`;

export const Button = styled(_Button)`
  ${tw`py-1 w-10 flex justify-center ml-1`}
`;

export const FormGroup = styled.div`
  ${tw`grid grid-cols-1 phone:grid-cols-2 gap-2`}
`;

export const Other = {
  ButtonWrapper: styled.div`
    ${tw`flex gap-1`}
  `,
  Button: styled(_Button)`
    ${tw`py-1 w-20 flex justify-center ml-1`}
  `,
};

export const Group = {
  Wrapper: styled.div`
    ${tw`flex flex-col gap-2 pb-2 border-b border-solid border-neutral-4`}
  `,
  Right: styled.div`
    ${tw`flex gap-2`}
  `,
  InputsWrapper: styled.div`
    ${tw`flex flex-col gap-y-3`}
  `,
  Text: styled.p`
    ${tw`font-regular  text-black text-14 text-left`}
  `,
  Input: styled(InputV2)`
    ${tw`border-none w-full outline-none`}
  `,
};
