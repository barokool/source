import { GenFields, ReportRevenue } from "apiCaller";

export const fragmentGetAllReport: GenFields<ReportRevenue> = [
  "totalRevenue",
  {
    orderResult: [
      "totalCount",
      {
        results: [
          "_id",
          "code",
          "excessMoney",
          "barcode",
          "customerName",
          "shippingTime",
          "customerPay",
          {
            price: ["discount", "finalPrice"],
          },
        ],
      },
    ],
  },
];
