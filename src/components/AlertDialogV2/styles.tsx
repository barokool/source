import styled from "styled-components/macro";
import tw from "twin.macro";
import _Button from "designs/Button";

export const ElementButton = styled.div`
  ${tw`cursor-pointer`}
`;

export const AlertDialogContainer = styled.div`
  ${tw`p-1.5 phone:p-3 w-full bg-white text-left box-border rounded-sm`}
`;

export const Title = styled.p`
  ${tw`text-20 phone:text-26 font-semibold leading-7`}
`;

export const Message = styled.p`
  ${tw`text-14 phone:text-16 font-regular mt-1 phone:mt-2 mb-1.5 phone:mb-3`}
`;

export const ButtonWrapper = styled.div`
  ${tw`flex justify-end`}
`;

export const Button = styled(_Button)`
  ${tw`py-1 px-2 w-max rounded-sm flex justify-center ml-1 text-14 phone:text-16`}
`;
