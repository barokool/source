import styled from "styled-components/macro";
import tw from "twin.macro";
import _Button from "designs/Button";

export const RightProcessingContainer = styled.div`
  ${tw`w-full h-screen shadow-lg flex flex-col`}
`;

export const CompleteButton = styled(_Button)`
  ${tw`mt-4 text-center mx-auto px-2 py-1 rounded-md`}
`;

export const RightProcessingWrapper = styled.div`
  ${tw`my-auto`}
`;

export const Image = styled.img`
  ${tw`mx-auto w-30 h-25`}
`;

export const Content = {
  Wrapper: styled.div`
    ${tw`mt-4 w-full`}
  `,
  Title: styled.p`
    ${tw`text-black text-20 font-semibold text-center`}
  `,
  Description: styled.p`
    ${tw`text-black text-16 text-center`}
  `,
};

export const HeaderContainer = styled.div`
  ${tw`flex items-center justify-between p-2 border-b-2 border-line w-full`}
`;

export const OrderCodeTag = styled.div`
  ${tw`p-1 text-black text-16 font-semibold rounded-sm border-2 border-black w-max`}
`;

export const PrintBillButton = styled(_Button)`
  ${tw`px-3.5 py-[12.5px] font-medium bg-secondary text-white w-max `}
`;
