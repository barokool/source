import { useCallback, useEffect, useState } from "react";
import { PATH } from "common/constants/routes";

//typings
import { IAccount } from "typings";
import { getLoggedInAccount, isAuthenticated } from "common/utils/auth";

//redux
import { useDispatch, useSelector } from "react-redux";
import { removeCurrentUser } from "redux/slices/auth";
import { IRootState } from "redux/storeToolkit";

//hooks
import { useRedirect } from "./useRedirect";
import { useCurrentBranch } from "./useCurrentBranch";



const useAuth = () => {
  const dispatch = useDispatch();
  const redirect = useRedirect();
  const [isAuth, setIsAuth] = useState<boolean>(isAuthenticated());
  const [accountInfo, setAccountInfo] = useState<IAccount>(
    getLoggedInAccount(),
  );
  const { isLogoutAction } = useSelector((state: IRootState) => state.auth);
  const { branchSelected, setBranchSelected } = useCurrentBranch();

  useEffect(() => {
    if (isLogoutAction) {
      return setIsAuth(false);
    }
    setIsAuth(isAuthenticated());
    setAccountInfo(getLoggedInAccount());
  }, [isLogoutAction]);

  const logout = useCallback(() => {
    dispatch(removeCurrentUser());
    setBranchSelected(null);
    redirect(PATH.AUTH.LOGIN);
  }, [branchSelected]);

  return { isAuth, accountInfo, logout };
};

export default useAuth;
