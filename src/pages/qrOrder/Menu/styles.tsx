import styled from "styled-components/macro";
import tw from "twin.macro";

export const MenuPageContainer = styled.div`
  ${tw`my-4.5 px-2 py-1.5`}
`;

export const HeaderContainer = styled.div`
  ${tw`flex justify-between`}
`;

export const HeaderLabel = styled.h1`
  ${tw`text-16 text-black`}
`;

export const ScrollMenuContainer = styled.div`
  ${tw`flex justify-between my-2`}
`;

export const ScrollItem = styled.button<{ isSelected?: boolean }>`
  ${tw` text-13 text-black  w-max  `}
  ${({ isSelected }) =>
    isSelected ? tw`border-b-4 border-primary` : tw`border-none`}
`;

export const AddButtonWrapper = styled.div`
  ${tw`flex items-center`}
`;

export const DishQuantity = styled.p`
  ${tw`text-black text-14 mr-1`}
`;

export const LeftButtonContent = styled.h3`
  ${tw`text-white text-13 mr-1`}
`;

export const RightButtonContent = styled.h3`
  ${tw`text-white text-16 font-bold`}
`;
export const QuantityContent = styled.button`
  ${tw`text-white border-2 border-white px-0.5`}
`;
