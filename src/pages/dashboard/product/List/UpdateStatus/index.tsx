import { useEffect, useState } from "react";
import Dialog from "components/Dialog";
import RadioGroup from "components/RadioGroup";
import { DialogTitle } from "common/styles/Title";
import { toast } from "react-toastify";

//api
import {
  Product,
  ProductStatus,
  ProductUpdateInput,
  useUpdateProduct,
} from "apiCaller";
import { fragmentUpdateProduct } from "services/product";

//fragment
import { t } from "language";

//hooks
import { useCurrentBranch } from "hooks/useCurrentBranch";

//locals
import {
  ElementWrapper,
  UpdateStatusContainer,
  ButtonWrapper,
  Button,
  FormContainer,
  BoxPoint,
  CustomRadio,
  Point,
  RadioItem,
} from "./styles";

interface IUpdateStatusDialogProps {
  ButtonMenu?: React.ReactElement;
  editField: Product;
  title?: string;
  onClose?: () => void;
  onSuccess?: () => void;
  open?: boolean;
}

interface IOptions<T> {
  _id: T;
  name: string;
}

const listStatus: IOptions<ProductStatus>[] = [
  {
    _id: ProductStatus.Instock,
    name: t("product.product-list.status.in-stock"),
  },
  {
    _id: ProductStatus.Outofstock,
    name: t("product.product-list.status.out-of-stock"),
  },
  {
    _id: ProductStatus.Deleted,
    name: t("product.product-list.status.deleted"),
  },
];

const UpdateStatusDialog: React.FC<IUpdateStatusDialogProps> = ({
  ButtonMenu,
  editField,
  title,
  onSuccess,
  onClose,
  open = false,
}) => {
  const { branchSelected } = useCurrentBranch();
  const [status, setStatus] = useState<IOptions<ProductStatus>>();
  const [isOpen, setIsOpen] = useState(open);
  const [updateProduct, { loading, error, data }] = useUpdateProduct(
    fragmentUpdateProduct,
  );

  useEffect(() => {
    if (editField) {
      editField.status === ProductStatus.Instock && setStatus(listStatus[0]);
      editField.status === ProductStatus.Outofstock && setStatus(listStatus[1]);
      editField.status === ProductStatus.Deleted && setStatus(listStatus[2]);
    }
  }, [editField]);

  const onHandleSubmit = () => {
    const input: ProductUpdateInput = {
      price: editField.price as number,
      branch: (branchSelected && branchSelected._id) || "",
      productCategory: editField?.productCategory?._id as string,
      description: editField?.description as string,
      name: editField.name as string,
      status: status?._id,
    };
    if (editField) {
      updateProduct({
        variables: { id: editField?._id!, productUpdateInput: input },
      });
      return;
    }
  };

  useEffect(() => {
    if (data) {
      toast.success(t("product.product-list.update-status-success"));
      onSuccess?.();
      onHandleClose();
    }
  }, [data]);

  useEffect(() => {
    if (error) {
      toast.error(error.message);
      setIsOpen(false);
    }
  }, [error]);

  const onHandleClose = () => {
    setIsOpen(false);
    onClose && onClose();
  };

  return (
    <>
      <ElementWrapper onClick={() => setIsOpen(true)}>
        {ButtonMenu}
      </ElementWrapper>
      <Dialog open={isOpen} onClose={onHandleClose} className="overflow-auto">
        <UpdateStatusContainer>
          <DialogTitle>{title}</DialogTitle>
          <FormContainer>
            <RadioGroup
              options={listStatus}
              onChange={option => setStatus(option)}
              optionSelected={status}
              renderOption={({ option, checked }) => (
                <RadioItem.Container>
                  <RadioItem.LeftWrapper>
                    <BoxPoint>
                      <Point>
                        <CustomRadio active={checked} />
                      </Point>
                    </BoxPoint>
                    <RadioItem.Label>{option?.name}</RadioItem.Label>
                  </RadioItem.LeftWrapper>
                </RadioItem.Container>
              )}
            />
            <ButtonWrapper>
              <Button outline type="button" onClick={onHandleClose}>
                {t("common.cancel")}
              </Button>
              <Button
                primary
                type="button"
                loading={loading}
                onClick={onHandleSubmit}
              >
                {t("common.accept")}
              </Button>
            </ButtonWrapper>
          </FormContainer>
        </UpdateStatusContainer>
      </Dialog>
    </>
  );
};

export default UpdateStatusDialog;
