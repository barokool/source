import styled from "styled-components/macro";
import tw from "twin.macro";

export const ListDataLayoutContainer = styled.div`
  ${tw`w-full min-h-screen p-2 bg-white  border border-neutral-4 shadow-md border-solid`}
`;

export const Title = styled.h1`
  ${tw` text-26 font-bold `}
`;

export const ToolBar = styled.section`
  ${tw` flex flex-row justify-end py-2 `}
`;

export const TopBar = {
  Container: styled.section`
    ${tw`w-full py-2 flex flex-row items-center justify-between`}
  `,
  Left: styled.div`
    ${tw`max-w-[320px] w-full`}
  `,
  Right: styled.div`
    ${tw``}
  `,
};

export const ListDataContent = styled.div`
  ${tw` w-full `}
`;
