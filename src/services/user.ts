import { GenFields, JwtPayload, User, UserResults } from "apiCaller";

export const fragmentGetAllUser: GenFields<UserResults> = [
  "totalCount",
  {
    results: [
      "_id",
      "clientId",

      "provider",
      "birthday",
      "gender",
      "ranking",
      { role: ["_id", "name", "position"] },
      "displayName",
      "email",
      "phoneNumber",
      {
        location: [
          { country: ["_id", "name"] },
          { state: ["_id", "name"] },
          { city: ["_id", "name"] },
          { district: ["_id", "name"] },
          "addressLine",
        ],
      },
      "status",
      "customerCode",
      "createdAt",
      "updatedAt",
      {
        branches: [
          "_id",
          "name",
          { businessProductTypes: ["_id", "name"] },
          { serviceType: ["_id", "name"] },
          { images: ["default", "medium", "small"] },
        ],
      },
    ],
  },
];
export const fragmentCreateUser: GenFields<User> = [
  "_id",
  "displayName",
  {
    location: [
      { country: ["_id", "name"] },
      { state: ["_id", "name"] },
      { city: ["_id", "name"] },
      { district: ["_id", "name"] },
      "addressLine",
    ],
    branches: [
      "_id",
      "name",
      "introduction",
      { businessProductTypes: ["_id", "name"] },
      { serviceType: ["_id", "name"] },
    ],
  },
];
export const fragmentIsExistingPhoneNumber: GenFields<User> = [
  "_id",
  "phoneNumber",
];
export const fragmentUpdateUser: GenFields<User> = [
  "_id",
  "clientId",
  "provider",
  "birthday",
  "gender",
  "ranking",
  { role: ["_id", "name", "position"] },
  "displayName",
  "email",
  "phoneNumber",
  {
    location: [
      { country: ["_id", "name"] },
      { state: ["_id", "name"] },
      { city: ["_id", "name"] },
      { district: ["_id", "name"] },
      "addressLine",
    ],
  },
  "status",
  "customerCode",
  "createdAt",
  "updatedAt",
  {
    branches: [
      "_id",
      "name",
      { businessProductTypes: ["_id", "name"] },
      { serviceType: ["_id", "name"] },
    ],
  },
];
export const fragmentGetUserById: GenFields<User> = [
  "_id",
  "clientId",
  "provider",
  "birthday",
  "gender",
  "ranking",
  { role: ["_id", "name", "position"] },
  "displayName",
  "email",
  "phoneNumber",
  {
    location: [
      { country: ["_id", "name"] },
      { state: ["_id", "name"] },
      { city: ["_id", "name"] },
      { district: ["_id", "name"] },
      "addressLine",
    ],
  },
  "status",
  "customerCode",
  "createdAt",
  "updatedAt",
  {
    branches: [
      "_id",
      "name",
      { businessProductTypes: ["_id", "name"] },
      { serviceType: ["_id", "name"] },
      { images: ["default", "medium", "small"] },
    ],
  },
];

export const fragmentForgotPassword: GenFields<JwtPayload> = [
  "accessToken",
  "refreshToken",
  { userInfo: ["_id"] },
];
