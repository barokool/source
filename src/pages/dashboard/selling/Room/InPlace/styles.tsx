import styled from "styled-components/macro";
import tw from "twin.macro";

export const InPlaceTabContainer = styled.div`
  ${tw`grid grid-cols-7 gap-x-2  items-center`}
`;

export const WiderWrapper = styled.div`
  ${tw`col-span-4`}
`;

export const NarrowerWrapper = styled.div`
  ${tw`col-span-3`}
`;
