import styled from "styled-components/macro";
import tw from "twin.macro";

export const BillContainer = styled.div`
  ${tw`py-5 px-2`}
`;

export const ImageWrapper = styled.div`
  ${tw`flex`}
`;

export const Logo = styled.img`
  ${tw`mx-auto`}
`;

export const Address = styled.h3`
  ${tw`text-center mt-2`}
`;

export const BillCode = {
  Wrapper: styled.div`
    ${tw`flex mx-auto items-center justify-center mt-2`}
  `,
  Label: styled.p`
    ${tw`text-20 mr-2`}
  `,

  Content: styled.p`
    ${tw`text-20 font-bold`}
  `,
};

export const Header = {
  Wrapper: styled.div`
    ${tw`w-full items-center text-center grid grid-cols-5 gap-x-2 mt-4`}
  `,
  Item: styled.p`
    ${tw`text-16 mt-2 font-semibold`}
  `,
};

export const ListItem = {
  Wrapper: styled.div`
    ${tw`w-full grid grid-cols-5 gap-x-2 items-center`}
  `,
  Content: styled.p`
    ${tw`text-16 mt-2 text-center`}
  `,
};

export const CheckOut = {
  Wrapper: styled.div`
    ${tw`mt-4 border-t-neutral-1 border-t`}
  `,
  ItemWrapper: styled.div`
    ${tw`w-full flex justify-between items-center  mt-2`}
  `,

  LeftContent: styled.p`
    ${tw`text-20 font-bold`}
  `,

  RightContent: styled.p`
    ${tw`text-20`}
  `,
};

export const Footer = {
  Wrapper: styled.div`
    ${tw`mt-4 border-t-neutral-1 border-t`}
  `,
  ItemWrapper: styled.div`
    ${tw`mt-2`}
  `,

  ItemContent: styled.p`
    ${tw`text-16 text-center`}
  `,
};
