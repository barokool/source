import React, { useState, useEffect, useCallback } from "react";
import { toast } from "react-toastify";
import Pagination from "components/Pagination";

//api
import { Product, useGetAllProduct } from "apiCaller";
import { fragmentGetAllProduct } from "services/product";

//locals
import {
  ListProductContainer,
  ListProductWrapper,
  MenuTabContainer,
} from "./styles";
import ProductCard from "../../../components/ProductCard";

interface IMenuTabProps {
  _id: string;
}

const SIZE_PER_PAGE = 8;

const MenuTab: React.FC<IMenuTabProps> = props => {
  const [page, setPage] = useState<number>(0);
  const [getAllProducts, { data, loading, error: getAllProductsErros }] =
    useGetAllProduct(fragmentGetAllProduct);
  const [listProduct, setListProduct] = useState<Product[]>([]);

  useEffect(() => {
    invokeGetAllDataAPI();
  }, [page]);

  useEffect(() => {
    if (data) {
      setListProduct(data.getAllProduct.results as Product[]);
    }
  }, [data]);

  useEffect(() => {
    if (getAllProductsErros) {
      toast.error(getAllProductsErros.message);
    }
  }, [getAllProductsErros]);

  const invokeGetAllDataAPI = () => {
    getAllProducts({
      variables: {
        filterProduct: {
          productCategory: props._id,
        },
        page: page,
        size: SIZE_PER_PAGE,
      },
    });
  };

  const handleChangePage = useCallback((newPage: number) => {
    setPage(newPage);
  }, []);

  return (
    <MenuTabContainer>
      <ListProductContainer>
        <ListProductWrapper>
          {loading
            ? new Array(10)
                .fill(0)
                .map((_, index) => <ProductCard key={index} loading />)
            : listProduct.map(product => (
                <ProductCard
                  key={product._id}
                  product={product}
                  onClick={() => {}}
                />
              ))}
        </ListProductWrapper>
      </ListProductContainer>
      <Pagination
        onPageChange={handleChangePage}
        sizePerPage={SIZE_PER_PAGE}
        totalSize={data?.getAllProduct.totalCount as number}
        className="float-right"
      />
    </MenuTabContainer>
  );
};

export default MenuTab;
