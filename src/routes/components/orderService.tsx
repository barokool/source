import { IRoutes } from "typings";
import { PATH } from "common/constants/routes";
import { lazy } from "react";
import { t } from "language";

import OrderServiceIcon from "icons/Dashboard/OrderService";

const OrderService = lazy(() => import("pages/dashboard/orderService"));

export const orderServiceRoute: IRoutes = {
  name: t("dashboard.order-service"),
  path: PATH.ORDER_SERVICE.SELF,
  exact: true,
  Component: OrderService,
  isPrivate: true,
  Icon: OrderServiceIcon,
};
