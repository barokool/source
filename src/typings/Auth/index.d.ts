import { IMongoObjectId } from "typings";
import { User } from "apiCaller";

export interface IGetToken {
  idToken: string | null;
}
export interface IDecodeToken {
  exp: number;
  iat?: number;
  id?: string;
}

export interface IAccount {
  accessToken?: string;
  refreshToken?: string;
  token?: string;
  userId: {
    id: string;
  };
  userInfo: User;
}

export interface ILogin {
  user: { phoneNumber: string; password: string; roleName?: string };
}

export interface IIsExistPhoneNumber {
  phoneNumber: string;
}

export interface IResetPassword {
  phoneNumber: string;
  password: string;
}

export interface ISignUpInput {
  signUpInput: {
    nameRestaurant: string;
    phoneNumber: string;
    country?: string;
    district: string;
    businessProductType: string[];
    serviceType: string;
    password: string;
  };
}
