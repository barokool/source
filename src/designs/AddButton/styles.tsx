import styled from "styled-components/macro";
import tw from "twin.macro";

export const ButtonWrapper = styled.button<{
  primary?: boolean;
  outline?: boolean;
  loading?: boolean;
}>`
  ${tw`font-medium rounded-sm flex gap-2 items-center overflow-hidden focus:border-none focus:outline-none duration-200`}
  ${({ primary }) => primary && tw`bg-black text-white hover:bg-opacity-80`}
  ${({ outline }) =>
    outline &&
    tw`ring-1 ring-black text-black hover:ring-opacity-60 hover:text-opacity-60 border-2`}
  ${({ loading }) => loading && tw`bg-opacity-60 cursor-wait`}
`;
