import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Order, OrderTypeEnum } from "apiCaller";

interface OrderState {
  orderSelected: Order | null;
  orderTypePageSelected: OrderTypeEnum;
  isCheckOut: boolean;
}

const initialState: OrderState = {
  orderSelected: null,
  orderTypePageSelected: OrderTypeEnum.In_place,
  isCheckOut: false,
};

export const orderSlice = createSlice({
  name: "order",
  initialState,
  reducers: {
    setOrderSelected: (state: any, action: PayloadAction<Order | null>) => {
      state.orderSelected = action.payload;
    },
    setOrderTypePageSelected: (
      state: any,
      action: PayloadAction<OrderTypeEnum | null>,
    ) => {
      state.orderTypePageSelected = action.payload;
    },
    setCheckOut: (state: any, action: PayloadAction<boolean>) => {
      state.isCheckOut = action.payload;
    },
  },
});

// Actions
export const { setOrderSelected, setCheckOut, setOrderTypePageSelected } =
  orderSlice.actions;

export default orderSlice.reducer;
