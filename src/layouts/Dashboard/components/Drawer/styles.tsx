import styled from "styled-components/macro";
import tw from "twin.macro";
import _SVG from "designs/SVG";

export const DrawerContainer = styled.div<{
  isCollapse?: boolean;
  isExtendDrawer: boolean;
}>`
  ${tw`fixed h-full px-1 z-20 text-16 text-center text-white bg-white shadow-lg pt-1 transform duration-200 `}
  ${({ isCollapse }) =>
    isCollapse ? tw`w-8` : tw`max-w-[250px] w-[250px] overflow-y-auto`}
  ${({ isExtendDrawer }) => (isExtendDrawer ? tw`left-0` : tw`-left-26`)}
`;

export const Icon = styled(_SVG)<{
  isCollapse?: boolean;
}>`
  ${tw`w-15 transform duration-200`}
  ${({ isCollapse }) =>
    isCollapse ? tw`invisible h-0 mb-0` : tw`visible h-7 mb-2`}
`;
