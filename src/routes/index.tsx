import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import PrivateRoute from "components/PrivateRoute";
import { flattenedRoutes as routes, notFoundRoute } from "./Routes";
import DashboardLayout from "layouts/Dashboard";
import SellingLayout from "layouts/SellingLayout";
import { Suspense, useMemo } from "react";
import ErrorBoundary from "components/ErrorBoundary";
import QROrderLayout from "layouts/QROrderLayout";
import PageLoadingPulse from "components/PageLoadingSkeletons";

/**
 * @description
 *  - Because we using React.lazy, so we need to wrap <Suspense> outside
 *    of lazy component.
 */

const Routers: React.FC = () => {
  const sellingRoutes = useMemo(
    () => routes.filter(route => route.isPrivate && route.isSellRoute),
    [],
  );
  const privateRoutes = useMemo(
    () => routes.filter(route => route.isPrivate && !route.isSellRoute),
    [],
  );
  const publicRoutes = useMemo(
    () => routes.filter(route => !route.isPrivate),
    [],
  );

  return (
    <Router>
      <Switch>
        <Route
          exact
          path={notFoundRoute.path}
          component={notFoundRoute.Component}
        />
        {/* Public routes */}
        <Route path="/auth">
          <Suspense fallback={<PageLoadingPulse />}>
            <ErrorBoundary>
              <Switch>
                {publicRoutes.map(route => {
                  return (
                    route.Component && (
                      <Route
                        path={route.path}
                        exact
                        component={route.Component}
                        key={route.path}
                      />
                    )
                  );
                })}
              </Switch>
            </ErrorBoundary>
          </Suspense>
        </Route>
        <Route path="/qr-order">
          <QROrderLayout>
            <ErrorBoundary>
              <Suspense fallback={<PageLoadingPulse />}>
                <Switch>
                  {publicRoutes.map(route => {
                    return (
                      route.Component && (
                        <Route
                          path={route.path}
                          exact
                          component={route.Component}
                          key={route.path}
                        />
                      )
                    );
                  })}
                </Switch>
              </Suspense>
            </ErrorBoundary>
          </QROrderLayout>
        </Route>
        {/* Selling routes */}
        <Route path="/selling">
          <SellingLayout>
            <ErrorBoundary>
              <Suspense fallback={<PageLoadingPulse />}>
                <Switch>
                  {sellingRoutes.map(route => {
                    return (
                      route.Component && (
                        <PrivateRoute exact {...route} key={route.path} />
                      )
                    );
                  })}
                  <Redirect path="*" to={notFoundRoute.path} />
                </Switch>
              </Suspense>
            </ErrorBoundary>
          </SellingLayout>
        </Route>
        {/* Dashboard routes */}
        <Route path="/">
          <DashboardLayout>
            <ErrorBoundary>
              <Suspense fallback={<PageLoadingPulse />}>
                <Switch>
                  {privateRoutes.map(route => {
                    return (
                      route.Component && (
                        <PrivateRoute exact {...route} key={route.path} />
                      )
                    );
                  })}
                  <Redirect path="*" to={notFoundRoute.path} />
                </Switch>
              </Suspense>
            </ErrorBoundary>
          </DashboardLayout>
        </Route>
      </Switch>
    </Router>
  );
};

export default Routers;
