import {
  BusinessProductType,
  BusinessProductTypeResult,
  GenFields,
} from "apiCaller";

export const fragmentGetAllBusinessProductType: GenFields<BusinessProductTypeResult> =
  [
    {
      results: [
        "_id",
        "name",
        { image: ["default", "medium", "small"] },
        { serviceType: ["_id", "name"] },
        "createdAt",
        "updatedAt",
      ],
    },
    "totalCount",
  ];

export const fragmentCreateBusinessProductType: GenFields<BusinessProductType> =
  ["_id", "name", { serviceType: ["_id"] }];

export const fragmentUpdateBusinessProductType: GenFields<BusinessProductType> =
  ["_id", "name"];
