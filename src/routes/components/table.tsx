import { IRoutes } from "typings";
import { PATH } from "common/constants/routes";
import { lazy } from "react";
import { t } from "language";

import TableIcon from "icons/Dashboard/Table";

const TableCategory = lazy(() => import("pages/dashboard/table/TableType"));
const TableList = lazy(() => import("pages/dashboard/table/TableList"));

export const tableRoutes: IRoutes = {
  name: t("table.table-management"),
  path: PATH.PRODUCT.SELF,
  exact: true,
  isPrivate: true,
  Icon: TableIcon,
  children: [
    {
      name: t("table.table-list.table-list"),
      path: PATH.TABLE_MANAGEMENT.LIST,
      isPrivate: true,
      Component: TableList,
      exact: true,
    },
    {
      name: t("table.table-type.table-type"),
      path: PATH.TABLE_MANAGEMENT.TYPE,
      isPrivate: true,
      Component: TableCategory,
      exact: true,
    },
  ],
};
