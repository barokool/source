import * as S from "./styles";
import SVG from "designs/SVG";
import _Button from "designs/Button";
import { MouseEvent } from "react";
import AddPrintDialog from "../AddPrintDialog";
import { t } from "language";

interface IEmptyPrinter {
  onClick: (e: MouseEvent<HTMLButtonElement, globalThis.MouseEvent>) => void;
}

const EmptyPrinter: React.FC<IEmptyPrinter> = props => {
  const { onClick } = props;
  return (
    <S.EmptyPrinterContainer>
      <S.SVGBox>
        <SVG height={267} width={282} name="configuration/empty-print" />
      </S.SVGBox>
      <S.Title>{t("configuration.general-settings.empty-printer")}</S.Title>
      <S.Description>
        {t("configuration.general-settings.empty-printer-message")}
      </S.Description>
      <AddPrintDialog
        title={t("configuration.general-settings.add-new-printer")}
        ButtonMenu={
          <S.ButtonWrapper onClick={onClick} primary>
            {t("configuration.general-settings.add-new-printer")}
          </S.ButtonWrapper>
        }
      />
    </S.EmptyPrinterContainer>
  );
};

export default EmptyPrinter;
