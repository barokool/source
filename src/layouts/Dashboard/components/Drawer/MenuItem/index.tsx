import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";

import { IRoutes } from "typings";
import { useClickOutSide } from "hooks/useClickOutSide";

import {
  MenuItemContainer,
  Button,
  ButtonContainer,
  ButtonIconWrapper,
  Text,
  ArrowIcon,
  Dropdown,
  DropdownItem,
  SubmenuButton,
  ListStyle,
  ButtonNoDropdown,
  ButtonNoDropdownText,
} from "./styles";

interface IMenuItemProps {
  active: boolean;
  isShowDropDown: boolean;
  route: IRoutes;
  isCollapse?: boolean;
  setCurrentPath?: (value: string) => void;
  onShowDropDown: () => void;
  onActiveMenu: () => void;
  currentPath?: string;
}

const MenuItem = (props: IMenuItemProps) => {
  const {
    active,
    isCollapse,
    route,
    setCurrentPath,
    currentPath,
    onActiveMenu,
    isShowDropDown,
    onShowDropDown,
  } = props;

  const { name, children, path } = route;

  const Icon = route.Icon as React.FC<any>;

  const history = useHistory();

  const handleClick = () => {
    if (children) {
      onShowDropDown();
    } else {
      onActiveMenu();
      onShowDropDown();
      history.push(path);
      setCurrentPath && setCurrentPath(path);
    }
  };

  const handleSubmenuClick = (path: string) => {
    onActiveMenu();
    setCurrentPath && setCurrentPath(path);
    history.push(path);
  };

  return (
    <MenuItemContainer>
      <Button to={path} onClick={handleClick}>
        <ButtonContainer active={active}>
          <ButtonIconWrapper>
            <Icon className="text-secondary" />
          </ButtonIconWrapper>
          <Text active={active}>
            {name}
            {children ? (
              <ArrowIcon direction={isShowDropDown ? "UP" : "DOWN"} />
            ) : null}
          </Text>
        </ButtonContainer>
      </Button>
      {children ? (
        <Dropdown
          className={isCollapse ? "group-hover:block" : ""}
          isVisible={isShowDropDown}
          isCollapse={isCollapse}
        >
          {children.map((item, index) => {
            const isActive = item.path === currentPath;
            return (
              <DropdownItem key={`sub-item-${index}`}>
                <SubmenuButton
                  onClick={() => handleSubmenuClick(item.path)}
                  isActive={isActive}
                >
                  {!isCollapse && <ListStyle isActive={isActive} />}
                  {item.name}
                </SubmenuButton>
              </DropdownItem>
            );
          })}
        </Dropdown>
      ) : (
        <ButtonNoDropdown className={isCollapse ? "group-hover:block" : ""}>
          <ButtonNoDropdownText active={active}>{name}</ButtonNoDropdownText>
        </ButtonNoDropdown>
      )}
    </MenuItemContainer>
  );
};

export default MenuItem;
