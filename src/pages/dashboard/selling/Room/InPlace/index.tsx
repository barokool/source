import React from "react";

//redux
import { useSelector } from "react-redux";
import { IRootState } from "typings";

//api
import { OrderStatusEnum, OrderTypeEnum } from "apiCaller";

//locals
import { InPlaceTabContainer, NarrowerWrapper, WiderWrapper } from "./styles";
import InPlaceLeft from "./Left";
import InPlaceRight from "./Right";

interface IInPlaceTabProps {}

const InPlaceTab: React.FC<IInPlaceTabProps> = props => {
  const { orderSelected } = useSelector((state: IRootState) => state.order);
  return (
    <>
      {orderSelected?.status === OrderStatusEnum.Approving &&
      orderSelected.type === OrderTypeEnum.In_place ? (
        <InPlaceTabContainer>
          <WiderWrapper>
            <InPlaceLeft />
          </WiderWrapper>
          <NarrowerWrapper>
            <InPlaceRight />
          </NarrowerWrapper>
        </InPlaceTabContainer>
      ) : (
        <InPlaceTabContainer>
          <NarrowerWrapper>
            <InPlaceLeft />
          </NarrowerWrapper>
          <WiderWrapper>
            <InPlaceRight />
          </WiderWrapper>
        </InPlaceTabContainer>
      )}
    </>
  );
};

export default InPlaceTab;
