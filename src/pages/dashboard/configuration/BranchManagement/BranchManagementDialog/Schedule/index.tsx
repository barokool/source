import React, { useEffect, useState } from "react";
import { FieldArray, FieldMetaProps, useField } from "formik";
//hooks

//styles
import FormControlErrorHelper from "common/styles/FormControlErrorHelper";
import CheckboxButton from "designs/CheckboxButton";
import TimePicker from "designs/TimePicker";
import BinIcon from "icons/Bin";
import CheckboxIcon from "icons/Checkbox";
import PlusIcon from "icons/Plus";
import {
  ElementWrapper,
  Input,
  ItemContainer,
  Label,
  ScheduleContainer,
  StyleRadioButton,
} from "./styles";

import { t } from "language";
import { BranchTimeInput } from "apiCaller";

type IDayOfWeek =
  | "monday"
  | "tuesday"
  | "wednesday"
  | "thursday"
  | "friday"
  | "saturday"
  | "sunday";

export interface IOptionSchedule {
  day: IDayOfWeek;
  times: BranchTimeInput[];
}

interface IScheduleProps {
  name: string;
  maxField: number;
  onChange: (item: BranchTimeInput[]) => void;
}

const timeItemDefault: BranchTimeInput = { closeHours: "", openingHours: "" };

const Schedule: React.FC<IScheduleProps> = props => {
  const { name, onChange } = props;
  const [field, meta, helpers] = useField<IOptionSchedule[]>(name);
  const { value } = field;
  const { setValue } = helpers;
  const [time, setTimes] = useState<BranchTimeInput[]>([]);

  const handleCheckDisable = (day: IDayOfWeek) => {
    let newList = value.reduce((list: IOptionSchedule[], item) => {
      const timeNew =
        item.day === day
          ? {
              ...item,
              times: item.times.length > 0 ? [] : [timeItemDefault],
            }
          : item;
      return [...list, timeNew];
    }, []);

    setValue(newList);
  };
  //2022-07-11T09:13:43Z
  useEffect(() => {
    if (!value) return;

    const newTimes: BranchTimeInput[] = value.map((item, i) => {
      const { day, times } = item;

      return {
        closeHours: `2022-07-11T${times[0].closeHours}:00Z`,
        openingHours: `2022-07-11T${times[0].openingHours}:00Z`,
      };
    });

    setTimes(newTimes);
  }, [value]);

  useEffect(() => {
    time && onChange(time);
  }, [time]);

  return (
    <ScheduleContainer>
      <FieldArray
        name={name}
        render={() => (
          <ElementWrapper className="w-full space-y-3">
            {value &&
              value.length > 0 &&
              value.map((option: IOptionSchedule, indexLv1: number) => {
                const disable = option.times.length > 0;
                return (
                  <ItemContainer key={indexLv1}>
                    <Checkbox
                      option={option}
                      disable={disable}
                      handleCheckDisable={handleCheckDisable}
                    />
                    <InputTime
                      {...props}
                      meta={meta}
                      option={option}
                      indexLv1={indexLv1}
                      disable={disable}
                    />
                  </ItemContainer>
                );
              })}
          </ElementWrapper>
        )}
      />
    </ScheduleContainer>
  );
};

export default Schedule;

const InputTime: React.FC<{
  name: string;
  option: IOptionSchedule;
  indexLv1: number;
  disable: boolean;
  meta: FieldMetaProps<any[]>;
  maxField: number;
}> = ({ name, option, disable, indexLv1, meta, maxField }) => {
  const isError: boolean = Boolean(!!meta.error && !!meta.touched);

  if (!disable) {
    return (
      <p className="self-center">
        {t("configuration.branch-management.general-info.close-the-door")}
      </p>
    );
  }

  return (
    <FieldArray
      name={`${name}.${indexLv1}.times`}
      render={arrayHelper => (
        <ElementWrapper className="w-full space-y-3">
          {option.times?.map((time: BranchTimeInput, indexListTime: number) => (
            <ElementWrapper
              key={indexListTime}
              className="w-full flex gap-2 items-center"
            >
              <TimePicker
                label={t("configuration.branch-management.general-info.open")}
                name={`${name}.${indexLv1}.times.${indexListTime}.openingHours`}
                required
              />
              <TimePicker
                label={t("configuration.branch-management.general-info.closed")}
                name={`${name}.${indexLv1}.times.${indexListTime}.closeHours`}
                required
              />
              {indexListTime + 1 === 1 ? (
                option.times?.length === maxField ? (
                  <BinIcon
                    className="cursor-pointer w-5 h-5"
                    onClick={() => {
                      arrayHelper.remove(indexListTime);
                    }}
                  />
                ) : (
                  <PlusIcon
                    className="cursor-pointer w-5 h-5"
                    onClick={() => arrayHelper.push(timeItemDefault)}
                  />
                )
              ) : (
                <BinIcon
                  className="cursor-pointer w-5 h-5"
                  onClick={() => {
                    arrayHelper.remove(indexListTime);
                  }}
                />
              )}
            </ElementWrapper>
          ))}
        </ElementWrapper>
      )}
    />
  );
};

const Checkbox: React.FC<{
  option: IOptionSchedule;
  disable: boolean;
  handleCheckDisable: (day: IDayOfWeek) => void;
}> = ({ option, disable, handleCheckDisable }) => (
  <>
    <Input
      id={`option-${option.day}`}
      type="checkbox"
      onClick={() => {
        handleCheckDisable(option.day);
      }}
      value={disable}
    />
    <Label htmlFor={`option-${option.day}`}>
      <StyleRadioButton checked={disable}>
        <CheckboxIcon className="text-white" />
      </StyleRadioButton>
      {option.day}
    </Label>
  </>
);
