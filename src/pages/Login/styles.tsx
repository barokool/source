import styled from "styled-components/macro";
import tw from "twin.macro";
import _Button from "designs/Button";
import { Form as _Form } from "formik";

export const Form = styled(_Form)`
  ${tw`flex flex-col gap-1`}
`;
export const FormContainer = styled.div`
  ${tw`w-full phone:w-50 p-1 phone:p-4 bg-white rounded-sm`}
`;

export const Heading = {
  Wrapper: styled.div`
    ${tw`flex flex-col gap-[15px] mb-4`}
  `,
  Title: styled.p`
    ${tw`font-semibold text-26 text-black`}
  `,
  SubTitle: styled.p`
    ${tw`font-regular text-16 text-black`}
  `,
};

export const ButtonWrapper = styled.div`
  ${tw`flex justify-end gap-2`}
`;

export const OptionWrapper = styled.div`
  ${tw`flex gap-x-1 items-center w-full justify-between`}
`;

export const PhoneInputWrapper = styled.div`
  ${tw`grid grid-cols-12 gap-x-0.5`}
`;

export const OptionTitle = styled.h3`
  ${tw`text-14 text-gray`}
`;

export const Button = styled(_Button)`
  ${tw`w-full flex items-center bg-secondary justify-center text-16 font-medium text-white py-1 mt-2`}
`;

export const LogoWrapper = styled.div`
  ${tw`hidden laptop:flex items-center justify-center w-full h-full bg-primary`}
`;

export const ElementWrapper = styled.div`
  ${tw`flex flex-col gap-3`}
`;

export const Other = styled.div`
  ${tw`flex justify-between mt-2`}
`;
export const TextDesc = styled.p`
  ${tw`font-regular text-16 text-secondary hover:text-secondary hover:underline  cursor-pointer`}
`;
