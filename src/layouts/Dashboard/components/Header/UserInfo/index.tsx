import { useDispatch, useSelector } from "react-redux";

import { useClickOutSide } from "hooks/useClickOutSide";
import { removeUserCookies } from "common/utils/auth";

import { PATH } from "common/constants/routes";

import Button from "designs/Button";
import SVG from "designs/SVG";
import AlertDialog from "components/AlertDialogV2";

import { useHistory } from "react-router-dom";

import {
  UserInfoContainer,
  ButtonContainer,
  Name,
  Icon,
  Dropdown,
  ElementWrapper,
  Address,
} from "./styles";
import useAuth from "hooks/useAuth";
import { t } from "language";
import { removeCurrentUser } from "redux/slices/auth";
import SimpleSelect from "designs/SimpleSelect";
import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { setLanguageSelected } from "redux/slices/language";
import { IRootState } from "redux/storeToolkit";
import { IItemLanguage } from "typings";
const listOptionsLanguage: IItemLanguage[] = [
  {
    id: "1",
    name: "Eng",
    phonePrefix: "+01",
    lng: "en",
    flag: "register/american-flag",
  },
  {
    id: "2",
    name: "Ger",
    phonePrefix: "+49",
    lng: "de",
    flag: "register/german-flag",
  },
  {
    id: "3",
    name: "Viet",
    phonePrefix: "+84",
    lng: "vi",
    flag: "register/vietnam-flag",
  },
];
const UserInfo = () => {
  const { elementRef, isVisible, setElementVisible } = useClickOutSide(false);
  const { logout } = useAuth();

  const { languageSelected } = useSelector(
    (state: IRootState) => state.language,
  );

  const dispatch = useDispatch();
  const history = useHistory();

  const handleClick = () => {
    setElementVisible(!isVisible);
  };
  const handleLogout = () => {
    logout();
  };

  return (
    <UserInfoContainer ref={elementRef}>
      <SimpleSelect
        options={listOptionsLanguage}
        optionSelected={languageSelected}
        onSelect={country => dispatch(setLanguageSelected(country))}
        renderOption={option => {
          return (
            <div className="flex gap-x-1 items-center">
              <SVG name={option?.flag as string} />
              <h3 className="text-14 text-gray">{option?.name as string}</h3>
            </div>
          );
        }}
      />
      <ElementWrapper>
        <ButtonContainer onClick={handleClick}>
          <Name> Manager</Name>
          <Icon
            className={`transform ${isVisible && "rotate-180"}`}
            name="common/square-arrow"
            width={18}
            height={19}
          />
        </ButtonContainer>
        <Dropdown.Container isVisible={isVisible}>
          <Dropdown.Item>
            <Dropdown.Button
            // to={PATH.ACCOUNT_SETTING}
            >
              <Dropdown.Icon name="dropdown/user" height="20px" width="20px" />
              {t("common.store-info")}
            </Dropdown.Button>
          </Dropdown.Item>
          <Dropdown.Item>
            <Dropdown.Button onClick={handleLogout}>
              <Dropdown.Icon
                name="dropdown/logout"
                height="20px"
                width="20px"
              />
              {t("common.log-out")}
            </Dropdown.Button>
          </Dropdown.Item>
        </Dropdown.Container>
      </ElementWrapper>
    </UserInfoContainer>
  );
};

export default UserInfo;
