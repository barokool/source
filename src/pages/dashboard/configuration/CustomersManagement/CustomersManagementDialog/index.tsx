import React, { useEffect, useState } from "react";
import { Formik } from "formik";
import * as yup from "yup";

//hooks
import { useCustomerRoles } from "hooks/useCustomerRoles";

//styles
import Input from "designs/InputV2";
import Dialog from "components/Dialog";
import DatePicker from "designs/DatePicker";
import { DialogTitle } from "common/styles/Title";
import Select from "designs/Select";
import {
  ElementWrapper,
  CustomersManagementDialogContainer,
  Form,
  ButtonWrapper,
  Button,
} from "./styles";

import { t } from "language";
import {
  City,
  Country,
  CreateUserInput,
  District,
  Gender,
  Role,
  State,
  useCreateUser,
  useGetAllCity,
  useGetAllCountry,
  useGetAllDistrict,
  useGetAllState,
  User,
} from "apiCaller";
import {
  fragmentGetAllCities,
  fragmentGetAllCountry,
  fragmentGetAllDistricts,
  fragmentGetAllStates,
} from "services/location";
import { fragmentCreateUser, fragmentUpdateUser } from "services/user";
import { toast } from "react-toastify";

interface ICustomersManagementDialogProps {
  ButtonMenu?: React.ReactElement;
  editField?: User;
  title?: string;
  open?: boolean;
  onClose?: () => void;
  onSuccess?: () => void;
  isAdd?: boolean;
}

interface IFormValue {
  displayName: string;
  phoneNumber: string;
  gender?: string;
  rank?: string;
  email?: string;
  birthday?: string;
  country: string;
  state?: string;
  district?: string;
  city?: string;
  street?: string;
  role: string;
  password: string;
  code?: string;
}

const validationSchema = yup
  .object()
  .shape<{ [key in keyof IFormValue]: any }>({
    displayName: yup
      .string()
      .required(t("configuration.customer-management.please-enter-name")),
    country: yup.string().required(t("register.please-select-country")),
    phoneNumber: yup
      .string()
      .required(
        t("configuration.customer-management.please-enter-phone-number"),
      ),
    password: yup
      .string()
      .required(t("configuration.customer-management.please-enter-password"))
      .min(6),
    role: yup
      .string()
      .required(t("configuration.customer-management.please-select-role")),
  });

const CustomersManagementDialog: React.FC<ICustomersManagementDialogProps> = ({
  ButtonMenu,
  editField,
  title,
  onSuccess,
  open = false,
  onClose,
  isAdd = false,
}) => {
  const [loading, setLoading] = useState(false);
  const [initialValues, setInitialValues] = useState<IFormValue>({
    displayName: "",
    phoneNumber: "",
    password: "",
    code: "",
    district: "",
    role: "",
    email: "",
    gender: "",
    rank: "",
    state: "",
    street: "",
    city: "",
    birthday: "",
    country: "",
  });
  const [isOpen, setOpen] = useState<boolean>(open);
  const [allowAdd] = useState<boolean>(isAdd);

  const [isChangeCountry, setIsChangeCountry] = useState(false);
  const [oldStateSelected, setOldStateSelected] = useState<State | null>(null);
  const [oldDistrictSelected, setOldDistrictSelected] =
    useState<District | null>(null);
  const [getAllCountry, { data: countryData, error: countryError }] =
    useGetAllCountry(fragmentGetAllCountry);
  const [getAllState, { data: getAllStateData }] =
    useGetAllState(fragmentGetAllStates);
  const [getAllDistrict, { data: getAllDistrictData }] = useGetAllDistrict(
    fragmentGetAllDistricts,
  );
  const [getAllCity, { data: getAllCityData }] =
    useGetAllCity(fragmentGetAllCities);
  const [listCountryState, setListCountryState] = useState<Country[]>([]);
  const [listState, setListState] = useState<State[]>([]);
  const [listDistrict, setListDistrict] = useState<District[]>([]);
  const [listCity, setListCity] = useState<City[]>([]);
  const [countryAPISelected, setCountryAPISelected] = useState<Country | null>(
    null,
  );
  const [stateSelected, setStateSelected] = useState<State | null>(null);
  const [districtSelected, setDistrictSelected] = useState<District | null>(
    null,
  );
  const [citySelected, setCitySelected] = useState<City | null>(null);

  const { listCustomerRole } = useCustomerRoles();
  const [roleSelected, setRoleSelected] = useState<Role | null>(null);

  const [createUser, { data: createData, error: errorCreate }] =
    useCreateUser(fragmentCreateUser);

  const [listGender] = useState<{ _id: Gender; name: string }[]>([
    {
      _id: "female" as Gender,
      name: "Female",
    },
    {
      _id: "male" as Gender,
      name: "Male",
    },
  ]);
  const [genderSelected, setGenderSelected] = useState<{
    _id: Gender;
    name: string;
  } | null>(null);

  useEffect(() => {
    invokegetAllCountryAPI();
  }, []);

  useEffect(() => {
    if (isChangeCountry) {
      setListDistrict([]);
      setListCity([]);
    }
  }, [isChangeCountry]); // dung

  useEffect(() => {
    if (stateSelected != oldStateSelected) {
      setListDistrict([]);
      setListCity([]);
      setDistrictSelected(null);
      setCitySelected(null);
    }
  }, [stateSelected]);

  useEffect(() => {
    if (districtSelected != oldDistrictSelected) {
      setListCity([]);
      setCitySelected(null);
    }
  }, [districtSelected]);

  useEffect(() => {
    countryData && setListCountryState(countryData.getAllCountry.results || []);
  }, [countryData]);

  useEffect(() => {
    getAllStateData &&
      setListState(getAllStateData.getAllState.results as State[]);
  }, [getAllStateData]);

  useEffect(() => {
    getAllDistrictData &&
      setListDistrict(getAllDistrictData.getAllDistrict.results as District[]);
  }, [getAllDistrictData]);

  useEffect(() => {
    getAllCityData && setListCity(getAllCityData.getAllCity.results as City[]);
  }, [getAllCityData]);

  useEffect(() => {
    if (isChangeCountry) {
      setStateSelected(null);
    }
    countryAPISelected && invoketGetAllStatesAPI();
  }, [countryAPISelected, isChangeCountry]);

  useEffect(() => {
    if (isChangeCountry) setDistrictSelected(null);
    stateSelected && invokeGetAllDistrictSAPI();
  }, [stateSelected, isChangeCountry]);

  useEffect(() => {
    if (isChangeCountry) setCitySelected(null);
    districtSelected && invokeGetAllCitiAPI();
  }, [districtSelected, isChangeCountry]);

  const invokegetAllCountryAPI = () => {
    getAllCountry({ variables: {} });
  };

  const invoketGetAllStatesAPI = () => {
    getAllState({
      variables: {
        filter: {
          country: countryAPISelected?._id || "",
        },
      },
    });
  };

  const invokeGetAllDistrictSAPI = () => {
    getAllDistrict({
      variables: {
        filter: { state: stateSelected?._id || "" },
      },
    });
  };

  const invokeGetAllCitiAPI = () => {
    getAllCity({
      variables: {
        filter: {
          district: districtSelected?._id || "",
        },
      },
    });
  };

  useEffect(() => {
    //when add new branch but dont create, close form => then set null all fields
    if (!isOpen) {
      setCountryAPISelected(null);
      setStateSelected(null);
      setDistrictSelected(null);
      setCitySelected(null);
    }
  }, [
    countryAPISelected,
    stateSelected,
    districtSelected,
    citySelected,
    isOpen,
  ]);

  useEffect(() => {
    if (createData && !errorCreate) {
      toast.success(t("common.create-success"));
      onSuccess && onSuccess();
      handleClose();
      setLoading(false);
    }
    if (!createData && errorCreate) {
      toast.error(t("common.create-fail"));
      handleClose();
      setLoading(false);
    }
  }, [createData, errorCreate]);

  useEffect(() => {
    if (editField) {
      setCountryAPISelected(editField?.location?.country as Country);
      setStateSelected(editField?.location?.state as State);
      setOldStateSelected(editField?.location?.state as State);
      setDistrictSelected(editField?.location?.district as District);
      setOldDistrictSelected(editField?.location?.district as District);
      setCitySelected(editField?.location?.city as City);
      setGenderSelected({
        _id: editField?.gender as Gender,
        name: ((editField.gender?.charAt(0).toUpperCase() as string) +
          editField?.gender?.toLowerCase().slice(1)) as string,
      });
      setRoleSelected(editField?.role as Role);
      setInitialValues({
        displayName: editField?.displayName as string,
        phoneNumber: editField?.phoneNumber as string,
        email: editField?.email as string,
        password: "",
        code: editField?.customerCode as string,
        street: editField?.location?.addressLine as string,
        country: countryAPISelected?._id as string,
        state: stateSelected?._id as string,
        district: districtSelected?._id as string,
        city: citySelected?._id as string,
        birthday: editField?.birthday as string,
        role: roleSelected?._id as string,
      });
    }
  }, [editField]);

  const handleSubmit = (values: IFormValue) => {
    const input: CreateUserInput = {
      displayName: values.displayName,
      phoneNumber: values.phoneNumber,
      gender: genderSelected?._id as Gender,
      email: values.email,
      birthday: values.birthday as string,
      location: {
        country: countryAPISelected?._id as string,
        state: stateSelected?._id as string,
        district: districtSelected?._id as string,
        city: citySelected?._id as string,
        addressLine: values.street,
      },
      password: values.password as string,
      role: roleSelected?._id as string,
    };

    setLoading(true);
    createUser({
      variables: {
        input: input,
      },
    });
  };
  const handleClose = () => {
    setOpen(false);
    onClose && onClose();
  };

  return (
    <>
      <ElementWrapper
        onClick={() => setOpen(!isOpen)}
        className="flex justify-end w-full"
      >
        {ButtonMenu}
      </ElementWrapper>

      <Dialog
        open={isOpen}
        onClose={handleClose}
        className="overflow-auto"
        size="lg"
      >
        <CustomersManagementDialogContainer>
          <DialogTitle>{title}</DialogTitle>
          <Formik
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={handleSubmit}
            enableReinitialize
          >
            <Form>
              <Input
                name="displayName"
                label={t("configuration.customer-management.name")}
                placeholder={t("configuration.customer-management.enter-name")}
                required
                disabled={allowAdd ? false : true}
              />
              <div className="flex items-center gap-2">
                <Select
                  name="country"
                  label={t("register.country")}
                  onSelect={country => {
                    setCountryAPISelected(country);
                    setIsChangeCountry(true);
                  }}
                  optionSelected={countryAPISelected}
                  options={listCountryState}
                  className="max-w-[150px] w-full"
                  disabled={allowAdd ? false : true}
                />
                <Select
                  name="state"
                  label={
                    countryAPISelected?.alpha2Code === "de"
                      ? t("register.state")
                      : t("register.state-selectedVietnam")
                  }
                  placeholder={t(
                    "configuration.customer-management.select-state",
                  )}
                  optionSelected={stateSelected}
                  options={listState}
                  onSelect={state => setStateSelected(state)}
                  className="max-w-[300px] w-full"
                  disabled={allowAdd ? false : true}
                />
              </div>
              <Input
                name="phoneNumber"
                label={t("configuration.customer-management.phone-number")}
                placeholder={t(
                  "configuration.customer-management.enter-phone-number",
                )}
                required
                disabled={allowAdd ? false : true}
              />
              <Select
                name="district"
                label={t("configuration.customer-management.district")}
                placeholder={t(
                  "configuration.customer-management.select-district",
                )}
                options={listDistrict}
                optionSelected={districtSelected}
                onSelect={district => setDistrictSelected(district)}
                disabled={allowAdd ? false : true}
              />
              <Select
                name="gender"
                label={t("configuration.customer-management.gender")}
                placeholder={t(
                  "configuration.customer-management.select-gender",
                )}
                options={listGender}
                onSelect={gender => setGenderSelected(gender)}
                optionSelected={genderSelected}
                disabled={allowAdd ? false : true}
              />
              <Select
                name="city"
                label={t("configuration.account-info.city")}
                onSelect={option => setCitySelected(option)}
                optionSelected={citySelected}
                options={listCity}
                disabled={allowAdd ? false : true}
              />

              <Input
                name="street"
                label={t("configuration.customer-management.street")}
                placeholder={t(
                  "configuration.customer-management.enter-street",
                )}
                disabled={allowAdd ? false : true}
              />

              <Input
                name="email"
                label={t("configuration.customer-management.email")}
                placeholder={t("configuration.customer-management.enter-email")}
                required
                disabled={allowAdd ? false : true}
              />
              <Select
                name="role"
                label={t("configuration.customer-management.role")}
                placeholder={t("configuration.customer-management.select-role")}
                options={listCustomerRole?.getAllRole?.results || []}
                optionSelected={roleSelected}
                onSelect={role => setRoleSelected(role)}
                optionTarget="position"
                required
                disabled={allowAdd ? false : true}
              />
              <DatePicker
                className="-mt-3"
                name="birthday"
                label={t("configuration.customer-management.date-of-birth")}
                disabled={allowAdd ? false : true}
              />

              <Input
                name="password"
                label={t("configuration.customer-management.password")}
                placeholder={t(
                  "configuration.customer-management.enter-password",
                )}
                type="password"
                required
                disabled={allowAdd ? false : true}
              />
              <div></div>
              <div></div>
              <ButtonWrapper>
                {allowAdd ? (
                  <>
                    <Button
                      outline
                      type="button"
                      onClick={() => setOpen(false)}
                    >
                      {t("common.cancel")}
                    </Button>
                    <Button type="submit" primary loading={loading}>
                      {t("common.accept")}
                    </Button>
                  </>
                ) : (
                  <Button outline type="button" onClick={() => setOpen(false)}>
                    {t("common.close")}
                  </Button>
                )}
              </ButtonWrapper>
            </Form>
          </Formik>
        </CustomersManagementDialogContainer>
      </Dialog>
    </>
  );
};

export default CustomersManagementDialog;
