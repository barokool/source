import { QROrderLayoutContainer, QROrderPageWrapper } from "./styles";

interface IQROrderLayoutProps {}

const QROrderLayout: React.FC<IQROrderLayoutProps> = ({ children }) => {
  return (
    <QROrderLayoutContainer>
      <QROrderPageWrapper>{children}</QROrderPageWrapper>
    </QROrderLayoutContainer>
  );
};

export default QROrderLayout;
