import React, { ChangeEvent, useState, KeyboardEvent } from "react";

import { randomId } from "common/functions";
import SVG from "designs/SVG";

import {
  InputContainer,
  Label,
  InputLabel,
  IconWrapper,
  Input,
  Error,
} from "./styles";

interface IInputWithIconProps {
  type?: "text" | "number";
  className?: string;
  initialValue?: string;
  valueRef?: React.MutableRefObject<string>;
  Icon: React.ReactNode;
  placeholder?: string | undefined;
  error?: string;
  maxLength?: number;
  onChange?: (value: string) => void;
  onPressEnter?: (value: string) => void;
  label?: string;
  onKeyPress?: (e: KeyboardEvent<HTMLInputElement>) => void;
}

const InputWithIcon: React.FC<IInputWithIconProps> = props => {
  const {
    type = "text",
    className = "",
    initialValue = "",
    Icon,
    valueRef,
    placeholder = "",
    maxLength,
    onChange,
    error,
    onPressEnter,
    label = "",
    onKeyPress,
    ...rest
  } = props;
  const [text, setText] = useState(initialValue);
  const [id, _] = useState(randomId());

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    let newValue = event.target.value;
    if (event.target.value.length > event.target.maxLength) {
      newValue = event.target.value.slice(0, event.target.maxLength);
    }
    setText(newValue);
    if (valueRef) valueRef.current = newValue;
    onChange && onChange(newValue);
  };

  const handleKeyDown = (e: React.KeyboardEvent<HTMLInputElement>) => {
    if (e.key === "Enter") {
      onPressEnter && onPressEnter(text);
    }
  };

  return (
    <InputContainer className={className}>
      {label && <Label>{label}</Label>}
      <InputLabel htmlFor={id}>
        <IconWrapper>{Icon}</IconWrapper>
        <Input
          id={id}
          placeholder={placeholder}
          type={type}
          value={text}
          maxLength={maxLength}
          onChange={handleChange}
          onKeyDown={handleKeyDown}
          onKeyPress={onKeyPress}
          {...(rest as any)}
        />
      </InputLabel>
      {error && (
        <Error>
          <SVG
            className="w-2 h-2 mr-0.5"
            name="common/warning"
            width={16}
            height={16}
          />
          {error}
        </Error>
      )}
    </InputContainer>
  );
};

export default InputWithIcon;
