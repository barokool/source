import { ReactChild } from "react";
import { StyledComponent } from "styled-components/macro";
import {
  Box,
  TwoSideLayoutContainer,
  LeftSide,
  RightSide,
  ButtonBox,
  ListBox,
} from "./styles";

interface ITwoSideLayoutProps {}

const TwoSideLayout: React.FC<ITwoSideLayoutProps> & {
  LeftSide: StyledComponent<"div", any, {}, never>;
  RightSide: StyledComponent<"div", any, {}, never>;
  ListBox: StyledComponent<"div", any, {}, never>;
  ButtonBox: StyledComponent<"div", any, {}, never>;
} = props => {
  const { children } = props;
  return (
    <TwoSideLayoutContainer>
      <Box>{children}</Box>
    </TwoSideLayoutContainer>
  );
};

TwoSideLayout.LeftSide = LeftSide;
TwoSideLayout.RightSide = RightSide;
TwoSideLayout.ButtonBox = ButtonBox;
TwoSideLayout.ListBox = ListBox;

export default TwoSideLayout;
