import { useState, useCallback, useMemo, useEffect } from "react";
import { toast } from "react-toastify";
import { PATH } from "common/constants/routes";
import { getQueryFromLocation } from "common/functions/route";
import { RouteComponentProps } from "react-router";
import Table, { IColumns } from "designs/Table";
import ActionButtons from "designs/ActionButton";
import Empty from "components/Empty";
import ListDataLayout from "layouts/ListDataLayout";
import EmptyImage from "assets/images/shipping/DriverListEmpty.png";

//hooks
import { usePage } from "hooks/usePage";
import { useBreadcrumb } from "hooks/useBreadcrumb";
import { useCurrentBranch } from "hooks/useCurrentBranch";
import { useLoading } from "hooks/useLoading";
import { useDebouncedState } from "hooks/useDebouncedState";
import useAuth from "hooks/useAuth";

//languages
import i18n, { t } from "language";

//api
import {
  User,
  useGetAllUser,
  useDeleteUser,
  Branch,
  Location,
  RoleEnum,
} from "apiCaller";
import { fragmentGetAllUser } from "services/user";

//locals
import { AddButton, SearchBox, DriverContainer } from "./styles";
import DriverDialog from "./DriverDialog";

const SIZE_PER_PAGE = 10;
const LOAD_DATA = "LOAD_DATA";
const DEBOUNCE_TIME = 300;

interface ISearchDependencies {
  branchSelected: Branch | null;
  text: string;
}

interface IDriverListProps extends RouteComponentProps {}

const DriverList: React.FC<IDriverListProps> = ({ location }) => {
  const { branchSelected } = useCurrentBranch();
  const { startLoading, stopLoading } = useLoading();
  const { accountInfo } = useAuth();
  const { branches: listUserBranch } = accountInfo.userInfo;

  //local state
  const [listUser, setListUser] = useState<User[]>([]);
  const [page, setPage] = usePage(getQueryFromLocation(location)?.page);
  const [totalSize, setTotalSize] = useState(0);
  const [actionSuccess, setActionSuccess] = useState<boolean>(false);
  const [search, setSearch] = useDebouncedState<ISearchDependencies>(
    {
      branchSelected: null,
      text: "",
    },
    objectSearch => {
      if (objectSearch.branchSelected) {
        invokeGetAllDataAPI(objectSearch.text, objectSearch.branchSelected);
      }
    },
    DEBOUNCE_TIME,
  );

  //api
  const [
    getAllDriver,
    {
      data: getAllDriverData,
      error: getAllDriverErrors,
      loading: getAllDriverLoading,
    },
  ] = useGetAllUser(fragmentGetAllUser);

  const [
    deleteDriver,
    {
      loading: deleteDriverLoading,
      error: deleteDriverErrors,
      data: deleteDriverData,
    },
  ] = useDeleteUser();

  useBreadcrumb([
    {
      name: t("shipping.shipping.shipping"),
    },
    {
      name: t("shipping.driver.driver"),
      href: PATH.SHIPPING.DRIVER,
    },
  ]);

  useEffect(() => {
    branchSelected && invokeGetAllDataAPI("", branchSelected);
  }, [page, branchSelected]);

  useEffect(() => {
    if (actionSuccess === true && branchSelected) {
      invokeGetAllDataAPI("", branchSelected);
    }
    setActionSuccess(false);
  }, [actionSuccess]);

  useEffect(() => {
    if (getAllDriverLoading || deleteDriverLoading) {
      startLoading(LOAD_DATA);
    } else {
      stopLoading(LOAD_DATA);
    }
  }, [getAllDriverLoading, deleteDriverLoading]);

  useEffect(() => {
    branchSelected &&
      setSearch({
        ...search,
        branchSelected,
      });
  }, [branchSelected]);

  useEffect(() => {
    if (getAllDriverData) {
      setListUser(getAllDriverData.getAllUser?.results || []);
      setTotalSize(getAllDriverData.getAllUser.totalCount as number);
    }
  }, [getAllDriverData]);

  //toast errors when they occur
  useEffect(() => {
    getAllDriverErrors && toast.error(getAllDriverErrors.message);
    deleteDriverErrors && toast.error(deleteDriverErrors.message);
  }, [getAllDriverErrors, deleteDriverErrors]);

  useEffect(() => {
    if (deleteDriverData && !deleteDriverErrors) {
      toast.success(t("common.delete-success"));
      setActionSuccess(true);
    }
  }, [deleteDriverData]);

  //get table types filter on branch and name
  const invokeGetAllDataAPI = (searchText: string = "", branch: Branch) => {
    getAllDriver({
      variables: {
        page: page - 1,
        size: SIZE_PER_PAGE,
        filterUser: {
          branches: [branch._id || ""] as string[],
          displayName: searchText,
          role: RoleEnum.Driver,
        },
      },
    });
  };

  const onPageChange = useCallback((nextPage: number) => {
    setPage(nextPage);
  }, []);

  const renderAction = (record: User) => {
    return (
      <ActionButtons
        buttons={{
          edit: {
            DialogContent: ({ onClose }) => {
              return (
                <DriverDialog
                  handleOnClose={onClose}
                  open
                  editField={record}
                  title={t("shipping.driver.edit")}
                  onSuccess={() => setActionSuccess(true)}
                />
              );
            },
          },
          delete: {
            title: t("shipping.driver.delete"),
            message: t("shipping.driver.confirm-delete"),
            onDelete: () =>
              deleteDriver({
                variables: { id: record._id as string },
              }),
          },
        }}
      />
    );
  };

  const columns: IColumns = useMemo(
    () => [
      {
        text: t("shipping.driver.name"),
        dataField: "displayName",
      },
      {
        text: t("shipping.driver.phone-number"),
        dataField: "phoneNumber",
      },
      {
        text: t("shipping.driver.email"),
        dataField: "email",
      },
      {
        text: t("shipping.driver.address"),
        dataField: "location",
        formatter: (location: Location) =>
          (location &&
            location?.state?.name +
              ", " +
              location?.city?.name +
              ", " +
              location?.district?.name +
              ", " +
              location?.addressLine) ||
          t("common.no-info"),
      },
      // {
      //   text: t("shipping.driver.note"),
      //   dataField: "note",
      // },
      {
        text: "",
        dataField: "",
        style: { width: "100px" },
        formatter: (_: string, record: User) => renderAction(record),
      },
    ],
    [page, actionSuccess, i18n.language],
  );

  return totalSize === 0 && !search.text ? (
    <Empty
      SVG={<img src={EmptyImage} width={200} height={200} />}
      Title={t("shipping.driver.empty-title")}
      SubTitle={t("shipping.driver.empty-message")}
      ButtonMenu={
        <DriverDialog
          onSuccess={() => setActionSuccess(true)}
          title={t("shipping.driver.add")}
          ButtonMenu={
            <AddButton primary className="mx-auto mt-2">
              {t("shipping.driver.add")}
            </AddButton>
          }
        />
      }
    />
  ) : (
    <DriverContainer>
      <ListDataLayout
        rightTopBar={
          <DriverDialog
            onSuccess={() => setActionSuccess(true)}
            title={t("shipping.driver.add")}
            ButtonMenu={
              <AddButton primary>{t("shipping.driver.add")}</AddButton>
            }
          />
        }
        leftTopBar={
          <SearchBox
            onFetchData={text =>
              setSearch({
                ...search,
                text,
              })
            }
            placeholder={t("shipping.driver.search-by-name")}
          />
        }
        title={t("shipping.driver.driver")}
      >
        <Table
          columns={columns}
          data={listUser || []}
          totalSize={totalSize}
          sizePerPage={SIZE_PER_PAGE}
          onPageChange={onPageChange}
          page={page}
          isRemote
        />
      </ListDataLayout>
    </DriverContainer>
  );
};
export default DriverList;
