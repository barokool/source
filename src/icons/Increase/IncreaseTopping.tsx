/**
 * @TODO
 *  [ ] convert svg to jsx - Come here: https://magic.reactjs.net/htmltojsx.html
 *  [ ] replace all fill="..." to  fill="currentColor" (except fill=none)
 *  [ ] add {...props} to <svg >
 */

import { IIconSVGProps } from "typings";

const IncreaseToppingIcon: React.FC<IIconSVGProps> = props => {
  return (
    <svg
      width={26}
      height={26}
      viewBox="0 0 26 26"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <rect
        x={1}
        y={1}
        width={24}
        height={24}
        rx={8}
        fill="white"
        stroke="#EEEEEE"
      />
      <path
        d="M13.75 12.25H16.3161C16.6938 12.25 17 12.5858 17 13C17 13.4142 16.6938 13.75 16.3161 13.75H13.75V16.3161C13.75 16.6938 13.4142 17 13 17C12.5858 17 12.25 16.6938 12.25 16.3161V13.75H9.68394C9.30621 13.75 9 13.4142 9 13C9 12.5858 9.30621 12.25 9.68394 12.25H12.25V9.68394C12.25 9.30621 12.5858 9 13 9C13.4142 9 13.75 9.30621 13.75 9.68394V12.25Z"
        fill="#646464"
      />
    </svg>
  );
};
export default IncreaseToppingIcon;
