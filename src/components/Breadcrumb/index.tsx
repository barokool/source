import { Fragment } from "react";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import { BreadcrumbContainer, Container, ItemWrapper, Item } from "./styles";
import { IRootState } from "redux/storeToolkit";
import cn from "classnames";
import { IIconSVGProps } from "typings";

interface IBreadcrumbProps {
  className?: string;
}

const Breadcrumb: React.FC<IBreadcrumbProps> = (props: IBreadcrumbProps) => {
  const { className = "" } = props;
  const { breadcrumb } = useSelector((state: IRootState) => state._config);

  return (
    <BreadcrumbContainer className={className}>
      <Container>
        {breadcrumb.map((item, index) => {
          const lastItem = index === breadcrumb.length - 1;
          return (
            <Fragment key={index}>
              <ItemWrapper lastItem={lastItem}>
                {lastItem ? (
                  <Item className="text-neutral-1">{item.name}</Item>
                ) : item?.href ? (
                  <Link to={item.href!} className="text-neutral-2">
                    {item.name}
                  </Link>
                ) : (
                  <Item className="text-neutral-2">{item.name}</Item>
                )}
              </ItemWrapper>
              {index !== breadcrumb.length - 1 && (
                <ArrowIcon
                  className="text-neutral-1 w-1.5 h-1.5 mx-0.5"
                  direction="RIGHT"
                />
              )}
            </Fragment>
          );
        })}
      </Container>
    </BreadcrumbContainer>
  );
};

export default Breadcrumb;

const ArrowIcon: React.FC<IIconSVGProps> = ({ className }) => {
  return (
    <svg
      className={cn("arrow-icon icon rotate-90 transform", className)}
      width={16}
      height={16}
      viewBox="0 0 16 16"
      fill="currentColor"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path d="M2.9797 5.31312C3.15721 5.13561 3.43499 5.11947 3.63073 5.26471L3.68681 5.31312L7.99992 9.626L12.313 5.31312C12.4905 5.13561 12.7683 5.11947 12.9641 5.26471L13.0201 5.31312C13.1976 5.49063 13.2138 5.76841 13.0686 5.96415L13.0201 6.02023L8.35347 10.6869C8.17596 10.8644 7.89819 10.8805 7.70244 10.7353L7.64637 10.6869L2.9797 6.02023C2.78444 5.82496 2.78444 5.50838 2.9797 5.31312Z" />
    </svg>
  );
};
