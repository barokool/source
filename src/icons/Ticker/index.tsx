/**
 * @TODO
 *  [ ] convert svg to jsx - Come here: https://magic.reactjs.net/htmltojsx.html
 *  [ ] replace all fill="..." to  fill="currentColor" (except fill=none)
 *  [ ] add {...props} to <svg >
 */

import { IIconSVGProps } from "typings";

const TickerIcon: React.FC<IIconSVGProps> = props => {
  return (
    <svg
      width="16"
      height="16"
      viewBox="0 0 16 16"
      fill="currentColor"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M8 16C12.4183 16 16 12.4183 16 8C16 3.58172 12.4183 0 8 0C3.58172 0 0 3.58172 0 8C0 12.4183 3.58172 16 8 16Z"
        fill="currentColor"
      />
      <path
        d="M10.3292 5.83892C10.559 5.6092 10.9314 5.6092 11.1611 5.83892C11.3908 6.06864 11.3908 6.44109 11.1611 6.67081L7.63171 10.2002C7.40199 10.4299 7.02954 10.4299 6.79982 10.2002L4.83904 8.23943C4.60932 8.00971 4.60932 7.63726 4.83904 7.40754C5.06876 7.17782 5.44121 7.17782 5.67093 7.40754L7.21577 8.95238L10.3292 5.83892Z"
        fill="currentColor"
      />
    </svg>
  );
};

export default TickerIcon;
