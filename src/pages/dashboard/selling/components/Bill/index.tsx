import React, { useEffect, useState } from "react";
import GogobeiLogo from "assets/images/logo/logo-color.png";
import QrCodeImage from "assets/images/selling/qr-code.png";

//redux
import { useSelector } from "react-redux";
import { IRootState } from "typings";

//styles
import {
  Address,
  BillCode,
  BillContainer,
  CheckOut,
  Footer,
  Header,
  ImageWrapper,
  ListItem,
  Logo,
} from "./styles";

//languages
import { t } from "language";

//hooks
import useAuth from "hooks/useAuth";
import { useCurrentBranch } from "hooks/useCurrentBranch";

interface IBillProps {}

const Bill = React.forwardRef((props, ref) => {
  const { orderSelected } = useSelector((state: IRootState) => state.order);
  const { accountInfo } = useAuth();
  const { userInfo } = accountInfo;
  const { branchSelected } = useCurrentBranch();
  const [avatarSelected, setAvatarSelected] = useState<string>("");

  useEffect(() => {
    branchSelected?.images?.[0] &&
      setAvatarSelected(
        (branchSelected?.images?.[0].medium as string) ||
          (branchSelected?.images?.[0].small as string) ||
          (branchSelected?.images?.[0].default as string),
      );
  }, [branchSelected]);

  return (
    <BillContainer ref={ref as React.ForwardedRef<HTMLDivElement>}>
      <ImageWrapper>
        <Logo src={avatarSelected || GogobeiLogo} />
      </ImageWrapper>

      <Address>
        {(userInfo.branches?.[0].location?.addressLine as string) ||
          "73 Rạch Bùng Binh, Phường 9, Quận 3, Thành phố Hồ Chí Minh"}
      </Address>

      <BillCode.Wrapper>
        <BillCode.Label>{t("selling.bill.code")}: </BillCode.Label>
        <BillCode.Content>
          {"#" + (orderSelected?.barcode || "1231238234")}
        </BillCode.Content>
      </BillCode.Wrapper>

      <Header.Wrapper>
        <Header.Item>{t("selling.number")}</Header.Item>
        <Header.Item>{t("selling.product.name")}</Header.Item>
        <Header.Item>{t("selling.product.size")}</Header.Item>
        <Header.Item>{t("selling.quantity")}</Header.Item>
        <Header.Item>{t("selling.product.price")}</Header.Item>
      </Header.Wrapper>

      {orderSelected?.orderItems?.map((item, index) => (
        <ListItem.Wrapper>
          <ListItem.Content>{index + 1}</ListItem.Content>
          <ListItem.Content>{item?.product?.name}</ListItem.Content>
          <ListItem.Content>{item?.unit?.name}</ListItem.Content>
          <ListItem.Content>{item?.quantity}</ListItem.Content>
          <ListItem.Content>{item?.totalPrice}</ListItem.Content>
        </ListItem.Wrapper>
      ))}

      <CheckOut.Wrapper>
        <CheckOut.ItemWrapper className="border-t-neutral-1">
          <CheckOut.LeftContent>{t("selling.discount")}</CheckOut.LeftContent>
          <CheckOut.RightContent>
            {orderSelected?.coupon?.value || t("selling.no-info")}
          </CheckOut.RightContent>
        </CheckOut.ItemWrapper>

        <CheckOut.ItemWrapper className="border-t-neutral-1">
          <CheckOut.LeftContent>
            {t("selling.final-price")}
          </CheckOut.LeftContent>
          <CheckOut.RightContent>
            {orderSelected?.price?.finalPrice || t("selling.no-info")}
          </CheckOut.RightContent>
        </CheckOut.ItemWrapper>

        <CheckOut.ItemWrapper className="border-t-neutral-1">
          <CheckOut.LeftContent>
            {t("selling.customer-pay")}
          </CheckOut.LeftContent>
          <CheckOut.RightContent>
            {orderSelected?.customerPay || t("selling.no-info")}
          </CheckOut.RightContent>
        </CheckOut.ItemWrapper>

        <Footer.Wrapper>
          <Footer.ItemWrapper>
            <Footer.ItemContent>
              Giá sản phẩm trên đã bao gồm thuế VAT 8%
            </Footer.ItemContent>
            <Footer.ItemContent>
              Phiếu này chỉ có giá trị xuất hóa đơn trong ngày
            </Footer.ItemContent>
            <Footer.ItemContent>
              Mọi thắc mắc xin liên hệ 0819190227
            </Footer.ItemContent>
          </Footer.ItemWrapper>

          <Footer.ItemWrapper>
            <Footer.ItemContent>
              Mở camera trên điện thoại của bạn để web app và nhận ưu đãi
            </Footer.ItemContent>
            <Footer.ItemContent>
              Sử dụng app để tích điểm và đổi quà
            </Footer.ItemContent>
            <Footer.ItemContent>
              Living your life with Gogobei !
            </Footer.ItemContent>
          </Footer.ItemWrapper>
        </Footer.Wrapper>
      </CheckOut.Wrapper>
    </BillContainer>
  );
});
export default Bill;
