import styled from "styled-components/macro";
import tw from "twin.macro";
import _Button from "designs/Button";

export const NotFoundContainer = styled.div`
  ${tw`h-screen w-screen flex items-center justify-center`}
`;

export const Container = styled.div`
  ${tw`w-full gap-3 flex flex-col items-center justify-center`}
`;

export const Heading = styled.p`
  ${tw`text-20 phone:text-26 text-secondary font-bold `}
  letter-spacing : 0.04em;
  line-height: 31px;
`;

export const SubTitle = styled.p`
  ${tw`text-16 phone:text-20 text-body-color font-regular`}
  letter-spacing: 0.04em;
  line-height: 24px;
`;

export const ButtonWrapper = styled.div`
  ${tw`flex items-center text-white text-12  phone:text-16   font-medium`}
  padding: 13px 100px;
`;

export const Text = styled.p`
  ${tw`whitespace-nowrap phone:text-16 text-12 font-medium w-full`}
  font-weight: 500;
  font-size: 16px;
  line-height: 19px;
  letter-spacing: 0.04em;
  margin: 0px 10px;
`;

export const Button = styled(_Button)`
  ${tw`rounded-[12px]`}
`;
