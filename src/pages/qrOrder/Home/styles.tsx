import styled from "styled-components/macro";
import tw from "twin.macro";

export const HomePageContainer = styled.div`
  ${tw`px-2 py-1.5 `}
`;
