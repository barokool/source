import styled from "styled-components/macro";
import tw from "twin.macro";

export const SearchBoxContainer = styled.div`
  ${tw`flex-1 ml-3`}
`;
