import { ButtonHTMLAttributes, DetailedHTMLProps, useRef } from "react";
import { useHistory } from "react-router-dom";

import { ButtonWrapper } from "./styles";
import Spinner from "icons/Spinner";
interface IButtonProps
  extends DetailedHTMLProps<
    ButtonHTMLAttributes<HTMLButtonElement>,
    HTMLButtonElement
  > {
  to?: string;
  className?: string;
  primary?: boolean;
  outline?: boolean;
  loading?: boolean;
}

const Button: React.FC<IButtonProps> = props => {
  const history = useHistory();
  const {
    children,
    className = "",
    to,
    primary,
    loading = false,
    outline,
    ...rest
  } = props;
  const buttonRef = useRef<HTMLButtonElement>(null);
  const handleClick = () => {
    if (to) {
      setTimeout(() => {
        history.push(to);
      }, 300);
    }
  };

  return (
    <ButtonWrapper
      ref={buttonRef}
      outline={outline}
      primary={primary}
      loading={loading}
      className={className}
      onClick={handleClick}
      {...(rest as any)}
    >
      {loading ? <Spinner /> : ""}
      {children}
    </ButtonWrapper>
  );
};

export default Button;
