import { IImageInput } from "typings";

export const CUSTOM_SIZE_UPLOAD_AVATAR: IImageInput = {
  small: {
    width: 100,
    height: null,
  },
  medium: {
    width: 600,
    height: null,
  },
};

export const CUSTOM_SIZE_UPLOAD_ADS_IMAGE: IImageInput = {
  small: {
    width: 600,
    height: null,
  },
  medium: {
    width: 1250,
    height: null,
  },
};

export const CUSTOM_SIZE_IMAGE_Promotion_NEWS: IImageInput = {
  small: {
    width: 600,
    height: null,
  },
  medium: {
    width: 1250,
    height: null,
  },
};
