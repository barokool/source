import React, { useCallback, useEffect, useMemo } from "react";
import { RouteComponentProps } from "react-router-dom";
import {
  getQueryFromLocation,
  shouldDecreasePageIndex,
} from "common/functions";
import { resetAction } from "redux/slices/common";
import { PATH } from "common/constants/routes";

//hooks
import { useBreadcrumb } from "hooks/useBreadcrumb";
import { useDispatch, useSelector } from "react-redux";
import { usePage } from "hooks/usePage";

import SVG from "designs/SVG";
import Table, { IColumns } from "designs/Table";
import AlertDialog from "components/AlertDialog";
import StatusTag from "components/StatusTag";
import { PageTitle } from "common/styles/Title";
import UserDialog from "./Dialog";
import {
  UserContainer,
  Avatar,
  ActionWrapper,
  SearchBox,
  AddButton,
  RenderActionContainer,
} from "./styles";

import faker from "faker";
import { t } from "language";
import { IRootState } from "redux/storeToolkit";
import { User } from "apiCaller";

const SIZE_PER_PAGE = 10;
const UserPage: React.FC<RouteComponentProps> = ({ location }) => {
  const dispatch = useDispatch();
  const { actionSuccess } = useSelector((state: IRootState) => state.common);
  const [page, setPage] = usePage(getQueryFromLocation(location)?.page);

  useEffect(() => {
    invokeGetAllDataAPI();
  }, [page]);

  useEffect(() => {
    invokeGetAllDataAPI();
    setUpBreadcrumb();
  }, []);

  const setUpBreadcrumb = () => {
    dispatch(
      useBreadcrumb([
        {
          name: t("account.user.account"),
          href: PATH.DASHBOARD,
        },
        {
          name: t("account.user.application-user"),
        },
      ]),
    );
  };

  useEffect(() => {
    if (actionSuccess) {
      dispatch(resetAction());
      if (shouldDecreasePageIndex(page, 15, SIZE_PER_PAGE)) {
        setPage(page - 1);
        return;
      }
      invokeGetAllDataAPI();
    }
  }, [actionSuccess]);

  // Get your page API
  const invokeGetAllDataAPI = () => {};

  const handleChangePage = useCallback((nextPage: number) => {
    setPage(nextPage);
  }, []);

  const handleDelete = (record: User) => {
    console.log(record);
  };

  const renderAction = (record: User) => {
    return (
      <RenderActionContainer>
        <AlertDialog
          tooltip="Xoá"
          ButtonMenu={<SVG name="common/delete" width={20} height={20} />}
          title={t("account.user.delete-user")}
          message={t("account.user.confirm-delete-user")}
          onConfirm={() => handleDelete(record)}
        />
        <UserDialog
          title={t("account.user.edit-user")}
          editField={record}
          ButtonMenu={<SVG name="common/edit" width={20} height={20} />}
        />
      </RenderActionContainer>
    );
  };

  const renderAvatar = (record: User) => {
    return <Avatar src={record?.avatar?.small as string} />;
  };

  const columns: IColumns = useMemo(
    () => [
      {
        text: t("account.user.avt"),
        dataField: "avatar",
        formatter: (_: string, record: User) => renderAvatar(record),
      },
      {
        text: t("account.user.name"),
        dataField: "username",
      },
      {
        text: t("account.user.phone-number"),
        dataField: "phoneNumber",
      },
      {
        text: t("account.user.email"),
        dataField: "email",
      },
      {
        text: t("account.user.status.status"),
        dataField: "enable",
        formatter: (_: string, record: User) => (
          <StatusTag
            active={record?.status === "isActive" || false}
            activeLabel={t("account.user.status.active")}
            inactiveLabel={t("account.user.status.active")}
          />
        ),
      },
      {
        text: t("account.user.actions"),
        dataField: "actions",
        formatter: (_: string, record: User) => renderAction(record),
      },
    ],
    [page],
  );

  return (
    <UserContainer>
      <PageTitle>{t("account.user.application-user")}</PageTitle>
      <ActionWrapper>
        <SearchBox />
        <UserDialog
          ButtonMenu={
            <AddButton primary>{t("account.user.add-new-user")}</AddButton>
          }
        />
      </ActionWrapper>
      <Table
        data={fakeUserList}
        columns={columns}
        sizePerPage={SIZE_PER_PAGE}
        page={page}
        totalSize={15}
        onPageChange={handleChangePage}
      />
    </UserContainer>
  );
};

export default UserPage;

const fakeUserList: User[] = new Array(15).fill({
  avatar: {
    small: faker.internet.avatar(),
  },
  phoneNumber: faker.phone.phoneNumber(),
  username: faker.internet.userName(),
  email: faker.internet.email(),
  enabled: faker.datatype.boolean(),
});
