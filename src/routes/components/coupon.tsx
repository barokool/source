import { IRoutes } from "typings";
import { PATH } from "common/constants/routes";
import { lazy } from "react";
import { t } from "language";

import CouponIcon from "icons/Dashboard/Coupon";

const Coupon = lazy(() => import("pages/dashboard/coupon/Coupon"));

export const couponRoutes: IRoutes = {
  name: t("coupon.coupon"),
  path: PATH.COUPON.SELF,
  exact: true,
  isPrivate: true,
  Icon: CouponIcon,
  children: [
    {
      name: t("coupon.coupon-code.coupon-code"),
      path: PATH.COUPON.COUPON_CODE,
      isPrivate: true,
      Component: Coupon,
      exact: true,
    },
  ],
};
