import { KeyboardEvent } from "react";

/**
 * @Note place this function into onKeyPress Input, which you want to force it enter only number
 * */
export const forceTextInputEnterNumber = (
  event: KeyboardEvent<HTMLInputElement>,
) => {
  const validKeys = [
    "Delete",
    "Backspace",
    "ArrowLeft",
    "ArrowRight",
    "ArrowUp",
    "ArrowDown",
  ];
  if (!!/[a-z]/.test(event.key) && !validKeys.includes(event.key)) {
    event.preventDefault();
  }
};
