import styled from "styled-components/macro";
import tw from "twin.macro";
import Input from "designs/InputV2";

export const CallServiceContainer = styled.div`
  ${tw`my-1.5 mt-2`}
`;

export const CallServiceTitle = styled.h3`
  ${tw`text-14 text-black font-bold`}
`;

export const PressBellContainer = styled.div`
  ${tw`mt-3 justify-center`}
`;

export const PressBellMessage = styled.p`
  ${tw`mt-2 text-12 text-black`}
`;

export const NoteInput = styled(Input)`
  ${tw`w-full mt-1 h-4`}
`;
