import React, { useState, useCallback, useEffect } from "react";
import { Formik } from "formik";
import * as yup from "yup";

import {
  AccountInfoContainer,
  AccountInfoWrapper,
  Title,
  LeftSideContainer,
  LeftSideTitle,
  RightSideContainer,
  RightSideTitle,
  Form,
  Button,
  ButtonWrapper,
  ActionButton,
} from "./styles";

import { t } from "language";

import Input from "designs/InputV2";
import Select from "designs/Select";
import { PATH } from "common/constants/routes";

import { useBreadcrumb } from "hooks/useBreadcrumb";
import useAuth from "hooks/useAuth";
import { useBranches } from "hooks/useBranches";

import {
  Branch,
  City,
  Country,
  District,
  Role,
  RoleEnum,
  State,
  UpdateUserInput,
  useGetAllCity,
  useGetAllCountry,
  useGetAllDistrict,
  useGetAllRole,
  useGetAllState,
  useGetUserById,
  User,
} from "apiCaller";
import { fragmentGetUserById } from "services/user";
import { fragmentGetAllRole } from "services/role";

import ConfirmDialog from "./ConfirmDialog";
import {
  fragmentGetAllCities,
  fragmentGetAllCountry,
  fragmentGetAllDistricts,
  fragmentGetAllStates,
} from "services/location";
import MultipleSelect from "designs/MultipleSelect";
import MultipleAutocomplete from "designs/MultipleAutoComplete";

interface IAccountInfoProps {
  account: User;
}

interface IFormValue {
  phoneNumber?: string;
  displayName: string;
  branchName?: string[];
  country: string;
  state: string;
  district: string;
  city: string;
  street?: string;
  avatar?: string;
  images?: string;
  password?: string;
  confirmPassword?: string;
  role?: string;
  email?: string;
  birthday?: string;
}

const AccountInfoPage: React.FC<IAccountInfoProps> = ({ account }) => {
  const { accountInfo } = useAuth();
  const [getUserById, { data }] = useGetUserById(fragmentGetUserById);
  const userInfo = data?.getUserById;
  const [getAllRoles, { data: listRole }] = useGetAllRole(fragmentGetAllRole);
  const { listBranch } = useBranches();
  const [isChangeCountry, setIsChangeCountry] = useState(false);
  const [oldStateSelected, setOldStateSelected] = useState<State | null>(null);
  const [oldDistrictSelected, setOldDistrictSelected] =
    useState<District | null>(null);
  const [getAllCountry, { data: countryData, error: countryError }] =
    useGetAllCountry(fragmentGetAllCountry);
  const [getAllState, { data: getAllStateData }] =
    useGetAllState(fragmentGetAllStates);
  const [getAllDistrict, { data: getAllDistrictData }] = useGetAllDistrict(
    fragmentGetAllDistricts,
  );
  const [getAllCity, { data: getAllCityData }] =
    useGetAllCity(fragmentGetAllCities);
  const [listCountryState, setListCountryState] = useState<Country[]>([]);
  const [listState, setListState] = useState<State[]>([]);
  const [listDistrict, setListDistrict] = useState<District[]>([]);
  const [listCity, setListCity] = useState<City[]>([]);
  const [countryAPISelected, setCountryAPISelected] = useState<Country | null>(
    null,
  );
  const [stateSelected, setStateSelected] = useState<State | null>(
    (userInfo?.location?.state as State) || null,
  );
  const [districtSelected, setDistrictSelected] = useState<District | null>(
    (userInfo?.location?.district as District) || null,
  );
  const [citySelected, setCitySelected] = useState<City | null>(null);

  const [roleSelected, setRoleSelected] = useState<Role | null>(
    userInfo?.role as Role,
  );
  const [branchesSelected, setBranchesSelected] = useState<Branch[]>([]);
  const [inputForm, setInputForm] = useState<UpdateUserInput>({});
  const [passChange, setPassChange] = useState<string>("");
  const [initialValues, setInitialValues] = useState<IFormValue>({
    displayName: "",
    branchName: [""],
    phoneNumber: "",
    country: "",
    state: "",
    street: "",
    district: "",
    password: "",
    city: "",
  });

  const validationSchema = yup
    .object()
    .shape<{ [key in keyof IFormValue]: any }>({
      displayName: yup
        .string()
        .required(t("configuration.account-info.validate-name")),
      password: yup
        .string()
        .min(6, t("configuration.account-info.validate-pass")),
      confirmPassword: yup
        .string()
        .oneOf(
          [yup.ref("password"), null],
          t("configuration.account-info.validate-confirmPass"),
        ),
      city: yup.string().required(),
      country: yup.string().required(),
      district: yup.string().required(),
      state: yup.string().required(),
    });

  useBreadcrumb([
    {
      name: t("configuration.configuration"),
      href: PATH.CONFIGURATION.SELF,
    },
    {
      name: t("configuration.account-info.account-info"),
      href: PATH.CONFIGURATION.ACCOUNT_INFORMATION,
    },
  ]);

  const invokeSetAllRoles = useCallback(() => {
    getAllRoles({
      variables: {
        filterRole: {
          idBranch: accountInfo?.userInfo?.branches?.[0]?._id as string,
          name: accountInfo?.userInfo?.role?.name as RoleEnum,
        },
      },
    });
  }, []);

  useEffect(() => {
    invokeSetAllRoles();
  }, []);

  useEffect(() => {
    if (accountInfo?.userInfo?._id) {
      invokeGetUserInfoAPI();
      console.log({ data });
    }
  }, [accountInfo?.userInfo?._id]);
  console.log({ userInfo });

  const invokeGetUserInfoAPI = () => {
    getUserById({
      variables: {
        id: accountInfo?.userInfo?._id as string,
      },
    });
  };
  useEffect(() => {
    invokegetAllCountryAPI();
  }, []);

  useEffect(() => {
    if (isChangeCountry) {
      setListDistrict([]);
      setListCity([]);
    }
  }, [isChangeCountry]); // dung

  useEffect(() => {
    if (stateSelected != oldStateSelected) {
      setListDistrict([]);
      setListCity([]);
      setDistrictSelected(null);
      setCitySelected(null);
    }
  }, [stateSelected]);

  useEffect(() => {
    if (districtSelected != oldDistrictSelected) {
      setListCity([]);
      setCitySelected(null);
    }
  }, [districtSelected]);

  useEffect(() => {
    countryData && setListCountryState(countryData.getAllCountry.results || []);
  }, [countryData]);

  useEffect(() => {
    getAllStateData &&
      setListState(getAllStateData.getAllState.results as State[]);
  }, [getAllStateData]);

  useEffect(() => {
    getAllDistrictData &&
      setListDistrict(getAllDistrictData.getAllDistrict.results as District[]);
  }, [getAllDistrictData]);

  useEffect(() => {
    getAllCityData && setListCity(getAllCityData.getAllCity.results as City[]);
  }, [getAllCityData]);

  useEffect(() => {
    if (isChangeCountry) {
      setStateSelected(null);
    }
    countryAPISelected && invoketGetAllStatesAPI();
  }, [countryAPISelected, isChangeCountry]);

  useEffect(() => {
    if (isChangeCountry) setDistrictSelected(null);
    stateSelected && invokeGetAllDistrictSAPI();
  }, [stateSelected, isChangeCountry]);

  useEffect(() => {
    if (isChangeCountry) setCitySelected(null);
    districtSelected && invokeGetAllCitiAPI();
  }, [districtSelected, isChangeCountry]);

  const invokegetAllCountryAPI = () => {
    getAllCountry({ variables: {} });
  };

  const invoketGetAllStatesAPI = () => {
    getAllState({
      variables: {
        filter: {
          country: countryAPISelected?._id || "",
        },
      },
    });
  };

  const invokeGetAllDistrictSAPI = () => {
    getAllDistrict({
      variables: {
        filter: { state: stateSelected?._id || "" },
      },
    });
  };

  const invokeGetAllCitiAPI = () => {
    getAllCity({
      variables: {
        filter: {
          district: districtSelected?._id || "",
        },
      },
    });
  };

  useEffect(() => {
    if (userInfo) {
      setCountryAPISelected(userInfo?.location?.country as Country);
      setStateSelected(userInfo?.location?.state as State);
      setOldStateSelected(userInfo?.location?.state as State);
      setDistrictSelected(userInfo?.location?.district as District);
      setOldDistrictSelected(userInfo?.location?.district as District);
      setCitySelected(userInfo?.location?.city as City);
      setRoleSelected(userInfo?.role as Role);
      setBranchesSelected(userInfo?.branches as Branch[]);
      setInitialValues({
        displayName: userInfo?.displayName as string,
        branchName: userInfo?.branches?.map(item => item._id as string),
        phoneNumber: userInfo?.phoneNumber as string,
        country: countryAPISelected?._id as string,
        state: stateSelected?._id as string,
        district: districtSelected?._id as string,
        city: citySelected?._id as string,
        street: userInfo?.location?.addressLine as string,
        role: userInfo?.role?._id as string,
        password: "",
        email: userInfo?.email as string,
      });
    }
  }, [userInfo]);

  const handleSubmit = (values: IFormValue) => {
    let input: UpdateUserInput = {};
    if (passChange.length >= 6) {
      input = {
        displayName: values.displayName,
        branches: branchesSelected?.map(item => item._id as string),
        location: {
          country: countryAPISelected?._id as string,
          state: stateSelected?._id as string,
          district: districtSelected?._id as string,
          city: citySelected?._id as string,
          addressLine: values.street,
        },
        password: values.password,
        email: values.email,
      };
    } else {
      input = {
        displayName: values.displayName,
        branches: branchesSelected?.map(item => item._id as string),
        location: {
          country: countryAPISelected?._id as string,
          state: stateSelected?._id as string,
          district: districtSelected?._id as string,
          city: citySelected?._id as string,
          addressLine: values.street,
        },
        email: values.email,
      };
    }
    console.log(input);

    setInputForm(input);
  };

  return (
    <AccountInfoContainer>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={handleSubmit}
        enableReinitialize
      >
        <Form>
          <Title>{t("configuration.account-info.account-info")}</Title>
          <AccountInfoWrapper>
            <LeftSideContainer>
              <LeftSideTitle>
                <Input
                  name="phoneNumber"
                  label={t("configuration.account-info.phone-number")}
                  className="mt-4"
                  disabled
                />
                <Input
                  name="displayName"
                  label={t("configuration.account-info.display-name")}
                  className="mt-4"
                />
                {/* <Input
                  name="branchName"
                  label={t("configuration.account-info.branch-name")}
                  className="mt-4"
                  required
                /> */}
                <MultipleAutocomplete
                  label={t("configuration.account-info.branch-name")}
                  name="branchName"
                  options={userInfo?.branches as Branch[]}
                  listOptionsSelected={branchesSelected}
                  onSelect={setBranchesSelected}
                  className="mt-4"
                />
                <Input
                  name="password"
                  label={t("configuration.account-info.new-pass")}
                  className="mt-4"
                  type={"password"}
                  onChangeValue={e => setPassChange(e as string)}
                />
                <Input
                  name="confirmPassword"
                  label={t("configuration.account-info.pass-again")}
                  className="mt-4"
                  type={"password"}
                />

                <Input
                  name="street"
                  label={t("configuration.account-info.address")}
                  className="mt-4"
                />
              </LeftSideTitle>
            </LeftSideContainer>
            <RightSideContainer>
              <RightSideTitle>
                <Select
                  name="role"
                  label={t("configuration.account-info.role")}
                  optionSelected={roleSelected}
                  options={listRole?.getAllRole?.results || []}
                  onSelect={role => setRoleSelected(role)}
                  className="mt-4"
                  disabled
                />
                <Input
                  name="email"
                  label={t("configuration.account-info.email")}
                  className="mt-4"
                />
                <div className="flex items-center gap-2 mt-4">
                  <Select
                    name="country"
                    label={t("register.country")}
                    onSelect={country => {
                      setCountryAPISelected(country);
                      setIsChangeCountry(true);
                    }}
                    optionSelected={countryAPISelected}
                    options={listCountryState}
                    className="max-w-[150px] w-full"
                  />
                  <Select
                    name="state"
                    label={
                      countryAPISelected?.alpha2Code === "de"
                        ? t("register.state")
                        : t("register.state-selectedVietnam")
                    }
                    optionSelected={stateSelected}
                    options={listState}
                    onSelect={state => setStateSelected(state)}
                    className=" w-full"
                  />
                </div>

                <Select
                  name="district"
                  label={
                    countryAPISelected?.alpha2Code === "de"
                      ? t("register.district")
                      : t("register.district-selectedVietnam")
                  }
                  required
                  onSelect={value => setDistrictSelected(value)}
                  optionSelected={districtSelected}
                  options={listDistrict}
                  className="mt-4"
                />
                <Select
                  name="city"
                  label={
                    countryAPISelected?.alpha2Code === "de"
                      ? t("register.city")
                      : t("register.city-selectedVietnam")
                  }
                  className="mt-4"
                  required
                  onSelect={city => setCitySelected(city)}
                  optionSelected={citySelected}
                  options={listCity}
                />
              </RightSideTitle>
            </RightSideContainer>
          </AccountInfoWrapper>

          <ButtonWrapper>
            <Button type="button" outline>
              {t("common.cancel")}
            </Button>
            <ConfirmDialog
              userId={userInfo?._id as string}
              editField={inputForm}
              title={t("configuration.account-info.pass")}
              ButtonMenu={
                <ActionButton type="submit" primary>
                  {t("common.accept")}
                </ActionButton>
              }
            />
          </ButtonWrapper>
        </Form>
      </Formik>
    </AccountInfoContainer>
  );
};

export default AccountInfoPage;
