import tw from "twin.macro";
import styled from "styled-components/macro";
import _Searchbox from "designs/SearchBox";
import _Button from "designs/Button";

export const ConfigurationPageContainer = styled.div`
  ${tw`mt-0.5 phone:mt-1 laptop:mt-3`}
`;

export const TopFunctionContainer = styled.div`
  ${tw`flex justify-between space-x-1`}
`;

export const SearchBox = styled(_Searchbox)`
  ${tw`w-full phone:w-30 laptop:w-50 phone:mb-0`}
`;

export const InfoLabel = styled.p`
  ${tw`text-black text-16 font-semibold mt-1 phone:mt-2 laptop:mt-3`}
`;

export const AddButton = styled(_Button)`
  ${tw`px-3 py-1 font-medium `}
`;

export const ConfigChildrenContainer = styled.div`
  ${tw`grid grid-cols-2 gap-x-2 gap-y-2 mt-0.5 phone:mt-1 laptop:mt-2`}
`;
