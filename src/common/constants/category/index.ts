import { IImageInput } from "typings";
export const CUSTOM_SIZE_CATEGORY_LV_2: IImageInput = {
  medium: {
    width: 800,
    height: null,
  },
  small: {
    width: 200,
    height: null,
  },
};
