import SVG from "designs/SVG";
import { PATH } from "common/constants/routes";
import { t } from "language";
import { Button } from "./styles";
import {
  NotFoundContainer,
  Container,
  Heading,
  ButtonWrapper,
  Text,
  SubTitle,
} from "./styles";

const NotFound: React.FC = () => {
  return (
    <NotFoundContainer>
      <Container>
        <SVG name="404" className="w-full max-w-[485px]  h-full" />
        <div className="flex justify-center items-center flex-col gap-1">
          <Heading>{t("notfound.error-occur")}</Heading>
          <SubTitle>{t("notfound.error-message")}</SubTitle>
        </div>
        <Button primary to={PATH.DASHBOARD}>
          <ButtonWrapper>
            <Text>{t("notfound.back-to-home-page")}</Text>
          </ButtonWrapper>
        </Button>
      </Container>
    </NotFoundContainer>
  );
};

export default NotFound;
