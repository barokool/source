import styled from "styled-components/macro";
import tw from "twin.macro";
import { Form as _Form } from "formik";
import _Button from "designs/Button";

export const RegisterContainer = styled.div`
  ${tw`flex items-center justify-center`}
`;

export const RegisterContent = styled.div`
  ${tw`p-4 flex flex-col  gap-4 w-[800px] overflow-y-hidden`}
`;

export const Heading = {
  Container: styled.div`
    ${tw`flex justify-between`}
  `,
  Title: styled.p`
    ${tw`font-bold text-26 text-black`}
  `,

  RightWrapper: styled.div`
    ${tw`w-max`}
  `,

  LeftWrapper: styled.div`
    ${tw`flex flex-col justify-center gap-4`}
  `,
};

export const Form = styled(_Form)`
  ${tw`flex flex-col gap-4`}
`;

export const ButtonWrapper = styled.div`
  ${tw`flex justify-end gap-2`}
`;

export const Button = styled(_Button)`
  ${tw`w-[170px] flex items-center justify-center text-16 font-medium py-1`}
`;

export const FormContainer = styled.div`
  ${tw`grid phone:grid-cols-2 gap-3`}
`;

export const ElementWrapper = styled.div`
  ${tw`flex flex-col gap-4 `}
`;

export const RegisterLabel = styled.p`
  ${tw`font-medium text-16 text-black pb-1`}
`;
