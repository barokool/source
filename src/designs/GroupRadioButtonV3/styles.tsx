import styled from "styled-components/macro";
import tw from "twin.macro";

export const RadioButtonContainer = styled.div<{ vertical: boolean }>`
  ${({ vertical }) => vertical && tw`flex gap-3 flex-wrap`}
`;

export const Wrapper = styled.div<{ disabled?: boolean }>`
  ${tw`w-max`}
  ${({ disabled }) => disabled && tw`pointer-events-none text-neutral-3`}
`;

export const Label = styled.label`
  ${tw`cursor-pointer flex items-center text-16 font-regular`}
`;

export const StyleRadioButton = styled.div<{
  checked: boolean;
  isError: boolean;
  disabled?: boolean;
}>`
  ${tw`relative w-1.5 h-1.5 rounded-full flex items-center justify-center mr-1 border-2 border-primary`}
  ${({ isError }) => isError && tw`border border-danger`}
  ${({ disabled }) => disabled && tw`border-neutral-3`}
  &::before {
    content: "";
    ${tw`absolute w-[9px] h-[9px] rounded-full duration-300`}
    ${({ disabled }) => (disabled ? tw`bg-neutral-3` : tw`bg-primary`)}
    ${({ checked }) =>
      checked ? tw`visible opacity-100` : tw`invisible opacity-0`}
  }
`;

export const Input = styled.input`
  ${tw`hidden`}
`;

export const Title = styled.p`
  ${tw`mb-1`}
`;
export const GroupRadioContainer = styled.div`
  ${tw`  `}
`;
export const CheckboxButton = styled.div<{
  checked?: boolean;
}>`
  ${tw`relative w-[8px] h-[8px]  rounded-full flex items-center justify-center  border-primary`}
  ${({ checked }) => (checked ? tw`bg-primary` : tw` opacity-100`)}
`;
