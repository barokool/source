export const PATH = {
  NOT_FOUND: "/not-found",
  DASHBOARD: "/dashboard",
  AUTH: {
    SELF: "/auth",
    LOGIN: "/auth/login",
    RESET_PASSWORD: "/auth/reset-password",
    REGISTER: "/auth/register",
  },
  ACCOUNT: {
    SELF: "/account",
    USER: "/account/user-list",
    MANAGER: "/account/manager-list",
    ADMIN: "/account/admin-list",
  },
  SETTING: {
    SELF: "/setting",
    GENERAL: "/setting/general",
    SEO: "/setting/seo",
    AD_BY_CODE: "/setting/ad-by-code",
    AD_BY_IMAGE: "/setting/ad-by-image",
    STATIC_WEBSITE: "/setting/static-website",
    DISTRICT: "/setting/district",
    PROVINCE: "/setting/province",
    WARD: "/setting/ward",
  },
  PRODUCT: {
    SELF: "/products",
    COMBO: "/products/list-combo",
    LIST: "/products/list",
    CATEGORY: "/products/category",
  },
  SELLING: {
    SELF: "/selling",
    AT_STORE: "/selling/at-store",
    TABLE_BOOKING: "/selling/table-booking",
    PAYMENT: "/selling/payment/:id",
  },
  CONFIGURATION: {
    SELF: "/configuration",
    BRANCH_MANAGEMENT: "/branch-management",
    EMPLOYEES_MANAGEMENT: "/staffs-management",
    CUSTOMERS_MANAGEMENT: "/customers-management",
    ACCOUNT_INFORMATION: "/account-information",
    ROLE_DELEGATION: "/role-delegation",
    GENERAL_SETTING: {
      SELF: "/general-settings",
      PRINTER: "/printer-settings",
      PAYMENT: "/payment-settings",
      LANGUAGE: "/language-settings",
    },
  },
  COUPON: {
    SELF: "/promotion",
    COUPON_CODE: "/coupon-code",
    COUPON_NEWS: "/coupon-news",
  },
  REPORT: {
    SELF: "/report",
    SELLING_REPORT: "/selling-report",
    CUSTOMERS_REPORT: "/customer-report",
    PRODUCT_REPORT: "/product-report",
    STAFF_REPORT: "/staff-report",
  },
  TABLE_MANAGEMENT: {
    SELF: "/table-management",
    TYPE: "/table-category",
    LIST: "/table-list",
  },
  ORDER_SERVICE: {
    SELF: "/order-service",
  },
  ORDER: {
    SELF: "/order",
    DELIVERY: "/order-delivery",
    BOOKING: "/order-booking",
    SELL_AT_STORE: "/order-sell-at-store",
  },
  SHIPPING: {
    SELF: "/shipping-management",
    SHIPPING: "/shipping",
    SHIPPING_HISTORY: "/shipping-history",
    DRIVER: "/driver",
  },
  QR_ORDER: {
    HOME: "/qr-order/home",
    MENU: "/qr-order/menu",
    FAVOURITE_DISHES: "/qr-order/favourite-dishes",
    PAYMENT: "/qr-order/payment",
    DETAIL: "/qr-order/detail",
    CHECK_OUT_DISHES: "/qr-order/check-out-dishes",
  },
};

const getAllStringValuesOfObject = <T>(obj: T): string[] => {
  if (!obj) return [];
  const values: string[] = [];
  for (let key in obj) {
    const currValue = obj[key];
    if (typeof currValue === "string") values.push(currValue);
    else if (typeof currValue === "object")
      values.push(...getAllStringValuesOfObject(currValue));
  }
  return values;
};

export const PATH_URLS = getAllStringValuesOfObject(PATH);
