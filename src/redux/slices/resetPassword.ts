import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface IResetPasswordState {
  phoneNumber: string | null;
  userId: string | null;
  token: string | null;
}

const initialState: IResetPasswordState = {
  phoneNumber: null,
  userId: null,
  token: null,
};

export const resetPasswordSlice = createSlice({
  name: "resetPassword",
  initialState,
  reducers: {
    setPhoneNumber: (state, action: PayloadAction<string>) => {
      state.phoneNumber = action.payload;
    },
    setUserId: (state, action: PayloadAction<string>) => {
      state.userId = action.payload;
    },
    setResetPasswordToken: (state, action: PayloadAction<string>) => {
      state.token = action.payload;
    },
  },
});

// Actions
export const { setPhoneNumber, setUserId, setResetPasswordToken } =
  resetPasswordSlice.actions;

export default resetPasswordSlice.reducer;
