import styled from "styled-components/macro";
import tw from "twin.macro";
import _Button from "designs/Button";

export const LeftReviewBillContainer = styled.div`
  ${tw`w-full h-screen shadow-lg`}
`;

export const ProductThumbnail = styled.img`
  ${tw`rounded-full w-3 h-3`}
`;
