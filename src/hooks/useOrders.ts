import { useState, useCallback, useEffect } from "react";
import { useGetAllOrder, Order, OrderTypeEnum } from "apiCaller";
import { fragmentGetAllOrder } from "services/order";

export const useOrders = () => {
  const [getAllOrder, { loading, called, data }] =
    useGetAllOrder(fragmentGetAllOrder);

  useEffect(() => {
    invokeGetAllOrders();
  }, []);

  const invokeGetAllOrders = useCallback(async () => {
    try {
      getAllOrder({
        variables: {
          filter: {
            type: "in_place" as OrderTypeEnum,
          },
        },
      });
    } catch (error) {
      console.error(error);
    }
  }, []);

  return {
    listOrders: data,
    invokeGetAllOrders,
  };
};
