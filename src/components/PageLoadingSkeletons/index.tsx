import { randomInt } from "common/functions/calc/randomInt";

interface IPageLoadingSekeletonProps {}

const PageLoadingPulse: React.FC<IPageLoadingSekeletonProps> = props => {
  return (
    <div className="w-full space-y-2 animate-pulse">
      {new Array(3).fill(0).map((_, index) => (
        <div
          key={index}
          className="h-4 bg-line rounded-md"
          style={{ width: `${randomInt(50, 100)}%` }}
        ></div>
      ))}

      <div className="h-30 w-full bg-line rounded-md"></div>
    </div>
  );
};

export default PageLoadingPulse;
