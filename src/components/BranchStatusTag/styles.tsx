import { BranchStatus } from "apiCaller";
import styled from "styled-components/macro";
import tw, { TwStyle } from "twin.macro";

const listStatus: {
  [k in BranchStatus]: TwStyle;
} = {
  closed: tw`bg-[#f1c9c8] text-[#CE1611]`,
  opening: tw`bg-[#f3e0c8] text-primary`,
  pause: tw`bg-gray text-black`,
  isActive: tw`bg-successfull text-white`,
  stopWorking: tw`text-danger bg-tertiary`,
};
export const BranchStatusTagContainer = styled.div<{
  active: BranchStatus;
}>`
  ${tw`flex justify-center p-1 rounded-sm text-14 font-regular whitespace-nowrap w-max`}
  ${({ active }) => listStatus[active]}
`;
