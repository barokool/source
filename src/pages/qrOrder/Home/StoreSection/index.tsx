import StoreImage from "assets/images/qr-order/store-image.png";

import {
  LeftHeaderContainer,
  StoreName,
  StoreAddress,
  RightCornerButton,
  RightHeaderContainer,
  BodyContainer,
  StoreImageContainer,
  HeaderContainer,
  StoreSectionContainer,
} from "./styles";

interface IStoreSectionProps {}

const StoreSection: React.FC<IStoreSectionProps> = props => {
  return (
    <StoreSectionContainer>
      <HeaderContainer>
        <LeftHeaderContainer>
          <StoreName>VEE AYY FOOD</StoreName>
          <StoreAddress>
            6 Hoàng Dư Khương, Phường 12, Quận 10, Thành phố Hồ Chí Minh
          </StoreAddress>
        </LeftHeaderContainer>
        <RightHeaderContainer>
          <RightCornerButton>Bàn 020</RightCornerButton>
        </RightHeaderContainer>
      </HeaderContainer>
      <BodyContainer>
        <StoreImageContainer>
          <img src={StoreImage} alt="store-image" className="min-w-full" />
        </StoreImageContainer>
      </BodyContainer>
    </StoreSectionContainer>
  );
};

export default StoreSection;
