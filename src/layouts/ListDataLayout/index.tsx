import { ReactNode } from "react";
import * as S from "./styles";

interface IListDataLayoutProps {
  title: string;
  toolBar?: ReactNode;
  leftTopBar?: ReactNode;
  rightTopBar?: ReactNode;
}

const ListDataLayout: React.FC<IListDataLayoutProps> = ({
  title,
  toolBar = null,
  leftTopBar = null,
  rightTopBar = null,
  children,
}) => {
  return (
    <S.ListDataLayoutContainer>
      <S.Title>{title}</S.Title>
      {toolBar && <S.ToolBar>{toolBar}</S.ToolBar>}
      {(leftTopBar || rightTopBar) && (
        <S.TopBar.Container>
          <S.TopBar.Left>{leftTopBar}</S.TopBar.Left>
          <S.TopBar.Right>{rightTopBar}</S.TopBar.Right>
        </S.TopBar.Container>
      )}
      <S.ListDataContent>{children}</S.ListDataContent>
    </S.ListDataLayoutContainer>
  );
};

export default ListDataLayout;
