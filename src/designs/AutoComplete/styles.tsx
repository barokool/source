import styled from "styled-components/macro";
import tw from "twin.macro";
import { Listbox } from "@headlessui/react";
import { formControlCommon } from "common/styles/FormControl";

export const AutocompleteContainer = styled.div`
  ${tw`w-full relative `}
`;

export const HiddenInput = styled.input`
  ${tw`absolute w-1 h-1 opacity-0 `}
`;

export const ListboxButton = styled(Listbox.Button)`
  ${tw`relative w-full text-left cursor-pointer`}
`;

export const Text = styled.p`
  ${tw`truncate`}
`;

export const ListboxOptionsContainer = styled(Listbox.Options)`
  ${tw`absolute z-30 w-full py-1 mt-1 overflow-auto border border-solid rounded-lg shadow-md max-h-25 border-neutral-4 bg-line focus:outline-none `}
`;

export const MenuButton = styled.div<{ isError: boolean; disabled: boolean }>`
  ${({ isError, disabled }) => formControlCommon(isError, disabled)}
  ${tw`grid items-center h-auto min-h-[50px]`}
  grid-template-columns: 1fr 25px;
`;

export const Placeholder = styled.p`
  ${tw`text-neutral-3`}
`;

export const MenuItem = styled.div<{ active?: boolean }>`
  ${tw` w-full px-1.5 py-0.5 text-14 font-regular cursor-pointer`}
  ${({ active }) => active && tw`bg-neutral-4`}
`;

export const EmptyData = styled.div`
  ${tw`w-full flex items-center justify-center py-0.5 text-14 font-semibold text-neutral-3`}
`;

export const SearchInput = styled.input`
  ${tw`w-full text-14 rounded-md px-1 py-0.5 bg-neutral-5 outline-none`}
`;

export const LabelWrapper = styled.div`
  ${tw`px-[4px] py-0 flex items-center justify-between absolute -top-2 left-[20px] z-10 bg-white rounded-sm`}
`;
