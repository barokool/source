import { useState, useCallback, useEffect } from "react";
import { Role, RoleEnum, useGetAllRole } from "apiCaller";
import { fragmentGetAllRole } from "services/role";

import { useCurrentBranch } from "./useCurrentBranch";

export const useDriverRoles = () => {
  const [getAllRoles, { data }] = useGetAllRole(fragmentGetAllRole);
  const [listDriverRole, setListDriverRole] = useState<Role[]>([]);
  const { branchSelected } = useCurrentBranch();

  useEffect(() => {
    branchSelected && invokeGetAllRoles();
  }, [branchSelected]);

  const invokeGetAllRoles = () => {
    getAllRoles({
      variables: {
        filterRole: {
          name: RoleEnum.Driver,
          idBranch: branchSelected?._id || "",
        },
      },
    });
  };

  useEffect(() => {
    data && setListDriverRole(data?.getAllRole.results as Role[]);
  }, [data]);

  return {
    listDriverRole,
    invokeGetAllRoles,
  };
};
