import styled from "styled-components/macro";
import tw from "twin.macro";

export const PageTitle = styled.p`
  ${tw`text-20 phone:text-26 font-semibold mt-1 phone:mt-3 mb-2 phone:mb-4`}
`;

export const DialogTitle = styled.p`
  ${tw`text-20 phone:text-26 font-semibold mb-1.5 phone:mb-3 text-left`}
`;
