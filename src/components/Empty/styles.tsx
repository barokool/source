import styled from "styled-components/macro";
import tw from "twin.macro";

export const EmptyContainer = styled.div`
  ${tw`flex flex-col gap-6 items-center justify-center h-full w-full my-auto`}
`;
export const Bottom = {
  Wrapper: styled.div`
    ${tw`flex flex-col gap-1 text-center`}
  `,
  Title: styled.p`
    ${tw`font-bold text-20 text-black`}
  `,
  SubTitle: styled.p`
    ${tw`font-regular text-16 text-black`}
  `,
};
