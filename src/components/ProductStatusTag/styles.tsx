import { ProductStatus } from "apiCaller";
import styled from "styled-components/macro";
import tw, { TwStyle } from "twin.macro";

const listStatus: {
  [k in ProductStatus]: TwStyle;
} = {
  Deleted: tw`bg-[#f1c9c8] text-[#CE1611]`,
  OutOfStock: tw`bg-[#f3e0c8] text-[#F88F01]`,
  InStock: tw`bg-[#b2e9cd] text-[#007637]`,
};
export const ProductStatusTagContainer = styled.div<{
  active: ProductStatus;
}>`
  ${tw`flex justify-center p-1 rounded-sm text-14 font-regular whitespace-nowrap w-max`}
  ${({ active }) => listStatus[active]}
`;
