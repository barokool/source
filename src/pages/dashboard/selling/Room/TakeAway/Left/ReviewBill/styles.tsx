import styled from "styled-components/macro";
import tw from "twin.macro";
import _Button from "designs/Button";

export const LeftReviewBillContainer = styled.div`
  ${tw`w-full h-screen relative shadow-lg`}
`;

export const HeaderContainer = styled.div`
  ${tw`p-2 border-b-2 border-line w-full`}
`;

export const OrderCodeTag = styled.div`
  ${tw`p-1 text-black text-16 font-semibold rounded-sm border-2 border-black w-max`}
`;

export const FooterContainer = styled.div`
  ${tw`py-1.5 px-2`}
`;

export const TemporaryCalculatedContainer = styled.div`
  ${tw`w-1/5 float-right flex items-center justify-between mb-4`}
`;

export const TemporaryCalculatedContent = styled.h3`
  ${tw`text-14 text-black`}
`;

export const TemporaryCalculatedPrice = styled.h2`
  ${tw`text-20 text-black font-semibold`}
`;
export const FooterButtonContainer = styled.div`
  ${tw` flex justify-between items-center w-full gap-x-4 mt-4`}
`;

export const FooterItemContent = styled.p`
  ${tw`text-14 text-black`}
`;

export const FooterItemContainer = styled.div`
  ${tw`flex cursor-pointer items-center w-max justify-center`}
`;

export const ButtonWrapper = styled.div`
  ${tw`w-full flex items-center mt-3 absolute bottom-0 `}
`;

export const RightButton = styled.button`
  ${tw`text-center py-1.5 bg-black text-16 text-white w-1/2`}
`;

export const LeftButton = styled.div`
  ${tw`text-center py-1.5 bg-line text-black text-16 w-1/2`}
`;

export const FooterItemWrapper = styled.div`
  ${tw`mx-auto flex`}
`;

export const RenderActionContainer = styled.div`
  ${tw`flex justify-end gap-2`}
`;

export const ProductThumbnail = styled.img`
  ${tw`rounded-full w-3 h-3`}
`;
