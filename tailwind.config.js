/**
 * @Note Please reset react app whenever you modify something inside
 */
const config = {
  // important: true,
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  darkMode: false,
  theme: {
    screens: {
      phone: "600px",
      // => @media (min-width: 600px) { ... }

      laptop: "1024px",
      // => @media (min-width: 1024px) { ... }

      desktop: "1270px",
      // => @media (min-width: 1270px) { ... }
    },
    colors: {
      primary: "#FFC107",
      secondary: "#252422",
      tertiary: "#E0E0E0",
      gray: "#959494",
      line: "#EFEFEF",
      danger: "#C72C41",
      warning: "#F88F01",
      successfull: "#309500",
      background: "#FAFAFA",
      white: "#FFFFFF",
      black: "#210C0F",
      "erorr-color": "#CE1611",
      "body-color": "#646464",
      transparent: "rgba(0,0,0,0)",
      "neutral-3": "#B1B1B1",
      "neutral-4": "#EEEEEE",
      "neutral-5": "#F8F8F8",
      //
      "primary-1": "#FFC107", // primary color
      "primary-2": "#373737",
      "primary-3": "#FFFFFF",
      "primary-4": "#F3F3F3",
      "sematic-1": "#CE1611", // Red color
      "sematic-2": "#F88F01", // Yellow color
      "sematic-3": "#007637", // Green color
      "neutral-1": "#252422", // title color
      "neutral-2": "#210C0F", // text color
      "neutral-3": "#E0E0E0",
      "neutral-3.5": "#e3e3e3",
      "neutral-4": "#EEEEEE",
      "neutral-5": "#F8F8F8",
      "b-1": "#DCD1AE", // background-1 :  primary color , opacity
      "b-2": "#F6F6FB",
      "b-3": "#3D3733",
      transparent: "rgba(0,0,0,0)",
      table: "rgba(0,0,0,.1)",
      "white-color": "#FFFFFF",
      "border-color": "#EEEEEE",
      "black-color": "#00000",
      "hover-color": "#F3F4F6",
    },
    fontSize: {
      12: "12px",
      13: "13px",
      14: "14px",
      16: "16px",
      20: "20px",
      26: "26px",
      30: "30px",
      32: "32px",
      36: "36px",
      40: "40px",
    },
    spacing: {
      0: "0",
      0.5: "5px",
      1: "10px",
      1.5: "15px",
      2: "20px",
      2.5: "25px",
      3: "30px",
      3.5: "35px",
      4: "40px",
      4.5: "45px",
      5: "50px",
      5.5: "55px",
      6: "60px",
      6.5: "65px",
      7: "70px",
      7.5: "75px",
      8: "80px",
      8.5: "85px",
      10: "100px",
      12: "120px",
      12.5: "125px",
      13: "130px",
      13.5: "135px",
      15: "150px",
      20: "200px",
      25: "250px",
      26: "260px",
      30: "300px",
      35: "350px",
      40: "400px",
      45: "450px",
      50: "500px",
      55: "550px",
      60: "600px",
    },
    opacity: {
      0: "0",
      10: "0.1",
      20: "0.2",
      30: "0.3",
      40: "0.4",
      50: "0.5",
      60: "0.6",
      70: "0.7",
      80: "0.8",
      90: "0.9",
      100: "1",
    },
    fontWeight: {
      extrathin: 100,
      thin: 200,
      light: 300,
      regular: 400,
      medium: 500,
      semibold: 600,
      bold: 700,
      extrabold: 800,
      black: 900,
    },
    extend: {
      boxShadow: {
        "as-border":
          "rgba(0, 0, 0, 0.05) 0px 6px 24px 0px, rgba(0, 0, 0, 0.08) 0px 0px 0px 1px",
        "drop-down":
          "rgb(15 15 15 / 5%) 0px 0px 0px 1px, rgb(15 15 15 / 10%) 0px 3px 6px, rgb(15 15 15 / 20%) 0px 9px 24px",
      },
      gridTemplateColumns: {
        "auto-1fr": "auto 1fr",
        "1fr-auto": "1fr auto",
      },
      borderRadius: {
        sm: "5px",
        md: "10px",
        lg: "15px",
        xl: "20px",
        full: "9999999px",
      },
      zIndex: {
        "-1": "-1",
      },
    },
  },
  variants: {
    extend: {
      display: ["group-hover"],
    },
  },
  plugins: [require("tailwind-scrollbar")],
};

module.exports = config;
