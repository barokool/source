import React, { useState, useEffect } from "react";

import {
  DishCardContainer,
  FavouriteDishesContainer,
  HeaderContainer,
  LeftHeaderContainer,
  LeftHeaderLabel,
  LeftButtonContent,
  QuantityContent,
  RightButtonContent,
  AddButtonWrapper,
  DishQuantity,
} from "./styles";
import SVG from "designs/SVG";
import OrderConfirmDialog from "components/OrderConfirmDialog";

interface IFavouriteDishesProps {}
import { IDish } from "pages/qrOrder/Menu";

import Item1 from "assets/images/qr-order/food-image-1.png";
import Item2 from "assets/images/qr-order/food-image-2.png";
import Item3 from "assets/images/qr-order/food-image-3.png";
import Item4 from "assets/images/qr-order/food-image-4.png";
import Item5 from "assets/images/qr-order/food-image-5.png";
import Item6 from "assets/images/qr-order/food-image-6.png";

import DishCard from "components/DishCard";
import CheckOutButton from "components/CheckOutButton";

const FavouriteDishes: React.FC<IFavouriteDishesProps> = props => {
  const [datas, setData] = useState<IDish[]>([
    {
      name: "Mực chiên nước mắm",
      price: 155000,
      thumbnail: Item1,
    },
    {
      name: "Tàu hũ nhồi thịt",
      price: 155000,
      thumbnail: Item2,
    },
    {
      name: "Gà chiên nước mắm",
      price: 200000,
      thumbnail: Item3,
    },
    {
      name: "Gà chiên nước mắm",
      price: 200000,
      thumbnail: Item4,
    },
    {
      name: "Gà chiên nước mắm",
      price: 200000,
      thumbnail: Item5,
    },
    {
      name: "Gà chiên nước mắm",
      price: 200000,
      thumbnail: Item6,
    },
  ]);
  return (
    <FavouriteDishesContainer>
      <HeaderContainer>
        <LeftHeaderContainer>
          <SVG name="common/heart" width={16} height={16} />
          <LeftHeaderLabel>Món yêu thích</LeftHeaderLabel>
        </LeftHeaderContainer>
        <SVG name="qrOrder/search" width={16} height={16} />
      </HeaderContainer>

      <DishCardContainer>
        {datas.map(item => (
          <DishCard
            dish={item}
            RightButton={
              <AddButtonWrapper>
                <DishQuantity>1</DishQuantity>
                <SVG name="qrOrder/add-dish" onClick={() => {}} />
              </AddButtonWrapper>
            }
          />
        ))}
      </DishCardContainer>

      <CheckOutButton
        LeftElements={
          <>
            {" "}
            <LeftButtonContent>Xem giỏ hàng</LeftButtonContent>
            <QuantityContent>1</QuantityContent>{" "}
          </>
        }
        RightElements={<RightButtonContent>đ 155,000</RightButtonContent>}
      />
    </FavouriteDishesContainer>
  );
};

export default FavouriteDishes;
