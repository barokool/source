import { t } from "language";
import { ReactNode, useEffect, useState } from "react";
import { DialogTitle } from "common/styles/Title";
import { toast } from "react-toastify";
import Dialog from "components/Dialog";
import { Formik } from "formik";
import * as Yup from "yup";
import Input from "designs/InputV2";

//api
import { Order, useDeleteOrder } from "apiCaller";

//locals
import {
  DeleteOrderDialogContainer,
  ElementWrapper,
  ButtonWrapper,
  Button,
  Form,
} from "./styles";

interface IDeleteOrderDialogProps {
  onSuccess?: () => void;
  onClose?: () => void;
  ButtonMenu?: ReactNode;
  order?: Order;
}

interface IFormValue {
  reason: string;
}

const validationSchema = Yup.object().shape<{ [key in keyof IFormValue]: any }>(
  {
    reason: Yup.string().required(t("order.common.please-enter-reason")),
  },
);

const DeleteOrderDialog: React.FC<IDeleteOrderDialogProps> = props => {
  const { children, onSuccess, order } = props;
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const [initialValues, setInitialValues] = useState<IFormValue>({
    reason: "",
  });
  const [deleteOrder, { data, error, loading }] = useDeleteOrder();

  useEffect(() => {
    if (data && !error) {
      toast.success(t("common.delete-success"));
      onSuccess && onSuccess();
      setIsOpen(false);
    }
    if (!data && error) {
      toast.error(error.message);
      setIsOpen(false);
    }
  }, [data, error]);

  const handleSubmit = (values: IFormValue) => {
    deleteOrder({
      variables: {
        listId: [order?._id as string],
      },
    });
  };

  return (
    <>
      <ElementWrapper onClick={() => setIsOpen(!isOpen)}>
        {children}
      </ElementWrapper>
      <Dialog
        open={isOpen}
        onClose={() => setIsOpen(false)}
        className="overflow-auto"
      >
        <DeleteOrderDialogContainer>
          <DialogTitle>{t("order.common.please-select-reason")}</DialogTitle>
          <Formik
            validationSchema={validationSchema}
            initialValues={initialValues}
            onSubmit={handleSubmit}
          >
            <Form>
              <Input
                name="reason"
                required
                type="text"
                label={t("order.common.delete-reason")}
              />
            </Form>
          </Formik>
          <ButtonWrapper>
            <Button outline type="button" onClick={() => setIsOpen(false)}>
              {t("common.cancel")}
            </Button>
            <Button primary type="submit" loading={loading}>
              {t("common.accept")}
            </Button>
          </ButtonWrapper>
        </DeleteOrderDialogContainer>
      </Dialog>
    </>
  );
};

export default DeleteOrderDialog;
