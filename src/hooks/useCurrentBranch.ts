import { useLocalStorage } from "./useLocalStorage";

import { Branch } from "apiCaller";

export const useCurrentBranch = () => {
  const [branchSelected, setBranchSelected] = useLocalStorage<Branch | null>(
    "branchSelected",
    null,
  );

  return { branchSelected, setBranchSelected };
};
