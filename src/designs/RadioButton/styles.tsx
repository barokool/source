import styled from "styled-components/macro";
import tw from "twin.macro";
import _SVG from "designs/SVG";

export const RadioButtonContainer = styled.div`
  ${tw`w-max flex items-center`}
`;

export const Icon = styled(_SVG)`
  ${tw`w-2.5 h-2.5 mr-1 cursor-pointer`}
`;

export const Label = styled.p`
  ${tw`font-medium text-black text-16`}
`;
