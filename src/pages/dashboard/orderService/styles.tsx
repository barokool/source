import _Button from "designs/Button";
import styled from "styled-components/macro";
import tw from "twin.macro";

export const OrderServiceContainer = styled.div`
  ${tw``}
`;

export const ButtonWrapper = styled.div`
  ${tw`flex justify-end gap-2`}
`;

export const Button = styled(_Button)`
  ${tw`py-1 px-1.5 float-right mt-2`}
`;
