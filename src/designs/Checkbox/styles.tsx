import styled from "styled-components/macro";
import tw from "twin.macro";

export const CheckboxContainer = styled.div`
  ${tw`  `}
`;
export const CheckboxButton = styled.div<{
  checked?: boolean;
}>`
  ${tw`relative w-[20px] h-[20px] rounded flex items-center justify-center border-[2px] border-primary`}
  & svg {
    ${tw`absolute w-full h-full duration-300`}
    ${({ checked }) =>
      checked ? tw`bg-primary visible opacity-100` : tw`invisible opacity-0`}
  }
`;
