import styled from "styled-components/macro";
import tw from "twin.macro";

export const TextAreaWrapper = styled.div`
  ${tw`w-full relative`}
`;

export const TextAreaContainer = styled.textarea<{ error: boolean }>`
  ${tw`w-full p-1.5 text-14 text-black border border-solid focus:border-primary`}
  ${({ error }) => (error ? tw`border-danger` : tw`border-gray`)}
`;

export const Label = styled.p`
  ${tw`px-[4px] py-0 flex items-center justify-between absolute -top-2 left-[20px] z-10 bg-white rounded-sm`}
`;

export const Required = styled.span`
  ${tw`text-16 text-danger font-medium ml-0.5`}
`;

export const Error = styled.p`
  ${tw`flex items-center text-14 text-danger font-regular mt-1`}
`;
