import React, { useState, useEffect } from "react";
import RadioGroup from "components/RadioGroup";
import { Formik } from "formik";
import * as Yup from "yup";
import { forceTextInputEnterNumber } from "common/functions/condition/forceTextInputEnterNumber";
import Input from "designs/InputV2";
import { toast } from "react-toastify";

//redux
import { IRootState } from "typings";
import { useSelector, useDispatch } from "react-redux";
import { setActionSuccess } from "redux/slices/common";
import { setOrderSelected } from "redux/slices/order";

//api
import {
  OrderStatusEnum,
  useUpdateOrder,
  useGetOrderById,
  Order,
} from "apiCaller";
import { fragmentUpdateOrder, fragmentGetOrderById } from "services/order";

//languages
import { t } from "language";

//locals
import {
  RightPaymentContainer,
  Bottom,
  BoxPoint,
  CustomRadio,
  Mid,
  Point,
  RadioItem,
  Top,
  ItemWrapper,
  PhoneNumberFieldWrapper,
  CustomerPayFieldWrapper,
  ItemLeft,
  ItemRight,
} from "./styles";

interface IRightPaymentProps {}

interface IFormValue {
  phoneNumber?: string;
  customerPay: number;
}

const validationSchema = Yup.object().shape<{
  [key in keyof IFormValue]: any;
}>({
  customerPay: Yup.number().required(t("selling.please-enter-customer-pay")),
});

const RightPayment: React.FC<IRightPaymentProps> = props => {
  const [initialValues] = useState<IFormValue>({
    customerPay: 0,
    phoneNumber: "",
  });
  //redux
  const { orderSelected } = useSelector((state: IRootState) => state.order);
  const dispatch = useDispatch();

  //local states
  const [customerPay, setCustomerPay] = useState<number>(0);
  const [customerPhoneNumber, setCustomerPhoneNumber] = useState<string>();
  const [excessMoney, setExcessMoney] = useState<number>();
  const [paymentTypeSelected, setPaymentTypeSelected] = useState<{
    id: string;
    name: string;
  } | null>(null);
  const [listPayment] = useState<{ id: string; name: string }[]>([
    {
      id: "1",
      name: t("selling.cash"),
    },
    {
      id: "2",
      name: t("selling.credit-card"),
    },
  ]);
  const [order, setOrder] = useState<Order | null>(null);

  //api
  const [updateOrder, { data: orderUpdated, error: updateOrderErrors }] =
    useUpdateOrder(fragmentUpdateOrder);
  const [getOrderById, { data: orderReturned, error: getOrderByIdErrors }] =
    useGetOrderById(fragmentGetOrderById);

  //catch data returned from api
  useEffect(() => {
    if (orderReturned) {
      setOrder(orderReturned?.getOrderById as Order);
    }
  }, [orderReturned]);

  useEffect(() => {
    if (order) {
      dispatch(setOrderSelected(order));
    }
  }, [order]);

  useEffect(() => {
    if (customerPay >= (orderSelected?.price?.finalPrice as number)) {
      setExcessMoney(
        customerPay - (orderSelected?.price?.finalPrice as number),
      );
    } else {
      setExcessMoney(0);
    }
  }, [customerPay]);

  useEffect(() => {
    if (orderUpdated) {
      dispatch(setActionSuccess());
      toast.success(t("selling.order-in-processing"));
      handleRefreshOrder();
    }
  }, [orderUpdated]);

  //toast errors when they occur
  useEffect(() => {
    updateOrderErrors && toast.error(updateOrderErrors.message);
  }, [updateOrderErrors]);

  useEffect(() => {
    getOrderByIdErrors && toast.error(getOrderByIdErrors.message);
  }, [getOrderByIdErrors]);

  //functions
  const handleRefreshOrder = () => {
    getOrderById({
      variables: { id: orderSelected?._id as string },
    });
  };

  const handleCancel = () => {
    dispatch(setOrderSelected(null));
    dispatch(setActionSuccess());
    handleRefreshOrder();
  };

  const handleProcessOrder = () => {
    updateOrder({
      variables: {
        id: orderSelected?._id as string,
        input: {
          status: OrderStatusEnum.Processing,
          phoneNumber: customerPhoneNumber,
          customerPay: customerPay,
        },
      },
    });
  };

  return (
    <Formik
      enableReinitialize
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={handleProcessOrder}
    >
      <RightPaymentContainer>
        {/*Top*/}
        <Top.Container>
          <ItemWrapper>
            <ItemLeft></ItemLeft>
            <ItemRight className="text-black text-14 mb-2">
              {orderSelected?.createdAt?.toString()}
            </ItemRight>
          </ItemWrapper>
          <PhoneNumberFieldWrapper>
            <ItemLeft>{t("selling.phone-number")}</ItemLeft>
            <Input
              name="phoneNumber"
              label={t("selling.phone-number")}
              placeholder={t("selling.enter-phone-number")}
              onChangeValue={phoneNumber =>
                setCustomerPhoneNumber(phoneNumber as string)
              }
              onKeyDown={forceTextInputEnterNumber}
            />
          </PhoneNumberFieldWrapper>
          <ItemWrapper>
            <ItemLeft>{t("selling.discount")}</ItemLeft>
            <ItemRight>
              {orderSelected?.coupon?.value?.toString().prettyMoney || "None"}
            </ItemRight>
          </ItemWrapper>
          <ItemWrapper>
            <ItemLeft>{t("selling.final-price")}</ItemLeft>
            <ItemRight className="font-semibold">
              {orderSelected?.price?.finalPrice}
            </ItemRight>
          </ItemWrapper>
        </Top.Container>

        {/*Middle*/}
        <Mid.Container>
          <CustomerPayFieldWrapper>
            <ItemLeft>{t("selling.customer-pay")}</ItemLeft>
            <Input
              name="customerPay"
              type="number"
              label={t("selling.customer-pay")}
              placeholder={t("selling.enter-customer-pay")}
              onChangeValue={customerPay =>
                setCustomerPay(customerPay as number)
              }
              onKeyDown={forceTextInputEnterNumber}
              required
            />
          </CustomerPayFieldWrapper>
          <ItemWrapper>
            <ItemLeft>{t("selling.payment-method")}</ItemLeft>
            <RadioGroup
              options={listPayment}
              onChange={option => setPaymentTypeSelected(option)}
              optionSelected={paymentTypeSelected}
              renderOption={({ option, checked }) => (
                <RadioItem.Container>
                  <RadioItem.LeftWrapper>
                    <BoxPoint>
                      <Point>
                        <CustomRadio active={checked} />
                      </Point>
                    </BoxPoint>
                    <RadioItem.Label>{option?.name}</RadioItem.Label>
                  </RadioItem.LeftWrapper>
                </RadioItem.Container>
              )}
            />
          </ItemWrapper>
        </Mid.Container>

        {/*Bottom*/}
        <Bottom.Container>
          <ItemWrapper>
            <ItemLeft>{t("selling.excess-money")}</ItemLeft>
            <ItemRight>{excessMoney}</ItemRight>
          </ItemWrapper>
          <ItemWrapper className="mt-2">
            <ItemLeft></ItemLeft>
            <Bottom.ButtonWrapper>
              <Bottom.Button
                outline
                onClick={() => {
                  dispatch(setOrderSelected(null));
                  handleCancel();
                }}
              >
                {t("common.cancel")}
              </Bottom.Button>
              <Bottom.Button primary type="submit">
                {t("common.accept")}
              </Bottom.Button>
            </Bottom.ButtonWrapper>
          </ItemWrapper>
        </Bottom.Container>
      </RightPaymentContainer>
    </Formik>
  );
};

export default RightPayment;
