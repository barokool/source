import styled from "styled-components/macro";
import tw from "twin.macro";
import _SVG from "designs/SVG";

export const SearchBoxContainer = styled.div`
  ${tw``}
`;

export const ContentWrapper = styled.div`
  ${tw`relative w-full max-w-full flex items-center`}
`;

export const Input = styled.input`
  ${tw`block w-full max-w-full pl-4 py-1.5 pr-1.5 text-14 leading-none font-regular border-solid border border-gray focus:outline-none`}
`;

export const SearchButton = styled.button`
  ${tw`absolute block px-1 leading-none`}
`;

export const Icon = styled(_SVG)`
  ${tw`w-2 h-2`}
`;

export const Dropdown = styled.ul`
  ${tw`absolute z-10 mt-1 overflow-y-auto bg-white rounded shadow-sm top-full w-15 max-h-30`}
`;

export const List = styled.li`
  ${tw``}
`;

export const Item = styled.button`
  ${tw`block w-full px-2 py-1 text-14 leading-none text-left hover:text-primary`}
`;
