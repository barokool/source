import styled from "styled-components/macro";
import tw from "twin.macro";
import { Listbox } from "@headlessui/react";
import { formControlCommon } from "common/styles/FormControl";

export const MultipleSelectContainer = styled.div`
  ${tw`w-full relative`}
`;

export const HiddenInput = styled.input`
  ${tw`absolute w-1 h-1 opacity-0 `}
`;

export const ListboxButton = styled(Listbox.Button)`
  ${tw`relative w-full text-left cursor-pointer focus:outline-none`}
`;

export const ListboxOptionsContainer = styled(Listbox.Options)`
  ${tw`absolute z-30 w-full  mt-1 overflow-auto rounded-sm shadow-as-border bg-white max-h-[300px] focus:outline-none`}
`;

export const MenuButton = styled.div<{ isError: boolean; disabled: boolean }>`
  ${({ isError, disabled }) => formControlCommon(isError, disabled)}
  ${tw`grid items-center h-auto min-h-[40px] max-h-[40px]`}
  grid-template-columns: 1fr 25px;
`;

export const TagContainer = styled.ul`
  ${tw`flex w-full flex-row flex-wrap gap-0.5 my-1`}
`;

export const Tag = styled.li`
  ${tw`  grid gap-1 p-0.5 items-center bg-primary text-white leading-none text-12 font-regular rounded-sm truncate max-w-[150px]`}
  grid-template-columns: 1fr 12px;
`;

export const TagText = styled.p`
  ${tw``}
`;

export const Placeholder = styled.p`
  ${tw`text-gray`}
`;

export const MenuItem = styled.div<{ active?: boolean }>`
  ${tw` w-full px-1.5 py-0.5 text-16 font-regular cursor-pointer`}
  ${({ active }) => active && tw`bg-line`}
`;

export const RemoveIconWrapper = styled.div`
  ${tw`w-1.5 h-full flex items-center`}
`;
export const LabelWrapper = styled.div`
  ${tw`px-[4px] py-0 flex items-center justify-between absolute -top-2 left-[20px] z-10 bg-white rounded-sm`},
`;
