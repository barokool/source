import styled from "styled-components/macro";
import tw from "twin.macro";
import _SVG from "designs/SVG";

export const LabelWrapper = styled.div`
  ${tw`flex items-center justify-center w-3 h-3 phone:w-4 phone:h-4`}
`;

export const Icon = styled(_SVG)`
  ${tw`w-1 phone:w-1.5`}
`;
