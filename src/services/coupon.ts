import { CreateCouponArgs, GenFields, Coupon, CouponResult } from "apiCaller";

export const fragmentGetAllCoupon: GenFields<CouponResult> = [
  {
    results: [
      "name",
      "_id",
      "code",
      "closeDate",
      "openingDate",
      "quantity",
      "method",
      "value",
      {
        branches: [
          "_id",
          "code",
          "createdAt",
          "name",
          "phoneNumber",
          "isMainBranch",
          "maxGuest",
          "slug",
          "status",
        ],
      },
      {
        products: ["_id", "name", "description"],
      },
    ],
  },
];

export const fragmentCreateCoupon: GenFields<Coupon> = [
  "name",
  "_id",
  "code",
  "closeDate",
  "openingDate",
  "method",
  "quantity",
  "value",
];

export const fragmentUpdateCoupon: GenFields<Coupon> = [
  "name",
  "_id",
  "code",
  "closeDate",
  "method",
  "openingDate",
  "quantity",
  "value",
  "method",
];
