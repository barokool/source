import styled from "styled-components/macro";
import tw from "twin.macro";
import _Button from "designs/Button";
import { Form as _Form } from "formik";

export const FeedBackDialogContainer = styled.div`
  ${tw` bg-white p-1 phone:p-2 laptop:p-5 rounded-sm`}
`;

export const DialogTitle = styled.p`
  ${tw`text-black text-14`}
`;

export const RateTitle = styled.h3`
  ${tw`text-black text-20 font-bold mb-1`}
`;

export const StarsContainer = styled.div`
  ${tw`flex mx-auto w-15 justify-between mb-1`}
`;

export const MidContentContainer = styled.div`
  ${tw`my-2`}
`;

export const ElementWrapper = styled.div`
  ${tw`cursor-pointer`}
`;

export const Form = styled(_Form)`
  ${tw``}
`;

export const ButtonWrapper = styled.div`
  ${tw`flex justify-center mt-2`}
`;

export const Button = styled(_Button)`
  ${tw`py-1.5 px-5.5 text-white rounded-sm`}
`;
