export { ReactComponent as DashboardIcon } from "assets/svg/drawer/dashboard.svg";
export { ReactComponent as AccountIcon } from "assets/svg/drawer/account.svg";
export { ReactComponent as SettingIcon } from "assets/svg/drawer/setting.svg";
export { ReactComponent as ProductIcon } from "assets/svg/drawer/product.svg";
