import {
  CheckOutButtonContainer,
  LeftButtonContainer,
  RightButtonContainer,
} from "./styles";

interface ICheckOutButtonProps {
  LeftElements?: React.ReactElement;
  RightElements?: React.ReactElement;
}

const CheckOutButton: React.FC<ICheckOutButtonProps> = ({
  LeftElements,
  RightElements,
}) => {
  return (
    <CheckOutButtonContainer>
      <LeftButtonContainer>{LeftElements}</LeftButtonContainer>
      <RightButtonContainer>{RightElements}</RightButtonContainer>
    </CheckOutButtonContainer>
  );
};

export default CheckOutButton;
