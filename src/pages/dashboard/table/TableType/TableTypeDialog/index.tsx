import React, { useEffect, useState } from "react";
import { Formik } from "formik";
import * as yup from "yup";
import Input from "designs/InputV2";
import Select from "designs/Select";
import Dialog from "components/Dialog";
import { DialogTitle } from "common/styles/Title";
import { toast } from "react-toastify";

//languages
import { t } from "language";

//hooks
import { useBranches } from "hooks/useBranches";

//api
import {
  Branch,
  TableType,
  TableTypeInput,
  useCreateTableType,
  useUpdateTableType,
} from "apiCaller";
import {
  fragmentCreateTableType,
  fragmentUpdateTableType,
} from "services/table";

//locals
import {
  ElementWrapper,
  TableCategoryDialogContainer,
  Form,
  ButtonWrapper,
  Button,
} from "./styles";
interface ITableTypeDialogProps {
  ButtonMenu?: React.ReactElement;
  editField?: TableType;
  title?: string;
  onClose?: () => void;
  onSuccess?: () => void;
  open?: boolean;
}

interface IFormValue {
  name: string;
  maxGuest?: number;
  quantity: number;
  branch?: string;
}

const formKey = (key: keyof IFormValue) => key;

const validationSchema = yup
  .object()
  .shape<{ [key in keyof IFormValue]: any }>({
    name: yup.string().required(t("table.table-type.please-enter-name")),
    maxGuest: yup
      .number()
      .required(t("table.table-type.please-enter-quantity")),
    quantity: yup
      .number()
      .required(t("table.table-type.please-enter-quantity")),
    branch: yup.string().required(t("table.table-type.please-select-branch")),
  });

const TableCategoryDialog: React.FC<ITableTypeDialogProps> = ({
  ButtonMenu,
  editField,
  title,
  onSuccess,
  onClose,
  open = false,
}) => {
  const [initialValues, setInitialValues] = useState<IFormValue>({
    name: "",
    maxGuest: 0,
    quantity: 0,
    branch: "",
  });

  const { listBranch: dataFromHooks } = useBranches();
  const listDataBranch = dataFromHooks?.getAllBranch.results;
  const [isOpen, setOpen] = useState<boolean>(open);
  const [branchSelected, setBranchSelected] = useState<Branch | null>(null);
  const [
    updateTableType,
    {
      data: updateTableTypeData,
      loading: updateTableTypeLoading,
      error: updateTableTypeErrors,
    },
  ] = useUpdateTableType(fragmentUpdateTableType);
  const [
    createTableType,
    {
      data: createTableTypeData,
      loading: createTableTypeLoading,
      error: createTableTypeErrors,
    },
  ] = useCreateTableType(fragmentCreateTableType);

  useEffect(() => {
    if (updateTableTypeData && !updateTableTypeErrors) {
      toast.success(t("table.table-type.update-success"));
      onSuccess?.();
      onHandleClose();
    }
    if (createTableTypeData && !createTableTypeErrors) {
      toast.success(t("table.table-type.create-success"));
      onSuccess?.();
      onHandleClose();
    }
  }, [updateTableTypeData, createTableTypeData]);

  useEffect(() => {
    updateTableTypeErrors && toast.error(updateTableTypeErrors.message);
    createTableTypeErrors && toast.error(createTableTypeErrors.message);
  }, [updateTableTypeErrors, createTableTypeErrors]);

  useEffect(() => {
    if (editField) {
      setBranchSelected(editField?.branch);
      setInitialValues({
        name: editField?.name as string,
        maxGuest: editField?.maxGuest as number,
        quantity: editField?.quantity as number,
        branch: editField?.branch?._id as string,
      });
    }
  }, [editField]);

  const handleSubmit = (values: IFormValue) => {
    const input: TableTypeInput = {
      name: values.name,
      maxGuest: values?.maxGuest,
      quantity: values?.quantity,
      branch: branchSelected?._id as string,
    };
    if (editField) {
      updateTableType({
        variables: {
          id: editField._id as string,
          input: input,
        },
      });
      onSuccess?.();
    } else {
      createTableType({
        variables: {
          input: { ...input },
        },
      });
    }
  };

  const onHandleClose = () => {
    setOpen(false);
    onClose && onClose();
  };

  return (
    <>
      <ElementWrapper onClick={() => setOpen(!isOpen)}>
        {ButtonMenu}
      </ElementWrapper>
      <Dialog
        open={isOpen}
        onClose={onHandleClose}
        className="overflow-auto"
        size="md"
      >
        <TableCategoryDialogContainer>
          <DialogTitle>{title}</DialogTitle>
          <Formik
            enableReinitialize
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={handleSubmit}
          >
            <Form>
              <Input
                name={formKey("name")}
                label={t("table.table-type.name")}
                type="text"
                placeholder={t("table.table-type.enter-name")}
                required
              />
              <Input
                name={formKey("maxGuest")}
                label={t("table.table-type.max-of-seat")}
                type="number"
                placeholder={t("table.table-type.enter-max-of-seat")}
                required
              />
              <Input
                name={formKey("quantity")}
                label={t("table.table-type.quantity")}
                type="number"
                placeholder={t("table.table-type.please-enter-quantity")}
                required
              />
              <Select
                optionSelected={branchSelected}
                onSelect={values => setBranchSelected(values)}
                options={listDataBranch as Branch[]}
                name={formKey("branch")}
                label={t("table.table-type.branch")}
                placeholder={t("table.table-type.select-branch")}
              />
              <ButtonWrapper>
                <Button outline type="button" onClick={onHandleClose}>
                  {t("common.cancel")}
                </Button>
                <Button
                  type="submit"
                  primary
                  loading={updateTableTypeLoading || createTableTypeLoading}
                >
                  {t("common.accept")}
                </Button>
              </ButtonWrapper>
            </Form>
          </Formik>
        </TableCategoryDialogContainer>
      </Dialog>
    </>
  );
};

export default TableCategoryDialog;
