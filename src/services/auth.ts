import { GenFields, JwtPayload } from "apiCaller";

export const fragmentLogin: GenFields<JwtPayload> = [
  "accessToken",
  "refreshToken",

  { userId: ["id"] },
  {
    userInfo: [
      "_id",
      "provider",
      {
        role: ["_id", "name", "permissions"],
      },
      "email",
      "displayName",
      "phoneNumber",
      {
        branches: [
          "_id",
          "code",
          "name",
          "phoneNumber",

          { images: ["default", "medium", "small"] },
          "maxGuest",
          "introduction",
          "status",
          {
            operationTimes: ["day", { times: ["openingHours", "closeHours"] }],
          },
          {
            location: [
              { state: ["name"] },
              { city: ["_id", "name"] },
              { district: ["_id", "name"] },
            ],
          },
          "isMainBranch",
          { serviceType: ["_id", "name"] },
          { businessProductTypes: ["_id", "name"] },
        ],
      },
    ],
  },
];
