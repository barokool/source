import styled from "styled-components/macro";
import tw from "twin.macro";

export const OrderDetailContainer = styled.div`
  ${tw`  `}
`;

export const Box = styled.div`
  ${tw`border-b border-neutral-4 border-dashed p-2`}
`;
export const CustomerInfoBox = styled(Box)`
  ${tw`  `}
`;
export const TableBookingInfoBox = styled(Box)`
  ${tw`  `}
`;
export const OrderInfoBox = styled(Box)`
  ${tw`  `}
`;
export const Overview = styled(Box)`
  ${tw`flex justify-between items-center`}
`;
export const OrderTotal = styled.p`
  ${tw`text-16 font-semibold float-left desktop:mr-2 mr-1`}
`;
export const DeliveryDriver = styled.p`
  ${tw``}
`;
export const DeliveryDriverLabel = styled.p`
  ${tw`text-14 text-body-color`}
`;
export const DeliveryDriverValue = styled.p`
  ${tw``}
`;
export const Title = styled.h4`
  ${tw`text-20 font-semibold pb-3`}
`;
export const Line = styled.div`
  ${tw`flex justify-between`}
`;
export const Text = styled.p`
  ${tw`text-14`}
`;
export const TextBold = styled.p`
  ${tw`text-14 font-semibold`}
`;
export const ProductList = styled.div`
  ${tw`mt-[-20px] mb-2`}
`;
export const ProductBox = styled.div`
  ${tw`py-2`}
  ${tw`border-b border-neutral-4 border-dashed`}
`;
export const SumBox = styled(Box)`
  ${tw` flex justify-between `}
`;
export const SumLabel = styled.div`
  ${tw`text-20 font-semibold `}
`;
export const SumValue = styled.div`
  ${tw`text-20 `}
`;

export default {
  Box,
  CustomerInfoBox,
  TableBookingInfoBox,
  OrderInfoBox,
  Overview,
  OrderTotal,
  DeliveryDriver,
  DeliveryDriverLabel,
  DeliveryDriverValue,
  Title,
  Line,
  Text,
  TextBold,
  ProductList,
  ProductBox,
  SumBox,
  SumLabel,
  SumValue,
};
