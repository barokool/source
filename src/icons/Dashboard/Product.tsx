/**
 * @TODO
 *  [ ] convert svg to jsx - Come here: https://magic.reactjs.net/htmltojsx.html
 *  [ ] replace all fill="..." to  fill="currentColor" (except fill=none)
 *  [ ] add {...props} to <svg >
 */

import { IIconSVGProps } from "typings";

const ComponentIcon: React.FC<IIconSVGProps> = props => (
  <svg
    width="20"
    height="21"
    viewBox="0 0 20 21"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <g clip-path="url(#clip0_1644_16740)">
      <path
        d="M13.4333 2.10757L4.3575 6.75424L0.75 4.97924L9.66583 0.576738C9.86583 0.475072 10.1058 0.475072 10.3158 0.576738L13.4333 2.10757Z"
        fill="currentColor"
      />
      <path
        d="M19.24 4.97912L10.005 9.55412L6.54663 7.84995L6.04663 7.59578L15.1325 2.94995L15.6325 3.20328L19.24 4.97912Z"
        fill="currentColor"
      />
      <path
        d="M9.265 10.8724L9.255 20.4999L0.41 15.884C0.16 15.7524 0 15.4882 0 15.204V6.29736L3.74833 8.14403V11.3907C3.74833 11.8065 4.08833 12.1515 4.49833 12.1515C4.90833 12.1515 5.24833 11.8065 5.24833 11.3907V8.89486L5.74833 9.1382L9.265 10.8724Z"
        fill="currentColor"
      />
      <path
        d="M19.9899 6.30762L10.7649 10.8626L10.7549 20.4901L19.9999 15.6626L19.9899 6.30762Z"
        fill="currentColor"
      />
    </g>
    <defs>
      <clipPath id="clip0_1644_16740">
        <rect
          width="20"
          height="20"
          fill="white"
          transform="translate(0 0.5)"
        />
      </clipPath>
    </defs>
  </svg>
);

export default ComponentIcon;
