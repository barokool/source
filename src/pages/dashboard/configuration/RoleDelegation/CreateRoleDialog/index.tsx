import React, { useEffect, useState } from "react";
import { Formik } from "formik";
import * as yup from "yup";
import { t } from "language";
import { toast } from "react-toastify";

import {
  ElementWrapper,
  CreateRoleDialogContainer,
  Form,
  FormGroup,
  Title,
  Button,
  ButtonWrapper,
} from "./styles";

import Input from "designs/InputV2";
import Select from "designs/Select";
import Checkbox from "designs/Checkbox";
import Dialog from "components/Dialog";
import { DialogTitle } from "common/styles/Title";

import {
  Permission,
  Role,
  RoleEnum,
  useCreateRole,
  useUpdateRole,
} from "apiCaller";

import { fragmentCreateRole, fragmentUpdateRole } from "services/role";

interface ICreateRoleDialogProps {
  ButtonMenu?: React.ReactElement;
  editField?: Role;
  title?: string;
  open?: boolean;
  onClose?: () => void;
  onSuccess?: () => void;
}

type IFormGroupId =
  | "printed-form"
  | "user"
  | "branch"
  | "report"
  | "booking"
  | "bill";

interface IValue {
  name: string;
  value: Permission;
}

interface IDataRole {
  id: IFormGroupId;
  title: string;
  data: IValue[];
}

type IFormValue = { [key: string]: any } & {
  [key in IFormGroupId]?: IValue[];
} & { position: string; role: string };

const validationSchema = yup
  .object()
  .shape<{ [key in keyof IFormValue]: any }>({
    role: yup.string().required("Please select role"),
    position: yup.string().required("Please enter position"),
  });

const CreateRoleDialog: React.FC<ICreateRoleDialogProps> = ({
  ButtonMenu,
  editField,
  title,
  onSuccess,
  open = false,
  onClose,
}) => {
  const [loading, setLoading] = useState(false);
  const [initialValues, setInitialValues] = useState<IFormValue>({
    position: "",
    role: "",
  });
  const [roleSelected, setRoleSelected] = useState<Role | null>(null);
  const [isOpen, setOpen] = useState<boolean>(open);
  const [updateRole, { data: updateData, error: updateError }] =
    useUpdateRole(fragmentUpdateRole);
  const [createRole, { data: createData, error: createError }] =
    useCreateRole(fragmentCreateRole);
  const [listPermisstionsSelected, setListPermisstionsSelected] = useState<
    string[]
  >([]);

  useEffect(() => {
    if (updateData && !updateError) {
      toast.success(t("common.edit-success"));
      onSuccess && onSuccess();
      handleClose();
    }
    if (!updateData && updateError) {
      toast.success(t("common.edit-fail"));
      handleClose();
    }
  }, [updateData, updateError]);

  useEffect(() => {
    if (createData && !createError) {
      toast.success(t("common.create-success"));
      onSuccess && onSuccess();
      handleClose();
    }
    if (!createData && createError) {
      toast.success(t("common.create-fail"));
      handleClose();
    }
  }, [createData, createError]);

  useEffect(() => {
    if (editField) {
      setRoleSelected({
        _id: editField._id as string,
        name: editField.name as RoleEnum,
        permissions: editField?.permissions || [],
        position: editField?.position as string,
      });
      setInitialValues({
        role: editField?._id as string,
        position: editField?.position as string,
      });
      setListPermisstionsSelected(editField.permissions || []);
    }
  }, [editField]);

  const handleClose = () => {
    setOpen(false);
    onClose && onClose();
  };

  const handleSubmit = (values: IFormValue) => {
    const input = {
      position: values.position as string,
      name: values?.role as RoleEnum,
      permissions: listPermisstionsSelected as Permission[],
    };

    try {
      setLoading(true);
      if (editField) {
        updateRole({
          variables: {
            id: editField._id as string,
            roleInput: input,
          },
        });
        onSuccess?.();
        return;
      }
      createRole({
        variables: {
          roleInput: input,
        },
      });
      onSuccess?.();
    } catch (error) {
    } finally {
      setLoading(false);
    }
  };

  return (
    <>
      <ElementWrapper
        onClick={() => setOpen(!isOpen)}
        className="flex justify-end w-full"
      >
        {ButtonMenu}
      </ElementWrapper>
      <Dialog open={isOpen} onClose={handleClose} size="lg">
        <CreateRoleDialogContainer>
          <DialogTitle>{title}</DialogTitle>
          <Formik
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={handleSubmit}
            enableReinitialize
          >
            <Form>
              <FormGroup>
                <ElementWrapper className="grid grid-cols-2 gap-2">
                  <Input
                    name="position"
                    label={t("configuration.role-delegation.position")}
                    placeholder="Enter position name"
                    required
                  />
                  <Select
                    name="role"
                    label={t("configuration.role-delegation.role")}
                    placeholder="Select role"
                    options={roleEnum}
                    onSelect={role => setRoleSelected(role)}
                    optionSelected={roleSelected}
                    formTarget="name"
                  />
                </ElementWrapper>
                <ElementWrapper>
                  <Title>{t("configuration.role-delegation.permission")}</Title>
                  <ElementWrapper className="flex flex-wrap gap-2 mobile:justify-between">
                    {initDataRole.map(({ id, title, data }, index) => (
                      <div key={id}>
                        <h4 className="mb-1 text-left text-16 font-medium">
                          {title}
                        </h4>
                        <div className="space-y-1">
                          {data.map(row => {
                            const { name, value } = row;
                            return (
                              <Checkbox
                                key={value}
                                content={name}
                                isChecked={listPermisstionsSelected.some(
                                  item => item === value,
                                )}
                                onChange={checked => {
                                  if (checked)
                                    setListPermisstionsSelected([
                                      ...listPermisstionsSelected,
                                      value,
                                    ]);
                                  else
                                    setListPermisstionsSelected(
                                      listPermisstionsSelected.filter(
                                        item => item !== value,
                                      ),
                                    );
                                }}
                              />
                            );
                          })}
                        </div>
                      </div>
                    ))}
                  </ElementWrapper>
                </ElementWrapper>
              </FormGroup>
              <ButtonWrapper>
                <Button outline type="button" onClick={() => setOpen(false)}>
                  {t("common.cancel")}
                </Button>
                <Button type="submit" primary loading={loading}>
                  {t("common.accept")}
                </Button>
              </ButtonWrapper>
            </Form>
          </Formik>
        </CreateRoleDialogContainer>
      </Dialog>
    </>
  );
};

export default CreateRoleDialog;
const roleEnum: Role[] = [
  {
    _id: "admin",
    name: RoleEnum.Admin,
    permissions: [],
    position: "",
  },
  {
    _id: "manager",
    name: RoleEnum.Manager,
    permissions: [],
    position: "",
  },
  {
    _id: "customer",
    name: RoleEnum.Customer,
    permissions: [],
    position: "",
  },
  {
    _id: "driver",
    name: RoleEnum.Driver,
    permissions: [],
    position: "",
  },
  {
    _id: "staff",
    name: RoleEnum.Staff,
    permissions: [],
    position: "",
  },
];
const initDataRole: IDataRole[] = [
  {
    id: "printed-form",
    title: "Mẫu in",
    data: [
      {
        name: t("configuration.role-delegation.read-list"),
        value: Permission.Read_printer,
      },
      {
        name: t("configuration.role-delegation.update-list"),
        value: Permission.Update_printer,
      },
    ],
  },
  {
    id: "user",
    title: "Người dùng",
    data: [
      {
        name: t("configuration.role-delegation.read-list"),
        value: Permission.Read_user,
      },
      {
        name: t("configuration.role-delegation.add-new-to-list"),
        value: Permission.Create_user,
      },
      {
        name: t("configuration.role-delegation.update-list"),
        value: Permission.Update_user,
      },
      {
        name: t("configuration.role-delegation.delete"),
        value: Permission.Delete_user,
      },
    ],
  },
  {
    id: "branch",
    title: "Chi nhánh",
    data: [
      {
        name: t("configuration.role-delegation.read-list"),
        value: Permission.Read_restaurant,
      },
      {
        name: t("configuration.role-delegation.add-new-to-list"),
        value: Permission.Create_restaurant,
      },
      {
        name: t("configuration.role-delegation.update-list"),
        value: Permission.Update_restaurant,
      },
      {
        name: t("configuration.role-delegation.delete"),
        value: Permission.Delete_restaurant,
      },
    ],
  },
  {
    id: "report",
    title: t("configuration.role-delegation.report"),
    data: [
      {
        name: t("configuration.role-delegation.report-sales"),
        value: Permission.Report_sales,
      },
      {
        name: t("configuration.role-delegation.report-stock"),
        value: Permission.Report_stock,
      },
      {
        name: t("configuration.role-delegation.report-user"),
        value: Permission.Report_user,
      },
      {
        name: t("configuration.role-delegation.report-staff"),
        value: Permission.Report_staff,
      },
    ],
  },
  {
    id: "booking",
    title: "Đặt bàn",
    data: [
      {
        name: t("configuration.role-delegation.read-list"),
        value: Permission.Read_table,
      },
      {
        name: t("configuration.role-delegation.add-new-to-list"),
        value: Permission.Create_table,
      },
      {
        name: t("configuration.role-delegation.update-list"),
        value: Permission.Update_table,
      },
      {
        name: t("configuration.role-delegation.delete"),
        value: Permission.Delete_table,
      },
    ],
  },
  {
    id: "bill",
    title: "Hóa đơn",
    data: [
      {
        name: t("configuration.role-delegation.read-list"),
        value: Permission.Read_bill,
      },
      {
        name: t("configuration.role-delegation.add-new-to-list"),
        value: Permission.Create_bill,
      },
      {
        name: "Update list bill",
        value: Permission.Update_bill,
      },
      {
        name: t("configuration.role-delegation.delete"),
        value: Permission.Delete_bill,
      },
      {
        name: t("configuration.role-delegation.detach-order"),
        value: Permission.Detach_order,
      },
      {
        name: t("configuration.role-delegation.attach-order"),
        value: Permission.Attach_order,
      },
      {
        name: t("configuration.role-delegation.change-price"),
        value: Permission.Change_price,
      },
    ],
  },
];
