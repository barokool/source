import ReactPaginate from "react-paginate";
import { LabelWrapper, Icon } from "./styles";

interface IPaginationProps {
  // count from 0
  page?: number;
  sizePerPage: number;
  totalSize: number;
  displayRanges?: number;
  marginPage?: number;
  onPageChange: (page: number) => void;
  className?: string;
}

const Pagination: React.FC<IPaginationProps> = props => {
  const {
    page: initPage,
    sizePerPage = 1,
    totalSize = 0,
    displayRanges = 5,
    marginPage = 1,
    onPageChange,
    className = "",
  } = props;
  const handlePageChange = (value: number) => {
    onPageChange && onPageChange(value);
  };
  const pageCount = Math.ceil(totalSize / sizePerPage);
  return (
    <ReactPaginate
      forcePage={initPage}
      pageCount={pageCount}
      pageRangeDisplayed={displayRanges}
      marginPagesDisplayed={marginPage}
      onPageChange={value => handlePageChange(value.selected)}
      containerClassName={`flex items-center text-md phone:text-lg text-black ${className}`}
      previousLabel={
        <LabelWrapper>
          <Icon name="common/table-prev" width={24} height={24} />
        </LabelWrapper>
      }
      nextLabel={
        <LabelWrapper>
          <Icon name="common/table-next" width={24} height={24} />
        </LabelWrapper>
      }
      previousLinkClassName="outline-none focus:outline-none select-none cursor-pointer"
      nextLinkClassName="outline-none focus:outline-none select-none cursor-pointer"
      pageLinkClassName="flex items-center justify-center mx-0.5 w-3 h-3 phone:w-4 phone:h-4 rounded-sm focus:outline-none select-none cursor-pointer"
      activeLinkClassName="bg-black text-white outline-none focus:outline-none select-none cursor-pointer"
    />
  );
};

export default Pagination;
