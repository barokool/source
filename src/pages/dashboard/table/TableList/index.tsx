import { useState, useCallback, useMemo, useEffect } from "react";
import { toast } from "react-toastify";
import { PATH } from "common/constants/routes";
import { getQueryFromLocation } from "common/functions/route";
import { RouteComponentProps } from "react-router";
import TableListComponent, { IColumns } from "designs/Table";
import ActionButtons from "designs/ActionButton";
import Empty from "components/Empty";
import ListDataLayout from "layouts/ListDataLayout";
import EmptyImage from "assets/images/product/ProductListEmpty.png";
import TableStatusTag from "components/TableStatusTag";

//hooks
import { usePage } from "hooks/usePage";
import { useBreadcrumb } from "hooks/useBreadcrumb";
import { useCurrentBranch } from "hooks/useCurrentBranch";
import { useLoading } from "hooks/useLoading";
import { useDebouncedState } from "hooks/useDebouncedState";

//languages
import i18n, { t } from "language";

//api
import {
  Table,
  useGetAllTable,
  useDeleteTable,
  Branch,
  TableStatus,
} from "apiCaller";
import { fragmentGetAllTable } from "services/table";

//locals
import {
  AddButton,
  SearchBox,
  TableListContainer,
  Avatar,
  Title,
} from "./styles";
import TableListDialog from "./TableListDialog";

const SIZE_PER_PAGE = 10;
const LOAD_DATA = "LOAD_DATA";
const DEBOUNCE_TIME = 1000;

interface ISearchDependencies {
  branchSelected: Branch | null;
  text: string;
}

interface ITableListProps extends RouteComponentProps {}

const TableList: React.FC<ITableListProps> = ({ location }) => {
  const { branchSelected } = useCurrentBranch();
  const { startLoading, stopLoading } = useLoading();

  //local state
  const [listTable, setListTable] = useState<Table[]>([]);
  const [page, setPage] = usePage(getQueryFromLocation(location)?.page);
  const [totalSize, setTotalSize] = useState(0);
  const [actionSuccess, setActionSuccess] = useState<boolean>(false);
  const [search, setSearch] = useDebouncedState<ISearchDependencies>(
    {
      branchSelected: null,
      text: "",
    },
    objectSearch => {
      if (objectSearch.branchSelected) {
        invokeGetAllDataAPI(objectSearch.text, objectSearch.branchSelected);
      }
    },
    DEBOUNCE_TIME,
  );

  //api
  const [
    getAllTable,
    {
      data: getAllTableData,
      error: getAllTableErrors,
      loading: getAllTableLoading,
    },
  ] = useGetAllTable(fragmentGetAllTable);
  const [
    deleteTable,
    {
      loading: deleteTableLoading,
      error: deleteTableErrors,
      data: deleteTableData,
    },
  ] = useDeleteTable();

  useBreadcrumb([
    {
      name: t("table.table-management"),
    },
    {
      name: t("table.table-list.table-list"),
      href: PATH.TABLE_MANAGEMENT.TYPE,
    },
  ]);

  useEffect(() => {
    branchSelected && invokeGetAllDataAPI("", branchSelected);
  }, [page, branchSelected]);

  useEffect(() => {
    if (actionSuccess === true && branchSelected) {
      invokeGetAllDataAPI("", branchSelected);
    }
    setActionSuccess(false);
  }, [actionSuccess]);

  useEffect(() => {
    if (getAllTableLoading || deleteTableLoading) {
      startLoading(LOAD_DATA);
    } else {
      stopLoading(LOAD_DATA);
    }
  }, [getAllTableLoading, deleteTableLoading]);

  useEffect(() => {
    branchSelected &&
      setSearch({
        ...search,
        branchSelected,
      });
  }, [branchSelected]);

  useEffect(() => {
    if (getAllTableData) {
      setListTable(getAllTableData.getAllTable.results || []);
      setTotalSize(getAllTableData.getAllTable.totalCount as number);
    }
  }, [getAllTableData]);

  //toast errors when they occur
  useEffect(() => {
    getAllTableErrors && toast.error(getAllTableErrors.message);
    deleteTableErrors && toast.error(deleteTableErrors.message);
  }, [getAllTableErrors, deleteTableErrors]);

  useEffect(() => {
    if (deleteTableData && !deleteTableErrors) {
      toast.success(t("table.table-list.delete-success"));
      setActionSuccess(true);
    }
  }, [deleteTableData]);

  //get table types filter on branch and name
  const invokeGetAllDataAPI = (searchText: string = "", branch: Branch) => {
    getAllTable({
      variables: {
        page: page - 1,
        size: SIZE_PER_PAGE,
        // filter: {
        //   code: Number(searchText),
        // },
      },
    });
  };

  const onPageChange = useCallback((nextPage: number) => {
    setPage(nextPage);
  }, []);

  const renderAction = (record: Table) => {
    return (
      <ActionButtons
        buttons={{
          edit: {
            DialogContent: ({ onClose }) => {
              return (
                <TableListDialog
                  onClose={onClose}
                  open
                  editField={record}
                  title={t("table.table-list.edit")}
                  onSuccess={() => setActionSuccess(true)}
                />
              );
            },
          },
          delete: {
            title: t("table.table-list.delete"),
            message: t("table.table-list.confirm-delete"),
            onDelete: () =>
              deleteTable({
                variables: { listId: [record._id as string] },
              }),
          },
        }}
      />
    );
  };

  const columns: IColumns = useMemo(
    () => [
      {
        text: t("table.table-type.name"),
        dataField: "code",
        formatter: (code: number) => (
          <Title>{t("table.table-list.table") + " " + code}</Title>
        ),
      },
      {
        text: t("table.table-list.table-status.status"),
        dataField: "status",
        formatter: (_: string, record: Table) => (
          <TableStatusTag active={record.status as TableStatus}>
            {record.status === "empty" &&
              t("table.table-list.table-status.empty")}
            {record.status === "using" &&
              t("table.table-list.table-status.using")}
            {record.status === "waiting" &&
              t("table.table-list.table-status.waiting")}
            {record.status === "ordered" &&
              t("table.table-list.table-status.ordered")}
            {record.status === "available" && "Available"}
          </TableStatusTag>
        ),
      },
      {
        text: t("table.table-list.actions"),
        dataField: "actions",
        formatter: (_, record: Table) => renderAction(record),
      },
    ],
    [page, i18n.language, search],
  );

  return totalSize === 0 && !search.text ? (
    <Empty
      SVG={<img src={EmptyImage} width={200} height={200} />}
      Title={t("table.table-list.empty.title")}
      SubTitle={t("table.table-list.empty.message")}
      ButtonMenu={
        <TableListDialog
          onSuccess={() => setActionSuccess(true)}
          title={t("table.table-list.add")}
          ButtonMenu={
            <AddButton primary className="mx-auto mt-2">
              {t("table.table-list.add")}
            </AddButton>
          }
        />
      }
    />
  ) : (
    <TableListContainer>
      <ListDataLayout
        rightTopBar={
          <TableListDialog
            onSuccess={() => setActionSuccess(true)}
            title={t("table.table-list.add")}
            ButtonMenu={
              <AddButton primary>{t("table.table-list.add")}</AddButton>
            }
          />
        }
        leftTopBar={
          <SearchBox
            onFetchData={text =>
              setSearch({
                ...search,
                text,
              })
            }
            placeholder={t("table.table-list.search-table")}
          />
        }
        title={t("table.table-list.table-list")}
      >
        <TableListComponent
          columns={columns}
          data={listTable || []}
          totalSize={totalSize}
          sizePerPage={SIZE_PER_PAGE}
          onPageChange={onPageChange}
          page={page}
          isRemote
        />
      </ListDataLayout>
    </TableListContainer>
  );
};
export default TableList;
