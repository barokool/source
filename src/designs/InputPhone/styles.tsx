import PhoneInput from "react-phone-number-input/input";
import styled from "styled-components";
import tw from "twin.macro";

import { formControlCommon } from "common/styles/FormControl";

export const InputPhoneContainer = styled.div`
  ${tw`flex`}
`;

export const Select = styled.div`
  ${tw`relative w-[135px] h-4 px-[15px] border rounded border-[#959494] text-neutral-1 
  cursor-pointer select-none flex-shrink-0`}
`;

export const Selected = {
  Wrapper: styled.div`
    ${tw`flex items-center h-full`}
  `,
  Image: styled.img`
    ${tw`block w-[24px] h-[17px] mr-auto pointer-events-none`}
  `,
  CountryCode: styled.span`
    ${tw`text-14 text-gray pointer-events-none`}
  `,
  SearchInput: styled.input`
    ${tw`block w-full outline-none border-none`}
  `,
};

export const OptionList = styled.ul`
  &::-webkit-scrollbar {
    width: 5px;
  }

  &::-webkit-scrollbar-track {
    background: #eee;
  }

  &::-webkit-scrollbar-thumb {
    ${tw`bg-primary`}
  }

  ${tw`absolute left-0 z-30 w-full max-h-30 py-1 mt-1 overflow-auto border rounded 
    shadow-as-border bg-white  focus:outline-none border-neutral-4`}
`;

export const Option = {
  Wrapper: styled.li`
    ${tw`flex items-center justify-between px-1.5 py-0.5 hover:bg-neutral-4`}
  `,
  Image: styled.img`
    ${tw`block w-[24px] h-[17px]`}
  `,
  CountryCode: styled.span`
    ${tw`text-14 text-gray`}
  `,
};

export const EmptyPlaceholderBox = styled.div`
  ${tw`w-full`}
`;

export const Image = styled.img`
  ${tw`block w-full`}
`;

export const InputPhoneStyled = {
  Wrapper: styled.div<{ isError: boolean }>`
    ${tw`w-full relative`}
    ${({ isError }) => (isError ? tw`text-danger` : tw`text-gray`)}
  `,
  LabelWrapper: styled.div`
    ${tw`px-[4px] py-0 flex items-center justify-between absolute -top-2 left-[20px] z-10 bg-white rounded-sm`}
  `,
};

export const PhoneInputStyled = styled(PhoneInput)<{ isError: boolean }>`
  ${tw`focus:outline-none w-full`}
  ${({ isError, disabled }) => formControlCommon(isError, disabled)}
`;

export const FieldWrapper = styled.div`
  ${tw`relative flex flex-row items-center w-full `}
`;
