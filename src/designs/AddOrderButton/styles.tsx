import styled from "styled-components/macro";
import tw from "twin.macro";

export const AddOrderButtonContainer = styled.div`
  ${tw`p-1 shadow-md flex cursor-pointer hover:shadow-xl h-12.5`}
`;
