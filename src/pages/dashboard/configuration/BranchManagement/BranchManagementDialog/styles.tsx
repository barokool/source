import tw from "twin.macro";
import styled from "styled-components/macro";
import { Form as _Form } from "formik";
import _Button from "designs/Button";
import _SearchBox from "components/SearchBoxTable";

export const ElementWrapper = styled.div`
  ${tw``}
`;

export const BranchManagementDialogContainer = styled.div`
  ${tw`bg-white p-1 phone:p-2 laptop:p-5`}
`;

export const Form = styled(_Form)`
  ${tw`space-y-1`}
`;

export const ButtonWrapper = styled.div`
  ${tw`flex justify-end mt-4`}
`;

export const Button = styled(_Button)`
  ${tw`py-1 px-2 w-max flex justify-center ml-1`}
`;

export const FormGroup = styled.div`
  ${tw``}
`;

export const Title = styled.p`
  ${tw`text-20 font-medium my-[24px] text-left`}
`;

export const MapSectionContainer = styled.div`
  ${tw`mt-10`}
`;

export const SearchBox = styled(_SearchBox)`
  ${tw``}
`;
