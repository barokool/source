import styled from "styled-components/macro";
import tw from "twin.macro";

export const OrderInfoContainer = styled.div`
  ${tw`w-full grid grid-cols-2 gap-x-2 p-2 bg-neutral-5 desktop:h-13`}
`;
