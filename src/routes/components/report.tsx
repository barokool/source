import { IRoutes } from "typings";
import { PATH } from "common/constants/routes";
import { lazy } from "react";
import { t } from "language";

import ReportIcon from "icons/Dashboard/Report";

const ReportPage = lazy(() => import("pages/dashboard/report"));
const ProductReport = lazy(
  () => import("pages/dashboard/report/ProductReport"),
);
const CustomerReport = lazy(
  () => import("pages/dashboard/report/CustomerReport"),
);
const SellingReport = lazy(
  () => import("pages/dashboard/report/SellingReport"),
);
const StaffReport = lazy(() => import("pages/dashboard/report/StaffReport"));

export const sellingReportRoutes = {
  name: t("report.selling-report.selling-report"),
  path: PATH.REPORT.SELLING_REPORT,
  isPrivate: true,
  Component: SellingReport,
  exact: true,
  hiddenRoute: true,
};

export const productReportRoutes = {
  name: t("report.product-report.product-report"),
  path: PATH.REPORT.PRODUCT_REPORT,
  isPrivate: true,
  Component: ProductReport,
  exact: true,
  hiddenRoute: true,
};

export const customerReportRoutes = {
  name: t("report.customer-report.customer-report"),
  path: PATH.REPORT.CUSTOMERS_REPORT,
  isPrivate: true,
  Component: CustomerReport,
  exact: true,
  hiddenRoute: true,
};

export const staffReportRoutes = {
  name: t("report.staffs-report.staffs-report"),
  path: PATH.REPORT.STAFF_REPORT,
  isPrivate: true,
  Component: StaffReport,
  exact: true,
  hiddenRoute: true,
};

export const reportRoutes: IRoutes = {
  name: t("report.report"),
  path: PATH.REPORT.SELF,
  exact: true,
  isPrivate: true,
  Icon: ReportIcon,
  Component: ReportPage,
};
