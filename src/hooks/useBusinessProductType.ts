import { useState, useCallback, useEffect } from "react";
import { BusinessProductType, useGetAllBusinessProductType } from "apiCaller";
import { fragmentGetAllBusinessProductType } from "services/businessProductType";

export const useBusinessProductType = () => {
  const [getAllBusinessProductType, { data, loading, called }] =
    useGetAllBusinessProductType(fragmentGetAllBusinessProductType);

  useEffect(() => {
    invokeGetAllBusinessProductType();
  }, []);

  const invokeGetAllBusinessProductType = useCallback(() => {
    getAllBusinessProductType({ variables: {} });
  }, []);

  return {
    productType: data,
    invokeGetAllBusinessProductType,
  };
};
