import { formControlCommon } from "common/styles/FormControl";
import styled from "styled-components/macro";
import tw from "twin.macro";

export const Container = styled.div`
  ${tw`w-full`}
`;

export const DateInputContainer = styled.label<{
  isError: boolean;
  disabled?: boolean;
}>`
  ${tw`grid items-center`}
  ${({ isError, disabled }) => formControlCommon(isError, disabled)}
  ${({ isError }) => isError && tw`text-black`}
  grid-template-columns: 1fr 24px;
`;

export const InputField = styled.input`
  ${tw`p-0 w-min font-medium text-secondary text-20 placeholder-neutral-3 focus:outline-none focus:ring-transparent`}
`;
