import React, { useState, useEffect } from "react";
import {
  OrderResponseDialogContainer,
  ElementWrapper,
  Button,
  Label,
  Description,
} from "./styles";

import Dialog from "components/Dialog";
import IconButton from "designs/IconButton";

interface OrderResponseDialogProps {
  ButtonMenu: React.ReactElement;
  icon?: React.ReactElement;
  onClose?: () => void;
  onSuccess?: () => void;
}

export type OrderResponseStatus = "failed" | "success";

const OrderResponseDialog: React.FC<OrderResponseDialogProps> = ({
  ButtonMenu,
  onClose,
  icon,
  onSuccess,
}) => {
  const [isOpen, setOpen] = useState<boolean>(false);
  return (
    <>
      <ElementWrapper
        className="flex justify-end w-full"
        onClick={() => setOpen(!isOpen)}
      >
        {ButtonMenu}
      </ElementWrapper>

      <Dialog
        open={isOpen}
        onClose={() => setOpen(false)}
        className="overflow-auto "
        size="md"
      >
        <OrderResponseDialogContainer>
          {icon}
          <Label>Bạn chờ chút nhé</Label>
          <Description>
            Nhân viên cửa hàng sẽ tới và xác nhận lại với bạn
          </Description>
          <Button onClick={() => setOpen(false)}>Đồng ý</Button>
        </OrderResponseDialogContainer>
      </Dialog>
    </>
  );
};

export default OrderResponseDialog;
