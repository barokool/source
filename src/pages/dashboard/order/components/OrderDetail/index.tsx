import { Order, OrderItem, PaymentTypeEnum, useGetOrderById } from "apiCaller";
import { t } from "language";
import { useEffect } from "react";
import Styles, { OrderDetailContainer } from "./styles";

interface OrderDetailProps {
  order: Order | null;
}
const OrderDetail: React.FC<OrderDetailProps> = props => {
  const { order } = props;
  let data = order;

  const CustomerInfo = () => (
    <Styles.CustomerInfoBox>
      <Styles.Title>{t("order.common.customer-information")}</Styles.Title>
      <Styles.Line>
        <Styles.Text>{t("order.common.fullname")}</Styles.Text>
        <Styles.Text>{data?.customerName || t("common.unknown")}</Styles.Text>
      </Styles.Line>
      <Styles.Line>
        <Styles.Text>{t("order.common.phone")}</Styles.Text>
        <Styles.Text>{data?.phoneNumber || t("common.unknown")}</Styles.Text>
      </Styles.Line>
    </Styles.CustomerInfoBox>
  );

  const Product: React.FC<{ orderItem?: OrderItem }> = ({ orderItem }) => {
    return (
      <Styles.ProductBox>
        <Styles.Line>
          <Styles.Text>
            {orderItem?.quantity}x {orderItem?.product?.name}
          </Styles.Text>
        </Styles.Line>
        <Styles.Line>
          <Styles.Text>{t("order.common.size")}</Styles.Text>
          <Styles.Text>{orderItem?.unit?.name}</Styles.Text>
        </Styles.Line>
        <Styles.Line>
          <Styles.Text>{t("order.common.toppings")}</Styles.Text>
          <Styles.Text>
            {orderItem?.toppings?.map((item, index) => (
              <>
                <span key={index}>{item.name + " "}</span>
                {item.quantity !== 0 && <span>x{item.quantity}</span>},{" "}
              </>
            ))}
          </Styles.Text>
        </Styles.Line>
      </Styles.ProductBox>
    );
  };

  const TableBookingInfo = () => (
    <Styles.TableBookingInfoBox>
      <Styles.Title>
        {t("order.common.table-reservation-information")}
      </Styles.Title>
      <Styles.Line>
        <Styles.Text>{t("order.common.guest-number")}</Styles.Text>
        <Styles.Text>{data?.phoneNumber || t("common.unknown")}</Styles.Text>
      </Styles.Line>
      <Styles.Line>
        <Styles.Text>{t("order.common.booking-date")}</Styles.Text>
        <Styles.Text>
          {data?.table?.bookingDate || t("common.unknown")}
        </Styles.Text>
      </Styles.Line>
      <Styles.Line>
        <Styles.Text>{t("order.common.table-type")}</Styles.Text>
        <Styles.Text>
          {data?.table?.tableType?.name || t("common.unknown")}
        </Styles.Text>
      </Styles.Line>
    </Styles.TableBookingInfoBox>
  );

  const OrderInfo = () => (
    <Styles.OrderInfoBox>
      <Styles.Title>{t("order.common.order-information")}</Styles.Title>
      <Styles.ProductList>
        {data?.orderItems?.map((item, index) => (
          <Product orderItem={item} key={index} />
        ))}
      </Styles.ProductList>
      <Styles.Line>
        <Styles.TextBold>{t("order.common.tax")}</Styles.TextBold>
        <Styles.Text>{data?.price?.tax || 0}</Styles.Text>
      </Styles.Line>
      <Styles.Line>
        <Styles.TextBold>{t("order.common.discount")}</Styles.TextBold>
        <Styles.Text>{data?.price?.discount || 0}</Styles.Text>
      </Styles.Line>
      <Styles.Line>
        <Styles.TextBold>{t("order.common.before-discount")}</Styles.TextBold>
        <Styles.Text>{data?.price?.beforeDiscount || 0}</Styles.Text>
      </Styles.Line>
      <Styles.Line>
        <Styles.TextBold>{t("order.common.shipping-fee")}</Styles.TextBold>
        <Styles.Text>{data?.price?.shippingFee || 0}</Styles.Text>
      </Styles.Line>
      <Styles.Line>
        <Styles.TextBold>{t("order.common.payments-type")}</Styles.TextBold>
        <Styles.Text>
          {data?.paymentType === PaymentTypeEnum.Directly &&
            t("order.payment-type.directly")}
          {data?.paymentType === PaymentTypeEnum.Online &&
            t("order.payment-type.online")}
          {data?.paymentType === null && t("common.unknown")}
        </Styles.Text>
      </Styles.Line>
    </Styles.OrderInfoBox>
  );

  const Sum = () => (
    <Styles.SumBox>
      <Styles.SumLabel>{t("order.common.total-bill")}</Styles.SumLabel>
      <Styles.SumValue>
        {data?.price?.finalPrice?.toString() || 0}
      </Styles.SumValue>
    </Styles.SumBox>
  );

  return (
    <OrderDetailContainer>
      <Styles.Overview>
        <div>
          <Styles.OrderTotal>{t("order.common.total-bill")}</Styles.OrderTotal>
          <Styles.OrderTotal>
            {data?.price?.finalPrice?.toString() || " 0"}
          </Styles.OrderTotal>
        </div>
        <Styles.DeliveryDriver>
          <Styles.DeliveryDriverLabel>
            {t("order.common.delivery-driver")}
          </Styles.DeliveryDriverLabel>
          <Styles.DeliveryDriverValue>
            {data?.driver?.displayName || t("common.unknown")}
          </Styles.DeliveryDriverValue>
        </Styles.DeliveryDriver>
      </Styles.Overview>
      <CustomerInfo />
      <TableBookingInfo />
      <OrderInfo />
      <Sum />
    </OrderDetailContainer>
  );
};

export default OrderDetail;
