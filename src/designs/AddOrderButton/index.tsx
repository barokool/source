import { ButtonHTMLAttributes, DetailedHTMLProps, useRef } from "react";

import { AddOrderButtonContainer } from "./styles";

interface IAddOrderButtonProps {
  onClick: () => void;
}

import SVG from "designs/SVG";

const AddOrderButton: React.FC<IAddOrderButtonProps> = props => {
  const { onClick } = props;
  return (
    <AddOrderButtonContainer onClick={onClick}>
      <SVG
        name="selling/order-plus"
        className="mx-auto my-auto justify-center "
      />
    </AddOrderButtonContainer>
  );
};

export default AddOrderButton;
