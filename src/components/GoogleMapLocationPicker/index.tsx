import { withScriptjs, withGoogleMap, GoogleMap } from "react-google-maps";
import { useEffect, useRef, useState } from "react";
import SVG from "designs/SVG";
import { IGoogleMapsProps, ILatLng } from "./interfaces";
import { useDebouncedState } from "hooks/useDebouncedState";

const key = "";
const GOOGLE_MAP_URL = `https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=${key}`;

const GoogleMapPicker = withScriptjs(
  withGoogleMap((props: any) => {
    const { center, defaultZoom, onChange } = props;
    const mapRef = useRef<GoogleMap | null>();

    useEffect(() => {
      handleCenterChanged();
    }, []);

    const handleCenterChanged = () => {
      const centerPos = mapRef?.current?.getCenter();
      const centerPoint: ILatLng = {
        lat: centerPos?.lat(),
        lng: centerPos?.lng(),
      };

      onChange && onChange(centerPoint);
    };
    return (
      <GoogleMap
        ref={ref => (mapRef.current = ref)}
        defaultZoom={defaultZoom}
        center={center}
        options={{
          styles: [
            {
              featureType: null,
              stylers: [{ visibility: "on" }],
            },
          ],
        }}
        onCenterChanged={handleCenterChanged}
      />
    );
  }),
);

const GoogleMapLocationPicker = (props: IGoogleMapsProps) => {
  const {
    center = {
      lat: 21.028511,
      lng: 105.804817,
    },
    zoom = 15,
    className = "",
    onChange,
  } = props;

  const [latlng, setLatlng] = useDebouncedState<ILatLng | null>(
    center,
    val => val && onChange?.(val),
  );

  // useEffect(() => {
  //   if (center) setLatlng(center);
  // }, [center]);

  return (
    <div className={`relative w-full h-full ${className}`}>
      <GoogleMapPicker
        googleMapURL={GOOGLE_MAP_URL}
        loadingElement={<div style={{ height: `100%` }} />}
        containerElement={<div style={{ height: `100%` }} />}
        mapElement={<div style={{ height: `100%` }} />}
        defaultZoom={zoom}
        center={latlng}
        onChange={setLatlng}
      />
      <SVG
        name="common/google-map-location-picker"
        className="absolute transform -translate-x-1/2 -translate-y-full CENTER-POINT top-1/2 left-1/2 z-99"
      />
    </div>
  );
};

export default GoogleMapLocationPicker;
