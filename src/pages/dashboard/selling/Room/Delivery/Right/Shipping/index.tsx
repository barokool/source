import React, { useEffect, useState } from "react";

import OrderShippingImage from "assets/images/selling/order-shipping.png";
import { toast } from "react-toastify";

//redux
import { IRootState } from "typings";
import { useSelector, useDispatch } from "react-redux";
import { setActionSuccess } from "redux/slices/common";

//api
import { Order, useGetOrderById } from "apiCaller";
import { fragmentGetOrderById } from "services/order";
import { setOrderSelected } from "redux/slices/order";

//languages
import { t } from "language";

//locals
import {
  RightShippingContainer,
  Content,
  Image,
  RightShippingWrapper,
  TrackingButton,
} from "./styles";

interface IRightProcessing {}

const RightProcessing: React.FC<IRightProcessing> = props => {
  const dispatch = useDispatch();
  const [order, setOrder] = useState<Order | null>(null);
  const [getOrderById] = useGetOrderById(fragmentGetOrderById);

  useEffect(() => {
    if (order) {
      dispatch(setOrderSelected(order));
    }
  }, [order]);

  return (
    <RightShippingContainer>
      <RightShippingWrapper>
        <Image src={OrderShippingImage} alt="order-processing" />
        <Content.Wrapper>
          <Content.Title>{t("selling.order-in-shipping")}</Content.Title>
          <Content.Description>
            {t("selling.order-in-shipping-message")}
          </Content.Description>
        </Content.Wrapper>

        <TrackingButton>{t("selling.watch-driver-location")}</TrackingButton>
      </RightShippingWrapper>
    </RightShippingContainer>
  );
};

export default RightProcessing;

//Something is missing in this components, we wil fix it later
