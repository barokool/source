import { getCountries, getCountryCallingCode } from "react-phone-number-input";
import { CountryCode } from "libphonenumber-js";
import DropdownArrowIcon from "icons/Arrows/SelectArrow";

import {
  ChangeEvent,
  DetailedHTMLProps,
  InputHTMLAttributes,
  useEffect,
  useRef,
  useState,
} from "react";

import {
  Select,
  OptionList,
  Option,
  Selected,
  EmptyPlaceholderBox,
  Image,
  InputPhoneContainer,
  PhoneInputStyled,
  InputPhoneStyled,
  FieldWrapper,
} from "./styles";

import emptySearchImage from "assets/images/not-found/bg-empty-search.png";
import { useField, useFormikContext } from "formik";
import FormControlLabel from "common/styles/FormControlLabel";
import FormControlErrorHelper from "common/styles/FormControlErrorHelper";
import { useClickOutSide } from "hooks/useClickOutSide";

interface IInputPhoneProps
  extends DetailedHTMLProps<
    InputHTMLAttributes<HTMLInputElement>,
    HTMLInputElement
  > {
  defaultCountry?: CountryCode;
  name: string;
  className?: string;
  label?: string | null;
  disabled?: boolean;
  required?: boolean;
  placeholder?: string;
  hasEvent?: boolean;
  onClickEvent?: () => void;
  // use onChangeValue instead of onChange, since Formik will overwrite the onChange
  onChangeValue?: (value: string) => void;
  readonly onChange?: (e: ChangeEvent<HTMLInputElement>) => void;
}

const InputPhone: React.FC<IInputPhoneProps> = props => {
  const {
    defaultCountry,
    label,
    name,
    required = false,
    className,
    onChangeValue,
    ...rest
  } = props;

  const [countrySelected, setCountrySelected] = useState<CountryCode>(
    defaultCountry || "VN",
  );

  const { elementRef, isVisible, setElementVisible } = useClickOutSide(false);

  const [field, meta] = useField(props);
  const { setFieldValue } = useFormikContext();
  const isError: boolean = !!meta.touched && !!meta.error;

  const [searchText, setSearchText] = useState("");
  const [showOptions, setShowOptions] = useState(false);
  const [showInput, setShowInput] = useState(false);

  const searchInputRef = useRef<HTMLInputElement | null>(null);

  const [allCountries] = useState<CountryCode[]>(() => getCountries());

  useEffect(() => {
    onChangeValue && onChangeValue(field.value || "");
  }, [field.value]);

  useEffect(() => {
    const inputEl = searchInputRef.current;

    if (!showInput) {
      setSearchText("");
    } else {
      inputEl?.focus();
    }
  }, [showInput]);

  useEffect(() => {
    if (isVisible) return;

    setShowOptions(false);
    setShowInput(false);
  }, [isVisible]);

  const handleSelectedClick = () => {
    setShowOptions(!showOptions);
    setShowInput(true);
    setElementVisible(true);
  };

  const handleOptionClick = (country: CountryCode) => {
    setCountrySelected(country);
    setShowOptions(false);
    setShowInput(false);
    setSearchText("");
  };

  const renderOptionItems = (searchText: string) => {
    const countryFiltered = allCountries.filter(
      country =>
        country.includes(searchText.toUpperCase()) ||
        getCountryCallingCode(country).includes(searchText),
    );

    if (countryFiltered.length === 0) {
      return (
        <EmptyPlaceholderBox>
          <Image src={emptySearchImage} />
        </EmptyPlaceholderBox>
      );
    }

    return countryFiltered.map((country: CountryCode) => {
      return (
        <Option.Wrapper
          key={country}
          onClick={() => handleOptionClick(country)}
        >
          <Option.Image
            src={`https://flag.pk/flags/4x3/${country}.svg`}
            alt={`${country}`}
          />
          <Option.CountryCode>
            (+{getCountryCallingCode(country)})
          </Option.CountryCode>
        </Option.Wrapper>
      );
    });
  };

  const handleSearchClick = () => {
    setShowInput(true);
  };

  return (
    <InputPhoneContainer className={className}>
      <Select ref={elementRef}>
        <Selected.Wrapper onClick={() => handleSelectedClick()}>
          {showInput ? (
            <Selected.SearchInput
              type="text"
              value={searchText}
              onChange={e => {
                setSearchText(e.target.value);
              }}
              onClick={() => handleSearchClick()}
              ref={searchInputRef}
            />
          ) : (
            <>
              <Selected.Image
                src={`https://flag.pk/flags/4x3/${countrySelected}.svg`}
                alt={`${countrySelected}`}
              />
              <Selected.CountryCode>
                (+{getCountryCallingCode(countrySelected)})
              </Selected.CountryCode>
            </>
          )}

          <DropdownArrowIcon className="flex-shrink-0" />
        </Selected.Wrapper>

        {showOptions && (
          <OptionList>{renderOptionItems(searchText)}</OptionList>
        )}
      </Select>
      <InputPhoneStyled.Wrapper isError={isError}>
        <InputPhoneStyled.LabelWrapper>
          <FormControlLabel isError={isError} required={required}>
            {label}
          </FormControlLabel>
        </InputPhoneStyled.LabelWrapper>
        <FieldWrapper>
          <PhoneInputStyled
            country={countrySelected}
            isError={isError}
            {...(rest as any)}
            {...field}
            onChange={(value: string) => {
              setFieldValue(name, value);
            }}
          />
        </FieldWrapper>

        {isError && (
          <FormControlErrorHelper>{meta?.error}</FormControlErrorHelper>
        )}
      </InputPhoneStyled.Wrapper>
    </InputPhoneContainer>
  );
};

export default InputPhone;
