import styled from "styled-components/macro";
import tw from "twin.macro";

export const EmptyContainer = styled.div`
  ${tw`w-full h-screen shadow-lg flex flex-col`}
`;
export const ContentWrapper = styled.div`
  ${tw`mx-auto text-center justify-center `}
`;

export const Message = styled.h2`
  ${tw`text-20 text-black font-semibold`}
`;

export const Description = styled.h3`
  ${tw`mt-1.5 text-black text-14`}
`;

export const EmptyWrapper = styled.div`
  ${tw`mx-auto justify-center my-auto`}
`;

export const CodeTag = styled.div`
  ${tw`p-1 text-black text-16 font-semibold rounded-sm border-2 border-black w-max`}
`;

export const CodeTagWrapper = styled.div`
  ${tw`px-3 py-2.5`}
`;
