import { IRoutes } from "typings";
import { PATH } from "common/constants/routes";
import { lazy } from "react";
import { t } from "language";

import ConfigurationIcon from "icons/Dashboard/Configuration";

const Configuration = lazy(() => import("pages/dashboard/configuration"));

const BranchManagement = lazy(
  () => import("pages/dashboard/configuration/BranchManagement"),
);
const StaffsManagement = lazy(
  () => import("pages/dashboard/configuration/StaffsManagement"),
);
const CustomersManagement = lazy(
  () => import("pages/dashboard/configuration/CustomersManagement"),
);
const GeneralSetting = lazy(
  () => import("pages/dashboard/configuration/GeneralSetting"),
);

const AccountInfo = lazy(
  () => import("pages/dashboard/configuration/AccountInfo"),
);
const RoleDelegation = lazy(
  () => import("pages/dashboard/configuration/RoleDelegation"),
);

export const configurationRoute: IRoutes = {
  name: t("dashboard.configuration.configuration"),
  path: PATH.CONFIGURATION.SELF,
  exact: true,
  Component: Configuration,
  isPrivate: true,
  Icon: ConfigurationIcon,
};

export const branchManagementRoute: IRoutes = {
  name: t("dashboard.configuration.branch-management"),
  path: PATH.CONFIGURATION.BRANCH_MANAGEMENT,
  exact: true,
  Component: BranchManagement,
  isPrivate: true,
  hiddenRoute: true,
};

export const accountInfoRoute: IRoutes = {
  name: t("configuration.account-info.account-info"),
  path: PATH.CONFIGURATION.ACCOUNT_INFORMATION,
  exact: true,
  Component: AccountInfo,
  isPrivate: true,
  hiddenRoute: true,
};

export const roleDelegationRoute: IRoutes = {
  name: t("configuration.role-delegation.role-delegation"),
  path: PATH.CONFIGURATION.ROLE_DELEGATION,
  exact: true,
  Component: RoleDelegation,
  isPrivate: true,
  hiddenRoute: true,
};

export const staffsManagementRoute: IRoutes = {
  name: t("dashboard.configuration.staff-management"),
  path: PATH.CONFIGURATION.EMPLOYEES_MANAGEMENT,
  exact: true,
  Component: StaffsManagement,
  isPrivate: true,
  hiddenRoute: true,
};

export const customersManagementRoute: IRoutes = {
  name: t("dashboard.configuration.customer-management"),
  path: PATH.CONFIGURATION.CUSTOMERS_MANAGEMENT,
  exact: true,
  Component: CustomersManagement,
  isPrivate: true,
  hiddenRoute: true,
};

export const generalSettingRoute: IRoutes = {
  name: t("dashboard.configuration.general-settings.general-settings"),
  path: PATH.CONFIGURATION.GENERAL_SETTING.SELF,
  exact: true,
  Component: GeneralSetting,
  isPrivate: true,
  hiddenRoute: true,
};
