import { useState, useEffect } from "react";
import { setUserCookies } from "common/utils/auth";
import { toast } from "react-toastify";

//typings
import { ILogin } from "typings";
import { IAccount } from "typings";

//api
import { useLogin as useLoginUser } from "apiCaller";
import { fragmentLogin } from "services/auth";

//languages
import { t } from "language";

const useLogin = () => {
  const [loginService, { data, loading, error }] = useLoginUser(fragmentLogin);
  const [isLoginSuccess, setIsLoginSuccess] = useState<boolean>(false);

  useEffect(() => {
    if (data) {
      setIsLoginSuccess(true);
      setUserCookies(data.login as IAccount);
      toast.success(t("login.login-success.title"));
    } else {
      toast.error(error);
    }
  }, [data]);

  const login = (variables: ILogin) => {
    setIsLoginSuccess(false);
    loginService({
      variables: variables,
    });
  };

  return { error, login, loading, isLoginSuccess, data };
};
export default useLogin;
