import {
  GenFields,
  Product,
  ProductResult,
  ProductCategory,
  ProductCategoryResult,
} from "apiCaller";

export const fragmentGetAllProduct: GenFields<ProductResult> = [
  {
    results: [
      "_id",
      "name",
      "description",
      { images: ["default", "medium", "small"] },
      { thumbnail: ["default", "medium", "small"] },
      "status",
      { productCategory: ["_id", "name"] },
      "barcode",
      { branch: ["_id", "code", "name"] },
      { units: ["_id", "name", "price"] },
      { toppings: ["_id", "name", "price", "quantity"] },
      "price",
      { tags: ["_id", "name"] },
    ],
  },
  "totalCount",
];

export const fragmentCreateProduct: GenFields<Product> = [
  "_id",
  "name",
  "description",
];

export const fragmentUpdateProduct: GenFields<Product> = [
"_id",
  "name",
  "description",
  { images: ["default", "medium", "small"] },
  { branch: ["_id", "name"] },
];

export const fragmentGetProductById: GenFields<Product> = [
  "_id",
  "name",
  "description",
  { images: ["default", "medium", "small"] },
  { thumbnail: ["default", "medium", "small"] },
  "status",
  "price",
  "barcode",
  { units: ["_id", "name", "price"] },
  { toppings: ["_id", "name", "price", "quantity"] },
];
export const fragmentGetProductByBarcode: GenFields<Product> = [
  "_id",
  "barcode",
  { branch: ["_id", "code"] },
  "createdAt",
  "description",
  { toppings: ["_id", "name", "price", "quantity"] },
  { images: ["default", "medium", "small", "description"] },
  "name",
  "price",
  { productCategory: ["_id"] },
  "slug",
  "status",
  { tags: ["_id"] },
  { units: ["_id", "name", "price"] },
  { thumbnail: ["medium", "small", "default"] },
  "updatedAt",
];

export const fragmentGetAllProductCategory: GenFields<ProductCategoryResult> = [
  {
    results: [
      "_id",
      "name",
      "description",
      { images: ["default", "medium", "small"] },
      {
        businessProductType: ["_id", "createdAt", "name", "slug", "updatedAt"],
      },
      "description",
      {
        products: [
          "_id",
          "name",
          { images: ["default", "medium", "small"] },
          { toppings: ["_id", "name", "price", "quantity"] },
          { thumbnail: ["default", "medium", "small"] },
          "status",
          { branch: ["_id", "code", "name"] },

          "price",
        ],
      },
    ],
  },
];

export const fragmentCreateProductCategory: GenFields<ProductCategory> = [
  "_id",
  "name",
  "description",
  { images: ["default", "medium", "small"] },
  "createdAt",
  "updatedAt",
  { tag: ["_id", "name"] },
];

export const fragmentUpdateProductCategory: GenFields<ProductCategory> = [
  "_id",
  "name",
  "description",
  { images: ["default", "medium", "small"] },
  "createdAt",
  "updatedAt",
  { tag: ["_id", "name"] },
];
export const fragmentGetProductCategoryById: GenFields<ProductCategory> = [
  "_id",
  "name",
  "description",
  { images: ["default", "medium", "small"] },
  { thumbnail: ["default", "medium", "small"] },
  { businessProductType: ["_id", "name"] },
  { branch: ["_id", "name"] },
  {
    products: [
      "_id",
      "price",
      "name",
      { thumbnail: ["default", "medium", "small"] },
      { images: ["default", "medium", "small"] },
      { toppings: ["_id", "name", "price", "quantity"] },
    ],
  },
];
