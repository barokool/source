import React, { useState, useEffect } from "react";
import {
  OrderResponseDialogContainer,
  ElementWrapper,
  Button,
  Label,
  Description,
} from "./styles";

import Dialog from "components/Dialog";
import IconButton from "designs/IconButton";

interface OrderResponseDialogProps {
  ButtonMenu: React.ReactElement;
  status?: OrderResponseStatus;
  onClose?: () => void;
  onSuccess?: () => void;
  icon?: React.ReactElement;
  label?: string;
  description?: string;
}

export type OrderResponseStatus = "failed" | "success";

const OrderResponseDialog: React.FC<OrderResponseDialogProps> = ({
  ButtonMenu,
  status,
  onClose,
  onSuccess,
  icon,
  label,
  description,
}) => {
  const [isOpen, setOpen] = useState<boolean>(false);
  return (
    <>
      <ElementWrapper
        className="flex justify-end w-full"
        onClick={() => setOpen(!isOpen)}
      >
        {ButtonMenu}
      </ElementWrapper>

      <Dialog
        open={isOpen}
        onClose={() => setOpen(false)}
        className="overflow-auto "
        size="md"
      >
        <OrderResponseDialogContainer isDanger={status as OrderResponseStatus}>
          {icon}
          <Label>{label}</Label>
          <Description>{description}</Description>
          <Button
            isDanger={status as OrderResponseStatus}
            onClick={() => setOpen(false)}
          >
            OK
          </Button>
        </OrderResponseDialogContainer>
      </Dialog>
    </>
  );
};

export default OrderResponseDialog;
