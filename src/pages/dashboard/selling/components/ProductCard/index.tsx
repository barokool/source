import React, { useState, useEffect } from "react";
import {
  ProductCardContainer,
  TextWrapper,
  TextName,
  TextPrice,
  Image,
} from "./styles";
import ProductDialog from "../ProductDialog";

import { useSelector, useDispatch } from "react-redux";
import { IRootState } from "typings";
import { setOrderSelected } from "redux/slices/order";

import { Order, Product, useGetOrderById } from "apiCaller";
import { fragmentGetOrderById } from "services/order";

type IProductCard = {
  className?: string;
  onClick?: () => void;
} & (
  | {
      loading?: false;
      product: Product;
    }
  | {
      loading: true;
      readonly product?: Product;
    }
);

const ProductCard: React.FC<IProductCard> = props => {
  const { product, onClick, loading, className } = props;
  const dispatch = useDispatch();
  const { orderSelected } = useSelector((state: IRootState) => state.order);
  const [getOrderById, { data }] = useGetOrderById(fragmentGetOrderById);

  useEffect(() => {
    if (data) {
      dispatch(setOrderSelected(data.getOrderById as Order));
    }
  }, [data]);

  const handleReloadOrder = async () => {
    try {
      await getOrderById({
        variables: {
          id: orderSelected?._id as string,
        },
      });
    } catch (error) {
      console.log(error);
    }
  };

  if (loading) {
    return (
      <ProductCardContainer
        onClick={onClick}
        className={"animate-pulse bg-line space-y-1 h-15 " + className}
      ></ProductCardContainer>
    );
  }
  return (
    <ProductCardContainer onClick={onClick}>
      <ProductDialog
        ButtonMenu={
          <Image
            src={product?.thumbnail?.medium as string}
            alt="Image product"
          />
        }
        title={product?.name as string}
        product={product}
        onSuccess={() => handleReloadOrder()}
      />
      <TextWrapper>
        <TextName>{product?.name}</TextName>
        <TextPrice>{product?.price?.toString().prettyMoney()}€</TextPrice>
      </TextWrapper>
    </ProductCardContainer>
  );
};

export default ProductCard;
