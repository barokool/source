import { lazy } from "react";
import { IRoutes } from "typings";
import { Redirect } from "react-router";
import { PATH } from "common/constants/routes";

// Login page
import NotFound from "pages/NotFound";

const Login = lazy(() => import("pages/Login"));
// Not Found page

const ResetPassword = lazy(() => import("pages/ResetPassword"));
const Register = lazy(() => import("pages/Register"));
export const rootRoute: IRoutes = {
  name: "root",
  path: "/",
  exact: true,
  Component: () => <Redirect to={PATH.DASHBOARD} />,
  isPrivate: true,
};

export const authRoutes: IRoutes = {
  name: "auth",
  path: PATH.AUTH.SELF,
  isPrivate: false,
  children: [
    {
      name: "login",
      path: PATH.AUTH.LOGIN,
      Component: Login,
      isPrivate: false,
    },
    {
      name: "Reset password",
      path: PATH.AUTH.RESET_PASSWORD,
      Component: ResetPassword,
      isPrivate: false,
    },
    {
      name: "Register ",
      path: PATH.AUTH.REGISTER,
      Component: Register,
      isPrivate: false,
    },
  ],
};

export const notFoundRoute: IRoutes = {
  name: "notfound",
  path: PATH.NOT_FOUND,
  exact: false,
  Component: NotFound,
  isPrivate: false,
};
