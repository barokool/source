import ReactDOM from "react-dom";
import { PersistGate } from "redux-persist/integration/react";
import { persistStore } from "redux-persist";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import Toast from "components/Toast";
import store, { history } from "redux/storeToolkit";
import PageLoading from "components/PageLoading";
import "common/utils/prototype";
import "./language/index";
import { Provider } from "react-redux";
import { ApolloProvider } from "@apollo/client";
import { client } from "common/config/graphql";
import { I18nextProvider } from "react-i18next";
import i18n from "language";
let persistor = persistStore(store);

ReactDOM.render(
  <ApolloProvider client={client}>
    <Provider store={store}>
      <PersistGate loading={<div>Loading...</div>} persistor={persistor}>
        <Toast>
          <PageLoading />
          <I18nextProvider i18n={i18n}>
            <App history={history} />
          </I18nextProvider>
        </Toast>
      </PersistGate>
    </Provider>
  </ApolloProvider>,
  document.getElementById("root"),
);

reportWebVitals();
