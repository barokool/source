import styled from "styled-components/macro";
import tw from "twin.macro";
import _SVG from "designs/SVG";

export const HeaderContainer = styled.header`
  ${tw`fixed  bg-white-color top-0 right-0 left-0 z-50 flex items-center w-full text-16 text-center text-primary-1 px-2 py-1 border-solid border-b border-border-color `}
`;

export const IconWrapper = styled.div`
  ${tw`absolute right-0 laptop:left-0 w-6 h-6 flex items-center justify-center`}
`;

export const Icon = styled(_SVG)`
  ${tw`w-2.5 h-2.5 cursor-pointer`}
`;

export const Container = styled.div`
  ${tw`flex items-center justify-end flex-1 h-full phone:flex-row phone:items-center w-40 `}
`;

export const Heading = styled.div`
  ${tw`text-left font-bold text-secondary text-26`}
`;
