import {
  ChangeEvent,
  DetailedHTMLProps,
  InputHTMLAttributes,
  useEffect,
  useState,
} from "react";
import { useField } from "formik";
import {
  FieldWrapper,
  IconWrapper,
  InputContainer,
  InputField,
  EventChange,
  LabelWrapper,
} from "./styles";
import FormControlErrorHelper from "common/styles/FormControlErrorHelper";
import FormControlLabel from "common/styles/FormControlLabel";
import HidePasswordIcon from "icons/PasswordEye/HidePassword";
import ShowPasswordIcon from "icons/PasswordEye/ShowPassword";
interface IInput
  extends DetailedHTMLProps<
    InputHTMLAttributes<HTMLInputElement>,
    HTMLInputElement
  > {
  name: string;
  className?: string;
  label: string | null;
  disabled?: boolean;
  required?: boolean;
  placeholder?: string;
  hasEvent?: boolean;
  onClickEvent?: () => void;
  // use onChangeValue instead of onChange, since Formik will overwrite the onChange
  onChangeValue?: (value: string | number) => void;
  readonly onChange?: (e: ChangeEvent<HTMLInputElement>) => void;
}
const TimePicker: React.FC<IInput> = props => {
  const {
    name,
    className,
    required,
    label = null,
    hasEvent = false,
    onClickEvent,
    onChangeValue,
    ...rest
  } = props;
  const [field, meta] = useField(props);

  const isError: boolean = !!meta.touched && !!meta.error;

  useEffect(() => {
    onChangeValue && onChangeValue(field.value || "");
  }, [field.value]);

  return (
    <InputContainer className={className} isError={isError}>
      <LabelWrapper>
        <FormControlLabel isError={isError} required={required}>
          {label}
        </FormControlLabel>
      </LabelWrapper>
      <FieldWrapper>
        <InputField
          type="time"
          isError={isError}
          {...(rest as any)}
          {...field}
        />
      </FieldWrapper>
      {isError && (
        <FormControlErrorHelper>{meta?.error}</FormControlErrorHelper>
      )}
    </InputContainer>
  );
};
export default TimePicker;
