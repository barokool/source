import styled from "styled-components/macro";
import tw from "twin.macro";

export const MenuRightContainer = styled.div`
  ${tw`w-full`}
`;
