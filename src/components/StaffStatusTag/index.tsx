import { UserStatus } from "apiCaller";
import { StaffStatusTagContainer } from "./styles";

interface IStaffStatusTagProps {
  active: UserStatus;
}

const StaffStatusTag: React.FC<IStaffStatusTagProps> = ({
  active,
  children,
}) => {
  return (
    <StaffStatusTagContainer active={active}>
      {children}
    </StaffStatusTagContainer>
  );
};

export default StaffStatusTag;
