import React, { useEffect, useState } from "react";
import { Formik } from "formik";
import * as yup from "yup";
import { toast } from "react-toastify";

import Input from "designs/InputV2";
import Select from "designs/Select";
import Dialog from "components/Dialog";
import Autocomplete from "designs/AutoComplete";
import { DialogTitle } from "common/styles/Title";

import {
  ElementWrapper,
  StaffsManagementDialogContainer,
  Form,
  ButtonWrapper,
  Button,
} from "./styles";

import useAuth from "hooks/useAuth";

import { t } from "language";
import MultipleAutocomplete from "designs/MultipleAutoComplete";
import {
  Branch,
  CreateUserInput,
  GetAllUserArgs,
  Role,
  RoleEnum,
  UpdateUserInput,
  useCreateUser,
  useGetAllRole,
  useGetAllUser,
  User,
  useUpdateUser,
} from "apiCaller";
import {
  fragmentCreateUser,
  fragmentGetAllUser,
  fragmentUpdateUser,
} from "services/user";
import { fragmentGetAllRole } from "services/role";

interface IStaffsManagementDialogProps {
  ButtonMenu: React.ReactElement;
  editField?: User;
  title?: string;
  onClose?: () => void;
  onSuccess?: () => void;
  open?: boolean;
}

interface IFormValue {
  displayName: string;
  role?: string;
  branches?: string[];
  managerStaff?: string;
  phoneNumber: string;
  password: string;
  email?: string;
}

const validationSchema = yup
  .object()
  .shape<{ [key in keyof IFormValue]: any }>({
    displayName: yup
      .string()
      .required(t("configuration.staff-management.please-enter-name")),
    phoneNumber: yup
      .string()
      .required(t("configuration.staff-management.enter-staff-phone-number")),
    password: yup
      .string()
      .required(t("configuration.staff-management.please-enter-password")),
  });
const validationSchemaCreate = yup
  .object()
  .shape<{ [key in keyof IFormValue]: any }>({
    displayName: yup
      .string()
      .required(t("configuration.staff-management.please-enter-name")),
    phoneNumber: yup
      .string()
      .required(t("configuration.staff-management.enter-staff-phone-number")),
    password: yup
      .string()
      .required(t("configuration.staff-management.please-enter-password")),
  });

const StaffsManagementDialog: React.FC<IStaffsManagementDialogProps> = ({
  ButtonMenu,
  editField,
  title,
  onSuccess,
  onClose,
  open = false,
}) => {
  console.log("Edit field: ", editField);
  const [initialValues, setInitialValues] = useState<IFormValue>({
    displayName: "",
    phoneNumber: "",
    email: "",
    password: "",
  });

  const [isOpen, setOpen] = useState<boolean>(open);
  const [loading, setLoading] = useState(false);
  const [staffManagerSelected, setStaffManager] = useState<User | null>(null);
  const [branchesSelected, setBranchesSelected] = useState<Branch[]>([]);
  const [roleSelected, setRoleSelected] = useState<Role | null>(null);

  const [updateUser, { data: dataUpdate, error: errorUpdate }] =
    useUpdateUser(fragmentUpdateUser);

  const [createUser, { data: dataCreate, error: errorCreate }] =
    useCreateUser(fragmentCreateUser);

  const [getAllRole, { data: listRole, error: errorRoles }] =
    useGetAllRole(fragmentGetAllRole);

  const [getAllUser, { data: ListStaff, called, error }] =
    useGetAllUser(fragmentGetAllUser);

  const { accountInfo } = useAuth();

  const invokeSetAllStaffs = () => {
    getAllUser({
      variables: {
        filterUser: {
          role: RoleEnum.Staff,
          branches: accountInfo?.userInfo?.branches?.map(
            branch => branch._id,
          ) as string[],
        },
      },
    });
  };

  const invokeGetAllRole = () => {
    getAllRole({
      variables: {
        filterRole: {
          idBranch: accountInfo?.userInfo?.branches?.[0]?._id as string,
        },
      },
    });
  };

  useEffect(() => {
    invokeGetAllRole();
    invokeSetAllStaffs();
  }, []);

  useEffect(() => {
    setBranchesSelected(editField?.branches as Branch[]);
    setRoleSelected(editField?.role as Role);
    if (editField) {
      setInitialValues({
        displayName: editField.displayName as string,
        role: editField?.role?._id as string,
        branches: editField?.branches?.map(branch => branch._id) as string[],
        phoneNumber: editField.phoneNumber as string,
        email: editField.email as string,
        password: "",
      });
    }
  }, [editField]);

  useEffect(() => {
    if (dataCreate && !errorCreate) {
      invokeSetAllStaffs();
      toast.success(t("common.create-success"));
      onSuccess?.();
      setOpen(false);
    }
    if (!dataCreate && errorCreate) {
      toast.error(t("common.create-fail"));
      onHandleClose();
    }
  }, [dataCreate]);

  useEffect(() => {
    if (dataUpdate && !errorUpdate) {
      toast.success(t("common.edit-success"));
      onSuccess?.();
      onHandleClose();
    }
    if (!dataUpdate && errorUpdate) {
      toast.error(t("common.edit-fail"));
      onHandleClose();
    }
  }, [dataUpdate]);

  useEffect(() => {
    errorCreate && !dataCreate && toast.error(errorCreate.message);
    errorUpdate && !dataUpdate && toast.error(errorUpdate.message);
  }, [errorCreate, errorUpdate]);

  const handleSubmit = (values: IFormValue) => {
    const input: CreateUserInput = {
      displayName: values.displayName,
      phoneNumber: values.phoneNumber,
      email: values.email,
      password: values.password,
      role: values.role as string,
      branches: branchesSelected.map(branch => branch._id) as string[],
    };
    try {
      if (editField) {
        console.log("update data");
        setLoading(true);
        updateUser({
          variables: {
            id: editField._id as string,
            updateUserInput: input as UpdateUserInput,
          },
        });
      } else {
        createUser({
          variables: {
            input: input,
          },
        });
      }
    } catch (error) {
      console.error();
    }
  };

  const onHandleClose = () => {
    setOpen(false);
    onClose && onClose();
  };

  return (
    <>
      <ElementWrapper
        onClick={() => setOpen(!isOpen)}
        className="flex justify-end w-full"
      >
        {ButtonMenu}
      </ElementWrapper>

      <Dialog
        open={isOpen}
        onClose={() => setOpen(false)}
        className="overflow-auto"
        size="lg"
      >
        <StaffsManagementDialogContainer>
          <DialogTitle>{title}</DialogTitle>
          <Formik
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={handleSubmit}
            enableReinitialize
          >
            <Form>
              <Input
                name="displayName"
                label={t("configuration.staff-management.name")}
                placeholder={t("configuration.staff-management.enter-name")}
                required
              />
              <Autocomplete
                optionSelected={staffManagerSelected}
                options={ListStaff?.getAllUser?.results || []}
                onSelect={values => setStaffManager(values)}
                label={t("configuration.staff-management.staff-in-charge")}
                placeholder={t(
                  "configuration.staff-management.select-staff-in-charge",
                )}
                name="fullNameManager"
                optionTarget="displayName"
              />
              <Input
                name="phoneNumber"
                label={t("configuration.staff-management.staff-phone-number")}
                placeholder={t(
                  "configuration.staff-management.enter-staff-phone-number",
                )}
                required
              />

              <MultipleAutocomplete
                listOptionsSelected={branchesSelected}
                onSelect={values => setBranchesSelected(values)}
                options={accountInfo.userInfo?.branches as Branch[]}
                name="branches"
                label={t("configuration.staff-management.branch")}
                placeholder={t("configuration.staff-management.select-branch")}
              />
              <Input
                name="password"
                label={t("configuration.staff-management.password")}
                placeholder={t("configuration.staff-management.enter-password")}
                required
                type="password"
              />

              <Select
                optionSelected={roleSelected}
                onSelect={role => setRoleSelected(role)}
                options={listRole?.getAllRole?.results || []}
                name="role"
                label={t("configuration.staff-management.select-role")}
                optionTarget="position"
              />

              <Input
                name="email"
                type="text"
                label={t("configuration.staff-management.email")}
                placeholder={t("configuration.staff-management.enter-email")}
                required
              />

              <ButtonWrapper>
                <Button outline type="button" onClick={() => setOpen(false)}>
                  {t("common.cancel")}
                </Button>
                <Button type="submit" primary loading={loading}>
                  {t("common.accept")}
                </Button>
              </ButtonWrapper>
            </Form>
          </Formik>
        </StaffsManagementDialogContainer>
      </Dialog>
    </>
  );
};

export default StaffsManagementDialog;
