import { OrderStatusEnum } from "apiCaller";
import { OrdertatusTagContainer } from "./styles";

interface OrderStatusTagProps {
  active: OrderStatusEnum;
}

const OrderStatusTag: React.FC<OrderStatusTagProps> = ({
  active,
  children,
}) => {
  return (
    <OrdertatusTagContainer active={active}>{children}</OrdertatusTagContainer>
  );
};

export default OrderStatusTag;
