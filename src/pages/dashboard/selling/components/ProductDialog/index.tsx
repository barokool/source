import React, { useMemo, useState, useEffect } from "react";
import { toast } from "react-toastify";
import { Formik } from "formik";
import * as Yup from "yup";

import Select from "designs/SimpleSelect";
import Input from "designs/InputV2";
import Dialog from "components/Dialog";
import RichTextEditor from "designs/RichTextEditor";
import IncreaseToppingIcon from "icons/Increase/IncreaseTopping";
import DecreaseToppingIcon from "icons/Decrease/DecreaseTopping";

import { DialogTitle } from "common/styles/Title";
import { t } from "language";

import { useSelector } from "react-redux";

import {
  ProductDialogContainer,
  Button,
  ButtonWrapper,
  ElementWrapper,
  Form,
  FormGroup,
  FormLabel,
  SelectUnit,
  TableWrapper,
  UpDownQuantityWrapper,
  TableHeaderItem,
  TableItem,
} from "./styles";

import { IRootState } from "typings";

import {
  Product,
  ProductUnit,
  ProductUnitInput,
  Topping,
  ToppingInput,
  useAddOrderItemToOrder,
} from "apiCaller";

interface OrderItemDialog {
  ButtonMenu?: React.ReactElement;
  product?: Product;
  title?: string;
  onClose?: () => void;
  open?: boolean;
  onSuccess?: () => void;
}

interface IFormValue {
  quantity?: number;
  product?: string;
  toppings?: ToppingInput[];
  unit?: ProductUnitInput;
  note?: string;
}

const validationSchema = Yup.object().shape<{ [key in keyof IFormValue]: any }>(
  {},
);

const ProductDialog: React.FC<OrderItemDialog> = props => {
  const { ButtonMenu, product, title, onClose, onSuccess, open } = props;
  const [unitSelected, setUnitSelected] = useState<ProductUnit | null>(null);
  const [isOpen, setOpen] = useState(open || false);
  const [initialValues, setInitialValues] = useState<IFormValue>({
    note: "",
    product: "",
    toppings: [],
    unit: {
      name: "",
      price: 0,
    },
    quantity: 1,
  });
  const [toppings, setAllToppings] = useState<Topping[]>([]);
  const [toppingsInput, setToppingsInput] = useState<ToppingInput[]>([]);
  const [toppingQuantities, setToppingQuantities] = useState(
    new Array(20).fill(0),
  );
  const { orderSelected } = useSelector((state: IRootState) => state.order);

  const [addOrderItemToOrder, { data, loading, called, error }] =
    useAddOrderItemToOrder();

  useEffect(() => {
    let tempToppings: Topping[] = [];
    product?.toppings?.map(topping => tempToppings.push(topping)),
      setAllToppings(tempToppings);
  }, []);

  useEffect(() => {
    if (toppings) {
      let tempToppings: ToppingInput[] = [];
      toppings.map((topping, index) => {
        if (toppingQuantities[index] >= 1) {
          tempToppings.push({
            name: topping.name as string,
            price: topping.price as number,
            quantity: toppingQuantities[index],
          });
        }
      });
      setToppingsInput(tempToppings);
    }
  }, [toppings, toppingQuantities]);

  const onHandleClose = () => {
    setOpen(false);
    onClose && onClose();
  };

  useEffect(() => {
    console.log(error);
  }, [error]);

  const handleSubmit = async (values: IFormValue) => {
    if (orderSelected?.status === "new") {
      try {
        await addOrderItemToOrder({
          variables: {
            idOrder: orderSelected?._id as string,
            input: {
              note: values.note,
              product: product?._id as string,
              quantity: values.quantity,
              unit: {
                name: unitSelected?.name as string,
                price: unitSelected?.price as number,
              },
              toppings: toppingsInput,
            },
          },
        });
        toast.success(t("selling.add-item-success"));
        onSuccess?.();
      } finally {
        setOpen(false);
      }
    } else {
      toast.warning(t("selling.cannot-add-item"));
      setOpen(false);
    }
  };

  return (
    <>
      <ElementWrapper
        onClick={() => setOpen(true)}
        className="flex justify-end w-full"
      >
        {ButtonMenu}
      </ElementWrapper>
      <Dialog
        size="md"
        open={isOpen}
        onClose={onHandleClose}
        className="overflow-auto"
      >
        <ProductDialogContainer>
          <DialogTitle>{title}</DialogTitle>
          <Formik
            enableReinitialize
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={handleSubmit}
          >
            <Form>
              <ElementWrapper className="flex flex-col gap-y-4">
                <FormGroup.Container>
                  <FormLabel.Container>
                    <FormLabel.Content>
                      {t("selling.enter-quantity")}
                    </FormLabel.Content>
                  </FormLabel.Container>
                  <SelectUnit.Container>
                    <Input
                      label={t("selling.quantity")}
                      placeholder={t("selling.enter-quantity")}
                      name="quantity"
                      type="number"
                    />
                  </SelectUnit.Container>
                </FormGroup.Container>
                <FormGroup.Container>
                  <FormLabel.Container>
                    <FormLabel.Content>
                      {t("selling.select-unit")}
                    </FormLabel.Content>
                  </FormLabel.Container>
                  <SelectUnit.Container>
                    <SelectUnit.Content>{t("selling.name")}</SelectUnit.Content>
                    <SelectUnit.Content>
                      {t("selling.price")}
                    </SelectUnit.Content>
                    <Select
                      label="Unit"
                      options={product?.units as ProductUnit[]}
                      onSelect={unit => setUnitSelected(unit)}
                      optionSelected={unitSelected}
                    />
                    <SelectUnit.Price>
                      {unitSelected?.price?.toString().prettyMoney()}
                    </SelectUnit.Price>
                  </SelectUnit.Container>
                </FormGroup.Container>
                <FormGroup.Container>
                  <FormLabel.Container>
                    <FormLabel.Content>
                      {t("selling.select-toppings")}
                    </FormLabel.Content>
                  </FormLabel.Container>
                  <TableWrapper>
                    <TableHeaderItem>{t("selling.name")}</TableHeaderItem>
                    <TableHeaderItem>{t("selling.quantity")}</TableHeaderItem>
                    <TableHeaderItem>{t("selling.price")}</TableHeaderItem>

                    {toppings.map((topping, index) => (
                      <>
                        <TableItem>{topping.name}</TableItem>
                        <UpDownQuantityWrapper>
                          <DecreaseToppingIcon
                            onClick={() => {
                              const newItems = [...toppingQuantities];
                              if (newItems[index] >= 0) {
                                newItems[index]--;
                              }
                              setToppingQuantities(newItems);
                            }}
                          />
                          <TableItem>{toppingQuantities[index]}</TableItem>
                          <IncreaseToppingIcon
                            onClick={() => {
                              const newItems = [...toppingQuantities];
                              newItems[index]++;
                              setToppingQuantities(newItems);
                            }}
                          />
                        </UpDownQuantityWrapper>
                        <TableItem>
                          {(
                            (topping.price as number) * toppingQuantities[index]
                          )
                            .toString()
                            .prettyMoney()}
                        </TableItem>
                      </>
                    ))}
                  </TableWrapper>
                </FormGroup.Container>
                <FormGroup.Container>
                  <FormLabel.Container>
                    <FormLabel.Content>{t("selling.note")}</FormLabel.Content>
                  </FormLabel.Container>
                  <RichTextEditor
                    name="note"
                    label={t("selling.note")}
                    defaultValue={initialValues.note}
                  />
                </FormGroup.Container>
              </ElementWrapper>
              <ButtonWrapper>
                <Button outline type="button" onClick={onHandleClose}>
                  {t("common.cancel")}
                </Button>
                <Button primary type="submit" loading={loading}>
                  {t("common.accept")}
                </Button>
              </ButtonWrapper>
            </Form>
          </Formik>
        </ProductDialogContainer>
      </Dialog>
    </>
  );
};

export default ProductDialog;
