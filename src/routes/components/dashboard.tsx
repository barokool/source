import { IRoutes } from "typings";
import { PATH } from "common/constants/routes";
import { lazy } from "react";
import { t } from "language";

import DashboardIcon from "icons/Dashboard/Dashboard";

const Overview = lazy(() => import("pages/dashboard/Overview"));

export const dashboardRoute: IRoutes = {
  name: t("dashboard.overview"),
  path: PATH.DASHBOARD,
  exact: true,
  Component: Overview,
  isPrivate: true,
  Icon: DashboardIcon,
};
