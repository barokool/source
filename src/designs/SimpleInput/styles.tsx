import styled from "styled-components/macro";
import tw from "twin.macro";
import { formControlCommon } from "common/styles/FormControl";

export const InputContainer = styled.div<{ isError: boolean }>`
  ${tw`w-full relative `}
  ${({ isError }) => (isError ? tw`text-danger` : tw`text-neutral-3`)}
`;

export const FieldWrapper = styled.div`
  ${tw`relative flex flex-row items-center w-full`}
`;

export const InputField = styled.input<{ isError: boolean; isBorder: boolean }>`
  ${tw`focus:outline-none`}
  ${({ isError, disabled }) => formControlCommon(isError, disabled)}
  ${({ isBorder }) => !isBorder && tw`border-0`}
`;

export const IconWrapper = styled.div`
  ${tw`absolute cursor-pointer select-none right-1`}
`;
export const LabelWrapper = styled.div`
  ${tw`flex items-center justify-between absolute -top-2 left-[20px] z-10 bg-white rounded-sm px-[4px] `}
`;
export const EventChange = styled.div`
  ${tw`text-16 duration-300 cursor-default hover:text-neutral-3 text-primary`}
`;
export const SubLabel = styled.span`
  ${tw`font-regular phone:ml-0.5 text-14 text-neutral-3`}
`;
