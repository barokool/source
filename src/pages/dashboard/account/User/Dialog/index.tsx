import { useState } from "react";
import { Formik, FormikValues } from "formik";
import * as Yup from "yup";

import { phoneNumberRegexp } from "common/functions/validate/phone";
import { forceTextInputEnterNumber } from "common/functions/condition/forceTextInputEnterNumber";

import Input from "designs/InputV2";
import Dialog from "components/Dialog";
import ToolTip from "designs/Tooltip";
import RadioButton from "designs/RadioButton";
import { DialogTitle } from "common/styles/Title";

import {
  ElementWrapper,
  UserDialogContainer,
  Form,
  ButtonWrapper,
  Button,
} from "./styles";

import { t } from "language";

interface IUserDialogProps {
  ButtonMenu: React.ReactElement;
  editField?: any;
  title?: string;
}

const UserDialog: React.FC<IUserDialogProps> = ({
  ButtonMenu,
  editField,
  title,
}) => {
  const initialValues = {
    phoneNumber: "",
    password: "",
    displayName: "",
    email: "",
    status: false,
  };

  const [isOpen, setOpen] = useState(false);
  const [isHidePassword, setHidePassword] = useState(true);

  const validationSchema = Yup.object({
    phoneNumber: Yup.string()
      .required(t("account.user.please-enter-name"))
      .matches(phoneNumberRegexp, t("account.user.please-enter-phone-number")),
    password: Yup.string().required(t("account.user.please-enter-password")),
  });

  const handleSubmit = (values: FormikValues) => {
    console.log("results: ", values);
  };

  return (
    <>
      <ToolTip text={title || ""}>
        <ElementWrapper onClick={() => setOpen(!isOpen)}>
          {ButtonMenu}
        </ElementWrapper>
      </ToolTip>
      <Dialog open={isOpen} onClose={() => setOpen(false)}>
        <UserDialogContainer>
          <DialogTitle>
            {editField
              ? t("account.user.edit-user")
              : t("account.user.add-new-user")}
          </DialogTitle>
          <Formik
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={handleSubmit}
          >
            {formik => {
              return (
                <Form onSubmit={formik.handleSubmit}>
                  <Input
                    name="phoneNumber"
                    id="phoneNumber"
                    label={t("account.user.phone-number")}
                    placeholder={t("account.user.enter-phone-number")}
                    onKeyPress={forceTextInputEnterNumber}
                    type="tel"
                    required
                  />
                  <Input
                    name="password"
                    id="password"
                    label={t("account.user.password")}
                    placeholder={t("account.user.enter-password")}
                    type="text"
                    required
                  />
                  <Input
                    name="displayName"
                    id="displayName"
                    label={t("account.user.full-name")}
                    placeholder={t("account.user.enter-ful-name")}
                    type="text"
                  />
                  <Input
                    name="email"
                    id="email"
                    label={t("account.user.email")}
                    placeholder={t("account.user.enter-email")}
                    type="text"
                  />
                  <RadioButton
                    value={formik.values.status}
                    label={t("account.user.status.status")}
                    onChange={value => formik.setFieldValue("status", value)}
                  />
                  <ButtonWrapper>
                    <Button
                      outline
                      type="button"
                      onClick={() => setOpen(false)}
                    >
                      {t("common.cancel")}
                    </Button>
                    <Button primary type="submit">
                      {t("common.accept")}
                    </Button>
                  </ButtonWrapper>
                </Form>
              );
            }}
          </Formik>
        </UserDialogContainer>
      </Dialog>
    </>
  );
};

export default UserDialog;
