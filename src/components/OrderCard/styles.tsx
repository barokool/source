import styled from "styled-components/macro";
import tw from "twin.macro";

export const OrderCardContainer = styled.button`
  ${tw`py-1.5 px-2 mt-1 w-full border-2 text-left hover:border-secondary border-dashed cursor-pointer`}
`;
