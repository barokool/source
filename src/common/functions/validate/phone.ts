export const phoneNumberRegexp =
  /^[+]?[(]?[0-9]{3}[)]?[-s.]?[0-9]{3}[-s.]?[0-9]{3,5}$/;

export const americaPhoneNumberRegex = /\(?\d{3}\)?-? *\d{3}-? *-?\d{4}/;

export const germanyPhoneNumberRegex =
  /(\(?([\d \-\)\–\+\/\(]+){6,}\)?([ .\-–\/]?)([\d]+))/;
export const vietnamPhoneNumberRegex = /(((\+|)84)|0)(3|5|7|8|9)+([0-9]{8})\b/;

export const isPhoneNumber = (phone: string): boolean =>
  germanyPhoneNumberRegex.test(phone) || vietnamPhoneNumberRegex.test(phone);

export const formatPhoneNumber = (phoneNumber: string): string => {
  let formattedPhoneNumber = "";

  if (isPhoneNumber(phoneNumber)) {
    if (phoneNumber[0] === "0") {
      if (germanyPhoneNumberRegex.test(phoneNumber)) {
        formattedPhoneNumber = "+49" + phoneNumber.substring(1);
      } else if (vietnamPhoneNumberRegex.test(phoneNumber)) {
        formattedPhoneNumber = "+84" + phoneNumber.substring(1);
      }
    } else {
      formattedPhoneNumber = phoneNumber;
    }
  }
  return formattedPhoneNumber;
};
