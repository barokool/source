import React, { useCallback, useEffect, useMemo, useState } from "react";
import { RouteComponentProps } from "react-router";
import { toast } from "react-toastify";
import i18n, { t } from "language";

import CreateRoleDialog from "./CreateRoleDialog";
import {
  Button,
  Description,
  RoleDelegationContainer,
  TableBox,
  Title,
  SearchBox,
} from "./styles";

import EmptyImage from "assets/images/product/ProductListEmpty.png";
import { getQueryFromLocation } from "common/functions";
import ListDataLayout from "layouts/ListDataLayout";
import ActionButtons from "designs/ActionButton";
import Table, { IColumns } from "designs/Table";

import { fragmentGetAllRole } from "services/role";
import { Role, useDeleteRoleById, useGetAllRole } from "apiCaller";
import { usePage } from "hooks/usePage";
import { useLoading } from "hooks/useLoading";
import useAuth from "hooks/useAuth";
import Empty from "components/Empty";

interface RoleDelegationProps extends RouteComponentProps {}

const SIZE_PER_PAGE = 10;
const LOAD_DATA = "LOAD_DATA";

const RoleDelegation: React.FC<RoleDelegationProps> = ({ location }) => {
  const { accountInfo } = useAuth();
  const [page, setPage] = usePage(getQueryFromLocation(location)?.page);
  const { startLoading, stopLoading } = useLoading();
  const [search, setSearch] = useState<string>();
  const [getAllRole, { data: getData, error: getError }] =
    useGetAllRole(fragmentGetAllRole);
  const [deleteRole, { data: deleteData, error: deleteError }] =
    useDeleteRoleById();

  const { results, totalCount } = getData?.getAllRole || {};

  const handleChangePage = useCallback((nextPage: number) => {
    setPage(nextPage);
  }, []);

  useEffect(() => {
    invokeSetAllDataAPI();
  }, [page, search]);

  useEffect(() => {
    if (!getData && getError) {
      toast.error(getError.message);
    }
  }, [getData, getError]);

  useEffect(() => {
    if (!deleteData && deleteError) {
      toast.error(deleteError.message);
    }
    if (deleteData && !deleteError) {
      toast.success(t("common.delete-success"));
    }
  }, [deleteData, deleteError]);

  const invokeSetAllDataAPI = (text: string = "") => {
    try {
      startLoading(LOAD_DATA);
      getAllRole({
        variables: {
          filterRole: {
            idBranch: accountInfo?.userInfo?.branches?.[0]?._id as string,
            position: text,
          },
        },
      });
    } catch (err) {
    } finally {
      stopLoading(LOAD_DATA);
    }
  };

  const columns: IColumns = useMemo(
    () => [
      {
        text: t("configuration.role-delegation.position"),
        dataField: "position",
      },
      {
        text: t("configuration.role-delegation.role"),
        dataField: "name",
      },
      {
        text: t("common.actions"),
        dataField: "",
        formatter: (_: string, record: Role) => renderAction(record),
      },
    ],
    [page, i18n.language],
  );

  const handleDelete = (record: Role) => {
    try {
      startLoading(LOAD_DATA);
      deleteRole({
        variables: {
          id: record._id as string,
        },
      });
    } catch (error) {
    } finally {
      stopLoading(LOAD_DATA);
      invokeSetAllDataAPI();
    }
  };

  const renderAction = (record: Role) => {
    return (
      <ActionButtons
        buttons={{
          delete: {
            title: t("common.delete"),
            message: t("configuration.role-delegation.confirm-delete"),
            onDelete: () => handleDelete(record),
          },
          edit: {
            DialogContent: ({ onClose }) => {
              return (
                <CreateRoleDialog
                  onClose={onClose}
                  open
                  onSuccess={() => {
                    invokeSetAllDataAPI();
                  }}
                  editField={record}
                  title={t("product.product-list.edit-product")}
                />
              );
            },
          },
        }}
      />
    );
  };

  return totalCount === 0 ? (
    <Empty
      SVG={<img src={EmptyImage} width={200} height={200} />}
      Title={t("configuration.role-delegation.empty.title")}
      SubTitle={t("configuration.role-delegation.empty.subtitle")}
      ButtonMenu={
        <CreateRoleDialog
          title={t("configuration.role-delegation.add-role")}
          ButtonMenu={
            <Button primary>
              {t("configuration.role-delegation.add-role")}
            </Button>
          }
          onSuccess={() => invokeSetAllDataAPI()}
        />
      }
    />
  ) : (
    <ListDataLayout
      title={t("configuration.role-delegation.role-delegation")}
      leftTopBar={<SearchBox onFetchData={invokeSetAllDataAPI} />}
      rightTopBar={
        <CreateRoleDialog
          title={t("configuration.role-delegation.add-role")}
          ButtonMenu={
            <Button primary>
              {t("configuration.role-delegation.add-role")}
            </Button>
          }
          onSuccess={() => invokeSetAllDataAPI()}
        />
      }
    >
      <RoleDelegationContainer>
        <Title>
          {t(
            "configuration.role-delegation.role-management-and-role-delegation",
          )}
        </Title>
        <Description>
          {t(
            "configuration.role-delegation.role-management-and-role-delegation-detail",
          )}
        </Description>
        <TableBox>
          <Table
            columns={columns}
            data={results || []}
            totalSize={totalCount as number}
            sizePerPage={SIZE_PER_PAGE}
            onPageChange={handleChangePage}
            page={page}
            isRemote
          />
        </TableBox>
      </RoleDelegationContainer>
    </ListDataLayout>
  );
};

export default RoleDelegation;

//missing empty
//missing i18n
