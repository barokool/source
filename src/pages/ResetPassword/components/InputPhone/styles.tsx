import styled from "styled-components/macro";
import tw from "twin.macro";
import _Button from "designs/Button";
import { Form as _Form } from "formik";

export const InputPhoneContainer = styled.div`
  ${tw``}
`;

export const InputPhoneWrapper = styled.div`
  ${tw``}
`;

export const Form = styled(_Form)`
  ${tw`flex flex-col gap-1`}
`;

export const OptionWrapper = styled.div`
  ${tw`flex gap-x-1 items-center w-full justify-between`}
`;

export const OptionTitle = styled.h3`
  ${tw`text-14 text-gray`}
`;

export const Button = styled(_Button)`
  ${tw`w-full flex items-center bg-secondary justify-center text-16 font-medium text-white py-1 mt-2`}
`;
