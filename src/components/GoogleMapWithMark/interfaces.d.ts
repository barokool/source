export type ILatLng = google.maps.LatLngLiteral;

export type IMark = {
  position: ILatLng;
};

export interface IGoogleMapsProps {
  className?: string;
  center?: ILatLng;
  zoom?: number;
  marks?: IMark[];
}
