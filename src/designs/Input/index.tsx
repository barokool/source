import { useEffect, useState } from "react";

import SVG from "designs/SVG";

import FormControlLabel from "common/styles/FormControlLabel";
import FormControlErrorHelper from "common/styles/FormControlErrorHelper";

import { Container, Icon, InputWrapper, Input, PasswordIcon } from "./styles";

interface IInputProps extends React.InputHTMLAttributes<HTMLInputElement> {
  onChangeValue?: (e: string) => void;
  classNameInput?: string;
  error?: string;
  touched?: boolean;
  ref?: React.MutableRefObject<string>;
  icon?: string;
  label?: string;
  placeholder?: string;
  required?: boolean;
  disabled?: boolean;
  value?: string;
  id?: string;
  isHidePassword?: boolean;
  type?: string;
  setHidePassword?: () => void;
}

const InputBox: React.FC<IInputProps> = props => {
  const {
    className = "",
    classNameInput = "",
    error,
    touched,
    icon,
    ref,
    onChangeValue,
    onChange,
    disabled = false,
    label = "",
    placeholder = "",
    required,
    value,
    id,
    isHidePassword,
    setHidePassword,
    type,
    ...restProps
  } = props;

  const [isError, setIsError] = useState<boolean>(false);

  useEffect(() => {
    if (touched && error) {
      !isError && setIsError(true);
    } else {
      isError && setIsError(false);
    }
  }, [touched, error]);

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    onChange?.(e);
    onChangeValue && onChangeValue(e.target.value);
  };
  return (
    <Container className={className}>
      {label && (
        <FormControlLabel required={required} isError={isError} htmlFor={id}>
          {label}
        </FormControlLabel>
      )}
      {icon && (
        <Icon>
          <SVG name={icon} className="fill-current text-primary" width="20" />
        </Icon>
      )}
      <InputWrapper error={isError}>
        <Input
          id={id}
          className={classNameInput}
          icon={!!icon}
          disabled={disabled}
          ref={ref}
          onChange={handleChange}
          placeholder={placeholder}
          value={value}
          type={isHidePassword ? "password" : type}
          {...(restProps as any)}
        />
        {isHidePassword !== undefined && (
          <PasswordIcon
            onClick={setHidePassword}
            name={`common/${
              isHidePassword ? "show-password" : "hide-password"
            }`}
            width={24}
            height={24}
          />
        )}
      </InputWrapper>
      {error && touched && (
        <FormControlErrorHelper>{error}</FormControlErrorHelper>
      )}
    </Container>
  );
};

export default InputBox;
