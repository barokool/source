import styled, { css } from "styled-components/macro";
import tw from "twin.macro";

export const CheckboxButtonContainer = styled.div``;

export const CheckboxButtonBox = styled.div<{
  vertical: boolean;
  colNumber: number;
}>`
  ${({ vertical, colNumber }) =>
    vertical && colNumber === 0 && tw`flex justify-between flex-wrap`}

  ${({ vertical, colNumber }) =>
    vertical &&
    colNumber !== 0 &&
    css`
      display: grid;
      gap: 20px 0;
      grid-template-columns: repeat(${colNumber}, 1fr);
    `}
`;

export const Wrapper = styled.div<{ disabled?: boolean }>`
  ${tw`w-max`}
  ${({ disabled }) => disabled && tw`pointer-events-none text-neutral-3`}
`;

export const Label = styled.label`
  ${tw`cursor-pointer flex flex-col-reverse justify-center items-center text-16 font-regular`}
`;

export const StyleRadioButton = styled.div<{
  checked: boolean;
  isError: boolean;
  disabled?: boolean;
}>`
  ${tw`relative w-[20px] h-[20px] rounded flex items-center justify-center border-[2px] border-primary`}
  ${({ isError }) => isError && tw`border border-danger`}
  ${({ disabled }) => disabled && tw`border-neutral-3`}
  & svg {
    ${tw`absolute w-full h-full duration-300`}
    ${({ disabled }) => (disabled ? tw`bg-neutral-3` : tw`bg-primary`)}
    ${({ checked }) =>
      checked ? tw`visible opacity-100` : tw`invisible opacity-0`}
  }
`;

export const Input = styled.input`
  ${tw`hidden`}
`;

export const Title = styled.p`
  ${tw`mb-1 text-left text-16 font-medium`}
`;
