/**
 * @TODO
 *  [ ] convert svg to jsx - Come here: https://magic.reactjs.net/htmltojsx.html
 *  [ ] replace all fill="..." to  fill="currentColor" (except fill=none)
 *  [ ] add {...props} to <svg >
 */

import { IIconSVGProps } from "typings";

const DecreaseToppingIcon: React.FC<IIconSVGProps> = props => {
  return (
    <svg
      width={26}
      height={26}
      viewBox="0 0 26 26"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <rect
        x={1}
        y={1}
        width={24}
        height={24}
        rx={8}
        fill="white"
        stroke="#EEEEEE"
      />
      <path
        d="M16.3161 12.25C16.6938 12.25 17 12.5858 17 13C17 13.4142 16.6938 13.75 16.3161 13.75H9.68394C9.30621 13.75 9 13.4142 9 13C9 12.5858 9.30621 12.25 9.68394 12.25H16.3161Z"
        fill="#646464"
      />
    </svg>
  );
};

export default DecreaseToppingIcon;
