import { combineReducers } from "@reduxjs/toolkit";
import { History } from "history";
import { connectRouter } from "connected-react-router";

import _config from "./_config";
import auth from "./auth";
import common from "./common";
import order from "./order";
import language from "./language";
import sellingPage from "./sellingPage";
import resetPassword from "./resetPassword";
export function createRootReducer(history: History) {
  return combineReducers({
    _config,
    auth,
    common,
    order,
    language,
    sellingPage,
    resetPassword,
    router: connectRouter(history),
  });
}
