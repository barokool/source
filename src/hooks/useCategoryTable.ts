import { useCallback, useEffect } from "react";
import { useGetAllTableType } from "apiCaller";
import { fragmentGetAllTableType } from "services/table";

export const useTableType = () => {
  const [getAllTableType, { data, loading, error }] = useGetAllTableType(
    fragmentGetAllTableType,
  );
  useEffect(() => {
    invokeGetAllTableType();
  }, []);
  const invokeGetAllTableType = useCallback(async () => {
    getAllTableType({ variables: {} });
  }, []);

  return {
    tableType: data,
    loading,
    error,
    invokeGetAllTableType,
  };
};
