import styled, { css } from "styled-components/macro";
import tw from "twin.macro";

export const formControlCommon = (
  isError: boolean | undefined,
  disabled: boolean | undefined,
) =>
  css`
    ${tw`
      w-full 
      rounded-sm
      border
      border-solid
      border-line
      bg-white
      px-1.5
      h-4
      focus-within:border-black
      text-14
      font-medium
      text-black
      placeholder-gray
    `}
    ${!isError
      ? tw`border-gray focus:border-black group-focus:border-black `
      : tw`border-danger focus:border-secondary group-focus:border-secondary`}
    ${disabled && tw`pointer-events-none opacity-60`}
  `;
