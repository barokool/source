import SVG from "designs/SVG";

//redux
import { useSelector } from "react-redux";
import { IRootState } from "typings";

//languages
import { t } from "language";

//locals
import {
  CodeTagWrapper,
  EmptyContainer,
  ContentWrapper,
  Description,
  Message,
  EmptyWrapper,
  CodeTag,
} from "./styles";

interface IEmptyProps {}

const Empty: React.FC<IEmptyProps> = props => {
  const { orderSelected } = useSelector((state: IRootState) => state.order);
  return (
    <EmptyContainer>
      <CodeTagWrapper>
        <CodeTag>#{orderSelected?.barcode}</CodeTag>
      </CodeTagWrapper>
      <EmptyWrapper>
        <SVG
          name="selling/restaurant"
          width={200}
          height={200}
          className="mx-auto my-auto"
        />
        <ContentWrapper>
          <Message>{t("selling.empty-title")}</Message>
          <Description>{t("selling.empty-message")}</Description>
        </ContentWrapper>
      </EmptyWrapper>
    </EmptyContainer>
  );
};

export default Empty;
