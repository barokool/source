import React, { useMemo, useState } from "react";
import Table, { IColumns } from "designs/Table";

//redux
import { useSelector } from "react-redux";
import { IRootState } from "typings";

//api
import { OrderItem, Product, ProductUnit } from "apiCaller";

//language
import { t } from "language";

//locals
import {
  LeftReviewBillContainer,
  ProductThumbnail,
  CodeTag,
  CodeTagWrapper,
} from "./styles";

interface ILeftReviewBill {}

const SIZE_PER_PAGE = 16;

const LeftReviewBill: React.FC<ILeftReviewBill> = () => {
  const { orderSelected } = useSelector((state: IRootState) => state.order);
  const listItem = (orderSelected && orderSelected.orderItems) || [];
  const [page, setPage] = useState<number>(0);

  const columns: IColumns = useMemo(
    () => [
      {
        text: t("selling.product.code"),
        dataField: "product",
        formatter: (product: Product) => product.barcode || "SP04",
      },
      {
        text: t("selling.product.name"),
        dataField: "product",
        formatter: (product: Product) => product.name,
      },
      {
        text: t("selling.product.image"),
        dataField: "product",
        formatter: (product: Product) => (
          <ProductThumbnail
            src={
              (product.thumbnail?.small as string) ||
              (product.thumbnail?.medium as string) ||
              (product.thumbnail?.default as string)
            }
          />
        ),
      },
      {
        text: t("selling.product.size"),
        dataField: "unit",
        formatter: (unit: ProductUnit) => unit.name,
      },

      {
        text: t("selling.quantity"),
        dataField: "quantity",
      },
      {
        text: t("selling.price"),
        dataField: "product",
        formatter: (product: Product) => product.price,
      },
      {
        text: t("selling.product.total-price"),
        dataField: "totalPrice",
        formatter: (totalPrice: number) => totalPrice?.toString().prettyMoney(),
      },
      {
        text: t("common.actions"),
        dataField: "",
        headerStyle: () => ({ display: "none" }),
      },
    ],
    [page],
  );

  return (
    <LeftReviewBillContainer>
      <CodeTagWrapper>
        <CodeTag>{orderSelected?.barcode}</CodeTag>
      </CodeTagWrapper>
      <Table
        columns={columns}
        data={listItem as OrderItem[]}
        totalSize={listItem?.length as number}
        sizePerPage={SIZE_PER_PAGE}
        onPageChange={() => {}}
        isRemote
        page={page}
      />
    </LeftReviewBillContainer>
  );
};

export default LeftReviewBill;
