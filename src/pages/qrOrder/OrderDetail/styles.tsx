import styled from "styled-components/macro";
import tw from "twin.macro";
import _Button from "designs/Button";

export const OrderDetailContainer = styled.div`
  ${tw`w-full  `}
`;

export const Card = {
  Container: styled.div`
    ${tw`w-full mx-auto`}
  `,
  ContentContainer: styled.div`
    ${tw`px-2 py-1.5 mx-auto flex w-full`}
  `,
  LeftContentContainer: styled.div`
    ${tw`w-2/3 grid gap-y-0.5`}
  `,
  Label: styled.h3`
    ${tw`text-black text-16 font-bold`}
  `,
  Description: styled.p`
    ${tw`text-body-color text-13`}
  `,
  Price: styled.h3`
    ${tw`text-primary text-20 font-bold w-1/3 text-right `}
  `,
};

export const RatioGroup = {
  Container: styled.div`
    ${tw`px-2 py-1.5`}
  `,
  Label: styled.h3`
    ${tw`text-black text-16 font-bold mb-1`}
  `,
};

export const AddNote = {
  Container: styled.div`
    ${tw`py-2.5 px-2 mt-1 `}
  `,
  Label: styled.div`
    ${tw`text-20 text-black font-bold mb-1`}
  `,
};

export const RadioItemLeftWrapper = styled.div`
  ${tw`flex items-center`}
`;

export const RadioItem = {
  Container: styled.button`
    ${tw`flex items-center justify-between w-full`}
  `,
  LeftWrapper: styled.div`
    ${tw`flex items-center`}
  `,

  Label: styled.h3`
    ${tw`text-13 text-black`}
  `,
};

export const Point = styled.div`
  ${tw`p-[2px] mx-1 flex items-center justify-center w-2 h-2 overflow-hidden border border-solid rounded-full outline-none cursor-pointer  border-primary `}
`;

export const CustomRadio = styled.p<{ active: boolean }>`
  ${tw`w-full h-full duration-300 rounded-full cursor-pointer`}
  ${({ active }) => (active ? tw`bg-primary` : tw`bg-transparent`)}
`;

export const BoxPoint = styled.div`
  ${tw`w-max`}
`;

export const RightContent = styled.p`
  ${tw`text-black text-13`}
`;

export const CheckOutButtonWrapper = styled.div`
  ${tw`px-2`}
`;
