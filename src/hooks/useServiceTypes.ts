import { ServiceType, useGetAllServiceType } from "apiCaller";
import { useState, useCallback, useEffect } from "react";
import { fragmentGetAllServiceType } from "services/serviceType";
export const useServiceTypes = () => {
  const [getServiceType, { data }] = useGetAllServiceType(
    fragmentGetAllServiceType,
  );

  useEffect(() => {
    invokeSetAllServiceTypes();
  }, []);
  const invokeSetAllServiceTypes = useCallback(() => {
    getServiceType({
      variables: {},
    });
  }, []);

  return {
    ServiceType: data,
    invokeSetAllServiceTypes,
  };
};
