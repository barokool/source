import SVG from "designs/SVG";
import { PATH } from "common/constants/routes";

//hooks
import { useRedirect } from "hooks/useRedirect";

//locals
import { StatisticContainer, Items } from "./styles";

//languages
import { t } from "language";

interface IStatisticProps {
  revenue: number;
  inPlace: number;
  delivery: number;
  orderTable: number;
}

const Statistic: React.FC<IStatisticProps> = props => {
  const { revenue, inPlace, delivery, orderTable } = props;
  const redirect = useRedirect();

  return (
    <StatisticContainer>
      <Items.Wrapper onClick={() => redirect(PATH.ORDER.SELF)}>
        <Items.Top>
          <SVG name="dashboard/profit" height={40} width={40} />
          <Items.Title>{t("overview.revenue")}</Items.Title>
        </Items.Top>
        <Items.Bottom>{revenue} €</Items.Bottom>
      </Items.Wrapper>
      <Items.Wrapper onClick={() => redirect(PATH.ORDER.SELL_AT_STORE)}>
        <Items.Top>
          <SVG name="dashboard/instore" height={40} width={40} />
          <Items.Title>{t("overview.sells-at-store")}</Items.Title>
        </Items.Top>
        <Items.Bottom>{inPlace}</Items.Bottom>
      </Items.Wrapper>
      <Items.Wrapper onClick={() => redirect(PATH.ORDER.DELIVERY)}>
        <Items.Top>
          <SVG name="dashboard/delivery" height={40} width={40} />
          <Items.Title>{t("overview.table.delivery")}</Items.Title>
        </Items.Top>
        <Items.Bottom>{delivery}</Items.Bottom>
      </Items.Wrapper>
      <Items.Wrapper onClick={() => redirect(PATH.ORDER.BOOKING)}>
        <Items.Top>
          <SVG name="dashboard/booking" height={40} width={40} />
          <Items.Title>{t("overview.table.order-table")}</Items.Title>
        </Items.Top>
        <Items.Bottom>{orderTable}</Items.Bottom>
      </Items.Wrapper>
    </StatisticContainer>
  );
};

export default Statistic;
