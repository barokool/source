import { useEffect, useState } from "react";
import OtpInput from "react-otp-input";
import { FormikTouched, FormikErrors, FormikValues } from "formik";

import { OTPInputContainer } from "./styles";

interface IOTPInputProps {
  value?: string;
  onChange?: (value: string) => void;
  quantity?: number;
  error?:
    | string
    | string[]
    | FormikErrors<FormikValues>
    | FormikErrors<FormikValues>[];
  touched?:
    | boolean
    | FormikTouched<FormikValues>
    | FormikTouched<FormikValues>[];
}

const OTPInput: React.FC<IOTPInputProps> = props => {
  const { value, onChange, quantity = 6, error, touched, ...rest } = props;

  const [isError, setIsError] = useState<boolean>(false);

  useEffect(() => {
    if (touched && error) {
      !isError && setIsError(true);
    } else {
      isError && setIsError(false);
    }
  }, [touched, error]);

  const handleChange = (value: string) => {
    onChange && onChange(value);
  };

  const inputStyle = `otp-input border-none outline-none text-secondary bg-neutral-4 
                      selection:bg-primary selection:text-white 
                      focus:border focus:border-solid focus:border-secondary`;

  return (
    <OTPInputContainer>
      <OtpInput
        isInputNum={true}
        value={value}
        numInputs={quantity}
        onChange={handleChange}
        hasErrored={isError}
        inputStyle={inputStyle}
        containerStyle="flex gap-1"
        errorStyle="otp-error"
        {...(rest as any)}
      />
    </OTPInputContainer>
  );
};

export default OTPInput;
