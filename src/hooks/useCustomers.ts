import { useState, useCallback, useEffect } from "react";
import { User, useGetAllUser, RoleEnum } from "apiCaller";
import { fragmentGetAllUser } from "services/user";

import useAuth from "./useAuth";

export const useCustomers = () => {
  const [listCustomer, setListCustomer] = useState<User[]>([]);
  const [totalCount, setTotalCount] = useState(0);
  const [getAllCustomer, { data, loading, called }] =
    useGetAllUser(fragmentGetAllUser);
  const { accountInfo } = useAuth();

  useEffect(() => {
    invokeGetAllCustomers();
  }, []);

  const invokeGetAllCustomers = useCallback(async () => {
    await getAllCustomer({
      variables: {
        filterUser: {
          role: "CUSTOMER" as RoleEnum,
          branches: accountInfo?.userInfo?.branches?.map(
            branch => branch._id,
          ) as string[],
        },
      },
    });
    setListCustomer(data?.getAllUser.results || []);
  }, []);

  return {
    listCustomer,
    setListCustomer,
    invokeGetAllCustomers,
    totalCount,
  };
};
