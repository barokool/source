import { useBreadcrumb } from "hooks/useBreadcrumb";
import { ChangeButtonWrapper, GeneralSettingsLayoutContainer } from "./styles";
import { PATH } from "common/constants/routes";

interface IGeneralSettingsLayoutProps {
  settingChildSelected?: number;
}

const GeneralSettingsLayout: React.FC<IGeneralSettingsLayoutProps> = props => {
  const { children } = props;
  useBreadcrumb([
    {
      name: "Cấu hình",
    },
  ]);
  return (
    <GeneralSettingsLayoutContainer>
      <div>{props.settingChildSelected}</div>
      <div>{children}</div>
    </GeneralSettingsLayoutContainer>
  );
};

export default GeneralSettingsLayout;
