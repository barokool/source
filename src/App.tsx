import React, { useEffect, useLayoutEffect } from "react";
import { History } from "history";
import Routes from "routes";
import { ConnectedRouter } from "connected-react-router";

import { useSelector } from "react-redux";
import { IRootState } from "redux/storeToolkit";
import { useTranslation } from "react-i18next";

import "common/scss/index.scss";
import "react-responsive-carousel/lib/styles/carousel.min.css";
interface IAppProps {
  history: History;
}

const App: React.FC<IAppProps> = ({ history }) => {
  const { i18n } = useTranslation();
  const { languageSelected } = useSelector(
    (state: IRootState) => state.language,
  );

  useEffect(() => {
    if (languageSelected) {
      i18n.changeLanguage(languageSelected.lng);
    }
  }, [languageSelected]);

  return (
    <ConnectedRouter history={history}>
      <Routes />
    </ConnectedRouter>
  );
};

export default App;
