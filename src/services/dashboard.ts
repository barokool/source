import { Dashboard, GenFields } from "apiCaller";

export const something = () => {};
export const fragmentGetDashBoard: GenFields<Dashboard> = [
  "totalOrderBookingTable",
  "totalOrderDelivery",
  "totalOrderInPlace",
  "revenueTotal",
  {
    recentOrderDelivery: [
      "_id",
      "cancellationReason",
      "code",

      "customerName",
      "customerPay",
      "excessMoney",
      "paymentType",
      "phoneNumber",
      "phoneNumber",
      "shippingTime",
      "status",
      "type",
      { coupon: ["_id", "name", {}] },
      {
        deliveryAddress: [
          "displayName",
          "phoneNumber",
          "type",
          "note",
          { city: ["_id", "name"] },
          { district: ["_id", "name"] },
          { state: ["_id", "name"] },
        ],
      },
      { driver: ["_id", "displayName", "phoneNumber", {}] },
      { fromBranch: ["_id", "name", {}] },
      { orderItems: ["_id", "quantity"] },
      { price: ["finalPrice", "discount", "shippingFee"] },

      {},
    ],
  },
  {
    recentOrderBookingTable: [
      "_id",
      "cancellationReason",
      "code",

      "customerName",
      "customerPay",
      "excessMoney",
      "paymentType",
      "phoneNumber",
      "phoneNumber",
      "shippingTime",
      "status",
      "type",
      { coupon: ["_id", "name", {}] },
      {
        deliveryAddress: [
          "displayName",
          "phoneNumber",
          "type",
          "note",
          { city: ["_id", "name"] },
          { district: ["_id", "name"] },
          { state: ["_id", "name"] },
        ],
      },
      { driver: ["_id", "displayName", "phoneNumber", {}] },
      { fromBranch: ["_id", "name", {}] },
      { orderItems: ["_id", "quantity"] },
      { price: ["finalPrice", "discount", "shippingFee"] },

      {},
    ],
  },
];
