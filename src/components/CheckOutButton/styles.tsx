import styled from "styled-components/macro";
import tw from "twin.macro";

export const CheckOutButtonContainer = styled.div`
  ${tw`cursor-pointer flex justify-between w-full bg-primary px-1.5 rounded-sm items-center h-5 mt-2`}
`;

export const LeftButtonContainer = styled.div`
  ${tw`flex items-center`}
`;

export const RightButtonContainer = styled.div`
  ${tw``}
`;
