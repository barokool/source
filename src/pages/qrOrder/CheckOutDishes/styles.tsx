import styled from "styled-components/macro";
import tw from "twin.macro";

export const CheckOutDishesContainer = styled.div`
  ${tw`my-4.5 px-2 py-1.5`}
`;

export const HeaderContainer = styled.div`
  ${tw`flex laptop:w-2/5  justify-between`}
`;

export const HeaderLabel = styled.h3`
  ${tw`text-16 text-black font-bold`}
`;

export const BodyContainer = styled.div`
  ${tw`mt-4`}
`;

export const ButtonsWrapper = styled.div`
  ${tw`flex justify-between w-8 items-center `}
`;

export const ChangeQuantityButton = styled.button`
  ${tw`p-1 border-line text-black font-semibold text-12 border-2  items-center text-center rounded-sm`}
`;

export const QuantityContent = styled.h3`
  ${tw`text-black text-14 font-semibold`}
`;

export const LeftButtonContent = styled.h3`
  ${tw`text-white text-13 mr-1`}
`;

export const RightButtonContent = styled.h3`
  ${tw`text-white text-16 font-bold`}
`;
