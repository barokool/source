import React, { useState, useEffect } from "react";
import ImageCropper from "components/ImageCropper";
import { IBase64Image, IImage, IImageInput } from "typings";
import ImageUploadLayout from "layouts/ImageUpload";
import {
  SingleImageUploaderContainer,
  HiddenInput,
  ImageUploadContainer,
  PreviewImage,
  SkeletonContainer,
  SkeletonMessage,
} from "./styles";
import { useField, useFormikContext } from "formik";
import UploadImageIcon from "icons/UploadImage";
import FormControlLabel from "common/styles/FormControlLabel";
import FormControlErrorHelper from "common/styles/FormControlErrorHelper";

import { toast } from "react-toastify";
import { getCustomSizeImagesFromFile } from "services/uploader";

interface ISingleImageUploaderProps {
  className?: string;
  name: string;
  image: IImage | null;
  customSize: IImageInput;
  label: string;
  subLabel?: string;
  required?: boolean;
  onChange?: (customSizeImages: IImage) => void;
  text?: string;
  /**
   * @example aspect={16/9}
   */
  aspect?: number;
  isCropImage?: boolean;
}

const SingleImageUploader: React.FC<ISingleImageUploaderProps> = props => {
  const {
    image,
    name = "",
    label = "",
    text = "Drag your image here",
    subLabel = "(File with extension .jpg .jpeg .png .gif and size <1MB)",
    customSize,
    aspect = 1,
    required = false,
    onChange,
    isCropImage = true,
    className = "",
  } = props;

  const [isLoading, setIsLoading] = useState(false);
  const [fileSelected, setFileSelected] = useState<File | undefined>();
  const [displayImage, setDisplayImage] = useState<IBase64Image | string>("");
  const [croppedFile, setCroppedFile] = useState<File | null>(null);
  const [openCropImage, setOpenCropImage] = useState(false);

  const { setFieldValue } = useFormikContext();
  const [field, meta] = useField(name);
  const isError: boolean = Boolean(!!meta.error && !!meta.touched);

  useEffect(() => {
    if (!croppedFile) {
      const imageUrl = image?.small || "";
      setDisplayImage(imageUrl);
      setFieldValue(name, image ? JSON.stringify(image) : "");
    }
  }, [image]);

  const handleUploadRawImage = (files: File[]) => {
    if (!files) return;

    const file = files[0];
    setFileSelected(file);
    if (!isCropImage) {
      loadImage(file);
    } else {
      setOpenCropImage(true);
    }
  };

  const handleCloseImageCropper = () => {
    setOpenCropImage(false);
  };

  const loadImage = async (file: File) => {
    const customSizeImage = await getCustomSizeImagesFromFile(file, customSize);
    setFieldValue(name, JSON.stringify(customSizeImage));
    onChange && onChange(customSizeImage);
    setDisplayImage(customSizeImage?.small || "");
  };

  const handleCroppedImage = async (file: File) => {
    setOpenCropImage(false);
    setCroppedFile(file);
    const loadingId = toast.loading(`Uploading ${name} image...`);
    setIsLoading(true);

    console.log("Before upload");

    try {
      const customSizeImage = await getCustomSizeImagesFromFile(
        file,
        customSize,
      );

      console.log({ customSizeImage });

      toast.update(loadingId, {
        render: `Upload ${name} image successfully`,
        type: "success",
        isLoading: false,
        autoClose: 3000,
      });

      setFieldValue(name, JSON.stringify(customSizeImage));
      setDisplayImage(customSizeImage?.small || "");

      onChange && onChange(customSizeImage);
    } catch (error) {
      toast.update(loadingId, {
        render: `Upload ${name} image fail!`,
        type: "error",
        isLoading: false,
      });
      console.error(error);
    } finally {
      setIsLoading(false);
    }
  };
  return (
    <>
      <SingleImageUploaderContainer className={className}>
        <FormControlLabel isError={isError} required={required}>
          {label}
        </FormControlLabel>
        <ImageUploadLayout onUpload={handleUploadRawImage}>
          <ImageUploadContainer isError={isError}>
            {displayImage ? (
              <PreviewImage
                src={displayImage}
                alt="img-display"
                height="250px"
                width="auto"
              />
            ) : (
              <SkeletonContainer>
                <UploadImageIcon />
                <SkeletonMessage>{text}</SkeletonMessage>
              </SkeletonContainer>
            )}
          </ImageUploadContainer>
        </ImageUploadLayout>

        {isError && (
          <FormControlErrorHelper>{meta.error}</FormControlErrorHelper>
        )}

        <HiddenInput {...field} />
      </SingleImageUploaderContainer>
      <ImageCropper
        aspect={aspect}
        image={fileSelected}
        isOpen={openCropImage}
        onClose={handleCloseImageCropper}
        onConfirm={handleCroppedImage}
      />
    </>
  );
};

export default SingleImageUploader;
