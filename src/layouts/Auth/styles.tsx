import styled from "styled-components/macro";
import tw from "twin.macro";

import backgroundImage from "assets/images/login/background.png";

export const LayoutContainer = styled.div`
  ${tw`grid laptop:grid-cols-2 w-full h-full bg-white`}
`;

export const BackgroundWrapper = styled.div`
  ${tw`w-full pt-[165px] laptop:block hidden bg-[#faf4e2] `}
`;

export const BackgroundContent = styled.div`
  background: url("${backgroundImage}") center bottom no-repeat;
  ${tw`w-full h-full bg-contain`};
`;

export const ContentWrapper = styled.div`
  ${tw`w-full h-full flex items-center justify-center bg-white overflow-y-auto py-2 `}
`;

export const SelectLanguageWrapper = styled.div`
  ${tw`fixed right-2 top-2`}
`;

export const OptionWrapper = styled.div`
  ${tw`flex gap-x-1 items-center w-full justify-between`}
`;

export const OptionTitle = styled.h3`
  ${tw`text-14 text-gray`}
`;
