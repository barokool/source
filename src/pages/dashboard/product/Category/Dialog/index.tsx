import { useEffect, useState } from "react";
import { Formik } from "formik";
import * as Yup from "yup";
import MultipleImageUploader from "components/MultiImageUploaderV2";
import { CUSTOM_SIZE_CATEGORY_LV_2 } from "common/constants/category";
import { DialogTitle } from "common/styles/Title";
import Input from "designs/InputV2";
import Select from "designs/Select";
import Dialog from "components/Dialog";
import RichTextEditor from "designs/RichTextEditor";
import { toast } from "react-toastify";

//languages
import { t } from "language";

//api
import {
  ProductCategory,
  ImageInput,
  BusinessProductType,
  useUpdateProductCategory,
  useCreateProductCategory,
  useGetAllBusinessProductType,
  Image,
} from "apiCaller";
import {
  fragmentCreateProductCategory,
  fragmentUpdateProductCategory,
} from "services/product";
import { fragmentGetAllBusinessProductType } from "services/businessProductType";
import { IImage } from "typings";

//locals
import {
  ElementWrapper,
  ProductCategoryDialogContainer,
  Form,
  ButtonWrapper,
  Button,
  FormGroup,
} from "./styles";

interface UserDialogProps {
  ButtonMenu: React.ReactElement;
  editField?: ProductCategory;
  title?: string;
  onClose?: () => void;
  onSuccess?: () => void;
}

interface IFormValue {
  name: string;
  businessProductType: string;
  description?: string;
}

const validationSchema = Yup.object().shape<{ [key in keyof IFormValue]: any }>(
  {
    name: Yup.string().required(
      t("product.product-category.please-enter-product-category-name"),
    ),
    businessProductType: Yup.string().required(
      t("product.product-category.please-enter-original-product-category"),
    ),
  },
);

const ProductCategoryDialog: React.FC<UserDialogProps> = ({
  ButtonMenu,
  editField,
  title,
  onSuccess,
}) => {
  //local states
  const [initialValues, setInitialValues] = useState<IFormValue>({
    name: "",
    businessProductType: "",
  });
  const [isOpen, setOpen] = useState(false);
  const [imagesSelected, setImagesSelected] = useState<Image[]>([]);
  const [businessProductTypeSelected, setBusinessProductTypeSelected] =
    useState<BusinessProductType | null>(null);

  //api
  const [
    updateProductCategory,
    {
      data: updateProductCategoryData,
      loading: updateProductCategoryLoading,
      error: updateProductCategoryErrors,
    },
  ] = useUpdateProductCategory(fragmentUpdateProductCategory);
  const [
    createProductCategory,
    {
      data: createProductCategoryData,
      loading: createProductCategoryLoading,
      error: createProductCategoryErrors,
    },
  ] = useCreateProductCategory(fragmentCreateProductCategory);
  const [listBusinessProductType, setListBusinessProductType] = useState<
    BusinessProductType[]
  >([]);
  const [getAllBusinessProductType, { data: listBusinessProductTypeReturned }] =
    useGetAllBusinessProductType(fragmentGetAllBusinessProductType);

  useEffect(() => {
    if (editField) {
      setBusinessProductTypeSelected(editField.businessProductType);
      setInitialValues({
        name: editField.name as string,
        businessProductType: editField.businessProductType?._id as string,
        description: editField.description as string,
      });
    }
  }, [editField]);

  useEffect(() => {
    if (createProductCategoryData && !createProductCategoryErrors) {
      toast.success(t("product.product-category.create-success"));
      onSuccess?.();
      setOpen(false);
    }
  }, [createProductCategoryData]);

  useEffect(() => {
    if (updateProductCategoryData && !updateProductCategoryErrors) {
      toast.success(t("product.product-category.update-success"));
      onSuccess?.();
      setOpen(false);
    }
  }, [updateProductCategoryData]);

  useEffect(() => {
    invoketGetAllBusinessProductType();
  }, []);

  useEffect(() => {
    createProductCategoryErrors &&
      toast.error(createProductCategoryErrors.message);
    updateProductCategoryErrors &&
      toast.error(updateProductCategoryErrors.message);
  }, [createProductCategoryErrors, updateProductCategoryErrors]);

  useEffect(() => {
    setListBusinessProductType(
      listBusinessProductTypeReturned?.getAllBusinessProductType
        .results as BusinessProductType[],
    );
  }, [listBusinessProductTypeReturned]);

  const invoketGetAllBusinessProductType = () => {
    getAllBusinessProductType({
      variables: {
        filter: {},
      },
    });
  };

  const handleSubmit = (values: IFormValue) => {
    if (editField) {
      updateProductCategory({
        variables: {
          id: editField?._id as string,
          productCategoryUpdateInput: {
            name: values.name,
            description: values.description,
            businessProductType: businessProductTypeSelected?._id as string,
            images: imagesSelected as ImageInput[],
          },
        },
      });
      return;
    }
    createProductCategory({
      variables: {
        productCategoryCreateInput: {
          name: values.name,
          description: values.description,
          businessProductType: businessProductTypeSelected?._id as string,
          images: imagesSelected as ImageInput[],
        },
      },
    });
  };

  return (
    <>
      <ElementWrapper onClick={() => setOpen(!isOpen)}>
        {ButtonMenu}
      </ElementWrapper>
      <Dialog
        open={isOpen}
        onClose={() => setOpen(false)}
        className="overflow-auto"
      >
        <ProductCategoryDialogContainer>
          <DialogTitle>{title}</DialogTitle>
          <Formik
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={handleSubmit}
            enableReinitialize
          >
            <Form>
              <FormGroup>
                <ElementWrapper className="flex flex-col gap-5">
                  <Input
                    name="name"
                    label={t("product.product-category.name")}
                    placeholder={t(
                      "product.product-category.enter-product-category-name",
                    )}
                    required
                  />
                  <Select
                    label={t("product.product-category.business-type")}
                    name="businessProductType"
                    options={listBusinessProductType}
                    onSelect={value => setBusinessProductTypeSelected(value)}
                    optionSelected={businessProductTypeSelected}
                  />
                  <RichTextEditor
                    name="description"
                    defaultValue={initialValues.description}
                    label={t("product.product-category.description")}
                  />
                  <MultipleImageUploader
                    label={t("product.product-list.upload-image")}
                    images={imagesSelected as IImage[]}
                    name="images"
                    onChange={value => setImagesSelected(value as Image[])}
                    customSize={CUSTOM_SIZE_CATEGORY_LV_2}
                  />
                </ElementWrapper>
              </FormGroup>
              <ButtonWrapper>
                <Button outline type="button" onClick={() => setOpen(false)}>
                  {t("common.cancel")}
                </Button>
                <Button
                  primary
                  type="submit"
                  loading={
                    createProductCategoryLoading || updateProductCategoryLoading
                  }
                >
                  {t("common.accept")}
                </Button>
              </ButtonWrapper>
            </Form>
          </Formik>
        </ProductCategoryDialogContainer>
      </Dialog>
    </>
  );
};

export default ProductCategoryDialog;
