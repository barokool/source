import Breadcrumb from "components/Breadcrumb";
import { useDispatch, useSelector } from "react-redux";
import { IRootState } from "redux/storeToolkit";
import {
  setExtendDrawer,
  setCollapseDrawer,
  toggleExtendDrawer,
} from "redux/slices/_config";
import UserInfo from "./UserInfo";

import {
  HeaderContainer,
  IconWrapper,
  Icon,
  Container,
  Heading,
} from "./styles";
import IconButton from "designs/IconButton";
import HamburgerIcon from "icons/Hamburger";
import { IIconSVGProps } from "typings";
import LOGO from "../../../../assets/images/logo/logo-color.png";
import SearchBox from "./SearchBox";

interface ITopBarProps {}

const LAPTOP_SCREEN = 1024;

const Header: React.FC<ITopBarProps> = (props: ITopBarProps) => {
  const dispatch = useDispatch();

  // const { breadcrumb } = useSelector((state: IRootState) => state._config);
  const { isExtendDrawer, isCollapse } = useSelector(
    (state: IRootState) => state._config,
  );

  return (
    <HeaderContainer
      style={{
        height: 60,
      }}
    >
      <div className="flex gap-2 items-center flex-shrink-0 w-2/3 ">
        <IconButton
          tooltip={isExtendDrawer ? "Close" : "Open"}
          className="flex-shrink-0"
        >
          {!isExtendDrawer ? (
            <HamburgerIcon
              onClick={() => dispatch(toggleExtendDrawer())}
              className="w-2 h-2 flex-shrink-0 text-neutral-1"
            />
          ) : (
            <DeleteIcon
              onClick={() => dispatch(toggleExtendDrawer())}
              className="w-2 h-2 flex-shrink-0 text-neutral-1"
            />
          )}
        </IconButton>
        <img
          src={LOGO}
          className="h-auto hover:cursor-pointer w-[140px] hidden laptop:block"
          alt="Logo"
        />
        <SearchBox />
      </div>
      <Container>
        <UserInfo />
      </Container>
    </HeaderContainer>
  );
};

export default Header;
const DeleteIcon: React.FC<IIconSVGProps> = props => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    fill="none"
    viewBox="0 0 24 24"
    stroke="currentColor"
    strokeWidth={2}
    {...props}
  >
    <path
      strokeLinecap="round"
      strokeLinejoin="round"
      d="M6 18L18 6M6 6l12 12"
    />
  </svg>
);
