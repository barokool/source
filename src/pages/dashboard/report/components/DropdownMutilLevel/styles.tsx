import styled, { css } from "styled-components/macro";
import tw from "twin.macro";

export const DropdownMultiLevelContainer = styled.div`
  ${tw`relative w-max z-10`}
`;
export const List = styled.div`
  ${tw`absolute bg-background shadow-md py-1 right-0`}
`;
export const Box = styled.div`
  ${tw`w-[240px]  `}
`;
export const FilterButton = styled.button`
  ${tw`max-w-[150px] w-[150px] px-2.5 py-[12.5px] font-medium bg-white  border-2 rounded-sm border-secondary`}
`;
export const Main = styled.div`
  ${tw` hover:cursor-pointer `}
`;
export const ContentLevel1 = styled.div<{ active: boolean }>`
  ${tw`relative text-13 font-medium py-1 px-2 border-b-[1px] border-neutral-4 cursor-pointer`}
  &::after {
    content: "";
    ${tw`absolute h-1 w-1 border-t-[2px] border-l-[2px] `}
    top: 50%;
    right: 15px;
    transform: ${({ active }) =>
      active
        ? "translateY(-50%) rotate(45deg)"
        : "translateY(-50%) rotate(-135deg)"};
    transition: all 200ms ease-in;
  }
`;
export const ContentLevel2 = styled.div`
  ${tw`text-13 py-1 px-3  hover:bg-neutral-4 border-b-[1px] border-neutral-4 cursor-pointer`}
`;
