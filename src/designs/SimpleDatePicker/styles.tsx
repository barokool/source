import styled, { css } from "styled-components/macro";
import tw from "twin.macro";

export const Container = styled.div`
  ${tw`w-full `}
`;
export const formControlCommon = (
  isError: boolean | undefined,
  disabled: boolean | undefined,
) =>
  css`
    ${tw`
      w-full 
      rounded-sm
      border
      border-solid
      border-line
      bg-white
      px-0.5
      h-4
      focus-within:border-black
      text-14
      font-medium
      text-black
      placeholder-gray
    `}
    ${!isError
      ? tw`border-gray focus:border-black group-focus:border-black `
      : tw`border-danger focus:border-secondary group-focus:border-secondary`}
    ${disabled && tw`pointer-events-none opacity-60`}
  `;

export const DateInputContainer = styled.label<{
  isError: boolean;
  disabled?: boolean;
}>`
  ${tw`grid items-center`}
  ${({ isError, disabled }) => formControlCommon(isError, disabled)}
  ${({ isError }) => isError && tw`text-black`}
  grid-template-columns: 1fr 24px;
`;

export const InputField = styled.input`
  ${tw`p-0 font-medium text-secondary text-14 placeholder-neutral-3 focus:outline-none focus:ring-transparent`}
`;
