import styled from "styled-components/macro";
import tw from "twin.macro";
import { Form as _Form } from "formik";
import _Button from "designs/Button";
import _SearchBox from "components/SearchBoxTable";
import AddButton from "designs/AddButton";

export const AccountInfoContainer = styled.div`
  /* laptop:p-5 */
  ${tw` bg-white phone:p-2	  `}
`;

export const Title = styled.div`
  ${tw`text-26 text-black font-bold`}
`;

export const AccountInfoWrapper = styled.div`
  ${tw`grid grid-cols-2 gap-3`}
`;

export const LeftSideContainer = styled.div`
  ${tw`w-full grid gap-y-4`}
`;

export const LeftSideTitle = styled.h2`
  ${tw`text-16 text-black font-semibold`}
`;

export const RightSideContainer = styled.div`
  ${tw`w-full`}
`;

export const RightSideTitle = styled.h2`
  ${tw`text-16 text-black font-semibold`}
`;

export const Form = styled(_Form)`
  ${tw``}
`;

export const ButtonWrapper = styled.div`
  ${tw`flex justify-end mt-4`}
`;

export const Button = styled(_Button)`
  ${tw`py-1 px-2 w-max flex justify-center ml-1`}
`;
export const ActionButton = styled(AddButton)`
  ${tw`py-1 px-2 w-max flex justify-center ml-1`}
`;
export const DialogTitle = styled.p`
  ${tw`text-20 phone:text-26 font-semibold mb-1.5 phone:mb-3 text-left`}
`;
