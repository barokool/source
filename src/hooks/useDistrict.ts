import { GetAllDistrictArgs, useGetAllDistrict } from "apiCaller";
import React, { useEffect } from "react";
import { fragmentGetAllDistricts } from "services/location";

export const useDistrict = (value?: GetAllDistrictArgs) => {
  const [getAllDistrict, { data }] = useGetAllDistrict(fragmentGetAllDistricts);

  useEffect(() => {
    value && invokeGetAllDistrict(value.filter?.state as string);
  }, []);
  const invokeGetAllDistrict = async (stateValue: string) => {
    await getAllDistrict({
      variables: {
        filter: {
          state: stateValue,
        },
      },
    });
  };
  return {
    listDistrict: data,
    invokeGetAllDistrict,
  };
};
