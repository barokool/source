import styled from "styled-components/macro";
import tw from "twin.macro";

export const Wrapper = styled.div`
  ${tw`px-2 pt-3 laptop:px-6`}
`;
