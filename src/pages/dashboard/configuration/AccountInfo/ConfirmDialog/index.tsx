import { useEffect, useState } from "react";
import { Formik } from "formik";
import * as yup from "yup";

import { DialogTitle } from "common/styles/Title";
import Input from "designs/InputV2";
import Dialog from "components/Dialog";
import { toast } from "react-toastify";

//languages
import { t } from "language";

//locals
import {
  ElementWrapper,
  ProductCategoryDialogContainer,
  Form,
  ButtonWrapper,
  Button,
  FormGroup,
} from "./styles";

//api
import {
  UpdateUserInput,
  useCheckPasswordIsOk,
  useUpdateUser,
} from "apiCaller";
import { fragmentUpdateUser } from "services/user";

interface UserDialogProps {
  ButtonMenu: React.ReactElement;
  editField?: UpdateUserInput;
  title?: string;
  onClose?: () => void;
  onSuccess?: () => void;
  userId: string;
}

interface IFormValue {
  password: string;
}

const validationSchema = yup
  .object()
  .shape<{ [key in keyof IFormValue]: any }>({
    password: yup.string().required(t("configuration.account-info.pass")),
  });

const ConfirmDialog: React.FC<UserDialogProps> = ({
  ButtonMenu,
  editField,
  title,
  onSuccess,
  userId,
}) => {
  //local states
  const [initialValues] = useState<IFormValue>({
    password: "",
  });
  const [isOpen, setOpen] = useState(false);

  //api
  const [
    updateUser,
    { data: updateData, error: updateError, loading: updateLoading },
  ] = useUpdateUser(fragmentUpdateUser);
  const [checkPasswordIsOk, { data: dataCheckPass, loading: checkLoading }] =
    useCheckPasswordIsOk();

  const handleSubmit = (values: IFormValue) => {
    checkPasswordIsOk({
      variables: {
        userId: userId,
        password: values.password,
      },
    });
  };

  useEffect(() => {
    if (editField)
      dataCheckPass &&
        updateUser({
          variables: {
            id: userId,
            updateUserInput: editField,
          },
        });
  }, [dataCheckPass, editField]);

  useEffect(() => {
    if (updateData && !updateError) {
      toast.success(t("common.edit-success"));
      onSuccess && onSuccess();
      setOpen(false);
    }
    if (!updateData && updateError) {
      toast.error(t("common.edit-fail"));
      onSuccess && onSuccess();
      setOpen(false);
    }
  }, [updateData, updateError]);

  return (
    <>
      <ElementWrapper onClick={() => setOpen(!isOpen)}>
        {ButtonMenu}
      </ElementWrapper>
      <Dialog
        open={isOpen}
        onClose={() => setOpen(false)}
        className="overflow-auto"
      >
        <ProductCategoryDialogContainer>
          <DialogTitle>{title}</DialogTitle>
          <Formik
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={handleSubmit}
            enableReinitialize
          >
            <Form>
              <FormGroup>
                <ElementWrapper className="flex flex-col gap-5">
                  <Input
                    name="password"
                    type={"password"}
                    label={t("configuration.account-info.pass")}
                    placeholder={t("configuration.account-info.pass")}
                  />
                </ElementWrapper>
              </FormGroup>
              <ButtonWrapper>
                <Button outline type="button" onClick={() => setOpen(false)}>
                  {t("common.cancel")}
                </Button>
                <Button
                  primary
                  type="submit"
                  loading={updateLoading || checkLoading}
                >
                  {t("common.accept")}
                </Button>
              </ButtonWrapper>
            </Form>
          </Formik>
        </ProductCategoryDialogContainer>
      </Dialog>
    </>
  );
};

export default ConfirmDialog;
