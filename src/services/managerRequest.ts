import { GenFields, User } from "apiCaller";

export const fragmentCreateManagerRequest: GenFields<User> = [
  "_id",
  "displayName",
  "email",
];
