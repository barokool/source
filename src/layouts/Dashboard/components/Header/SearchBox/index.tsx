import { SearchBoxContainer } from "./styles";
import SearchBoxTable from "components/SearchBoxTable";
interface ISearchBoxProps {}

const SearchBox: React.FC<ISearchBoxProps> = props => {
  const onHandleSearch = (text: string) => {};
  return (
    <SearchBoxContainer>
      <SearchBoxTable onFetchData={onHandleSearch} />
    </SearchBoxContainer>
  );
};

export default SearchBox;
