import i18n from "i18next";
import LanguageDetector from "i18next-browser-languagedetector";
import localesDE from "assets/locales/de";
import localesEN from "assets/locales/en";
import localesVI from "assets/locales/vi";

const languages = [
  {
    name: "en",
    icon: <></>,
    fullName: "English",
  },
  {
    name: "vi",
    icon: <></>,
    fullName: "Vietnamese",
  },
  {
    name: "de",
    icon: <></>,
    fullName: "German",
  },
];

i18n.use(LanguageDetector).init({
  debug: true,
  resources: {
    en: {
      common: localesEN,
    },
    vi: {
      common: localesVI,
    },
    de: {
      common: localesDE,
    },
  },
  preload: ["en", "de", "vi"],
  fallbackLng: "en",
  load: "currentOnly",
  ns: ["common"],
  react: {
    bindI18n: "languageChanged loaded",
  },
});

const t: (key: NestedObjectPaths<typeof localesEN>) => string =
  i18n.t.bind(i18n);

export { t, languages };
export default i18n;
