import React from "react";

//languages
import { t } from "language";

//api
import { Order } from "apiCaller";

//locals
import { SectionContainer, Title, Body, Item } from "./styles";

interface ISectionProps {
  title: string;
  data: Order[];
}

const Section: React.FC<ISectionProps> = ({ title, data }) => {
  return (
    <SectionContainer>
      <Title>{title}</Title>
      <Body>
        {data &&
          data.map((value, index) => {
            return (
              <Item.Wrapper key={index}>
                <Item.ElementWrapper>
                  <Item.TextWrapper>
                    <Item.TextSecondary>#{value?.code}</Item.TextSecondary>
                    <Item.TextPrimary>
                      {/* name order */}
                      {t("order.order")}{" "}
                    </Item.TextPrimary>
                  </Item.TextWrapper>
                  <Item.TextWrapper className="text-right">
                    <Item.TextSecondary>
                      {t("overview.revenue")}
                    </Item.TextSecondary>
                    <Item.TextPrimary>
                      {value?.price?.finalPrice}€
                    </Item.TextPrimary>
                  </Item.TextWrapper>
                </Item.ElementWrapper>
                <Item.ElementWrapper>
                  <Item.TextWrapper>
                    <Item.TextSecondary>
                      {t("overview.order-time")} 15:56
                    </Item.TextSecondary>
                    <Item.TextPrimary>
                      {/* full name order */}
                      {value.phoneNumber || t("common.no-info")}
                    </Item.TextPrimary>
                  </Item.TextWrapper>
                  <Item.TextWrapper className="text-right">
                    <Item.TextSecondary>
                      {t("overview.driver")}:
                    </Item.TextSecondary>
                    <Item.TextPrimary>
                      {value.driver?.displayName || t("common.no-info")}
                    </Item.TextPrimary>
                  </Item.TextWrapper>
                </Item.ElementWrapper>
              </Item.Wrapper>
            );
          })}
      </Body>
    </SectionContainer>
  );
};

export default Section;
