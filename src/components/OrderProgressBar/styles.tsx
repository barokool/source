import styled from "styled-components/macro";
import tw from "twin.macro";

export const OrderProgressBarContainer = styled.div`
  ${tw`flex justify-between items-center relative w-max float-right`}
`;

export const ActiveContainer = styled.div`
  ${tw`desktop:mr-2 laptop:mr-1.5 mr-1`}
`;

export const ActivePoint = styled.div<{
  isActive: boolean;
}>`
  ${tw`rounded-full mx-auto text-center w-2.5 h-2.5`},
  ${({ isActive }) =>
    isActive
      ? tw`bg-primary text-white font-bold`
      : tw`bg-gray text-gray text-12`}
`;

export const ActiveContent = styled.p<{
  isActive: boolean;
}>`
  ${tw`text-16 text-center`}
  ${({ isActive }) =>
    isActive ? tw`text-black font-semibold` : tw`text-body-color`}
`;
