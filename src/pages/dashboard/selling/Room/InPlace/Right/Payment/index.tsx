import React, { useState, useEffect } from "react";
import { Formik } from "formik";
import * as Yup from "yup";
import RadioGroup from "components/RadioGroup";
import Input from "designs/InputV2";
import { toast } from "react-toastify";
import { forceTextInputEnterNumber } from "common/functions/condition/forceTextInputEnterNumber";

//redux
import { useSelector, useDispatch } from "react-redux";
import { setActionSuccess } from "redux/slices/common";
import { setOrderSelected } from "redux/slices/order";
import { IRootState } from "typings";

//api
import {
  OrderStatusEnum,
  useUpdateOrder,
  useGetOrderById,
  Order,
  PaymentTypeEnum,
} from "apiCaller";
import { fragmentUpdateOrder, fragmentGetOrderById } from "services/order";

//language
import { t } from "language";

//locals
import {
  RightPaymentContainer,
  Bottom,
  BoxPoint,
  CustomRadio,
  Middle,
  Point,
  RadioItem,
  Top,
  ItemWrapper,
  PhoneNumberFieldWrapper,
  CustomerPayFieldWrapper,
  ItemLeft,
  ItemRight,
} from "./styles";

interface IRightPaymentProps {}

interface IFormValue {
  phoneNumber?: string;
  customerPay: number;
}

const validationSchema = Yup.object().shape<{
  [key in keyof IFormValue]: any;
}>({
  customerPay: Yup.number().required(t("selling.please-enter-customer-pay")),
});

const RightPayment: React.FC<IRightPaymentProps> = props => {
  const { orderSelected } = useSelector((state: IRootState) => state.order);
  const [initialValues] = useState<IFormValue>({
    customerPay: 0,
    phoneNumber: "",
  });
  const [customerPay, setCustomerPay] = useState<number>(0);
  const [customerPhoneNumber, setCustomerPhoneNumber] = useState<string>();
  const [excessMoney, setExcessMoney] = useState<number>();
  const [listPayment] = useState<{ id: PaymentTypeEnum; name: string }[]>([
    {
      id: PaymentTypeEnum.Directly,
      name: t("selling.cash"),
    },
    {
      id: PaymentTypeEnum.Online,
      name: t("selling.credit-card"),
    },
  ]);
  const [paymentTypeSelected, setPaymentTypeSelected] = useState<{
    id: PaymentTypeEnum;
    name: string;
  } | null>(listPayment[0]);

  const [order, setOrder] = useState<Order | null>(null);
  const dispatch = useDispatch();
  //api
  const [updateOrder, { data: orderUpdated, loading: updateOrderLoading }] =
    useUpdateOrder(fragmentUpdateOrder);
  const [getOrderById, { data: orderReturned }] =
    useGetOrderById(fragmentGetOrderById);

  //get data from api
  useEffect(() => {
    if (orderReturned) {
      setOrder(orderReturned?.getOrderById as Order);
    }
  }, [orderReturned]);

  useEffect(() => {
    order && dispatch(setOrderSelected(order));
  }, [order]);

  useEffect(() => {
    if (customerPay >= (orderSelected?.price?.finalPrice as number)) {
      setExcessMoney(
        customerPay - (orderSelected?.price?.finalPrice as number),
      );
    } else {
      setExcessMoney(0);
    }
  }, [customerPay]);

  useEffect(() => {
    if (orderUpdated) {
      dispatch(setActionSuccess());
      toast.success(t("selling.order-in-processing"));
      handleRefreshOrder();
    }
  }, [orderUpdated]);

  //functions
  const handleRefreshOrder = () => {
    getOrderById({
      variables: { id: orderSelected?._id as string },
    });
  };

  const handleCancel = () => {
    dispatch(setOrderSelected(null));
    dispatch(setActionSuccess());
    toast.success(t("selling.cancel-payment-success"));
    handleRefreshOrder();
  };

  const handleProcessOrder = () => {
    console.log({
      status: OrderStatusEnum.Processing,
      phoneNumber: customerPhoneNumber,
      customerPay: customerPay,
      paymentType: paymentTypeSelected?.id as PaymentTypeEnum,
    });
    updateOrder({
      variables: {
        id: orderSelected?._id as string,
        input: {
          status: OrderStatusEnum.Processing,
          phoneNumber: customerPhoneNumber,
          customerPay: customerPay,
          paymentType: paymentTypeSelected?.id,
        },
      },
    });
  };

  return (
    <Formik
      enableReinitialize
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={handleProcessOrder}
    >
      <RightPaymentContainer>
        {/*Top*/}
        <Top.Container>
          <ItemWrapper>
            <ItemLeft></ItemLeft>
            <ItemRight className="text-black mb-2">
              {orderSelected?.createdAt}
            </ItemRight>
          </ItemWrapper>
          <PhoneNumberFieldWrapper>
            <ItemLeft>{t("selling.phone-number")}</ItemLeft>
            <Input
              name="phoneNumber"
              label={t("selling.phone-number")}
              placeholder={t("selling.enter-phone-number")}
              onChangeValue={phoneNumber =>
                setCustomerPhoneNumber(phoneNumber as string)
              }
              onKeyDown={forceTextInputEnterNumber}
            />
          </PhoneNumberFieldWrapper>
          <ItemWrapper>
            <ItemLeft>{t("selling.discount")}</ItemLeft>
            <ItemRight>
              {orderSelected?.coupon?.value?.toString().prettyMoney ||
                t("selling.no-info")}
            </ItemRight>
          </ItemWrapper>
          <ItemWrapper>
            <ItemLeft>{t("selling.final-price")}</ItemLeft>
            <ItemRight className="font-semibold">
              {orderSelected?.price?.finalPrice}
            </ItemRight>
          </ItemWrapper>
        </Top.Container>

        {/*Middle*/}
        <Middle.Container>
          <CustomerPayFieldWrapper>
            <ItemLeft>{t("selling.customer-pay")}</ItemLeft>
            <Input
              name="customerPay"
              type="number"
              label={t("selling.customer-pay")}
              placeholder={t("selling.enter-customer-pay")}
              onChangeValue={customerPay =>
                setCustomerPay(customerPay as number)
              }
              onKeyDown={forceTextInputEnterNumber}
              required
            />
          </CustomerPayFieldWrapper>
          <ItemWrapper>
            <ItemLeft>{t("selling.payment-method")}</ItemLeft>
            <RadioGroup
              options={listPayment}
              onChange={option => setPaymentTypeSelected(option)}
              optionSelected={paymentTypeSelected}
              renderOption={({ option, checked }) => (
                <RadioItem.Container type="button">
                  <RadioItem.LeftWrapper>
                    <BoxPoint>
                      <Point>
                        <CustomRadio active={checked} />
                      </Point>
                    </BoxPoint>
                    <RadioItem.Label>{option?.name}</RadioItem.Label>
                  </RadioItem.LeftWrapper>
                </RadioItem.Container>
              )}
            />
          </ItemWrapper>
        </Middle.Container>

        {/*Bottom*/}
        <Bottom.Container>
          <ItemWrapper>
            <ItemLeft>{t("selling.excess-money")}</ItemLeft>
            <ItemRight>{excessMoney}</ItemRight>
          </ItemWrapper>
          <ItemWrapper className="mt-2">
            <ItemLeft></ItemLeft>
            <Bottom.ButtonWrapper>
              <Bottom.Button
                outline
                onClick={() => {
                  dispatch(setOrderSelected(null));
                  handleCancel();
                }}
              >
                {t("common.cancel")}
              </Bottom.Button>
              <Bottom.Button primary loading={updateOrderLoading} type="submit">
                {t("common.accept")}
              </Bottom.Button>
            </Bottom.ButtonWrapper>
          </ItemWrapper>
        </Bottom.Container>
      </RightPaymentContainer>
    </Formik>
  );
};

export default RightPayment;
