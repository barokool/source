import dayjs from "dayjs";
import { string } from "yup/lib/locale";

String.prototype.prettyDate = function () {
  return dayjs(String(this)).format("DD/MM/YYYY");
};

String.prototype.prettyDateReverse = function () {
  return dayjs(String(this)).format("YYYY-MM-DD");
};


String.prototype.prettyMoney = function () {
  return this.replace(/\B(?=(\d{3})+(?!\d))/g, ".");
};
