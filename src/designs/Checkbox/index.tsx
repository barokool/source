import CheckboxIcon from "icons/Checkbox";
import { CheckboxButton, CheckboxContainer } from "./styles";

interface ICheckboxProps {
  content: string;
  isChecked: boolean;
  className?: string;
  onChange: (isChecked: boolean) => void;
  disabled?: boolean;
}

const Checkbox: React.FC<ICheckboxProps> = ({
  content,
  isChecked,
  className = "",
  onChange,
  disabled = false,
}) => {
  return (
    <label
      className={
        "flex select-none cursor-pointer gap-1 flex-row items-center " +
        className
      }
      onClick={() => {
        onChange(!isChecked);
        console.log(isChecked);
      }}
    >
      <CheckboxButton checked={isChecked}>
        <CheckboxIcon className="text-white" />
      </CheckboxButton>
      {content}
    </label>
  );
};

export default Checkbox;
