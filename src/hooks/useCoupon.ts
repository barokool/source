import {
  FilterCoupon,
  Coupon,
  useGetAllCoupon,
  GetAllCouponArgs,
} from "apiCaller";
import { useEffect, useState, useCallback } from "react";
import { fragmentGetAllCoupon } from "services/coupon";
export const usePromotion = (params: GetAllCouponArgs) => {
  const [listCoupon, setListCoupon] = useState<Coupon[]>([]);
  const [getAllPromotion, { data }] = useGetAllCoupon(fragmentGetAllCoupon);
  useEffect(() => {
    invokeGetAllCoupons(params);
  }, []);
  const invokeGetAllCoupons = useCallback((value: GetAllCouponArgs) => {
    getAllPromotion({
      variables: {
        page: value.page,
        size: value.size,
        filter: value.filter,
      },
    });
    setListCoupon(data?.getAllCoupon?.results || []);
  }, []);

  return {
    listCoupon,
    setListCoupon,
    invokeGetAllCoupons,
  };
};
