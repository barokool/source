import styled from "styled-components/macro";
import tw from "twin.macro";

export const DishCardContainer = styled.div`
  ${tw`w-full  flex justify-between py-1.5`}
`;

export const DishCardContentContainer = styled.div`
  ${tw`flex items-center`}
`;

export const DishCardName = styled.div`
  ${tw`text-14 text-black`}
`;

export const DishCardPrice = styled.h3`
  ${tw`text-14 text-primary`}
`;

export const AddButtonWrapper = styled.div`
  ${tw`flex items-center`}
`;

export const DishQuantity = styled.p`
  ${tw`text-black text-14 mr-1`}
`;
