import BaseButton from "designs/BaseButton";
import styled from "styled-components/macro";
import tw from "twin.macro";

export const IconButtonContainer = styled(BaseButton)`
  ${tw`flex items-center justify-center w-auto rounded-full cursor-pointer select-none`}
`;
