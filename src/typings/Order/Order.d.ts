import { IMongoObjectId, User } from "typings";
import { Branch } from "typings/Branch";
import { IProduct, IProductUnitInput } from "typings/Product";
import { ITable } from "typings/Table";
import { ITopping, IToppingInput } from "typings/Topping";

import { IProductUnit } from "typings/Product";

export interface OrderItem {
  _id?: IMongoObjectId;
  product?: IProduct;
  toppings: ITopping[];
  unit?: IProductUnit;
  note?: string;
  quantity: number;
  totalPrice?: number;
}

export interface OrderPrice {
  beforeDiscount?: number;
  afterDiscount?: number;
  tax?: number;
  shippingFee?: number;
  discount?: number;
  finalPrice?: number;
}

export type OrderStatusEnum =
  | "new"
  | "ready"
  | "approving"
  | "shipping"
  | "cancel"
  | "complete"
  | "processing";

export type IPaymentTypeEnum = "online" | "directly";

export type OrderTypeEnum =
  | "take_away"
  | "delivery"
  | "booking_table"
  | "in_place";

export interface Order {
  _id: IMongoObjectId;
  code?: string;
  orderTime?: Date;
  shippingTime?: Date;
  customerName?: string;
  phoneNumber?: string;
  cancellationReason?: string;
  status?: OrderStatusEnum;
  paymentType?: IPaymentTypeEnum;
  barcode?: string;
  price?: OrderPrice;
  orderItems?: OrderItem[];
  createdBy?: User;
  driver?: User;
  type?: OrderTypeEnum;
  fromBranch?: Branch;
  table?: ITable;
  note?: string;
  createdAt?: Date;
  updatedAt?: Date;
}

export interface IFilterOrder {
  code?: string;
  type: OrderTypeEnum;
}

export interface OrderCreateInput {
  orderTime?: string;
  shippingTime?: string;
  customerName?: string;
  phoneNumber?: string;
  cancellationReason?: string;
  paymentType?: IPaymentTypeEnum;
  type?: OrderTypeEnum;
  price?: OrderPriceInput;
  carts?: OrderItemInput[];
  driver?: string;
  type: OrderTypeEnum;
  fromBranch?: string;
  table?: string;
}

export interface OrderPriceInput {
  beforeDiscount?: number = 0;
  afterDiscount?: number = 0;
  tax?: number = 0;
  shippingFee?: number = 0;
  discount?: number = 0;
  finalPrice?: number = 0;
}

export interface OrderItemInput {
  quantity?: number;
  product?: string;
  toppings?: IToppingInput[];
  unit?: IProductUnitInput;
  note?: string;
}

export interface OrderUpdateInput {
  orderTime?: string;
  shippingTime?: string;
  customerName?: string;
  phoneNumber?: string;
  cancellation?: string;
  status?: OrderStatusEnum;
  paymentType?: IPaymentTypeEnum;
  price?: OrderPriceInput;
  orderItems?: OrderItemInput[];
  driver?: string;
  fromBranch?: string;
  table?: string;
  type?: OrderTypeEnum;
}

export interface IGetAllOrder {
  page?: number;
  size?: number;
  filter?: IFilterOrder;
}

export interface ICreateOrder {
  input: OrderCreateInput;
}

export interface IUpdateOrder {
  id: string;
  input: OrderUpdateInput;
}

export interface IDeleteOrder {
  listId: string[];
}

export interface IUpdateOrderItem {
  idOrder: string;
  idOrderItem: string;
  input?: OrderItemInput;
}

export interface IDeleteOrderItem {
  idOrder: string;
  listIdOrderItem?: string[];
}

export interface IAddOrderItemToOrder {
  idOrder: string;
  input: OrderItemInput;
}

export interface IGetOrderByBarcode {
  barcode: string;
}
