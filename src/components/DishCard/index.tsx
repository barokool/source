import React, { useState } from "react";
import {
  DishCardContainer,
  DishCardContentContainer,
  DishCardName,
  DishCardPrice,
} from "./styles";
import { IDish } from "pages/qrOrder/Menu";
import SVG from "designs/SVG";

interface IDishCardProps {
  dish?: IDish;
  RightButton?: React.ReactElement;
  quantity?: number;
}

const DishCard: React.FC<IDishCardProps> = ({ dish, RightButton }) => {
  return (
    <DishCardContainer>
      <DishCardContentContainer>
        <img src={dish?.thumbnail} alt={dish?.name?.toString()} />
        <div className="ml-1">
          <DishCardName>{dish?.name}</DishCardName>
          <DishCardPrice>{dish?.price}</DishCardPrice>
        </div>
      </DishCardContentContainer>
      {RightButton}
    </DishCardContainer>
  );
};

export default DishCard;
