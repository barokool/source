import styled from "styled-components/macro";
import tw from "twin.macro";
import { Form as _Form } from "formik";
import { Tab } from "@headlessui/react";
import _Button from "designs/Button";

export const TabContainer = styled.div`
  ${tw`w-full`}
`;
export const Title = styled.h3<{ active: boolean; length: number }>`
  ${tw`flex font-medium w-max  text-16 items-center gap-1 py-1 px-2 rounded-xl cursor-pointer `}
  ${({ active }) => (active ? tw`text-white` : tw`text-black`)};
  ${({ length, active }) =>
    length > 1 && active ? tw`bg-black` : tw`bg-white`};
`;

export const ListTitle = styled(Tab.List)`
  ${tw`flex items-center flex-wrap overflow-auto w-full gap-2.5 font-bold text-26 pb-1`}
`;
export const Content = styled(Tab.Panel)`
  ${tw`w-full`}
`;
export const Container = styled.div<{ size: string }>`
  ${tw`w-full  mx-auto my-2.5`}
  ${({ size }) => size === "lg" && tw`max-w-full`}
  ${({ size }) => size === "md" && tw`max-w-3xl`}
`;
