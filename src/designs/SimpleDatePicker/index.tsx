import { useField, useFormikContext } from "formik";
import {
  DetailedHTMLProps,
  InputHTMLAttributes,
  useEffect,
  useState,
} from "react";
import { Container, InputField, DateInputContainer } from "./styles";
import en from "date-fns/locale/en-US";
import { DatePicker as NiceDatePicker } from "react-nice-dates";
import SVG from "designs/SVG";
import FormControlLabel from "common/styles/FormControlLabel";
import FormControlErrorHelper from "common/styles/FormControlErrorHelper";
import DatePickerIcon from "icons/DatePicker";
import { Title } from "components/AlertDialog/styles";

interface IInput
  extends DetailedHTMLProps<
    InputHTMLAttributes<HTMLInputElement>,
    HTMLInputElement
  > {
  className?: string;
  label: string;
  disabled?: boolean;
  placeholder?: string;
  maximumDate?: Date;
  minimumDate?: Date;
  onDateChange?: (newDate: Date) => void;
  autoComplete?: "on" | "off";
  required?: boolean;
  classNameSVG?: string;
}

const SimpleDatePicker: React.FC<IInput> = props => {
  const {
    className,
    classNameSVG,
    label,
    minimumDate,
    maximumDate,
    required = false,
    onDateChange,
    ...rest
  } = props;
  const [date, setDate] = useState<Date | undefined | null>(null);

  const handleChange = (newDate: Date | null) => {
    setDate(newDate);
    if (newDate) {
      onDateChange && onDateChange(newDate);
    }
  };

  return (
    <Container className={`datePicker ${className}`}>
      <FormControlLabel isError={false} required={required}>
        {label}
      </FormControlLabel>
      <NiceDatePicker
        date={date as Date}
        onDateChange={handleChange}
        locale={en}
        format="dd/MM/yyyy"
        maximumDate={maximumDate}
        minimumDate={minimumDate}
      >
        {({ inputProps }) => (
          <DateInputContainer isError={false}>
            <InputField autoComplete="off" {...(rest as any)} {...inputProps} />
            <DatePickerIcon className={` ${classNameSVG}`} />
          </DateInputContainer>
        )}
      </NiceDatePicker>
    </Container>
  );
};

export default SimpleDatePicker;
