import {
  DishItem,
  ListDishContainer,
  OrderSectionContainer,
  OrderTitle,
} from "./styles";

import SVG from "designs/SVG";

interface OrderSectionProps {}

const OrderSection: React.FC<OrderSectionProps> = props => {
  return (
    <OrderSectionContainer>
      <OrderTitle>Đơn đặt hàng</OrderTitle>
      <ListDishContainer>
        <DishItem.Container>
          <SVG
            name="qrOrder/favourite-dishes"
            width={48}
            height={48}
            className="mx-auto"
          />
          <DishItem.Label>Món yêu thích</DishItem.Label>
        </DishItem.Container>
        <DishItem.Container>
          <SVG
            name="qrOrder/icon-dishes"
            width={48}
            height={48}
            className="mx-auto"
          />
          <DishItem.Label>Món đặc trưng</DishItem.Label>
        </DishItem.Container>
        <DishItem.Container>
          <SVG
            name="qrOrder/new-dishes"
            width={48}
            height={48}
            className="mx-auto"
          />
          <DishItem.Label>Món mới</DishItem.Label>
        </DishItem.Container>
      </ListDishContainer>
    </OrderSectionContainer>
  );
};

export default OrderSection;
