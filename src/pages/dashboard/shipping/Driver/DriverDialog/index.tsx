import React, { useState, useEffect } from "react";
import { Formik } from "formik";
import * as yup from "yup";
import { toast } from "react-toastify";
import Input from "designs/InputV2";
import Select from "designs/Select";
import Dialog from "components/Dialog";
import { DialogTitle } from "common/styles/Title";
import MultipleAutocomplete from "designs/MultipleAutoComplete";

//hooks
import useAuth from "hooks/useAuth";
import { useDriverRoles } from "hooks/useDriverRoles";

//languages
import { t } from "language";

//api
import {
  Branch,
  State,
  District,
  City,
  User,
  useGetAllState,
  useCreateUser,
  useGetAllDistrict,
  useGetAllCity,
  useUpdateUser,
  CreateUserInput,
  UpdateUserInput,
  Role,
  Country,
  useGetAllCountry,
} from "apiCaller";
import { fragmentCreateUser, fragmentUpdateUser } from "services/user";
import {
  fragmentGetAllStates,
  fragmentGetAllDistricts,
  fragmentGetAllCities,
  fragmentGetAllCountry,
} from "services/location";

//locals
import {
  Button,
  Form,
  ElementWrapper,
  ButtonWrapper,
  DriverDialogContainer,
} from "./styles";
import InputPhone from "designs/InputPhone";

interface ICouponDialogProps {
  ButtonMenu?: React.ReactElement;
  editField?: User;
  handleOnClose?: () => void;
  open?: boolean;
  onSuccess?: () => void;
  title: string;
}

interface IFormValue {
  country?: string;
  displayName: string;
  role?: string;
  branches?: string[];
  managerStaff?: string;
  phoneNumber: string;
  password: string;
  email?: string;
  state?: string;
  district?: string;
  city?: string;
  street: string;
}

const DriverDialog: React.FC<ICouponDialogProps> = ({
  ButtonMenu,
  editField,
  open,
  handleOnClose,
  onSuccess,
  title,
}) => {
  const { accountInfo } = useAuth();
  const { branches: listBranch } = accountInfo.userInfo;
  const { listDriverRole } = useDriverRoles();

  //local states
  const [isOpen, setOpen] = useState<boolean>(open || false);
  const [initialValues, setInitialValues] = useState<IFormValue>({
    displayName: "",
    phoneNumber: "",
    email: "",
    password: "",
    street: "",
  });

  const [roleSelected, setRoleSelected] = useState<Role | null>(null);
  const [listBranchSelected, setListBranchSelected] = useState<Branch[]>([]);
  const [listCountryState, setListCountryState] = useState<Country[]>([]);
  const [listState, setListState] = useState<State[]>([]);
  const [listDistrict, setListDistrict] = useState<District[]>([]);
  const [listCity, setListCity] = useState<City[]>([]);
  const [stateSelected, setStateSelected] = useState<State | null>(null);
  const [countrySelected, setCountrySelected] = useState<Country | null>(null);
  const [districtSelected, setDistrictSelected] = useState<District | null>(
    null,
  );
  const [citySelected, setCitySelected] = useState<City | null>(null);

  const validationSchema = yup
    .object()
    .shape<{ [key in keyof IFormValue]: any }>({
      country: yup.string().required(t("register.please-select-country")),
      state: yup.string().required(t("register.please-select-state")),
      district: yup.string().required(t("register.please-select-district")),
      city: yup.string().required(t("register.please-select-city")),
      displayName: yup
        .string()
        .required(t("configuration.staff-management.please-enter-name")),
      phoneNumber: yup
        .string()
        .required(t("configuration.staff-management.enter-staff-phone-number")),
      email: yup
        .string()
        .required(t("configuration.staff-management.please-enter-email")),
      street: yup.string().required(t("register.please-enter-address")),
      password:
        !editField &&
        yup
          .string()
          .required(t("configuration.staff-management.please-enter-password")),
      role: yup.string().required("Bạn phải nhập trường này"),
    });

  //api
  const [
    createDriver,
    {
      data: createDriverData,
      loading: createDriverLoading,
      error: createDriverErrors,
    },
  ] = useCreateUser(fragmentCreateUser);
  const [
    updateDriver,
    {
      data: updateDriverData,
      loading: updateDriverLoading,
      error: updateDriverErrors,
    },
  ] = useUpdateUser(fragmentUpdateUser);
  const [getAllState, { data: getAllStateData }] =
    useGetAllState(fragmentGetAllStates);
  const [getAllDistrict, { data: getAllDistrictData }] = useGetAllDistrict(
    fragmentGetAllDistricts,
  );
  const [getAllCity, { data: getAllCityData }] =
    useGetAllCity(fragmentGetAllCities);
  const [getAllCountry, { data: allCountryData, error: countryError }] =
    useGetAllCountry(fragmentGetAllCountry);

  useEffect(() => {
    if (createDriverData && !createDriverErrors) {
      toast.success(t("common.create-success"));
      onSuccess?.();
      setOpen(false);
      handleOnClose && handleOnClose();
    }
    if (updateDriverData && !updateDriverErrors) {
      toast.success(t("common.edit-success"));
      onSuccess?.();
      setOpen(false);
      handleOnClose && handleOnClose();
    }
  }, [createDriverData, updateDriverData]);

  const handleCountryChange = (country: Country) => {
    if (country._id === countrySelected?._id) return;

    setCountrySelected(country);

    // Reset all state
    setListState([]);
    setListDistrict([]);
    setListCity([]);
    setStateSelected(null);
    setDistrictSelected(null);
    setCitySelected(null);
  };

  useEffect(() => {
    setCountrySelected(editField?.location?.country as Country);
    setListBranchSelected(editField?.branches as Branch[]);
    setRoleSelected(editField?.role as Role);
    setStateSelected(editField?.location?.state as State);
    setDistrictSelected(editField?.location?.district as District);
    setCitySelected(editField?.location?.city as City);
    if (editField) {
      setInitialValues({
        displayName: editField.displayName as string,
        role: editField?.role?._id as string,
        branches: editField?.branches?.map(branch => branch._id) as string[],
        phoneNumber: editField.phoneNumber as string,
        email: editField.email as string,
        password: "",
        street: editField?.location?.addressLine as string,
      });
    }
  }, [editField]);

  //getAllState
  useEffect(() => {
    invokegetAllCountryAPI();
  }, []);

  useEffect(() => {
    allCountryData &&
      setListCountryState(allCountryData.getAllCountry.results || []);
  }, [allCountryData]);

  useEffect(() => {
    getAllStateData &&
      setListState(getAllStateData.getAllState.results as State[]);
  }, [getAllStateData]);

  useEffect(() => {
    getAllDistrictData &&
      setListDistrict(getAllDistrictData.getAllDistrict.results as District[]);
  }, [getAllDistrictData]);

  useEffect(() => {
    getAllCityData && setListCity(getAllCityData.getAllCity.results as City[]);
  }, [getAllCityData]);

  useEffect(() => {
    if (!editField) {
      countrySelected && invoketGetAllStatesAPI();
    }
  }, [countrySelected]);

  useEffect(() => {
    if (!editField) {
      stateSelected && invokeGetAllDistrictSAPI();
    }
  }, [stateSelected]);

  useEffect(() => {
    if (!editField) {
      districtSelected && invokeGetAllCitiAPI();
    }
  }, [districtSelected]);

  const invokegetAllCountryAPI = () => {
    getAllCountry({ variables: {} });
  };

  const invoketGetAllStatesAPI = () => {
    getAllState({
      variables: {
        filter: {
          country: countrySelected?._id || "",
        },
      },
    });
  };

  const invokeGetAllDistrictSAPI = () => {
    getAllDistrict({
      variables: {
        filter: { state: stateSelected?._id || "" },
      },
    });
  };

  const invokeGetAllCitiAPI = () => {
    getAllCity({
      variables: {
        filter: {
          district: districtSelected?._id || "",
        },
      },
    });
  };

  const handleSubmit = (values: IFormValue) => {
    const input: CreateUserInput = {
      displayName: values.displayName,
      phoneNumber: values.phoneNumber,
      email: values.email,
      password: values.password,
      role: values.role as string,
      branches: listBranchSelected.map(branch => branch._id) as string[],
      location: {
        addressLine: values.street,
        city: citySelected?._id as string,
        district: districtSelected?._id as string,
        state: stateSelected?._id as string,
        country: countrySelected?._id as string,
      },
    };
    if (editField) {
      updateDriver({
        variables: {
          id: editField._id as string,
          updateUserInput: input as UpdateUserInput,
        },
      });
      return;
    }
    createDriver({
      variables: {
        input: input,
      },
    });
  };

  const handleCancelOnClick = () => {
    setOpen(false);
    handleOnClose && handleOnClose();
  };

  return (
    <>
      <ElementWrapper
        onClick={() => setOpen(!isOpen)}
        className="flex justify-end w-full"
      >
        {ButtonMenu}
      </ElementWrapper>

      <Dialog
        open={isOpen}
        onClose={handleCancelOnClick}
        className="overflow-auto"
        size="lg"
      >
        <DriverDialogContainer>
          <DialogTitle>{title}</DialogTitle>
          <Formik
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={handleSubmit}
            enableReinitialize
          >
            <Form>
              <Input
                name="displayName"
                label={t("shipping.driver.name")}
                placeholder={t("shipping.driver.enter-driver-name")}
                required
              />
              <div className="flex gap-2">
                <Select
                  name="country"
                  required
                  label={t("register.country")}
                  placeholder={t("shipping.driver.select-country")}
                  onSelect={country => {
                    handleCountryChange(country as Country);
                  }}
                  optionSelected={countrySelected}
                  options={listCountryState}
                  className="max-w-[150px] w-full"
                />
                <Select
                  name="state"
                  required
                  label={t("shipping.driver.state")}
                  placeholder={t("shipping.driver.select-state")}
                  options={listState || []}
                  optionSelected={stateSelected}
                  onSelect={values => setStateSelected(values)}
                />
              </div>

              <InputPhone
                name="phoneNumber"
                label={t("shipping.driver.phone-number")}
                placeholder={t("shipping.driver.enter-phone-number")}
                required
                className="gap-1 "
              />

              <Select
                name="district"
                required
                label={t("shipping.driver.district")}
                placeholder={t("shipping.driver.select-district")}
                options={listDistrict}
                optionSelected={districtSelected}
                onSelect={district => setDistrictSelected(district)}
              />

              <Input
                name="email"
                type="text"
                label={t("shipping.driver.email")}
                placeholder={t("configuration.staff-management.enter-email")}
                required
              />

              <Select
                name="city"
                required
                label={t("shipping.driver.city")}
                placeholder={t("shipping.driver.enter-city")}
                onSelect={option => setCitySelected(option)}
                optionSelected={citySelected}
                options={listCity}
              />

              <Select
                required
                optionSelected={roleSelected}
                onSelect={role => setRoleSelected(role)}
                options={listDriverRole}
                name="role"
                label={t("configuration.staff-management.select-role")}
                placeholder={t("shipping.driver.select-role")}
                optionTarget="position"
              />
              <Input
                name="street"
                required
                label={t("shipping.driver.address")}
                placeholder={t("shipping.driver.enter-address")}
              />

              <MultipleAutocomplete
                required
                listOptionsSelected={listBranchSelected}
                onSelect={values => setListBranchSelected(values)}
                options={listBranch as Branch[]}
                name="branches"
                label={t("configuration.staff-management.branch")}
                placeholder={t("configuration.staff-management.select-branch")}
              />

              <Input
                name="password"
                label={t("configuration.staff-management.password")}
                placeholder={t("configuration.staff-management.enter-password")}
                required
                type="password"
                disabled={editField ? true : false}
                className={editField ? "hidden" : ""}
              />

              <ButtonWrapper>
                <Button outline type="button" onClick={handleCancelOnClick}>
                  {t("common.cancel")}
                </Button>
                <Button
                  type="submit"
                  primary
                  loading={updateDriverLoading || createDriverLoading}
                >
                  {t("common.accept")}
                </Button>
              </ButtonWrapper>
            </Form>
          </Formik>
        </DriverDialogContainer>
      </Dialog>
    </>
  );
};

export default DriverDialog;
