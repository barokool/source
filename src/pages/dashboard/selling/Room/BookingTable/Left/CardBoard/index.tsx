import React, { useState, useEffect, useCallback } from "react";
import { toast } from "react-toastify";
import OrderCard from "pages/dashboard/selling/components/OrderCard";
import AddOrderButton from "designs/AddOrderButton";
import Pagination from "components/Pagination";

//redux
import { setOrderSelected } from "redux/slices/order";
import { setBranchSelected } from "redux/slices/sellingPage";
import { resetAction } from "redux/slices/common";
import { useDispatch, useSelector } from "react-redux";

//hooks
import useAuth from "hooks/useAuth";

//api
import { IRootState } from "typings";
import { fragmentCreateOrder, fragmentGetAllOrder } from "services/order";

import {
  Order,
  useGetAllOrder,
  useCreateOrder,
  OrderTypeEnum,
  Branch,
} from "apiCaller";

//languages
import { t } from "language";

//locals
import {
  LeftCardBoardContainer,
  LeftCardBoardWrapper,
  CardListContainer,
} from "./styles";

interface ILeftCardBoardProps {}

const SIZE_PER_PAGE = 11;

const LeftCardBoard: React.FC<ILeftCardBoardProps> = props => {
  //redux
  const dispatch = useDispatch();
  const { orderSelected } = useSelector((state: IRootState) => state.order);

  const [page, setPage] = useState<number>(0);
  const { actionSuccess } = useSelector((state: IRootState) => state.common);
  const { branchSelected } = useSelector(
    (state: IRootState) => state.sellingPage,
  );
  const { accountInfo } = useAuth();
  const { branches: listBranch } = accountInfo.userInfo;
  const [listOrder, setListOrder] = useState<Order[]>([]);
  const [getAllOrder, { data, loading }] = useGetAllOrder(fragmentGetAllOrder);
  const [createOrder, { data: orderReturned }] =
    useCreateOrder(fragmentCreateOrder);

  useEffect(() => {
    dispatch(setBranchSelected(listBranch?.[0] as Branch));
  }, []);

  useEffect(() => {
    console.log(page);
    invokeSetAllDataAPI();
  }, [branchSelected, page]);

  useEffect(() => {
    if (actionSuccess === true) {
      invokeSetAllDataAPI();
      dispatch(resetAction());
    }
  }, [actionSuccess]);

  useEffect(() => {
    if (data) {
      setListOrder(data?.getAllOrder.results as Order[]);
    }
  }, [data]);

  useEffect(() => {
    if (orderReturned) {
      invokeSetAllDataAPI();
      dispatch(setOrderSelected(orderReturned?.createOrder as Order));
      toast.success(t("selling.create-booking-table-success"));
    }
  }, [orderReturned]);

  const invokeSetAllDataAPI = () => {
    getAllOrder({
      variables: {
        filter: {
          type: OrderTypeEnum.Booking_table,
          fromBranch: branchSelected?._id as string,
        },
        page: page,
        size: SIZE_PER_PAGE,
      },
    });
  };

  const handleCreateOrder = () => {
    createOrder({
      variables: {
        input: {
          type: OrderTypeEnum.Booking_table,
          fromBranch: branchSelected?._id as string,
        },
      },
    });
    dispatch(resetAction());
  };

  const handleChangePage = useCallback((newPage: number) => {
    setPage(newPage);
  }, []);

  return (
    <LeftCardBoardContainer>
      <LeftCardBoardWrapper>
        <CardListContainer>
          {loading ? (
            new Array(10)
              .fill(0)
              .map((_, index) => <OrderCard key={index} loading />)
          ) : (
            <>
              {listOrder.map(order => (
                <OrderCard
                  key={order._id}
                  data={order}
                  onClick={() => dispatch(setOrderSelected(order))}
                  isActive={order._id === orderSelected?._id}
                />
              ))}
            </>
          )}
          <AddOrderButton
            onClick={() => {
              handleCreateOrder();
            }}
          ></AddOrderButton>
        </CardListContainer>
      </LeftCardBoardWrapper>
      <Pagination
        onPageChange={handleChangePage}
        page={page}
        sizePerPage={SIZE_PER_PAGE}
        totalSize={data?.getAllOrder.totalCount as number}
        className="float-right"
      />
    </LeftCardBoardContainer>
  );
};

export default LeftCardBoard;
