import styled from "styled-components/macro";
import tw from "twin.macro";
import _SearchBox from "designs/SearchBox";
import _Button from "designs/Button";

export const UserContainer = styled.div`
  ${tw``}
`;

export const ActionWrapper = styled.p`
  ${tw`flex flex-col phone:flex-row justify-between mb-4`}
`;

export const SearchBox = styled(_SearchBox)`
  ${tw`w-full phone:w-30 mb-1 phone:mb-0`}
`;

export const AddButton = styled(_Button)`
  ${tw`px-3.5 py-[12.5px] font-medium`}
`;

export const Avatar = styled.img`
  ${tw`w-4 h-4 rounded-full`}
`;

export const RenderActionContainer = styled.div`
  ${tw`flex justify-end space-x-1`}
`;
