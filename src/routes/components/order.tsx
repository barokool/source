import { IRoutes } from "typings";
import { PATH } from "common/constants/routes";
import { lazy } from "react";
import { t } from "language";

import OrderIcon from "icons/Dashboard/Order";

const AllOrder = lazy(() => import("pages/dashboard/order/AllOrder"));
const InPlace = lazy(() => import("pages/dashboard/order/InPlace"));
const Delivery = lazy(() => import("pages/dashboard/order/Delivery"));
const Booking = lazy(() => import("pages/dashboard/order/Booking"));

export const orderRoutes: IRoutes = {
  name: t("dashboard.order"),
  path: "/order",
  exact: true,
  isPrivate: true,
  Icon: OrderIcon,
  children: [
    {
      name: t("order.all.all"),
      path: PATH.ORDER.SELF,
      isPrivate: true,
      Component: AllOrder,
      exact: true,
    },
    {
      name: t("order.sell-at-store.sell-at-store"),
      path: PATH.ORDER.SELL_AT_STORE,
      isPrivate: true,
      Component: InPlace,
      exact: true,
    },
    {
      name: t("order.delivery.delivery"),
      path: PATH.ORDER.DELIVERY,
      isPrivate: true,
      Component: Delivery,
      exact: true,
    },
    {
      name: t("order.booking.booking"),
      path: PATH.ORDER.BOOKING,
      isPrivate: true,
      Component: Booking,
      exact: true,
    },
  ],
};
