import styled from "styled-components/macro";
import tw from "twin.macro";
import { Form as _Form } from "formik";
import _Button from "designs/Button";

export const ElementWrapper = styled.div`
  ${tw`flex justify-end w-full cursor-pointer `}
`;

export const UpdateStatusContainer = styled.div`
  ${tw`bg-white p-1 phone:p-2 laptop:p-5 rounded-sm`}
`;

export const Form = styled(_Form)`
  ${tw`grid gap-2`}
`;

export const ButtonWrapper = styled.div`
  ${tw`flex justify-end mt-3`}
`;

export const Button = styled(_Button)`
  ${tw`py-1 w-max px-2 flex justify-center ml-1`}
`;

export const FormGroup = styled.div`
  ${tw``}
`;

export const FormContainer = styled.div`
  ${tw`flex flex-col gap-2`}
`;

export const RadioItem = {
  Container: styled.button`
    ${tw`flex items-center justify-between w-full`}
  `,
  LeftWrapper: styled.div`
    ${tw`flex items-center`}
  `,
  Label: styled.h3`
    ${tw`text-13 text-black`}
  `,
};

export const Point = styled.div`
  ${tw`p-[2px] mx-1 flex items-center justify-center w-2 h-2 overflow-hidden border border-solid rounded-full outline-none cursor-pointer  border-primary `}
`;

export const CustomRadio = styled.p<{ active: boolean }>`
  ${tw`w-full h-full duration-300 rounded-full cursor-pointer`}
  ${({ active }) => (active ? tw`bg-primary` : tw`bg-transparent`)}
`;

export const BoxPoint = styled.div`
  ${tw`w-max`}
`;
