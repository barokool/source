import React, { useState, useEffect } from "react";
import { toast } from "react-toastify";
import { Formik } from "formik";
import * as Yup from "yup";
import Select from "designs/SimpleSelect";
import Input from "designs/InputV2";
import Dialog from "components/Dialog";
import RichTextEditor from "designs/RichTextEditor";
import IncreaseToppingIcon from "icons/Increase/IncreaseTopping";
import DecreaseToppingIcon from "icons/Decrease/DecreaseTopping";
import { DialogTitle } from "common/styles/Title";

//languages
import { t } from "language";

//locals
import {
  ProductDialogContainer,
  Button,
  ButtonWrapper,
  ElementWrapper,
  Form,
  FormGroup,
  FormLabel,
  SelectUnit,
  TableWrapper,
  UpDownQuantityWrapper,
  TableItem,
  TableHeaderItem,
} from "./styles";

//redux
import { useSelector } from "react-redux";
import { IRootState } from "typings";

//api
import {
  ProductUnit,
  ProductUnitInput,
  Topping,
  ToppingInput,
  OrderItem,
  useUpdateOrderItem,
} from "apiCaller";

interface OrderItemDialog {
  ButtonMenu?: React.ReactElement;
  editFields?: OrderItem;
  title?: string;
  onClose?: () => void;
  onSuccess?: () => void;
  open?: boolean;
}

interface IFormValue {
  quantity?: number;
  product?: string;
  toppings?: ToppingInput[];
  unit?: ProductUnitInput;
  note?: string;
}

const validationSchema = Yup.object().shape<{ [key in keyof IFormValue]: any }>(
  {},
);

const OrderItemDialog: React.FC<OrderItemDialog> = props => {
  const {
    ButtonMenu,
    editFields,
    title,
    onClose,
    onSuccess,
    open = false,
  } = props;
  const { orderSelected } = useSelector((state: IRootState) => state.order);
  //local states
  const [initialValues, setInitialValues] = useState<IFormValue>({
    note: "",
    product: "",
    toppings: [],
    unit: {
      name: "",
      price: 0,
    },
    quantity: 1,
  });
  const [isOpen, setOpen] = useState<boolean>(open);
  const [listTopping, setListTopping] = useState<Topping[]>();
  const [toppingQuantity, setToppingQuantity] = useState<number[]>([]);
  const [listToppingInput, setListToppingInput] = useState<ToppingInput[]>([]);
  const [unitSelected, setUnitSelected] = useState<ProductUnit | null>(null);
  const [updateOrderItem, { data, loading, called }] = useUpdateOrderItem();

  useEffect(() => {
    if (editFields) {
      let tempToppingQuantities = new Array(
        editFields.product?.toppings?.length,
      ).fill(0);

      //Set an array of order toppings base on product toppings
      setListTopping(editFields.product?.toppings as Topping[]);

      //Set an array of quantity match with each topping of products in order item
      tempToppingQuantities.map((quantity, index) => {
        if (
          index <= (editFields?.toppings?.length as number) &&
          (editFields?.toppings?.length as number) > 0
        ) {
          tempToppingQuantities[index] =
            (editFields?.toppings?.[index]?.quantity as number) || 0;
        }
      });
      setToppingQuantity(tempToppingQuantities);

      //Set unit, note and quantity base on editfields
      setUnitSelected(editFields.unit);
      setInitialValues({
        note: editFields?.note as string,
        quantity: editFields?.quantity as number,
      });
    }
  }, [editFields]);

  useEffect(() => {
    if (data) {
      toast.success(t("selling.update-item-success"));
      onSuccess?.();
      onHandleClose();
    }
  }, [data]);

  //Set topping inputs everytime toppings and quantity of that topping
  useEffect(() => {
    if (listTopping) {
      setListToppingInput(
        listTopping.map((topping, index) => {
          if (toppingQuantity[index] > 0) {
            return {
              name: topping.name,
              price: topping.price,
              quantity: toppingQuantity[index],
            };
          }
        }) as ToppingInput[],
      );
    }
  }, [listTopping, toppingQuantity]);

  const onHandleClose = () => {
    setOpen(false);
    onClose && onClose();
  };

  const handleSubmit = (values: IFormValue) => {
    updateOrderItem({
      variables: {
        idOrder: orderSelected?._id as string,
        idOrderItem: editFields?._id as string,
        input: {
          note: values.note,
          product: editFields?.product?._id as string,
          quantity: values.quantity,
          unit: {
            name: unitSelected?.name as string,
            price: unitSelected?.price as number,
          },
          toppings: listToppingInput as ToppingInput[],
        },
      },
    });
  };

  return (
    <>
      <ElementWrapper
        onClick={() => setOpen(true)}
        className="flex justify-end w-full"
      >
        {ButtonMenu}
      </ElementWrapper>
      <Dialog
        size="md"
        open={isOpen}
        onClose={onHandleClose}
        className="overflow-auto"
      >
        <ProductDialogContainer>
          <DialogTitle>{title}</DialogTitle>
          <Formik
            enableReinitialize
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={handleSubmit}
          >
            <Form>
              <ElementWrapper className="flex flex-col gap-y-4">
                <FormGroup.Container>
                  <FormLabel.Container>
                    <FormLabel.Content>
                      {t("selling.enter-quantity")}
                    </FormLabel.Content>
                  </FormLabel.Container>
                  <SelectUnit.Container>
                    <Input
                      label={t("selling.quantity")}
                      placeholder={t("selling.enter-quantity")}
                      type="number"
                      name="quantity"
                    />
                  </SelectUnit.Container>
                </FormGroup.Container>
                <FormGroup.Container>
                  <FormLabel.Container>
                    <FormLabel.Content>
                      {t("selling.select-unit")}
                    </FormLabel.Content>
                  </FormLabel.Container>
                  <SelectUnit.Container>
                    <SelectUnit.Content>{t("selling.unit")}</SelectUnit.Content>
                    <SelectUnit.Content>
                      {t("selling.price")}
                    </SelectUnit.Content>
                    <Select
                      label={t("selling.unit")}
                      options={editFields?.product?.units as ProductUnit[]}
                      onSelect={unit => setUnitSelected(unit)}
                      optionSelected={unitSelected}
                    />
                    <SelectUnit.Price>
                      {unitSelected?.price?.toString().prettyMoney()}
                    </SelectUnit.Price>
                  </SelectUnit.Container>
                </FormGroup.Container>
                <FormGroup.Container>
                  <FormLabel.Container>
                    <FormLabel.Content>
                      {t("selling.select-unit")}
                    </FormLabel.Content>
                  </FormLabel.Container>
                  <TableWrapper>
                    <TableHeaderItem>{t("selling.name")}</TableHeaderItem>
                    <TableHeaderItem>{t("selling.quantity")}</TableHeaderItem>
                    <TableHeaderItem>{t("selling.price")}</TableHeaderItem>
                    {listTopping &&
                      listTopping.map((topping, index) => (
                        <>
                          <TableItem>{topping.name}</TableItem>
                          <UpDownQuantityWrapper>
                            <DecreaseToppingIcon
                              onClick={() => {
                                const newItems = [...toppingQuantity];
                                if (newItems[index] >= 1) {
                                  newItems[index]--;
                                }
                                setToppingQuantity(newItems);
                              }}
                            />
                            <TableItem>{toppingQuantity?.[index]}</TableItem>
                            <IncreaseToppingIcon
                              onClick={() => {
                                const newItems = [...toppingQuantity];
                                newItems[index]++;
                                setToppingQuantity(newItems);
                              }}
                            />
                          </UpDownQuantityWrapper>
                          <TableItem>
                            {(
                              (topping.price as number) *
                              toppingQuantity?.[index]
                            )
                              .toString()
                              .prettyMoney()}
                          </TableItem>
                        </>
                      ))}
                  </TableWrapper>
                </FormGroup.Container>
                <FormGroup.Container>
                  <FormLabel.Container>
                    <FormLabel.Content>{t("selling.note")}</FormLabel.Content>
                  </FormLabel.Container>
                  <RichTextEditor
                    name="note"
                    label="Note"
                    defaultValue={initialValues.note}
                  />
                </FormGroup.Container>
              </ElementWrapper>
              <ButtonWrapper>
                <Button outline type="button" onClick={onHandleClose}>
                  {t("common.cancel")}
                </Button>
                <Button primary type="submit" loading={loading}>
                  {t("common.accept")}
                </Button>
              </ButtonWrapper>
            </Form>
          </Formik>
        </ProductDialogContainer>
      </Dialog>
    </>
  );
};

export default OrderItemDialog;
