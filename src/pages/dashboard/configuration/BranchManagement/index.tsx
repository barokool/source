import React, { useCallback, useEffect, useMemo, useState } from "react";
import { RouteComponentProps } from "react-router";
import { PATH } from "common/constants/routes";
import { getQueryFromLocation } from "common/functions";
import ActionButtons from "designs/ActionButton";
import EmptyImage from "assets/images/product/ProductListEmpty.png";
//hooks
import { usePage } from "hooks/usePage";
import { useBreadcrumb } from "hooks/useBreadcrumb";
import { useLoading } from "hooks/useLoading";
import useAuth from "hooks/useAuth";

//styles
import ListDataLayout from "layouts/ListDataLayout";
import Table, { IColumns } from "designs/Table";
import BranchStatusTag from "components/BranchStatusTag";
import { SearchBox, AddButton } from "./styles";
import BranchManagementDialog from "./BranchManagementDialog";

//api
import {
  Branch,
  Location,
  BranchStatus,
  useGetAllBranch,
  useDeleteBranch,
} from "apiCaller";
import { fragmentGetAllBranch } from "services/branch";

//languages
import i18n, { t } from "language";
import { toast } from "react-toastify";
import Empty from "components/Empty";

const SIZE_PER_PAGE = 10;
const LOAD_DATA = "LOAD_DATA";

const BranchManagementPage: React.FC<RouteComponentProps> = ({ location }) => {
  const { startLoading, stopLoading } = useLoading();
  const [page, setPage] = usePage(getQueryFromLocation(location)?.page);
  const [getAllBranch, { data, data: getData, error: getError }] =
    useGetAllBranch(fragmentGetAllBranch);
  const [deleteBranch, { data: deleteData, error: deleteError }] =
    useDeleteBranch();
  const { results, totalCount } = data?.getAllBranch || {};
  const { accountInfo } = useAuth();

  useEffect(() => {
    invokeGetAllBranch();
  }, [page]);

  useEffect(() => {
    if (!getData && getError) {
      toast.error(t("common.error"));
    }
  }, [getData, getError]);

  useEffect(() => {
    if (deleteData && !deleteError) {
      toast.success(t("common.delete-success"));
    }
    if (!deleteData && deleteError) {
      toast.error(t("common.delete-fail"));
    }
  }, [deleteData, deleteError]);

  useBreadcrumb([
    {
      name: t("configuration.configuration"),
      href: PATH.CONFIGURATION.SELF,
    },
    {
      name: t("configuration.branch-management.branch-management"),
    },
  ]);

  const invokeGetAllBranch = (search: string = "") => {
    try {
      startLoading(LOAD_DATA);
      getAllBranch({
        variables: {
          size: 10,
          filterBranch: {
            name: search,
            userId: accountInfo?.userInfo?._id as string,
          },
        },
      });
    } catch (error) {
    } finally {
      stopLoading(LOAD_DATA);
    }
  };

  const handleChangePage = useCallback((nextPage: number) => {
    setPage(nextPage);
  }, []);

  const handleDelete = (record: Branch) => {
    deleteBranch({
      variables: {
        listId: [record?._id as string],
      },
    });
    invokeGetAllBranch();
  };

  const renderAction = (record: Branch) => {
    return (
      <ActionButtons
        buttons={{
          edit: {
            DialogContent: ({ onClose }) => {
              return (
                <BranchManagementDialog
                  onSuccess={() => {
                    invokeGetAllBranch();
                  }}
                  open
                  editField={record}
                  onClose={onClose}
                />
              );
            },
          },
          delete: {
            title: t("configuration.branch-management.delete-branch"),
            message: t("configuration.branch-management.confirm-delete"),
            onDelete: () => {
              handleDelete(record);
            },
          },
        }}
      />
    );
  };

  const columns: IColumns = useMemo(
    () => [
      {
        text: t("configuration.branch-management.name"),
        dataField: "name",
      },
      {
        text: t("configuration.branch-management.branch-code"),
        dataField: "code",
      },
      {
        text: t("configuration.branch-management.province/city"),
        dataField: "location",
        formatter: (location: Location) => location?.state?.name,
      },
      {
        text: t("configuration.branch-management.general-info.status"),
        dataField: "status",
        headerStyle: () => ({ width: "150px" }),
        formatter: (status: BranchStatus) => (
          <BranchStatusTag active={status}>
            {status == "opening"
              ? t("configuration.branch-management.status.opening")
              : ""}
            {status == "closed"
              ? t("configuration.branch-management.status.closed")
              : ""}
            {status === "pause"
              ? t("configuration.branch-management.status.pause")
              : ""}
            {status === "stopWorking"
              ? t("configuration.branch-management.status.stopWorking")
              : ""}
            {status === "isActive"
              ? t("configuration.branch-management.status.isActive")
              : ""}
          </BranchStatusTag>
        ),
      },
      {
        text: t("common.actions"),
        dataField: "",
        formatter: (_: string, record: Branch) => renderAction(record),
      },
    ],
    [page, i18n.language],
  );

  return totalCount === 0 ? (
    <Empty
      SVG={<img src={EmptyImage} width={200} height={200} />}
      Title={t("configuration.branch-management.empty-title")}
      SubTitle={t("configuration.branch-management.empty-message")}
      ButtonMenu={
        <BranchManagementDialog
          onSuccess={() => {
            invokeGetAllBranch();
          }}
          title={t("configuration.branch-management.add-new-branch")}
          ButtonMenu={
            <AddButton className="w-1/5">
              {t("configuration.branch-management.add-new-branch")}
            </AddButton>
          }
        />
      }
    />
  ) : (
    <ListDataLayout
      rightTopBar={
        <BranchManagementDialog
          onSuccess={() => {
            invokeGetAllBranch();
          }}
          title={t("configuration.branch-management.add-new-branch")}
          ButtonMenu={
            <AddButton className="w-1/5">
              {t("configuration.branch-management.add-new-branch")}
            </AddButton>
          }
        />
      }
      title={t("configuration.branch-management.branch")}
      leftTopBar={
        <SearchBox
          onFetchData={invokeGetAllBranch}
          placeholder={t("configuration.search-by-name-or-function")}
        />
      }
    >
      <Table
        columns={columns}
        data={results || []}
        totalSize={totalCount as number}
        sizePerPage={SIZE_PER_PAGE}
        onPageChange={handleChangePage}
        page={page}
        isRemote
      />
    </ListDataLayout>
  );
};

export default BranchManagementPage;
