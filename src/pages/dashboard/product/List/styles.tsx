import styled from "styled-components/macro";
import tw from "twin.macro";
import _Button from "designs/Button";
import _SearchBox from "components/SearchBoxTable";

export const Button = styled(_Button)`
  ${tw`flex items-center  justify-center text-16 font-medium  py-1 px-2 mt-2 w-max`}
`;

export const SearchBox = styled(_SearchBox)`
  ${tw`w-full phone:w-55 phone:mb-0`}
`;

export const TagWrapper = styled.div`
  ${tw`flex`}
`;

export const TagContent = styled.p`
  ${tw`mr-1`}
`;

export const Description = styled.p`
  ${tw``}
`;

export const RightTopBarWrapper = styled.div`
  ${tw`flex justify-between w-full`}
`;

export const ProductImage = styled.img`
  ${tw``}
`;
