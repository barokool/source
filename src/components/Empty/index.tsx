import { EmptyContainer, Bottom } from "./styles";

interface IEmptyProps {
  SVG?: JSX.Element;
  Title?: string;
  SubTitle?: string;
  ButtonMenu?: JSX.Element;
}

const Empty: React.FC<IEmptyProps> = ({ SVG, Title, SubTitle, ButtonMenu }) => {
  return (
    <EmptyContainer>
      {SVG}
      <Bottom.Wrapper>
        <Bottom.Title>{Title}</Bottom.Title>
        <Bottom.SubTitle>{SubTitle}</Bottom.SubTitle>
        {ButtonMenu}
      </Bottom.Wrapper>
    </EmptyContainer>
  );
};

export default Empty;
