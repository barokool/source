import { Fragment } from "react";
import { useDispatch, useSelector } from "react-redux";
import { IRootState } from "redux/storeToolkit";
import Drawer from "./components/Drawer";
import Header from "./components/Header";
import { setExtendDrawer } from "redux/slices/_config";

import {
  DashboardContainer,
  Viewpoint,
  HEIGHT_HEADER,
  ContentContainer,
  MainContent,
} from "./styles";
import Breadcrumb from "components/Breadcrumb";

const DashboardLayout: React.FC = props => {
  const { children } = props;
  const dispatch = useDispatch();
  const { isExtendDrawer } = useSelector((state: IRootState) => state._config);

  const handleClick = () => {
    dispatch(setExtendDrawer(!isExtendDrawer));
  };
  return (
    <DashboardContainer>
      <Header />
      <Drawer />
      <Viewpoint isExtendDrawer={isExtendDrawer}>
        <div
          style={{
            paddingTop: HEIGHT_HEADER,
          }}
        >
          <Drawer />
          <ContentContainer>
            <div className="flex flex-col gap-2 mt-2">
              <MainContent>
                <Breadcrumb />
                {children}
              </MainContent>
              {/* <Footer /> */}
            </div>
          </ContentContainer>
        </div>
      </Viewpoint>
    </DashboardContainer>
  );
};

export default DashboardLayout;
