import { useCallback, useLayoutEffect, useState } from "react";
import { useField, useFormikContext } from "formik";

import { randomId } from "common/functions";
import FormControlErrorHelper from "common/styles/FormControlErrorHelper";
import {
  RadioButtonContainer,
  Wrapper,
  Input,
  Label,
  StyleRadioButton,
  Title,
} from "./styles";

interface IValue {
  _id?: string;
  name?: string;
  value?: string;
  disabled?: boolean;
  [key: string]: any;
}

interface IRadioButtonProps {
  name: string;
  label?: string;
  values: IValue[];
  onChange?: (value: IValue) => void;
  fieldLabel?: string;
  fieldValue?: string;
  vertical?: boolean;
  disabled?: boolean;
}

const RadioButton = (props: IRadioButtonProps) => {
  const {
    values = [],
    label,
    name,
    onChange,
    fieldLabel = "name",
    fieldValue = "_id",
    vertical = false,
    disabled = false,
    ...restProps
  } = props;
  const [id, setId] = useState(randomId());

  const [field, meta] = useField(name);
  const [checkedIndex, setCheckedIndex] = useState(0);

  const { setFieldValue } = useFormikContext();

  const isError: boolean = Boolean(!!meta.error && !!meta.touched);

  const handleChange = useCallback((value: IValue, index: number) => {
    setCheckedIndex(index);
    onChange && onChange(value);
    setFieldValue(name, value[fieldValue]);
  }, []);

  useLayoutEffect(() => {
    if (field.value) {
      values.forEach((value, index) => {
        if (value[fieldValue] === field.value) {
          setCheckedIndex(index);
        }
      });
    }
  }, [field.value]);

  return (
    <RadioButtonContainer vertical={vertical}>
      {label && <Title>{label}</Title>}
      {values.map((value, index) => (
        <Wrapper
          key={`radio-button-${id}-${index}`}
          disabled={value.disabled || disabled}
        >
          <Input
            id={`option-${index}`}
            type="radio"
            value={value}
            onChange={() => handleChange(value, index)}
            name={name}
            {...(restProps as any)}
          />
          <Label htmlFor={`option-${index}`}>
            <StyleRadioButton
              checked={checkedIndex === index}
              isError={isError}
              disabled={value.disabled || disabled}
            />
            {value[fieldLabel]}
          </Label>
        </Wrapper>
      ))}
      {isError && (
        <FormControlErrorHelper>{meta?.error}</FormControlErrorHelper>
      )}
    </RadioButtonContainer>
  );
};

export default RadioButton;
