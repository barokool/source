import styled from "styled-components/macro";
import tw, { TwStyle } from "twin.macro";
import { Form as _Form } from "formik";
import _Button from "designs/Button";
import { OrderResponseStatus } from "./index";

export const OrderResponseDialogContainer = styled.div`
  ${tw`rounded-sm py-3 px-2 bg-white`}
`;

export const ElementWrapper = styled.div`
  ${tw`cursor-pointer`}
`;

export const Label = styled.h1`
  ${tw`text-16 text-black font-bold mt-2.5 mb-0.5`}
`;

export const Description = styled.div`
  ${tw`text-13 text-black mb-2`}
`;

export const Button = styled(_Button)`
  ${tw` px-5.5 py-1.5 mx-auto bg-primary `}
`;
