import { Table, useGetAllTable } from "apiCaller";
import { useState, useCallback, useEffect } from "react";
import { fragmentGetAllTable } from "services/table";
export const useTable = () => {
  const [listTable, setListTable] = useState<Table[]>([]);
  const [totalCount, setTotalCount] = useState(0);
  useEffect(() => {
    setAllTable();
  }, []);
  const setAllTable = useCallback(async () => {
    const [getAllTable, { data, called, loading }] =
      useGetAllTable(fragmentGetAllTable);
    await getAllTable({
      variables: {
        page: 0,
        size: 999,
        filter: {},
      },
    });
    setTotalCount(data?.getAllTable?.totalCount as number);
    setListTable(data?.getAllTable?.results || []);
  }, []);

  return {
    listTable,
    setListTable,
    setAllTable,
    totalCount,
  };
};
