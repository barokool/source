import styled from "styled-components/macro";
import tw from "twin.macro";
import _Button from "designs/Button";

export const OrderDetailContainer = styled.div`
  ${tw`w-full px-2`}
`;

export const ButtonWrapper = styled.div`
  ${tw`flex justify-end mt-3`}
`;

export const Button = styled(_Button)`
  ${tw`py-1 px-2 w-max flex justify-center ml-1`}
`;

export const Detail = {
  Wrapper: styled.div`
    ${tw`mb-3.5`}
  `,
  Title: styled.h1`
    ${tw`text-20 text-secondary font-semibold`}
  `,
  ChildrenWrapper: styled.div`
    ${tw`flex justify-between w-full mt-1`}
  `,

  LeftContent: styled.h1`
    ${tw`text-black text-14`}
  `,

  RightContent: styled.h1`
    ${tw`text-black text-14`}
  `,
};

export const OrderDetailTopContainer = styled.div`
  ${tw`flex justify-between items-center mb-4.5`}
`;

export const TotalBill = {
  Container: styled.div`
    ${tw`flex items-center`}
  `,
  Label: styled.h1`
    ${tw`text-16 text-black mr-3`}
  `,
  Tag: styled.div`
    ${tw`p-1 bg-gray text-black text-16`}
  `,
};

export const Driver = {
  Container: styled.div`
    ${tw`block`}
  `,

  Label: styled.h3`
    ${tw`text-body-color text-14`}
  `,

  Content: styled.h3`
    ${tw`text-14 text-black`}
  `,
};
