import { useState, useCallback, useMemo, useEffect } from "react";
import { toast } from "react-toastify";
import { PATH } from "common/constants/routes";
import { getQueryFromLocation } from "common/functions/route";
import { RouteComponentProps } from "react-router";
import Table, { IColumns } from "designs/Table";
import ActionButtons from "designs/ActionButton";
import Empty from "components/Empty";
import ListDataLayout from "layouts/ListDataLayout";
import EmptyImage from "assets/images/product/ProductListEmpty.png";

//hooks
import { usePage } from "hooks/usePage";
import { useBreadcrumb } from "hooks/useBreadcrumb";
import { useCurrentBranch } from "hooks/useCurrentBranch";
import { useLoading } from "hooks/useLoading";
import { useDebouncedState } from "hooks/useDebouncedState";

//languages
import i18n, { t } from "language";

//api
import { Coupon, useGetAllCoupon, useDeleteCoupon, Branch } from "apiCaller";
import { fragmentGetAllCoupon } from "services/coupon";

//locals
import { AddButton, SearchBox, CouponContainer } from "./styles";
import CouponDialog from "./CouponDialog";

const SIZE_PER_PAGE = 10;
const LOAD_DATA = "LOAD_DATA";
const DEBOUNCE_TIME = 1000;

interface ISearchDependencies {
  branchSelected: Branch | null;
  text: string;
}

interface ICouponListProps extends RouteComponentProps {}

const CouponList: React.FC<ICouponListProps> = ({ location }) => {
  const { branchSelected } = useCurrentBranch();
  const { startLoading, stopLoading } = useLoading();

  //local state
  const [listCoupon, setListCoupon] = useState<Coupon[]>([]);
  const [page, setPage] = usePage(getQueryFromLocation(location)?.page);
  const [totalSize, setTotalSize] = useState(0);
  const [actionSuccess, setActionSuccess] = useState<boolean>(false);
  const [search, setSearch] = useDebouncedState<ISearchDependencies>(
    {
      branchSelected: null,
      text: "",
    },
    objectSearch => {
      if (objectSearch.branchSelected) {
        invokeGetAllDataAPI(objectSearch.text, objectSearch.branchSelected);
      }
    },
    DEBOUNCE_TIME,
  );

  //api
  const [
    getAllCoupon,
    {
      data: getAllCouponData,
      error: getAllCouponErrors,
      loading: getAllCouponLoading,
    },
  ] = useGetAllCoupon(fragmentGetAllCoupon);
  const [
    deleteCoupon,
    {
      loading: deleteCouponLoading,
      error: deleteCouponErrors,
      data: deleteCouponData,
    },
  ] = useDeleteCoupon();

  useBreadcrumb([
    {
      name: t("coupon.coupon"),
    },
    {
      name: t("coupon.coupon-code.code"),
      href: PATH.COUPON.COUPON_CODE,
    },
  ]);

  useEffect(() => {
    branchSelected && invokeGetAllDataAPI("", branchSelected);
  }, [page, branchSelected]);

  useEffect(() => {
    console.log("Update lai di");
    if (actionSuccess === true && branchSelected) {
      invokeGetAllDataAPI("", branchSelected);
    }
    setActionSuccess(false);
  }, [actionSuccess]);

  useEffect(() => {
    if (getAllCouponLoading || deleteCouponLoading) {
      startLoading(LOAD_DATA);
    } else {
      stopLoading(LOAD_DATA);
    }
  }, [getAllCouponLoading, deleteCouponLoading]);

  useEffect(() => {
    branchSelected &&
      setSearch({
        ...search,
        branchSelected,
      });
  }, [branchSelected]);

  useEffect(() => {
    if (getAllCouponData) {
      setListCoupon(getAllCouponData.getAllCoupon?.results || []);
      setTotalSize(getAllCouponData.getAllCoupon.totalCount as number);
    }
  }, [getAllCouponData]);

  //toast errors when they occur
  useEffect(() => {
    getAllCouponErrors && toast.error(getAllCouponErrors.message);
    deleteCouponErrors && toast.error(deleteCouponErrors.message);
  }, [getAllCouponErrors, deleteCouponErrors]);

  useEffect(() => {
    if (deleteCouponData && !deleteCouponErrors) {
      toast.success(t("coupon.coupon-code.delete-success"));
      setActionSuccess(true);
    }
  }, [deleteCouponData]);

  //get table types filter on branch and name
  const invokeGetAllDataAPI = (searchText: string = "", branch: Branch) => {
    getAllCoupon({
      variables: {
        page: page - 1,
        size: SIZE_PER_PAGE,
        filter: {
          branches: [branch._id || ""] as string[],
          name: searchText,
        },
      },
    });
  };

  const onPageChange = useCallback((nextPage: number) => {
    setPage(nextPage);
  }, []);

  const renderAction = (record: Coupon) => {
    return (
      <ActionButtons
        buttons={{
          edit: {
            DialogContent: () => {
              return (
                <CouponDialog
                  open
                  editField={record}
                  title={t("coupon.coupon-code.edit")}
                  onSuccess={() => setActionSuccess(true)}
                />
              );
            },
          },
          delete: {
            title: t("coupon.coupon-code.delete"),
            message: t("coupon.coupon-code.confirm-delete"),
            onDelete: () =>
              deleteCoupon({
                variables: { listId: [record._id as string] },
              }),
          },
        }}
      />
    );
  };

  const columns: IColumns = useMemo(
    () => [
      {
        text: t("coupon.coupon-code.name"),
        dataField: "name",
      },
      {
        text: t("coupon.coupon-code.coupon-code"),
        dataField: "code",
      },
      {
        text: t("coupon.coupon-code.start-date"),
        dataField: "openingDate",
        formatter: (dateStart: Date) => dateStart?.toString().prettyDate(),
      },
      {
        text: t("coupon.coupon-code.end-date"),
        dataField: "closeDate",
        formatter: (dateEnd: Date) => dateEnd?.toString().prettyDate(),
      },
      {
        text: t("coupon.coupon-code.quantity"),
        dataField: "quantity",
      },
      {
        text: t("coupon.coupon-code.value"),
        dataField: "value",
      },
      {
        text: "",
        dataField: "actions",
        headerStyle: () => ({ display: "none" }),
        formatter: (_: string, record: Coupon) => renderAction(record),
      },
    ],
    [page, actionSuccess, i18n.language],
  );

  return totalSize === 0 && !search.text ? (
    <Empty
      SVG={<img src={EmptyImage} width={200} height={200} />}
      Title={t("coupon.coupon-code.empty.title")}
      SubTitle={t("coupon.coupon-code.empty.message")}
      ButtonMenu={
        <CouponDialog
          onSuccess={() => setActionSuccess(true)}
          title={t("coupon.coupon-code.add")}
          ButtonMenu={
            <AddButton primary className="mx-auto mt-2">
              {t("coupon.coupon-code.add")}
            </AddButton>
          }
        />
      }
    />
  ) : (
    <CouponContainer>
      <ListDataLayout
        rightTopBar={
          <CouponDialog
            onSuccess={() => setActionSuccess(true)}
            title={t("coupon.coupon-code.add")}
            ButtonMenu={
              <AddButton primary>{t("coupon.coupon-code.add")}</AddButton>
            }
          />
        }
        leftTopBar={
          <SearchBox
            onFetchData={text =>
              setSearch({
                ...search,
                text,
              })
            }
            placeholder={t("coupon.coupon-code.search")}
          />
        }
        title={t("coupon.coupon-code.coupon-code")}
      >
        <Table
          columns={columns}
          data={listCoupon || []}
          totalSize={totalSize}
          sizePerPage={SIZE_PER_PAGE}
          onPageChange={onPageChange}
          page={page}
          isRemote
        />
      </ListDataLayout>
    </CouponContainer>
  );
};
export default CouponList;
