/* eslint-disable */

// *******************************************************
// *******************************************************
// USUM SOFTWARE
// GENERATED FILE, DO NOT MODIFY
//
// *******************************************************
// *******************************************************
// 💙

export type Maybe<T> = T | null;

type NonNullable<T> = Exclude<T, null | undefined>;

type KeysMatching<T, V> = {
  [K in keyof T]-?: T[K] extends V ? K : never;
}[keyof T];

type KeysNotMatching<T, V> = {
  [K in keyof T]-?: T[K] extends V ? never : K;
}[keyof T];

type MatchingType = string | number | boolean | string[] | number[] | boolean[];

type FilterMaybe<T> = { [k in keyof T]: NonNullable<T[k]> };

type GenFieldsAll<T> = (
  | KeysMatching<T, MatchingType>
  | {
      [k in KeysNotMatching<T, MatchingType>]?: T[k] extends any[]
        ? GenFields<T[k][number]>
        : GenFields<T[k]>;
    }
)[];

export type GenFields<T> = T extends any[]
  ? GenFieldsAll<FilterMaybe<T[number]>>
  : GenFieldsAll<FilterMaybe<T>>;

const queryBuilder = <T>(fields?: GenFields<T>): string => {
  return fields
    ? fields
        .map((field: any) => {
          if (typeof field === "object") {
            let result = "";

            Object.entries<any>(field as Record<string, any>).forEach(
              ([key, values], index, array) => {
                result += `${key} ${
                  values.length > 0 ? "{ " + queryBuilder(values) + " }" : ""
                }`;

                // If it's not the last item in array, join with comma
                if (index < array.length - 1) {
                  result += ", ";
                }
              },
            );

            return result;
          } else {
            return `${field}`;
          }
        })
        .join("\n ")
    : "";
};

const guessFragmentType = (fragment: string | DocumentNode) => {
  let isString = false;
  let isFragment = false;
  let fragmentName = "";
  if (typeof fragment === "string") {
    isString = true;
  } else if (typeof fragment === "object" && fragment.definitions.length) {
    isFragment = true;
    const definition = fragment.definitions[0];
    if (definition.kind === "FragmentDefinition") {
      fragmentName = definition.name.value;
    } else {
      throw new Error(
        `The argument passed is not a fragment definition, got ${definition.kind} instead`,
      );
    }
  }
  return { isString, isFragment, fragmentName };
};

import {
  DocumentNode,
  gql,
  useMutation,
  useLazyQuery,
  useSubscription,
  QueryHookOptions,
  MutationHookOptions,
  SubscriptionHookOptions,
  MutationTuple,
} from "@apollo/client";

export interface Ads {
  _id: Maybe<string>;
  code: Maybe<string>;
  dictionary: Maybe<AdsDictionary>;
  image: Maybe<Image>;
  isAdsCode: Maybe<boolean>;
  keyword: Maybe<string>;
  name: Maybe<string>;
  path: Maybe<string>;
  position: Maybe<Category>;
  slug: Maybe<string>;
}

export interface AdsCreateInput {
  code?: string;
  dictionary?: AdsDictionaryInput;
  image?: ImageInput;
  isAdsCode?: boolean;
  name: string;
  path?: string;
  /** Category ObjectId*/
  position: string;
}

export interface AdsDictionary {
  german: Maybe<AdsDictionaryDetail>;
  vietnamese: Maybe<AdsDictionaryDetail>;
}

export interface AdsDictionaryDetail {
  code: Maybe<string>;
  image: Maybe<Image>;
  keyword: Maybe<string>;
  name: Maybe<string>;
  slug: Maybe<string>;
}

export interface AdsDictionaryDetailInput {
  code?: string;
  image?: ImageInput;
  name?: string;
}

export interface AdsDictionaryInput {
  german?: AdsDictionaryDetailInput;
  vietnamese?: AdsDictionaryDetailInput;
}

export interface AdsResult {
  results: Maybe<Ads[]>;
  totalCount: Maybe<number>;
}

export interface AdsUpdateInput {
  code?: string;
  dictionary?: AdsDictionaryInput;
  image?: ImageInput;
  isAdsCode?: boolean;
  name?: string;
  path?: string;
  /** Category ObjectId*/
  position?: string;
}

export interface Branch {
  _id: Maybe<string>;
  businessProductTypes: Maybe<BusinessProductType[]>;
  code: Maybe<string>;
  createdAt: Maybe<string>;
  dictionary: Maybe<BranchDictionary>;
  images: Maybe<Image[]>;
  introduction: Maybe<string>;
  isMainBranch: Maybe<boolean>;
  keyword: Maybe<string>;
  location: Maybe<Location>;
  maxGuest: Maybe<number>;
  name: Maybe<string>;
  operationTimes: Maybe<BranchOperationTime[]>;
  phoneNumber: Maybe<string>;
  serviceType: Maybe<ServiceType>;
  shippingFee: Maybe<BranchShippingFee>;
  slug: Maybe<string>;
  status: Maybe<BranchStatus>;
  tables: Maybe<Table[]>;
}

export interface BranchDictionary {
  german: Maybe<BranchDictionaryDetail>;
  vietnamese: Maybe<BranchDictionaryDetail>;
}

export interface BranchDictionaryDetail {
  images: Maybe<Image[]>;
  introduction: Maybe<string>;
  keyword: Maybe<string>;
  name: Maybe<string>;
  slug: Maybe<string>;
}

export interface BranchDictionaryDetailInput {
  images?: ImageInput[];
  introduction?: string;
  name?: string;
}

export interface BranchDictionaryInput {
  german?: BranchDictionaryDetailInput;
  vietnamese?: BranchDictionaryDetailInput;
}

export interface BranchOperationTime {
  day: Maybe<DayOfWeek>;
  times: Maybe<BranchTime[]>;
}

export interface BranchOperationTimeInput {
  day: DayOfWeek;
  times: BranchTimeInput[];
}

export interface BranchResult {
  results: Branch[];
  totalCount: Maybe<number>;
}

export interface BranchShippingFee {
  distanceDefault: Maybe<number>;
  /** Default currency is USD*/
  priceDefault: Maybe<number>;
  /** Default currency is USD*/
  pricePerKilometer: Maybe<number>;
}

export interface BranchShippingFeeInput {
  distanceDefault: number;
  /** Default currency is USD*/
  priceDefault: number;
  /** Default currency is USD*/
  pricePerKilometer: number;
}

export enum BranchStatus {
  Closed = "closed",
  Isactive = "isActive",
  Opening = "opening",
  Pause = "pause",
  Stopworking = "stopWorking",
}
export interface BranchTime {
  closeHours: Maybe<string>;
  openingHours: Maybe<string>;
}

export interface BranchTimeInput {
  closeHours?: string;
  openingHours?: string;
}

export interface BusinessProductType {
  _id: Maybe<string>;
  createdAt: Maybe<string>;
  dictionary: Maybe<BusinessProductTypeDictionary>;
  image: Maybe<Image>;
  keyword: Maybe<string>;
  name: Maybe<string>;
  serviceType: Maybe<ServiceType>;
  slug: Maybe<string>;
  updatedAt: Maybe<string>;
}

export interface BusinessProductTypeDictionary {
  german: Maybe<BusinessProductTypeDictionaryDetail>;
  vietnamese: Maybe<BusinessProductTypeDictionaryDetail>;
}

export interface BusinessProductTypeDictionaryDetail {
  image: Maybe<Image>;
  keyword: Maybe<string>;
  name: Maybe<string>;
  slug: Maybe<string>;
}

export interface BusinessProductTypeDictionaryDetailInput {
  image?: ImageInput;
  name?: string;
}

export interface BusinessProductTypeDictionaryInput {
  german?: BusinessProductTypeDictionaryDetailInput;
  vietnamese?: BusinessProductTypeDictionaryDetailInput;
}

export interface BusinessProductTypeInput {
  dictionary?: BusinessProductTypeDictionaryInput;
  image?: ImageInput;
  name?: string;
  serviceType?: string;
}

export interface BusinessProductTypeResult {
  results: Maybe<BusinessProductType[]>;
  totalCount: Maybe<number>;
}

export interface Category {
  _id: Maybe<string>;
  categoryCode: Maybe<CategoryCode>;
  createdAt: Maybe<string>;
  description: Maybe<string>;
  dictionary: Maybe<CategoryDictionary>;
  image: Maybe<Image>;
  keyword: Maybe<string>;
  name: Maybe<string>;
  slug: Maybe<string>;
  updatedAt: Maybe<string>;
}

export enum CategoryCode {
  Adscode = "adsCode",
  Adsimage = "adsImage",
  Home = "home",
  Seokeyword = "seoKeyword",
  Size = "size",
  Staticpage = "staticPage",
}
export interface CategoryDictionary {
  german: Maybe<CategoryDictionaryDetail>;
  vietnamese: Maybe<CategoryDictionaryDetail>;
}

export interface CategoryDictionaryDetail {
  description: Maybe<string>;
  image: Maybe<Image>;
  keyword: Maybe<string>;
  name: Maybe<string>;
  slug: Maybe<string>;
}

export interface CategoryDictionaryDetailInput {
  description?: string;
  image?: ImageInput;
  keyword?: string;
  name?: string;
  slug?: string;
}

export interface CategoryDictionaryInput {
  german?: CategoryDictionaryDetailInput;
  vietnamese?: CategoryDictionaryDetailInput;
}

export interface CategoryResult {
  results: Maybe<Category[]>;
  totalCount: Maybe<number>;
}

export interface ChangePasswordInput {
  newPassword: string;
  oldPassword: string;
  phoneNumber: string;
}

export interface City {
  _id: Maybe<string>;
  createdAt: Maybe<string>;
  /** Most cities belong to district, but Berlin, Bremen, Bremerhaven, Düsseldorf, Hamburg belong to state*/
  district: Maybe<District>;
  geoLocation: Maybe<GeoLocation>;
  keyword: Maybe<string>;
  name: Maybe<string>;
  slug: Maybe<string>;
  /** Just Berlin, Bremen, Bremerhaven, Düsseldorf, Hamburg have state*/
  state: Maybe<State>;
  updatedAt: Maybe<string>;
}

export interface CityResult {
  results: Maybe<City[]>;
  totalCount: Maybe<number>;
}

export interface Country {
  _id: Maybe<string>;
  alpha2Code: Maybe<string>;
  createdAt: Maybe<string>;
  icon: Maybe<string>;
  keyword: Maybe<string>;
  name: Maybe<string>;
  prefixPhone: Maybe<string>;
  slug: Maybe<string>;
  updatedAt: Maybe<string>;
}

export interface CountryCreateInput {
  alpha2Code: string;
  icon: string;
  name: string;
  prefixPhone: string;
}

export interface CountryResult {
  results: Maybe<Country[]>;
  totalCount: Maybe<number>;
}

export interface CountryUpdateInput {
  alpha2Code?: string;
  icon?: string;
  name?: string;
  prefixPhone?: string;
}

export interface Coupon {
  _id: Maybe<string>;
  applyForBooking: Maybe<boolean>;
  applyForShipping: Maybe<boolean>;
  branches: Maybe<Branch[]>;
  closeDate: Maybe<string>;
  code: Maybe<string>;
  createdAt: Maybe<string>;
  currency: Maybe<CurrencyEnum>;
  description: Maybe<string>;
  dictionary: Maybe<CouponDictionary>;
  keyword: Maybe<string>;
  maxValue: Maybe<number>;
  method: Maybe<CouponMethodEnum>;
  name: Maybe<string>;
  openingDate: Maybe<string>;
  products: Maybe<Product[]>;
  quantity: Maybe<number>;
  redemptionPoints: Maybe<number>;
  slug: Maybe<string>;
  status: Maybe<CouponStatusEnum>;
  updatedAt: Maybe<string>;
  value: Maybe<number>;
}

export interface CouponCreateInput {
  /** @default true*/
  applyForBooking?: boolean;
  /** @default true*/
  applyForShipping?: boolean;
  branches: string[];
  closeDate: string;
  code: string;
  description?: string;
  dictionary?: CouponDictionaryInput;
  /** Min is 0.01 USD*/
  maxValue?: number;
  method: CouponMethodEnum;
  name: string;
  openingDate: string;
  products: string[];
  /** @default 0*/
  quantity?: number;
  /** @default 0*/
  redemptionPoints?: number;
  status?: CouponStatusEnum;
  /** @default 0Default currency is USD*/
  value?: number;
}

export interface CouponDictionary {
  german: Maybe<CouponDictionaryDetail>;
  vietnamese: Maybe<CouponDictionaryDetail>;
}

export interface CouponDictionaryDetail {
  description: Maybe<string>;
  keyword: Maybe<string>;
  name: Maybe<string>;
  slug: Maybe<string>;
}

export interface CouponDictionaryDetailInput {
  description?: string;
  name?: string;
}

export interface CouponDictionaryInput {
  german?: CouponDictionaryDetailInput;
  vietnamese?: CouponDictionaryDetailInput;
}

export enum CouponMethodEnum {
  Cash = "cash",
  Percent = "percent",
}
export interface CouponResult {
  results: Maybe<Coupon[]>;
  totalCount: Maybe<number>;
}

export enum CouponStatusEnum {
  Expire = "expire",
  Instock = "inStock",
}
export interface CouponUpdateInput {
  applyForBooking?: boolean;
  applyForShipping?: boolean;
  branches?: string[];
  closeDate?: string;
  code?: string;
  description?: string;
  dictionary?: CouponDictionaryInput;
  maxValue?: number;
  method?: CouponMethodEnum;
  name?: string;
  openingDate?: string;
  products?: string[];
  quantity?: number;
  redemptionPoints?: number;
  status?: CouponStatusEnum;
  /** Default currency is USD*/
  value?: number;
}

export interface CreateBranchInput {
  businessProductTypes?: string[];
  dictionary?: BranchDictionaryInput;
  images?: ImageInput[];
  introduction?: string;
  location?: LocationInput;
  maxGuest?: number;
  name?: string;
  operationTimes: BranchOperationTimeInput[];
  phoneNumber?: string;
  serviceType?: string;
  status: BranchStatus;
  tables?: string[];
}

export interface CreateCategoryInput {
  /** @default home*/
  categoryCode?: CategoryCode;
  description?: string;
  dictionary?: CategoryDictionaryInput;
  image?: ImageInput;
  name: string;
}

export interface CreateManagerRequestInput {
  businessProductTypes: string[];
  email?: string;
  images?: ImageInput[];
  introduction?: string;
  location: LocationInput;
  maxGuest?: number;
  nameBranch: string;
  operationTimes?: BranchOperationTimeInput[];
  password?: string;
  phoneNumber: string;
  serviceType: string;
  /** @default opening*/
  statusBranch?: BranchStatus;
  tables?: string[];
}

export interface CreateUserInput {
  /** @default 0*/
  accumulatePoints?: number;
  avatar?: ImageInput;
  birthday?: string;
  branches?: string[];
  clientId?: string;
  deliveryAddresses?: DeliveryAddressInput[];
  displayName?: string;
  email?: string;
  gender?: Gender;
  location?: LocationInput;
  password: string;
  paymentInfo?: PaymentInfoInput;
  phoneNumber: string;
  provider?: string;
  /** @default bronze*/
  ranking?: UserRanking;
  role: string;
  status?: UserStatus;
}

export enum CurrencyEnum {
  Eur = "EUR",
}
/** Recent order can only get: code, type, price, customerName, createdAt, updatedAt, phoneNumber, createdBy, driver */
export interface Dashboard {
  recentOrderBookingTable: Maybe<Order[]>;
  recentOrderDelivery: Maybe<Order[]>;
  revenueTotal: Maybe<number>;
  totalOrderBookingTable: Maybe<number>;
  totalOrderDelivery: Maybe<number>;
  totalOrderInPlace: Maybe<number>;
}

export enum DayOfWeek {
  Friday = "friday",
  Monday = "monday",
  Saturday = "saturday",
  Sunday = "sunday",
  Thursday = "thursday",
  Tuesday = "tuesday",
  Wednesday = "wednesday",
}
export interface DeliveryAddress {
  city: Maybe<City>;
  country: Maybe<Country>;
  displayName: Maybe<string>;
  district: Maybe<District>;
  isDefault: Maybe<boolean>;
  note: Maybe<string>;
  phoneNumber: Maybe<string>;
  state: Maybe<State>;
  type: Maybe<DeliveryAddressEnum>;
}

export enum DeliveryAddressEnum {
  Home = "home",
  Work = "work",
}
export interface DeliveryAddressInput {
  city?: string;
  country?: string;
  displayName?: string;
  district?: string;
  isDefault?: boolean;
  note?: string;
  phoneNumber?: string;
  state?: string;
  type?: DeliveryAddressEnum;
}

export interface District {
  _id: Maybe<string>;
  keyword: Maybe<string>;
  name: Maybe<string>;
  slug: Maybe<string>;
  state: Maybe<State>;
}

export interface DistrictInput {
  name?: string;
  state?: string;
}

export interface DistrictResult {
  results: Maybe<District[]>;
  totalCount: Maybe<number>;
}

export interface FilterAds {
  isAdsCode?: boolean;
  name?: string;
  /** Category ObjectId*/
  position?: string;
}

export interface FilterBranch {
  code?: string;
  name?: string;
  status?: BranchStatus[];
  userId?: string;
}

export interface FilterBusinessProductType {
  name?: string;
  serviceType?: string;
}

export interface FilterCategory {
  categoryCode?: CategoryCode;
  name?: string;
  slug?: string;
}

export interface FilterCity {
  district?: string;
  name?: string;
  state?: string;
}

export interface FilterCountry {
  alpha2Code?: string;
  name?: string;
  prefixPhone?: string;
}

export interface FilterCoupon {
  branches?: string[];
  code?: string;
  method?: CouponMethodEnum;
  name?: string;
  products?: string[];
  status?: CouponStatusEnum;
}

export interface FilterOrder {
  buyer?: string;
  code?: string;
  fromBranch?: string;
  orderDate?: string;
  status?: OrderStatusEnum;
  type?: OrderTypeEnum;
}

export interface FilterPrinter {
  branch?: string;
  name?: string;
}

export interface FilterProduct {
  branch?: string;
  name?: string;
  productCategory?: string;
}

export interface FilterProductCategory {
  branch?: string;
  businessProductType?: string;
  name?: string;
}

export enum FilterReportDateEnum {
  Last_month = "LAST_MONTH",
  Last_quarter = "LAST_QUARTER",
  Last_week = "LAST_WEEK",
  Last_year = "LAST_YEAR",
  This_month = "THIS_MONTH",
  This_quarter = "THIS_QUARTER",
  This_week = "THIS_WEEK",
  This_year = "THIS_YEAR",
  Today = "TODAY",
  Yesterday = "YESTERDAY",
}
export interface FilterReportRevenue {
  filterDate: FilterReportDateEnum;
  idBranch: string;
  orderCode?: string;
}

export interface FilterRole {
  idBranch?: string;
  name?: RoleEnum;
  position?: string;
}

export interface FilterServiceType {
  isBooking?: boolean;
  isDelivery?: boolean;
  name?: string;
}

export interface FilterStaticPage {
  name?: string;
}

export interface FilterStoreImage {
  folderId?: string;
  isAdmin?: boolean;
  isSample?: boolean;
  name?: string;
  sort?: SortEnum;
  storeId?: string;
}

export interface FilterTable {
  code?: number;
  status?: TableStatus;
  tableType?: string;
}

export interface FilterTableType {
  branch?: string;
  name?: string;
}

export interface FilterTag {
  name?: string;
}

export interface FilterTax {
  branches?: string[];
  name?: string;
}

export interface FilterUser {
  branches?: string[];
  displayName?: string;
  ranking?: UserRanking;
  /** This is idRole*/
  role?: RoleEnum;
  status?: UserStatus;
}

export interface FilterUserFolder {
  fromStore?: string;
  name?: string;
}

export interface ForgotPasswordInput {
  idToken: string;
  newPassword: string;
  phoneNumber: string;
}

export enum Gender {
  Female = "female",
  Male = "male",
}
export interface GeoLocation {
  /** [lng, lat]*/
  coordinates: Maybe<number[]>;
  type: Maybe<string>;
}

export interface GroupToppingDictionaryDetail {
  name: Maybe<string>;
  toppings: Maybe<ToppingDictionaryDetail[]>;
}

export interface GroupToppingDictionaryDetailInput {
  name?: string;
  toppings?: ToppingDictionaryDetailInput[];
}

export interface IJwtPayload {
  id: Maybe<string>;
}

export interface Image {
  alt: Maybe<string>;
  base64: Maybe<string>;
  default: Maybe<string>;
  description: Maybe<string>;
  medium: Maybe<string>;
  small: Maybe<string>;
}

export interface ImageInput {
  alt?: string;
  base64?: string;
  default?: string;
  description?: string;
  medium?: string;
  small?: string;
}

export interface JwtPayload {
  accessToken: Maybe<string>;
  payload: Maybe<JwtPayload>;
  refreshToken: Maybe<string>;
  userId: Maybe<IJwtPayload>;
  userInfo: Maybe<User>;
}

export interface Location {
  addressLine: Maybe<string>;
  city: Maybe<City>;
  country: Maybe<Country>;
  district: Maybe<District>;
  isDefault: Maybe<boolean>;
  state: Maybe<State>;
}

export interface LocationInput {
  addressLine?: string;
  city?: string;
  country?: string;
  district?: string;
  state?: string;
}

export interface LoginUserInput {
  password: string;
  phoneNumber: string;
}

export interface Order {
  _id: Maybe<string>;
  barcode: Maybe<string>;
  buyer: Maybe<User>;
  cancellationReason: Maybe<string>;
  code: Maybe<string>;
  coupon: Maybe<Coupon>;
  createdAt: Maybe<string>;
  createdBy: Maybe<User>;
  customerName: Maybe<string>;
  customerPay: Maybe<number>;
  deliveryAddress: Maybe<DeliveryAddress>;
  driver: Maybe<User>;
  excessMoney: Maybe<number>;
  fromBranch: Maybe<Branch>;
  orderItems: Maybe<OrderItem[]>;
  paymentType: Maybe<PaymentTypeEnum>;
  phoneNumber: Maybe<string>;
  price: Maybe<OrderPrice>;
  shippingTime: Maybe<string>;
  status: Maybe<OrderStatusEnum>;
  table: Maybe<Table>;
  taxes: Maybe<Tax[]>;
  type: Maybe<OrderTypeEnum>;
  updatedAt: Maybe<string>;
}

export interface OrderCreateInput {
  buyer?: string;
  cancellationReason?: string;
  coupon?: string;
  customerName?: string;
  customerPay?: number;
  deliveryAddress?: DeliveryAddressInput;
  driver?: string;
  fromBranch: string;
  paymentType?: PaymentTypeEnum;
  phoneNumber?: string;
  price?: OrderPriceInput;
  shippingTime?: string;
  table?: string;
  type: OrderTypeEnum;
}

export interface OrderItem {
  _id: Maybe<string>;
  note: Maybe<string>;
  product: Maybe<Product>;
  quantity: Maybe<number>;
  toppings: Maybe<Topping[]>;
  totalPrice: Maybe<number>;
  unit: Maybe<ProductUnit>;
}

export interface OrderItemInput {
  note?: string;
  product?: string;
  /** @default 1*/
  quantity?: number;
  toppings?: ToppingInput[];
  unit?: ProductUnitInput;
}

export interface OrderItemResult {
  results: Maybe<OrderItem[]>;
  totalCount: Maybe<number>;
}

export interface OrderPrice {
  beforeDiscount: Maybe<number>;
  currency: Maybe<CurrencyEnum>;
  discount: Maybe<number>;
  finalPrice: Maybe<number>;
  shippingFee: Maybe<number>;
  tax: Maybe<number>;
}

export interface OrderPriceInput {
  /** @default 0*/
  beforeDiscount?: number;
  /** @default 0*/
  discount?: number;
  /** @default 0*/
  finalPrice?: number;
  /** @default 0*/
  shippingFee?: number;
  /** @default 0*/
  tax?: number;
}

export interface OrderResult {
  results: Maybe<Order[]>;
  totalCount: Maybe<number>;
}

export enum OrderStatusEnum {
  Approving = "approving",
  Canceled = "canceled",
  Complete = "complete",
  New = "new",
  Processing = "processing",
  Ready = "ready",
  Shipping = "shipping",
}
export enum OrderTypeEnum {
  Booking_table = "booking_table",
  Delivery = "delivery",
  In_place = "in_place",
  Take_away = "take_away",
}
export interface OrderUpdateInput {
  buyer?: string;
  cancellationReason?: string;
  coupon?: string;
  customerName?: string;
  customerPay?: number;
  deliveryAddress?: DeliveryAddressInput;
  driver?: string;
  fromBranch?: string;
  orderItems?: OrderItemInput[];
  paymentType?: PaymentTypeEnum;
  phoneNumber?: string;
  shippingTime?: string;
  status?: OrderStatusEnum;
  table?: string;
}

export interface OrderUpdateStatusCompleteInput {
  customerPay?: number;
  orderId: string;
}

export interface PaymentInfo {
  accountName: Maybe<string>;
  accountNumber: Maybe<string>;
  bankBranch: Maybe<string>;
  bankName: Maybe<string>;
  identification: Maybe<string>;
}

export interface PaymentInfoInput {
  accountName?: string;
  accountNumber?: string;
  bankBranch?: string;
  bankName?: string;
  identification?: string;
}

export enum PaymentTypeEnum {
  Directly = "directly",
  Online = "online",
}
export enum Permission {
  Attach_order = "ATTACH_ORDER",
  Change_price = "CHANGE_PRICE",
  Create_bill = "CREATE_BILL",
  Create_restaurant = "CREATE_RESTAURANT",
  Create_table = "CREATE_TABLE",
  Create_user = "CREATE_USER",
  Delete_bill = "DELETE_BILL",
  Delete_restaurant = "DELETE_RESTAURANT",
  Delete_table = "DELETE_TABLE",
  Delete_user = "DELETE_USER",
  Detach_order = "DETACH_ORDER",
  Full = "FULL",
  Read_bill = "READ_BILL",
  Read_printer = "READ_PRINTER",
  Read_restaurant = "READ_RESTAURANT",
  Read_table = "READ_TABLE",
  Read_user = "READ_USER",
  Report_sales = "REPORT_SALES",
  Report_staff = "REPORT_STAFF",
  Report_stock = "REPORT_STOCK",
  Report_user = "REPORT_USER",
  Update_bill = "UPDATE_BILL",
  Update_printer = "UPDATE_PRINTER",
  Update_restaurant = "UPDATE_RESTAURANT",
  Update_table = "UPDATE_TABLE",
  Update_user = "UPDATE_USER",
}
export interface Printer {
  _id: Maybe<string>;
  branch: Maybe<Branch>;
  branchLocation: Maybe<string>;
  branchName: Maybe<string>;
  branchNote: Maybe<string>;
  config: Maybe<PrinterConfig>;
  configBill: Maybe<PrinterConfigBill>;
  customerLocation: Maybe<string>;
  customerName: Maybe<string>;
  customerPhoneNumber: Maybe<string>;
  dateOfPayment: Maybe<string>;
  fontSize: Maybe<PrinterFontSize>;
  footerContent: Maybe<string>;
  ipAddress: Maybe<string>;
  keyword: Maybe<string>;
  method: Maybe<PrinterMethod>;
  name: Maybe<string>;
  orderInfo: Maybe<PrinterOrderInfo>;
  productInfo: Maybe<PrinterProductInfo>;
  title: Maybe<string>;
}

export enum PrinterConfig {
  Billprinting = "billPrinting",
  Stampprinting = "stampPrinting",
}
export interface PrinterConfigBill {
  method: Maybe<PrinterConfigBillMethod>;
  numberOfPrints: Maybe<number>;
}

export interface PrinterConfigBillInput {
  method?: PrinterConfigBillMethod;
  numberOfPrints?: number;
}

export enum PrinterConfigBillMethod {
  Printwhencreatingorder = "printWhenCreatingOrder",
  Reviewbeforeprint = "reviewBeforePrint",
}
export enum PrinterFontSize {
  Large = "large",
  Medium = "medium",
  Small = "small",
}
export interface PrinterInput {
  branch: string;
  branchLocation?: string;
  branchName?: string;
  branchNote?: string;
  config?: PrinterConfig;
  configBill?: PrinterConfigBillInput;
  customerLocation?: string;
  customerName?: string;
  customerPhoneNumber?: string;
  dateOfPayment?: string;
  fontSize?: PrinterFontSize;
  footerContent?: string;
  ipAddress?: string;
  method?: PrinterMethod;
  name?: string;
  orderInfo?: PrinterOrderInfoInput;
  productInfo?: PrinterProductInfoInput;
  title?: string;
}

export enum PrinterMethod {
  A4printer = "a4Printer",
  Bluetooth = "bluetooth",
  Wifi = "wifi",
}
export interface PrinterOrderInfo {
  discountOrder: Maybe<boolean>;
  excessCash: Maybe<boolean>;
  shippingPrice: Maybe<boolean>;
  tax: Maybe<boolean>;
  totalAmount: Maybe<boolean>;
  totalAmountReceived: Maybe<boolean>;
  totalQuantity: Maybe<boolean>;
}

export interface PrinterOrderInfoInput {
  discountOrder?: boolean;
  excessCash?: boolean;
  shippingPrice?: boolean;
  tax?: boolean;
  totalAmount?: boolean;
  totalAmountReceived?: boolean;
  totalQuantity?: boolean;
}

export interface PrinterProductInfo {
  comboDetail: Maybe<boolean>;
  discountProduct: Maybe<boolean>;
  sku: Maybe<boolean>;
}

export interface PrinterProductInfoInput {
  comboDetail?: boolean;
  discountProduct?: boolean;
  sku?: boolean;
}

export interface PrinterResult {
  results: Maybe<Printer[]>;
  totalCount: Maybe<number>;
}

export interface Product {
  _id: Maybe<string>;
  barcode: Maybe<string>;
  branch: Maybe<Branch>;
  createdAt: Maybe<string>;
  description: Maybe<string>;
  dictionary: Maybe<ProductDictionary>;
  images: Maybe<Image[]>;
  keyword: Maybe<string>;
  name: Maybe<string>;
  price: Maybe<number>;
  productCategory: Maybe<ProductCategory>;
  slug: Maybe<string>;
  status: Maybe<ProductStatus>;
  tags: Maybe<Tag[]>;
  thumbnail: Maybe<Image>;
  toppings: Maybe<Topping[]>;
  units: Maybe<ProductUnit[]>;
  updatedAt: Maybe<string>;
}

export interface ProductCategory {
  _id: Maybe<string>;
  branch: Maybe<Branch>;
  businessProductType: Maybe<BusinessProductType>;
  createdAt: Maybe<string>;
  description: Maybe<string>;
  dictionary: Maybe<ProductCategoryDictionary>;
  images: Maybe<Image[]>;
  keyword: Maybe<string>;
  name: Maybe<string>;
  products: Maybe<Product[]>;
  slug: Maybe<string>;
  tag: Maybe<Tag>;
  thumbnail: Maybe<Image>;
  updatedAt: Maybe<string>;
}

export interface ProductCategoryCreateInput {
  branch?: string;
  businessProductType?: string;
  description?: string;
  dictionary?: ProductCategoryDictionaryInput;
  images?: ImageInput[];
  name: string;
  tag?: string;
}

export interface ProductCategoryDictionary {
  german: Maybe<ProductCategoryDictionaryDetail>;
  vietnamese: Maybe<ProductCategoryDictionaryDetail>;
}

export interface ProductCategoryDictionaryDetail {
  description: Maybe<string>;
  images: Maybe<Image[]>;
  keyword: Maybe<string>;
  name: Maybe<string>;
  slug: Maybe<string>;
}

export interface ProductCategoryDictionaryDetailInput {
  description?: string;
  images?: ImageInput[];
  name?: string;
}

export interface ProductCategoryDictionaryInput {
  german?: ProductCategoryDictionaryDetailInput;
  vietnamese?: ProductCategoryDictionaryDetailInput;
}

export interface ProductCategoryResult {
  results: Maybe<ProductCategory[]>;
  totalCount: Maybe<number>;
}

export interface ProductCategoryUpdateInput {
  branch?: string;
  businessProductType?: string;
  description?: string;
  dictionary?: ProductCategoryDictionaryInput;
  images?: ImageInput[];
  name?: string;
  tag?: string;
  thumbnail?: ImageInput;
}

export interface ProductCreateInput {
  branch: string;
  description?: string;
  dictionary?: ProductDictionaryInput;
  images?: ImageInput[];
  name: string;
  /** @default 0*/
  price?: number;
  productCategory: string;
  status?: ProductStatus;
  tags: string[];
  thumbnail?: ImageInput;
  toppings?: ToppingInput[];
  units?: ProductUnitInput[];
}

export interface ProductDictionary {
  german: Maybe<ProductDictionaryDetail>;
  vietnamese: Maybe<ProductDictionaryDetail>;
}

export interface ProductDictionaryDetail {
  description: Maybe<string>;
  groupTopping: Maybe<GroupToppingDictionaryDetail[]>;
  keyword: Maybe<string>;
  name: Maybe<string>;
  slug: Maybe<string>;
  units: Maybe<ProductUnitDictionaryDetail[]>;
}

export interface ProductDictionaryDetailInput {
  description?: string;
  groupTopping?: GroupToppingDictionaryDetailInput[];
  name?: string;
  units?: ProductUnitDictionaryDetailInput[];
}

export interface ProductDictionaryInput {
  german?: ProductDictionaryDetailInput;
  vietnamese?: ProductDictionaryDetailInput;
}

export interface ProductResult {
  results: Maybe<Product[]>;
  totalCount: Maybe<number>;
}

export enum ProductStatus {
  Deleted = "Deleted",
  Instock = "InStock",
  Outofstock = "OutOfStock",
}
export interface ProductUnit {
  _id: Maybe<string>;
  name: Maybe<string>;
  price: Maybe<number>;
}

export interface ProductUnitDictionaryDetail {
  name: Maybe<string>;
}

export interface ProductUnitDictionaryDetailInput {
  name?: string;
}

export interface ProductUnitInput {
  name?: string;
  /** @default 0*/
  price?: number;
}

export interface ProductUpdateInput {
  branch?: string;
  description?: string;
  dictionary?: ProductDictionaryInput;
  images?: ImageInput[];
  name?: string;
  price?: number;
  productCategory?: string;
  status?: ProductStatus;
  tags?: string[];
  thumbnail?: ImageInput;
  toppings?: ToppingInput[];
  units?: ProductUnitInput[];
}

export interface ReportRevenue {
  orderResult: Maybe<OrderResult>;
  totalRevenue: Maybe<number>;
}

export interface Role {
  _id: Maybe<string>;
  name: Maybe<RoleEnum>;
  permissions: Maybe<Permission[]>;
  position: Maybe<string>;
}

export enum RoleEnum {
  Admin = "ADMIN",
  Customer = "CUSTOMER",
  Driver = "DRIVER",
  Manager = "MANAGER",
  Staff = "STAFF",
}
export interface RoleInput {
  name?: RoleEnum;
  permissions?: Permission[];
  position?: string;
}

export interface RoleResult {
  results: Maybe<Role[]>;
  totalCount: Maybe<number>;
}

export interface ServiceType {
  _id: Maybe<string>;
  businessProductTypes: Maybe<BusinessProductType[]>;
  createdAt: Maybe<string>;
  dictionary: Maybe<ServiceTypeDictionary>;
  image: Maybe<Image>;
  isBooking: Maybe<boolean>;
  isDelivery: Maybe<boolean>;
  keyword: Maybe<string>;
  name: Maybe<string>;
  slug: Maybe<string>;
  updatedAt: Maybe<string>;
}

export interface ServiceTypeDictionary {
  german: Maybe<ServiceTypeDictionaryDetail>;
  vietnamese: Maybe<ServiceTypeDictionaryDetail>;
}

export interface ServiceTypeDictionaryDetail {
  image: Maybe<Image>;
  keyword: Maybe<string>;
  name: Maybe<string>;
  slug: Maybe<string>;
}

export interface ServiceTypeDictionaryDetailInput {
  image?: ImageInput;
  name?: string;
}

export interface ServiceTypeDictionaryInput {
  german?: ServiceTypeDictionaryDetailInput;
  vietnamese?: ServiceTypeDictionaryDetailInput;
}

export interface ServiceTypeInput {
  dictionary?: ServiceTypeDictionaryInput;
  image?: ImageInput;
  isBooking?: boolean;
  isDelivery?: boolean;
  name?: string;
}

export interface ServiceTypeResult {
  results: Maybe<ServiceType[]>;
  totalCount: Maybe<number>;
}

export interface SettingGeneral {
  _id: Maybe<string>;
  colorLogo: Maybe<Image>;
  contentFooter: Maybe<string[]>;
  createdAt: Maybe<string>;
  dictionary: Maybe<SettingGeneralDictionary>;
  updatedAt: Maybe<string>;
  whiteLogo: Maybe<Image>;
}

export interface SettingGeneralDictionary {
  german: Maybe<SettingGeneralDictionaryDetail>;
  vietnamese: Maybe<SettingGeneralDictionaryDetail>;
}

export interface SettingGeneralDictionaryDetail {
  colorLogo: Maybe<Image>;
  contentFooter: Maybe<string[]>;
  whiteLogo: Maybe<Image>;
}

export interface SettingGeneralDictionaryDetailInput {
  colorLogo?: ImageInput;
  contentFooter?: string[];
  whiteLogo?: ImageInput;
}

export interface SettingGeneralDictionaryInput {
  german?: SettingGeneralDictionaryDetailInput;
  vietnamese?: SettingGeneralDictionaryDetailInput;
}

export interface SettingGeneralInput {
  colorLogo?: ImageInput;
  contentFooter?: string[];
  dictionary?: SettingGeneralDictionaryInput;
  whiteLogo?: ImageInput;
}

export interface SettingSeo {
  _id: Maybe<string>;
  address: Maybe<string>;
  contentFooter: Maybe<string[]>;
  createdAt: Maybe<string>;
  dictionary: Maybe<SettingSeoDictionary>;
  seoDescription: Maybe<string>;
  seoKeyword: Maybe<Category[]>;
  seoTitle: Maybe<string>;
  updatedAt: Maybe<string>;
}

export interface SettingSeoDictionary {
  german: Maybe<SettingSeoDictionaryDetail>;
  vietnamese: Maybe<SettingSeoDictionaryDetail>;
}

export interface SettingSeoDictionaryDetail {
  address: Maybe<string>;
  contentFooter: Maybe<string[]>;
  seoDescription: Maybe<string>;
  seoTitle: Maybe<string>;
}

export interface SettingSeoDictionaryDetailInput {
  address?: string;
  contentFooter?: string[];
  seoDescription?: string;
  seoTitle?: string;
}

export interface SettingSeoDictionaryInput {
  german?: SettingSeoDictionaryDetailInput;
  vietnamese?: SettingSeoDictionaryDetailInput;
}

export interface SettingSeoInput {
  address?: string;
  contentFooter?: string[];
  dictionary?: SettingSeoDictionaryInput;
  seoDescription?: string;
  seoKeyword?: string[];
  seoTitle?: string;
}

export enum SortEnum {
  Nameatoz = "nameAToZ",
  Nameztoa = "nameZToA",
  Newest = "newest",
  Oldest = "oldest",
}
export interface State {
  _id: Maybe<string>;
  country: Maybe<Country>;
  keyword: Maybe<string>;
  name: Maybe<string>;
  slug: Maybe<string>;
}

export interface StateInput {
  country?: string;
  name?: string;
}

export interface StateResult {
  results: Maybe<State[]>;
  totalCount: Maybe<number>;
}

export interface StaticPage {
  _id: Maybe<string>;
  code: Maybe<string>;
  content: Maybe<string>;
  createdAt: Maybe<string>;
  dictionary: Maybe<StaticPageDictionary>;
  format: Maybe<Category>;
  image: Maybe<Image>;
  keyword: Maybe<string>;
  name: Maybe<string>;
  path: Maybe<string>;
  slug: Maybe<string>;
  updatedAt: Maybe<string>;
}

export interface StaticPageCreateInput {
  code?: string;
  content: string;
  dictionary?: StaticPageDictionaryInput;
  format: string;
  image?: ImageInput;
  name: string;
  path: string;
}

export interface StaticPageDictionary {
  german: Maybe<StaticPageDictionaryDetail>;
  vietnamese: Maybe<StaticPageDictionaryDetail>;
}

export interface StaticPageDictionaryDetail {
  code: Maybe<string>;
  content: Maybe<string>;
  image: Maybe<Image>;
  keyword: Maybe<string>;
  name: Maybe<string>;
  slug: Maybe<string>;
}

export interface StaticPageDictionaryDetailInput {
  code?: string;
  content?: string;
  image?: ImageInput;
  name?: string;
}

export interface StaticPageDictionaryInput {
  german?: StaticPageDictionaryDetailInput;
  vietnamese?: StaticPageDictionaryDetailInput;
}

export interface StaticPageResult {
  results: Maybe<StaticPage[]>;
  totalCount: Maybe<number>;
}

export interface StaticPageUpdateInput {
  code?: string;
  content?: string;
  dictionary?: StaticPageDictionaryInput;
  format?: string;
  image?: ImageInput;
  name?: string;
  path?: string;
}

export interface StoreImage {
  _id: Maybe<string>;
  base64: Maybe<string>;
  createdAt: Maybe<string>;
  description: Maybe<string>;
  dictionary: Maybe<StoreImageDictionary>;
  folderId: Maybe<UserFolder>;
  isAdmin: Maybe<boolean>;
  isSample: Maybe<boolean>;
  name: Maybe<string>;
  resolution: Maybe<string>;
  size: Maybe<number>;
  storeId: Maybe<UserStore>;
  thumbnail: Maybe<string>;
  type: Maybe<string>;
  updatedAt: Maybe<string>;
}

export interface StoreImageDictionary {
  german: Maybe<StoreImageDictionaryDetail>;
  vietnamese: Maybe<StoreImageDictionaryDetail>;
}

export interface StoreImageDictionaryDetail {
  base64: Maybe<string>;
  description: Maybe<string>;
  name: Maybe<string>;
  resolution: Maybe<string>;
  thumbnail: Maybe<string>;
}

export interface StoreImageDictionaryDetailInput {
  base64?: string;
  description?: string;
  name?: string;
  resolution?: string;
  thumbnail?: string;
}

export interface StoreImageDictionaryInput {
  german?: StoreImageDictionaryDetailInput;
  vietnamese?: StoreImageDictionaryDetailInput;
}

export interface StoreImageInput {
  base64?: string;
  description?: string;
  dictionary?: StoreImageDictionaryInput;
  folderId?: string;
  isAdmin?: boolean;
  isSample?: boolean;
  name?: string;
  resolution?: string;
  size?: number;
  storeId?: string;
  thumbnail?: string;
  type?: string;
}

export interface StoreImageResult {
  results: Maybe<StoreImage[]>;
  totalCount: Maybe<number>;
}

export interface Table {
  _id: Maybe<string>;
  bookingDate: Maybe<string>;
  code: Maybe<number>;
  createdAt: Maybe<string>;
  maxGuest: Maybe<number>;
  quantity: Maybe<number>;
  slug: Maybe<string>;
  status: Maybe<TableStatus>;
  tableType: Maybe<TableType>;
  updatedAt: Maybe<string>;
}

export interface TableInput {
  bookingDate?: string;
  code?: number;
  maxGuest?: number;
  quantity?: number;
  status?: TableStatus;
  tableType?: string;
}

export interface TableResult {
  results: Maybe<Table[]>;
  totalCount: Maybe<number>;
}

export enum TableStatus {
  Available = "available",
  Empty = "empty",
  Ordered = "ordered",
  Unavailable = "unavailable",
  Using = "using",
  Waiting = "waiting",
}
export interface TableType {
  _id: Maybe<string>;
  branch: Maybe<Branch>;
  createdAt: Maybe<string>;
  dictionary: Maybe<TableTypeDictionary>;
  keyword: Maybe<string>;
  maxGuest: Maybe<number>;
  name: Maybe<string>;
  quantity: Maybe<number>;
  slug: Maybe<string>;
  tables: Maybe<Table[]>;
  updatedAt: Maybe<string>;
}

export interface TableTypeDictionary {
  german: Maybe<TableTypeDictionaryDetail>;
  vietnamese: Maybe<TableTypeDictionaryDetail>;
}

export interface TableTypeDictionaryDetail {
  keyword: Maybe<string>;
  name: Maybe<string>;
  slug: Maybe<string>;
}

export interface TableTypeDictionaryDetailInput {
  name?: string;
}

export interface TableTypeDictionaryInput {
  german?: TableTypeDictionaryDetailInput;
  vietnamese?: TableTypeDictionaryDetailInput;
}

export interface TableTypeInput {
  branch?: string;
  dictionary?: TableTypeDictionaryInput;
  maxGuest?: number;
  name?: string;
  quantity?: number;
}

export interface TableTypeResult {
  results: Maybe<TableType[]>;
  totalCount: Maybe<number>;
}

export interface Tag {
  _id: Maybe<string>;
  createdAt: Maybe<string>;
  description: Maybe<string>;
  dictionary: Maybe<TagDictionary>;
  keyword: Maybe<string>;
  name: Maybe<string>;
  slug: Maybe<string>;
  updatedAt: Maybe<string>;
}

export interface TagCreateInput {
  description?: string;
  dictionary?: TagDictionaryInput;
  name: string;
}

export interface TagDictionary {
  german: Maybe<TagDictionaryDetail>;
  vietnamese: Maybe<TagDictionaryDetail>;
}

export interface TagDictionaryDetail {
  description: Maybe<string>;
  keyword: Maybe<string>;
  name: Maybe<string>;
  slug: Maybe<string>;
}

export interface TagDictionaryDetailInput {
  description?: string;
  name?: string;
}

export interface TagDictionaryInput {
  german?: TagDictionaryDetailInput;
  vietnamese?: TagDictionaryDetailInput;
}

export interface TagResult {
  results: Maybe<Tag[]>;
  totalCount: Maybe<number>;
}

export interface TagUpdateInput {
  description?: string;
  name?: string;
}

export interface Tax {
  _id: Maybe<string>;
  branches: Maybe<Branch[]>;
  createdAt: Maybe<string>;
  createdBy: Maybe<User>;
  dictionary: Maybe<TaxDictionary>;
  keyword: Maybe<string>;
  name: Maybe<string>;
  slug: Maybe<string>;
  updatedAt: Maybe<string>;
  /** Min value is 0%*/
  value: Maybe<number>;
}

export interface TaxCreateInput {
  branches: string[];
  dictionary?: TaxDictionaryInput;
  name: string;
  /** @default 0*/
  value?: number;
}

export interface TaxDictionary {
  german: Maybe<TaxDictionaryDetail>;
  vietnamese: Maybe<TaxDictionaryDetail>;
}

export interface TaxDictionaryDetail {
  keyword: Maybe<string>;
  name: Maybe<string>;
  slug: Maybe<string>;
}

export interface TaxDictionaryDetailInput {
  name?: string;
}

export interface TaxDictionaryInput {
  german?: TaxDictionaryDetailInput;
  vietnamese?: TaxDictionaryDetailInput;
}

export interface TaxResult {
  results: Maybe<Tax[]>;
  totalCount: Maybe<number>;
}

export interface TaxUpdateInput {
  branches?: string[];
  dictionary?: TaxDictionaryInput;
  name?: string;
  value?: number;
}

export interface Topping {
  _id: Maybe<string>;
  name: Maybe<string>;
  price: Maybe<number>;
  quantity: Maybe<number>;
}

export interface ToppingDictionaryDetail {
  name: Maybe<string>;
}

export interface ToppingDictionaryDetailInput {
  name?: string;
}

export interface ToppingInput {
  name?: string;
  /** @default 0*/
  price?: number;
  /** @default 1*/
  quantity?: number;
}

export interface UpdateBranchInput {
  dictionary?: BranchDictionaryInput;
  images?: ImageInput[];
  introduction?: string;
  location?: LocationInput;
  maxGuest?: number;
  name?: string;
  operationTimes?: BranchOperationTimeInput[];
  phoneNumber?: string;
  shippingFee?: BranchShippingFeeInput;
  status?: BranchStatus;
  tables?: string[];
}

export interface UpdateCategoryInput {
  categoryCode?: CategoryCode;
  description?: string;
  dictionary?: CategoryDictionaryInput;
  image?: ImageInput;
  name?: string;
}

export interface UpdateUserInput {
  accumulatePoints?: number;
  avatar?: ImageInput;
  birthday?: string;
  branches?: string[];
  businessProductTypes?: string[];
  clientId?: string;
  deliveryAddresses?: DeliveryAddressInput[];
  displayName?: string;
  email?: string;
  gender?: Gender;
  introduction?: string;
  location?: LocationInput;
  password?: string;
  paymentInfo?: PaymentInfoInput;
  phoneNumber?: string;
  provider?: string;
  ranking?: UserRanking;
  role?: string;
  serviceType?: string;
  status?: UserStatus;
}

export interface User {
  _id: Maybe<string>;
  accumulatePoints: Maybe<number>;
  avatar: Maybe<Image>;
  birthday: Maybe<string>;
  branches: Maybe<Branch[]>;
  clientId: Maybe<string>;
  createdAt: Maybe<string>;
  customerCode: Maybe<string>;
  deliveryAddresses: Maybe<DeliveryAddress[]>;
  displayName: Maybe<string>;
  email: Maybe<string>;
  gender: Maybe<Gender>;
  keyword: Maybe<string>;
  location: Maybe<Location>;
  paymentInfo: Maybe<PaymentInfo>;
  phoneNumber: Maybe<string>;
  provider: Maybe<string>;
  ranking: Maybe<UserRanking>;
  role: Maybe<Role>;
  slug: Maybe<string>;
  status: Maybe<UserStatus>;
  updatedAt: Maybe<string>;
}

export interface UserFolder {
  _id: Maybe<string>;
  createdAt: Maybe<string>;
  dictionary: Maybe<UserFolderDictionary>;
  fromStore: Maybe<UserStore>;
  images: Maybe<StoreImage[]>;
  name: Maybe<string>;
  updatedAt: Maybe<string>;
}

export interface UserFolderDictionary {
  german: Maybe<UserFolderDictionaryDetail>;
  vietnamese: Maybe<UserFolderDictionaryDetail>;
}

export interface UserFolderDictionaryDetail {
  name: Maybe<string>;
}

export interface UserFolderDictionaryDetailInput {
  name?: string;
}

export interface UserFolderDictionaryInput {
  german?: UserFolderDictionaryDetailInput;
  vietnamese?: UserFolderDictionaryDetailInput;
}

export interface UserFolderInput {
  dictionary?: UserFolderDictionaryInput;
  fromStore?: string;
  name?: string;
}

export interface UserFolderResult {
  results: Maybe<UserFolder[]>;
  totalCount: Maybe<number>;
}

export enum UserRanking {
  Bronze = "bronze",
  Diamond = "diamond",
  Gold = "gold",
  Sliver = "sliver",
}
export interface UserResults {
  results: Maybe<User[]>;
  totalCount: Maybe<number>;
}

export enum UserStatus {
  Blocked = "blocked",
  Isactive = "isActive",
  Waitforapproval = "waitForApproval",
}
export interface UserStore {
  _id: Maybe<string>;
  createdAt: Maybe<string>;
  createdBy: Maybe<User>;
  folders: Maybe<UserFolder[]>;
  images: Maybe<StoreImage[]>;
  updatedAt: Maybe<string>;
}

export interface UserStoreInput {
  createdBy?: string;
}

export interface GetAdsByIdArgs {
  id: string;
}

export interface GetAllAdsArgs {
  filter?: FilterAds;
  page?: number;
  size?: number;
}

export interface GetAllBranchArgs {
  filterBranch?: FilterBranch;
  page?: number;
  size?: number;
}

export interface GetAllBusinessProductTypeArgs {
  filter?: FilterBusinessProductType;
  page?: number;
  size?: number;
}

export interface GetAllCategoryArgs {
  filterCategory?: FilterCategory;
  page?: number;
  size?: number;
}

export interface GetAllCityArgs {
  filter?: FilterCity;
  page?: number;
  size?: number;
}

export interface GetAllCountryArgs {
  filter?: FilterCountry;
  page?: number;
  size?: number;
}

export interface GetAllCouponArgs {
  filter?: FilterCoupon;
  page?: number;
  size?: number;
}

export interface GetAllDistrictArgs {
  filter?: DistrictInput;
  page?: number;
  size?: number;
}

export interface GetAllOrderArgs {
  filter?: FilterOrder;
  page?: number;
  size?: number;
}

export interface GetAllOrderItemArgs {
  idOrder: string;
  page?: number;
  size?: number;
}

export interface GetAllPrinterArgs {
  filter?: FilterPrinter;
  page?: number;
  size?: number;
}

export interface GetAllProductArgs {
  filterProduct?: FilterProduct;
  page?: number;
  size?: number;
}

export interface GetAllProductCategoryArgs {
  filterProductCategory?: FilterProductCategory;
  page?: number;
  size?: number;
}

export interface GetAllReportRevenueArgs {
  filter: FilterReportRevenue;
  page?: number;
  size?: number;
}

export interface GetAllRoleArgs {
  filterRole?: FilterRole;
  page?: number;
  size?: number;
}

export interface GetAllServiceTypeArgs {
  filter?: FilterServiceType;
  page?: number;
  size?: number;
}

export interface GetAllStateArgs {
  filter?: StateInput;
  page?: number;
  size?: number;
}

export interface GetAllStaticPageArgs {
  filter?: FilterStaticPage;
  page?: number;
  size?: number;
}

export interface GetAllStoreImageArgs {
  filter?: FilterStoreImage;
  page?: number;
  size?: number;
}

export interface GetAllTableArgs {
  filter?: FilterTable;
  page?: number;
  size?: number;
}

export interface GetAllTableTypeArgs {
  filter?: FilterTableType;
  page?: number;
  size?: number;
}

export interface GetAllTagArgs {
  filter?: FilterTag;
  page?: number;
  size?: number;
}

export interface GetAllTaxArgs {
  filter?: FilterTax;
  page?: number;
  size?: number;
}

export interface GetAllUserArgs {
  filterUser?: FilterUser;
  page?: number;
  size?: number;
}

export interface GetAllUserFolderArgs {
  filter?: FilterUserFolder;
  page?: number;
  size?: number;
}

export interface GetBranchByIdArgs {
  id: string;
}

export interface GetBusinessProductTypeByIdArgs {
  id: string;
}

export interface GetCategoryByIdArgs {
  id: string;
}

export interface GetCityByIdArgs {
  id: string;
}

export interface GetCountryByIdArgs {
  id: string;
}

export interface GetCouponByIdArgs {
  id: string;
}

export interface GetDashboardArgs {
  idBranch: string;
}

export interface GetDistrictByIdArgs {
  id: string;
}

export interface GetOrderByBarcodeArgs {
  barcode: string;
}

export interface GetOrderByIdArgs {
  id: string;
}

export interface GetPrinterByIdArgs {
  id: string;
}

export interface GetProductByBarcodeArgs {
  barcode: string;
}

export interface GetProductByIdArgs {
  id: string;
}

export interface GetProductCategoryByIdArgs {
  id: string;
}

export interface GetRoleByIdArgs {
  id: string;
}

export interface GetServiceTypeByIdArgs {
  id: string;
}

export interface GetSettingGeneralArgs {}

export interface GetSettingSeoArgs {}

export interface GetStateByIdArgs {
  id: string;
}

export interface GetStaticPageByIdArgs {
  id: string;
}

export interface GetStoreImageByIdArgs {
  id: string;
}

export interface GetTableByIdArgs {
  id: string;
}

export interface GetTableTypeByIdArgs {
  id: string;
}

export interface GetTagByIdArgs {
  id: string;
}

export interface GetTaxByIdArgs {
  id: string;
}

export interface GetUserByIdArgs {
  id: string;
}

export interface GetUserFolderByIdArgs {
  id: string;
}

export interface GetUserStoreByCreatedByArgs {}

export interface GetUserStoreByIdArgs {
  id: string;
}

export interface IsExistingPhoneNumberArgs {
  phoneNumber: string;
}

export interface AddBranchManagerArgs {
  branchInput: CreateManagerRequestInput;
}

export interface AddOrderItemToOrderArgs {
  idOrder: string;
  input: OrderItemInput;
}

export interface ChangePasswordArgs {
  input: ChangePasswordInput;
}

export interface CheckPasswordIsOkArgs {
  password: string;
  userId: string;
}

export interface ClearCacheArgs {}

export interface CreateAdsArgs {
  input: AdsCreateInput;
}

export interface CreateBranchArgs {
  branchInput: CreateBranchInput;
}

export interface CreateBusinessProductTypeArgs {
  businessProductTypeInput: BusinessProductTypeInput;
}

export interface CreateCategoryArgs {
  createCategoryInput: CreateCategoryInput;
}

export interface CreateCountryArgs {
  input: CountryCreateInput;
}

export interface CreateCouponArgs {
  input: CouponCreateInput;
}

export interface CreateDistrictArgs {
  input: DistrictInput;
}

export interface CreateManagerRequestArgs {
  input: CreateManagerRequestInput;
}

export interface CreateOrderArgs {
  input: OrderCreateInput;
}

export interface CreatePrinterArgs {
  input: PrinterInput;
}

export interface CreateProductArgs {
  productCreateInput: ProductCreateInput;
}

export interface CreateProductCategoryArgs {
  productCategoryCreateInput: ProductCategoryCreateInput;
}

export interface CreateRoleArgs {
  roleInput: RoleInput;
}

export interface CreateServiceTypeArgs {
  serviceTypeInput: ServiceTypeInput;
}

export interface CreateStateArgs {
  input: StateInput;
}

export interface CreateStaticPageArgs {
  input: StaticPageCreateInput;
}

export interface CreateStoreImageArgs {
  input: StoreImageInput;
}

export interface CreateTableArgs {
  input: TableInput;
}

export interface CreateTableTypeArgs {
  input: TableTypeInput;
}

export interface CreateTagArgs {
  input: TagCreateInput;
}

export interface CreateTaxArgs {
  input: TaxCreateInput;
}

export interface CreateUserArgs {
  input: CreateUserInput;
}

export interface CreateUserFolderArgs {
  input: UserFolderInput;
}

export interface CreateUserStoreArgs {
  userStoreInput: UserStoreInput;
}

export interface DeleteAdsArgs {
  listId: string[];
}

export interface DeleteBranchArgs {
  listId: string[];
}

export interface DeleteBusinessProductTypeArgs {
  listId: string[];
}

export interface DeleteCategoryArgs {
  listId: string[];
}

export interface DeleteCountryArgs {
  listId: string[];
}

export interface DeleteCouponArgs {
  listId: string[];
}

export interface DeleteDistrictArgs {
  listId: string[];
}

export interface DeleteOrderArgs {
  listId: string[];
}

export interface DeleteOrderItemArgs {
  idOrder: string;
  listIdOrderItem: string[];
}

export interface DeletePrinterArgs {
  listId: string[];
}

export interface DeleteProductArgs {
  listId: string[];
}

export interface DeleteProductCategoryArgs {
  listId: string[];
}

export interface DeleteRoleByIdArgs {
  id: string;
}

export interface DeleteServiceTypeArgs {
  listId: string[];
}

export interface DeleteStateArgs {
  listId: string[];
}

export interface DeleteStaticPageArgs {
  listId: string[];
}

export interface DeleteStoreImageArgs {
  listId: string[];
}

export interface DeleteTableArgs {
  listId: string[];
}

export interface DeleteTableTypeArgs {
  listId: string[];
}

export interface DeleteTagArgs {
  listId: string[];
}

export interface DeleteTaxArgs {
  listId: string[];
}

export interface DeleteUserArgs {
  id: string;
}

export interface DeleteUserFolderArgs {
  listId: string[];
}

export interface DeleteUserStoreArgs {
  id: string[];
}

export interface ForgotPasswordArgs {
  input: ForgotPasswordInput;
}

export interface LoginArgs {
  user: LoginUserInput;
}

export interface LoginFirebaseArgs {
  idToken: string;
}

export interface RejectManagerArgs {
  id: string;
}

export interface UpdateAdsArgs {
  id: string;
  input: AdsUpdateInput;
}

export interface UpdateBranchArgs {
  branchInput: UpdateBranchInput;
  id: string;
}

export interface UpdateBusinessProductTypeArgs {
  businessProductTypeInput: BusinessProductTypeInput;
  id: string;
}

export interface UpdateCategoryArgs {
  fieldsToUpdate: UpdateCategoryInput;
  id: string;
}

export interface UpdateCompleteStatusOrderArgs {
  input: OrderUpdateStatusCompleteInput;
}

export interface UpdateCountryArgs {
  id: string;
  input: CountryUpdateInput;
}

export interface UpdateCouponArgs {
  id: string;
  input: CouponUpdateInput;
}

export interface UpdateDistrictArgs {
  id: string;
  input: DistrictInput;
}

export interface UpdateOrderArgs {
  id: string;
  input: OrderUpdateInput;
}

export interface UpdateOrderItemArgs {
  idOrder: string;
  idOrderItem: string;
  input: OrderItemInput;
}

export interface UpdatePrinterArgs {
  id: string;
  input: PrinterInput;
}

export interface UpdateProductArgs {
  id: string;
  productUpdateInput: ProductUpdateInput;
}

export interface UpdateProductCategoryArgs {
  id: string;
  productCategoryUpdateInput: ProductCategoryUpdateInput;
}

export interface UpdateRoleArgs {
  id: string;
  roleInput: RoleInput;
}

export interface UpdateServiceTypeArgs {
  id: string;
  serviceTypeInput: ServiceTypeInput;
}

export interface UpdateSettingGeneralArgs {
  input: SettingGeneralInput;
}

export interface UpdateSettingSeoArgs {
  input: SettingSeoInput;
}

export interface UpdateStateArgs {
  id: string;
  input: StateInput;
}

export interface UpdateStaticPageArgs {
  id: string;
  input: StaticPageUpdateInput;
}

export interface UpdateStoreImageArgs {
  id: string;
  input: StoreImageInput;
}

export interface UpdateTableArgs {
  id: string;
  input: TableInput;
}

export interface UpdateTableTypeArgs {
  id: string;
  input: TableTypeInput;
}

export interface UpdateTagArgs {
  id: string;
  input: TagUpdateInput;
}

export interface UpdateTaxArgs {
  id: string;
  input: TaxUpdateInput;
}

export interface UpdateUserArgs {
  id: string;
  updateUserInput: UpdateUserInput;
}

export interface UpdateUserFolderArgs {
  id: string;
  input: UserFolderInput;
}

export const useGetAdsById = (
  fields: GenFields<Ads>,
  options?: QueryHookOptions<{ getAdsById: Ads }, GetAdsByIdArgs>,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getAdsById ($id: String!) {
        getAdsById(id: $id) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<{ getAdsById: Ads }, GetAdsByIdArgs>(query, options);
};

export const useGetAllAds = (
  fields: GenFields<AdsResult>,
  options?: QueryHookOptions<{ getAllAds: AdsResult }, GetAllAdsArgs>,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getAllAds ($filter: FilterAds,$page: Int,$size: Int) {
        getAllAds(filter: $filter,page: $page,size: $size) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<{ getAllAds: AdsResult }, GetAllAdsArgs>(query, options);
};

export const useGetAllBranch = (
  fields: GenFields<BranchResult>,
  options?: QueryHookOptions<{ getAllBranch: BranchResult }, GetAllBranchArgs>,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getAllBranch ($filterBranch: FilterBranch,$page: Int,$size: Int) {
        getAllBranch(filterBranch: $filterBranch,page: $page,size: $size) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<{ getAllBranch: BranchResult }, GetAllBranchArgs>(
    query,
    options,
  );
};

export const useGetAllBusinessProductType = (
  fields: GenFields<BusinessProductTypeResult>,
  options?: QueryHookOptions<
    { getAllBusinessProductType: BusinessProductTypeResult },
    GetAllBusinessProductTypeArgs
  >,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getAllBusinessProductType ($filter: FilterBusinessProductType,$page: Int,$size: Int) {
        getAllBusinessProductType(filter: $filter,page: $page,size: $size) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<
    { getAllBusinessProductType: BusinessProductTypeResult },
    GetAllBusinessProductTypeArgs
  >(query, options);
};

export const useGetAllCategory = (
  fields: GenFields<CategoryResult>,
  options?: QueryHookOptions<
    { getAllCategory: CategoryResult },
    GetAllCategoryArgs
  >,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getAllCategory ($filterCategory: FilterCategory,$page: Int,$size: Int) {
        getAllCategory(filterCategory: $filterCategory,page: $page,size: $size) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<{ getAllCategory: CategoryResult }, GetAllCategoryArgs>(
    query,
    options,
  );
};

export const useGetAllCity = (
  fields: GenFields<CityResult>,
  options?: QueryHookOptions<{ getAllCity: CityResult }, GetAllCityArgs>,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getAllCity ($filter: FilterCity,$page: Int,$size: Int) {
        getAllCity(filter: $filter,page: $page,size: $size) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<{ getAllCity: CityResult }, GetAllCityArgs>(
    query,
    options,
  );
};

export const useGetAllCountry = (
  fields: GenFields<CountryResult>,
  options?: QueryHookOptions<
    { getAllCountry: CountryResult },
    GetAllCountryArgs
  >,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getAllCountry ($filter: FilterCountry,$page: Int,$size: Int) {
        getAllCountry(filter: $filter,page: $page,size: $size) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<{ getAllCountry: CountryResult }, GetAllCountryArgs>(
    query,
    options,
  );
};

export const useGetAllCoupon = (
  fields: GenFields<CouponResult>,
  options?: QueryHookOptions<{ getAllCoupon: CouponResult }, GetAllCouponArgs>,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getAllCoupon ($filter: FilterCoupon,$page: Int,$size: Int) {
        getAllCoupon(filter: $filter,page: $page,size: $size) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<{ getAllCoupon: CouponResult }, GetAllCouponArgs>(
    query,
    options,
  );
};

export const useGetAllDistrict = (
  fields: GenFields<DistrictResult>,
  options?: QueryHookOptions<
    { getAllDistrict: DistrictResult },
    GetAllDistrictArgs
  >,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getAllDistrict ($filter: DistrictInput,$page: Int,$size: Int) {
        getAllDistrict(filter: $filter,page: $page,size: $size) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<{ getAllDistrict: DistrictResult }, GetAllDistrictArgs>(
    query,
    options,
  );
};

export const useGetAllOrder = (
  fields: GenFields<OrderResult>,
  options?: QueryHookOptions<{ getAllOrder: OrderResult }, GetAllOrderArgs>,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getAllOrder ($filter: FilterOrder,$page: Int,$size: Int) {
        getAllOrder(filter: $filter,page: $page,size: $size) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<{ getAllOrder: OrderResult }, GetAllOrderArgs>(
    query,
    options,
  );
};

export const useGetAllOrderItem = (
  fields: GenFields<OrderItemResult>,
  options?: QueryHookOptions<
    { getAllOrderItem: OrderItemResult },
    GetAllOrderItemArgs
  >,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getAllOrderItem ($idOrder: String!,$page: Int,$size: Int) {
        getAllOrderItem(idOrder: $idOrder,page: $page,size: $size) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<
    { getAllOrderItem: OrderItemResult },
    GetAllOrderItemArgs
  >(query, options);
};

export const useGetAllPrinter = (
  fields: GenFields<PrinterResult>,
  options?: QueryHookOptions<
    { getAllPrinter: PrinterResult },
    GetAllPrinterArgs
  >,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getAllPrinter ($filter: FilterPrinter,$page: Int,$size: Int) {
        getAllPrinter(filter: $filter,page: $page,size: $size) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<{ getAllPrinter: PrinterResult }, GetAllPrinterArgs>(
    query,
    options,
  );
};

export const useGetAllProduct = (
  fields: GenFields<ProductResult>,
  options?: QueryHookOptions<
    { getAllProduct: ProductResult },
    GetAllProductArgs
  >,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getAllProduct ($filterProduct: FilterProduct,$page: Int,$size: Int) {
        getAllProduct(filterProduct: $filterProduct,page: $page,size: $size) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<{ getAllProduct: ProductResult }, GetAllProductArgs>(
    query,
    options,
  );
};

export const useGetAllProductCategory = (
  fields: GenFields<ProductCategoryResult>,
  options?: QueryHookOptions<
    { getAllProductCategory: ProductCategoryResult },
    GetAllProductCategoryArgs
  >,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getAllProductCategory ($filterProductCategory: FilterProductCategory,$page: Int,$size: Int) {
        getAllProductCategory(filterProductCategory: $filterProductCategory,page: $page,size: $size) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<
    { getAllProductCategory: ProductCategoryResult },
    GetAllProductCategoryArgs
  >(query, options);
};

export const useGetAllReportRevenue = (
  fields: GenFields<ReportRevenue>,
  options?: QueryHookOptions<
    { getAllReportRevenue: ReportRevenue },
    GetAllReportRevenueArgs
  >,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getAllReportRevenue ($filter: FilterReportRevenue!,$page: Int,$size: Int) {
        getAllReportRevenue(filter: $filter,page: $page,size: $size) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<
    { getAllReportRevenue: ReportRevenue },
    GetAllReportRevenueArgs
  >(query, options);
};

export const useGetAllRole = (
  fields: GenFields<RoleResult>,
  options?: QueryHookOptions<{ getAllRole: RoleResult }, GetAllRoleArgs>,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getAllRole ($filterRole: FilterRole,$page: Int,$size: Int) {
        getAllRole(filterRole: $filterRole,page: $page,size: $size) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<{ getAllRole: RoleResult }, GetAllRoleArgs>(
    query,
    options,
  );
};

export const useGetAllServiceType = (
  fields: GenFields<ServiceTypeResult>,
  options?: QueryHookOptions<
    { getAllServiceType: ServiceTypeResult },
    GetAllServiceTypeArgs
  >,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getAllServiceType ($filter: FilterServiceType,$page: Int,$size: Int) {
        getAllServiceType(filter: $filter,page: $page,size: $size) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<
    { getAllServiceType: ServiceTypeResult },
    GetAllServiceTypeArgs
  >(query, options);
};

export const useGetAllState = (
  fields: GenFields<StateResult>,
  options?: QueryHookOptions<{ getAllState: StateResult }, GetAllStateArgs>,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getAllState ($filter: StateInput,$page: Int,$size: Int) {
        getAllState(filter: $filter,page: $page,size: $size) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<{ getAllState: StateResult }, GetAllStateArgs>(
    query,
    options,
  );
};

export const useGetAllStaticPage = (
  fields: GenFields<StaticPageResult>,
  options?: QueryHookOptions<
    { getAllStaticPage: StaticPageResult },
    GetAllStaticPageArgs
  >,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getAllStaticPage ($filter: FilterStaticPage,$page: Int,$size: Int) {
        getAllStaticPage(filter: $filter,page: $page,size: $size) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<
    { getAllStaticPage: StaticPageResult },
    GetAllStaticPageArgs
  >(query, options);
};

export const useGetAllStoreImage = (
  fields: GenFields<StoreImageResult>,
  options?: QueryHookOptions<
    { getAllStoreImage: StoreImageResult },
    GetAllStoreImageArgs
  >,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getAllStoreImage ($filter: FilterStoreImage,$page: Int,$size: Int) {
        getAllStoreImage(filter: $filter,page: $page,size: $size) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<
    { getAllStoreImage: StoreImageResult },
    GetAllStoreImageArgs
  >(query, options);
};

export const useGetAllTable = (
  fields: GenFields<TableResult>,
  options?: QueryHookOptions<{ getAllTable: TableResult }, GetAllTableArgs>,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getAllTable ($filter: FilterTable,$page: Int,$size: Int) {
        getAllTable(filter: $filter,page: $page,size: $size) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<{ getAllTable: TableResult }, GetAllTableArgs>(
    query,
    options,
  );
};

export const useGetAllTableType = (
  fields: GenFields<TableTypeResult>,
  options?: QueryHookOptions<
    { getAllTableType: TableTypeResult },
    GetAllTableTypeArgs
  >,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getAllTableType ($filter: FilterTableType,$page: Int,$size: Int) {
        getAllTableType(filter: $filter,page: $page,size: $size) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<
    { getAllTableType: TableTypeResult },
    GetAllTableTypeArgs
  >(query, options);
};

export const useGetAllTag = (
  fields: GenFields<TagResult>,
  options?: QueryHookOptions<{ getAllTag: TagResult }, GetAllTagArgs>,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getAllTag ($filter: FilterTag,$page: Int,$size: Int) {
        getAllTag(filter: $filter,page: $page,size: $size) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<{ getAllTag: TagResult }, GetAllTagArgs>(query, options);
};

export const useGetAllTax = (
  fields: GenFields<TaxResult>,
  options?: QueryHookOptions<{ getAllTax: TaxResult }, GetAllTaxArgs>,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getAllTax ($filter: FilterTax,$page: Int,$size: Int) {
        getAllTax(filter: $filter,page: $page,size: $size) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<{ getAllTax: TaxResult }, GetAllTaxArgs>(query, options);
};

export const useGetAllUser = (
  fields: GenFields<UserResults>,
  options?: QueryHookOptions<{ getAllUser: UserResults }, GetAllUserArgs>,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getAllUser ($filterUser: FilterUser,$page: Int,$size: Int) {
        getAllUser(filterUser: $filterUser,page: $page,size: $size) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<{ getAllUser: UserResults }, GetAllUserArgs>(
    query,
    options,
  );
};

export const useGetAllUserFolder = (
  fields: GenFields<UserFolderResult>,
  options?: QueryHookOptions<
    { getAllUserFolder: UserFolderResult },
    GetAllUserFolderArgs
  >,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getAllUserFolder ($filter: FilterUserFolder,$page: Int,$size: Int) {
        getAllUserFolder(filter: $filter,page: $page,size: $size) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<
    { getAllUserFolder: UserFolderResult },
    GetAllUserFolderArgs
  >(query, options);
};

export const useGetBranchById = (
  fields: GenFields<Branch>,
  options?: QueryHookOptions<{ getBranchById: Branch }, GetBranchByIdArgs>,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getBranchById ($id: String!) {
        getBranchById(id: $id) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<{ getBranchById: Branch }, GetBranchByIdArgs>(
    query,
    options,
  );
};

export const useGetBusinessProductTypeById = (
  fields: GenFields<BusinessProductType>,
  options?: QueryHookOptions<
    { getBusinessProductTypeById: BusinessProductType },
    GetBusinessProductTypeByIdArgs
  >,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getBusinessProductTypeById ($id: String!) {
        getBusinessProductTypeById(id: $id) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<
    { getBusinessProductTypeById: BusinessProductType },
    GetBusinessProductTypeByIdArgs
  >(query, options);
};

export const useGetCategoryById = (
  fields: GenFields<Category>,
  options?: QueryHookOptions<
    { getCategoryById: Category },
    GetCategoryByIdArgs
  >,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getCategoryById ($id: String!) {
        getCategoryById(id: $id) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<{ getCategoryById: Category }, GetCategoryByIdArgs>(
    query,
    options,
  );
};

export const useGetCityById = (
  fields: GenFields<City>,
  options?: QueryHookOptions<{ getCityById: City }, GetCityByIdArgs>,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getCityById ($id: String!) {
        getCityById(id: $id) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<{ getCityById: City }, GetCityByIdArgs>(query, options);
};

export const useGetCountryById = (
  fields: GenFields<Country>,
  options?: QueryHookOptions<{ getCountryById: Country }, GetCountryByIdArgs>,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getCountryById ($id: String!) {
        getCountryById(id: $id) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<{ getCountryById: Country }, GetCountryByIdArgs>(
    query,
    options,
  );
};

export const useGetCouponById = (
  fields: GenFields<Coupon>,
  options?: QueryHookOptions<{ getCouponById: Coupon }, GetCouponByIdArgs>,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getCouponById ($id: String!) {
        getCouponById(id: $id) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<{ getCouponById: Coupon }, GetCouponByIdArgs>(
    query,
    options,
  );
};

export const useGetDashboard = (
  fields: GenFields<Dashboard>,
  options?: QueryHookOptions<{ getDashboard: Dashboard }, GetDashboardArgs>,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getDashboard ($idBranch: String!) {
        getDashboard(idBranch: $idBranch) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<{ getDashboard: Dashboard }, GetDashboardArgs>(
    query,
    options,
  );
};

export const useGetDistrictById = (
  fields: GenFields<District>,
  options?: QueryHookOptions<
    { getDistrictById: District },
    GetDistrictByIdArgs
  >,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getDistrictById ($id: String!) {
        getDistrictById(id: $id) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<{ getDistrictById: District }, GetDistrictByIdArgs>(
    query,
    options,
  );
};

export const useGetOrderByBarcode = (
  fields: GenFields<Order>,
  options?: QueryHookOptions<
    { getOrderByBarcode: Order },
    GetOrderByBarcodeArgs
  >,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getOrderByBarcode ($barcode: String!) {
        getOrderByBarcode(barcode: $barcode) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<{ getOrderByBarcode: Order }, GetOrderByBarcodeArgs>(
    query,
    options,
  );
};

export const useGetOrderById = (
  fields: GenFields<Order>,
  options?: QueryHookOptions<{ getOrderById: Order }, GetOrderByIdArgs>,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getOrderById ($id: String!) {
        getOrderById(id: $id) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<{ getOrderById: Order }, GetOrderByIdArgs>(
    query,
    options,
  );
};

export const useGetPrinterById = (
  fields: GenFields<Printer>,
  options?: QueryHookOptions<{ getPrinterById: Printer }, GetPrinterByIdArgs>,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getPrinterById ($id: String!) {
        getPrinterById(id: $id) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<{ getPrinterById: Printer }, GetPrinterByIdArgs>(
    query,
    options,
  );
};

export const useGetProductByBarcode = (
  fields: GenFields<Product>,
  options?: QueryHookOptions<
    { getProductByBarcode: Product },
    GetProductByBarcodeArgs
  >,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getProductByBarcode ($barcode: String!) {
        getProductByBarcode(barcode: $barcode) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<
    { getProductByBarcode: Product },
    GetProductByBarcodeArgs
  >(query, options);
};

export const useGetProductById = (
  fields: GenFields<Product>,
  options?: QueryHookOptions<{ getProductById: Product }, GetProductByIdArgs>,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getProductById ($id: String!) {
        getProductById(id: $id) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<{ getProductById: Product }, GetProductByIdArgs>(
    query,
    options,
  );
};

export const useGetProductCategoryById = (
  fields: GenFields<ProductCategory>,
  options?: QueryHookOptions<
    { getProductCategoryById: ProductCategory },
    GetProductCategoryByIdArgs
  >,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getProductCategoryById ($id: String!) {
        getProductCategoryById(id: $id) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<
    { getProductCategoryById: ProductCategory },
    GetProductCategoryByIdArgs
  >(query, options);
};

export const useGetRoleById = (
  fields: GenFields<Role>,
  options?: QueryHookOptions<{ getRoleById: Role }, GetRoleByIdArgs>,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getRoleById ($id: String!) {
        getRoleById(id: $id) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<{ getRoleById: Role }, GetRoleByIdArgs>(query, options);
};

export const useGetServiceTypeById = (
  fields: GenFields<ServiceType>,
  options?: QueryHookOptions<
    { getServiceTypeById: ServiceType },
    GetServiceTypeByIdArgs
  >,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getServiceTypeById ($id: String!) {
        getServiceTypeById(id: $id) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<
    { getServiceTypeById: ServiceType },
    GetServiceTypeByIdArgs
  >(query, options);
};

export const useGetSettingGeneral = (
  fields: GenFields<SettingGeneral>,
  options?: QueryHookOptions<{ getSettingGeneral: SettingGeneral }>,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getSettingGeneral  {
        getSettingGeneral {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<{ getSettingGeneral: SettingGeneral }>(query, options);
};

export const useGetSettingSeo = (
  fields: GenFields<SettingSeo>,
  options?: QueryHookOptions<{ getSettingSeo: SettingSeo }>,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getSettingSeo  {
        getSettingSeo {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<{ getSettingSeo: SettingSeo }>(query, options);
};

export const useGetStateById = (
  fields: GenFields<State>,
  options?: QueryHookOptions<{ getStateById: State }, GetStateByIdArgs>,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getStateById ($id: String!) {
        getStateById(id: $id) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<{ getStateById: State }, GetStateByIdArgs>(
    query,
    options,
  );
};

export const useGetStaticPageById = (
  fields: GenFields<StaticPage>,
  options?: QueryHookOptions<
    { getStaticPageById: StaticPage },
    GetStaticPageByIdArgs
  >,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getStaticPageById ($id: String!) {
        getStaticPageById(id: $id) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<{ getStaticPageById: StaticPage }, GetStaticPageByIdArgs>(
    query,
    options,
  );
};

export const useGetStoreImageById = (
  fields: GenFields<StoreImage>,
  options?: QueryHookOptions<
    { getStoreImageById: StoreImage },
    GetStoreImageByIdArgs
  >,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getStoreImageById ($id: String!) {
        getStoreImageById(id: $id) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<{ getStoreImageById: StoreImage }, GetStoreImageByIdArgs>(
    query,
    options,
  );
};

export const useGetTableById = (
  fields: GenFields<Table>,
  options?: QueryHookOptions<{ getTableById: Table }, GetTableByIdArgs>,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getTableById ($id: String!) {
        getTableById(id: $id) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<{ getTableById: Table }, GetTableByIdArgs>(
    query,
    options,
  );
};

export const useGetTableTypeById = (
  fields: GenFields<TableType>,
  options?: QueryHookOptions<
    { getTableTypeById: TableType },
    GetTableTypeByIdArgs
  >,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getTableTypeById ($id: String!) {
        getTableTypeById(id: $id) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<{ getTableTypeById: TableType }, GetTableTypeByIdArgs>(
    query,
    options,
  );
};

export const useGetTagById = (
  fields: GenFields<Tag>,
  options?: QueryHookOptions<{ getTagById: Tag }, GetTagByIdArgs>,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getTagById ($id: String!) {
        getTagById(id: $id) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<{ getTagById: Tag }, GetTagByIdArgs>(query, options);
};

export const useGetTaxById = (
  fields: GenFields<Tax>,
  options?: QueryHookOptions<{ getTaxById: Tax }, GetTaxByIdArgs>,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getTaxById ($id: String!) {
        getTaxById(id: $id) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<{ getTaxById: Tax }, GetTaxByIdArgs>(query, options);
};

export const useGetUserById = (
  fields: GenFields<User>,
  options?: QueryHookOptions<{ getUserById: User }, GetUserByIdArgs>,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getUserById ($id: String!) {
        getUserById(id: $id) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<{ getUserById: User }, GetUserByIdArgs>(query, options);
};

export const useGetUserFolderById = (
  fields: GenFields<UserFolder>,
  options?: QueryHookOptions<
    { getUserFolderById: UserFolder },
    GetUserFolderByIdArgs
  >,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getUserFolderById ($id: String!) {
        getUserFolderById(id: $id) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<{ getUserFolderById: UserFolder }, GetUserFolderByIdArgs>(
    query,
    options,
  );
};

export const useGetUserStoreByCreatedBy = (
  fields: GenFields<UserStore>,
  options?: QueryHookOptions<{ getUserStoreByCreatedBy: UserStore }>,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getUserStoreByCreatedBy  {
        getUserStoreByCreatedBy {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<{ getUserStoreByCreatedBy: UserStore }>(query, options);
};

export const useGetUserStoreById = (
  fields: GenFields<UserStore>,
  options?: QueryHookOptions<
    { getUserStoreById: UserStore },
    GetUserStoreByIdArgs
  >,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query getUserStoreById ($id: String!) {
        getUserStoreById(id: $id) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<{ getUserStoreById: UserStore }, GetUserStoreByIdArgs>(
    query,
    options,
  );
};

export const useIsExistingPhoneNumber = (
  fields: GenFields<User>,
  options?: QueryHookOptions<
    { isExistingPhoneNumber: User },
    IsExistingPhoneNumberArgs
  >,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const query = gql`
      query isExistingPhoneNumber ($phoneNumber: String!) {
        isExistingPhoneNumber(phoneNumber: $phoneNumber) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useLazyQuery<
    { isExistingPhoneNumber: User },
    IsExistingPhoneNumberArgs
  >(query, options);
};

export const useAddBranchManager = (
  fields: GenFields<Branch>,
  options?: MutationHookOptions<
    { addBranchManager: Branch },
    AddBranchManagerArgs
  >,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const mutation = gql`
      mutation addBranchManager ($branchInput: CreateManagerRequestInput!) {
        addBranchManager(branchInput: $branchInput) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useMutation<{ addBranchManager: Branch }, AddBranchManagerArgs>(
    mutation,
    options,
  );
};

export const useAddOrderItemToOrder = (
  options?: MutationHookOptions<
    { addOrderItemToOrder: boolean },
    AddOrderItemToOrderArgs
  >,
) => {
  const mutation = gql`
    mutation addOrderItemToOrder($idOrder: String!, $input: OrderItemInput!) {
      addOrderItemToOrder(idOrder: $idOrder, input: $input)
    }
  `;
  return useMutation<{ addOrderItemToOrder: boolean }, AddOrderItemToOrderArgs>(
    mutation,
    options,
  );
};

export const useChangePassword = (
  fields: GenFields<JwtPayload>,
  options?: MutationHookOptions<
    { changePassword: JwtPayload },
    ChangePasswordArgs
  >,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const mutation = gql`
      mutation changePassword ($input: ChangePasswordInput!) {
        changePassword(input: $input) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useMutation<{ changePassword: JwtPayload }, ChangePasswordArgs>(
    mutation,
    options,
  );
};

export const useCheckPasswordIsOk = (
  options?: MutationHookOptions<
    { checkPasswordIsOk: boolean },
    CheckPasswordIsOkArgs
  >,
) => {
  const mutation = gql`
    mutation checkPasswordIsOk($password: String!, $userId: String!) {
      checkPasswordIsOk(password: $password, userId: $userId)
    }
  `;
  return useMutation<{ checkPasswordIsOk: boolean }, CheckPasswordIsOkArgs>(
    mutation,
    options,
  );
};

export const useClearCache = (
  options?: MutationHookOptions<{ clearCache: string }>,
) => {
  const mutation = gql`
    mutation clearCache {
      clearCache
    }
  `;
  return useMutation<{ clearCache: string }>(mutation, options);
};

export const useCreateAds = (
  fields: GenFields<Ads>,
  options?: MutationHookOptions<{ createAds: Ads }, CreateAdsArgs>,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const mutation = gql`
      mutation createAds ($input: AdsCreateInput!) {
        createAds(input: $input) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useMutation<{ createAds: Ads }, CreateAdsArgs>(mutation, options);
};

export const useCreateBranch = (
  fields: GenFields<Branch>,
  options?: MutationHookOptions<{ createBranch: Branch }, CreateBranchArgs>,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const mutation = gql`
      mutation createBranch ($branchInput: CreateBranchInput!) {
        createBranch(branchInput: $branchInput) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useMutation<{ createBranch: Branch }, CreateBranchArgs>(
    mutation,
    options,
  );
};

export const useCreateBusinessProductType = (
  fields: GenFields<BusinessProductType>,
  options?: MutationHookOptions<
    { createBusinessProductType: BusinessProductType },
    CreateBusinessProductTypeArgs
  >,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const mutation = gql`
      mutation createBusinessProductType ($businessProductTypeInput: BusinessProductTypeInput!) {
        createBusinessProductType(businessProductTypeInput: $businessProductTypeInput) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useMutation<
    { createBusinessProductType: BusinessProductType },
    CreateBusinessProductTypeArgs
  >(mutation, options);
};

export const useCreateCategory = (
  fields: GenFields<Category>,
  options?: MutationHookOptions<
    { createCategory: Category },
    CreateCategoryArgs
  >,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const mutation = gql`
      mutation createCategory ($createCategoryInput: CreateCategoryInput!) {
        createCategory(createCategoryInput: $createCategoryInput) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useMutation<{ createCategory: Category }, CreateCategoryArgs>(
    mutation,
    options,
  );
};

export const useCreateCountry = (
  fields: GenFields<Country>,
  options?: MutationHookOptions<{ createCountry: Country }, CreateCountryArgs>,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const mutation = gql`
      mutation createCountry ($input: CountryCreateInput!) {
        createCountry(input: $input) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useMutation<{ createCountry: Country }, CreateCountryArgs>(
    mutation,
    options,
  );
};

export const useCreateCoupon = (
  fields: GenFields<Coupon>,
  options?: MutationHookOptions<{ createCoupon: Coupon }, CreateCouponArgs>,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const mutation = gql`
      mutation createCoupon ($input: CouponCreateInput!) {
        createCoupon(input: $input) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useMutation<{ createCoupon: Coupon }, CreateCouponArgs>(
    mutation,
    options,
  );
};

export const useCreateDistrict = (
  fields: GenFields<District>,
  options?: MutationHookOptions<
    { createDistrict: District },
    CreateDistrictArgs
  >,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const mutation = gql`
      mutation createDistrict ($input: DistrictInput!) {
        createDistrict(input: $input) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useMutation<{ createDistrict: District }, CreateDistrictArgs>(
    mutation,
    options,
  );
};

export const useCreateManagerRequest = (
  fields: GenFields<User>,
  options?: MutationHookOptions<
    { createManagerRequest: User },
    CreateManagerRequestArgs
  >,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const mutation = gql`
      mutation createManagerRequest ($input: CreateManagerRequestInput!) {
        createManagerRequest(input: $input) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useMutation<{ createManagerRequest: User }, CreateManagerRequestArgs>(
    mutation,
    options,
  );
};

export const useCreateOrder = (
  fields: GenFields<Order>,
  options?: MutationHookOptions<{ createOrder: Order }, CreateOrderArgs>,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const mutation = gql`
      mutation createOrder ($input: OrderCreateInput!) {
        createOrder(input: $input) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useMutation<{ createOrder: Order }, CreateOrderArgs>(
    mutation,
    options,
  );
};

export const useCreatePrinter = (
  fields: GenFields<Printer>,
  options?: MutationHookOptions<{ createPrinter: Printer }, CreatePrinterArgs>,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const mutation = gql`
      mutation createPrinter ($input: PrinterInput!) {
        createPrinter(input: $input) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useMutation<{ createPrinter: Printer }, CreatePrinterArgs>(
    mutation,
    options,
  );
};

export const useCreateProduct = (
  fields: GenFields<Product>,
  options?: MutationHookOptions<{ createProduct: Product }, CreateProductArgs>,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const mutation = gql`
      mutation createProduct ($productCreateInput: ProductCreateInput!) {
        createProduct(productCreateInput: $productCreateInput) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useMutation<{ createProduct: Product }, CreateProductArgs>(
    mutation,
    options,
  );
};

export const useCreateProductCategory = (
  fields: GenFields<ProductCategory>,
  options?: MutationHookOptions<
    { createProductCategory: ProductCategory },
    CreateProductCategoryArgs
  >,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const mutation = gql`
      mutation createProductCategory ($productCategoryCreateInput: ProductCategoryCreateInput!) {
        createProductCategory(productCategoryCreateInput: $productCategoryCreateInput) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useMutation<
    { createProductCategory: ProductCategory },
    CreateProductCategoryArgs
  >(mutation, options);
};

export const useCreateRole = (
  fields: GenFields<Role>,
  options?: MutationHookOptions<{ createRole: Role }, CreateRoleArgs>,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const mutation = gql`
      mutation createRole ($roleInput: RoleInput!) {
        createRole(roleInput: $roleInput) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useMutation<{ createRole: Role }, CreateRoleArgs>(mutation, options);
};

export const useCreateServiceType = (
  fields: GenFields<ServiceType>,
  options?: MutationHookOptions<
    { createServiceType: ServiceType },
    CreateServiceTypeArgs
  >,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const mutation = gql`
      mutation createServiceType ($serviceTypeInput: ServiceTypeInput!) {
        createServiceType(serviceTypeInput: $serviceTypeInput) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useMutation<{ createServiceType: ServiceType }, CreateServiceTypeArgs>(
    mutation,
    options,
  );
};

export const useCreateState = (
  fields: GenFields<State>,
  options?: MutationHookOptions<{ createState: State }, CreateStateArgs>,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const mutation = gql`
      mutation createState ($input: StateInput!) {
        createState(input: $input) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useMutation<{ createState: State }, CreateStateArgs>(
    mutation,
    options,
  );
};

export const useCreateStaticPage = (
  fields: GenFields<StaticPage>,
  options?: MutationHookOptions<
    { createStaticPage: StaticPage },
    CreateStaticPageArgs
  >,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const mutation = gql`
      mutation createStaticPage ($input: StaticPageCreateInput!) {
        createStaticPage(input: $input) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useMutation<{ createStaticPage: StaticPage }, CreateStaticPageArgs>(
    mutation,
    options,
  );
};

export const useCreateStoreImage = (
  fields: GenFields<StoreImage>,
  options?: MutationHookOptions<
    { createStoreImage: StoreImage },
    CreateStoreImageArgs
  >,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const mutation = gql`
      mutation createStoreImage ($input: StoreImageInput!) {
        createStoreImage(input: $input) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useMutation<{ createStoreImage: StoreImage }, CreateStoreImageArgs>(
    mutation,
    options,
  );
};

export const useCreateTable = (
  fields: GenFields<Table>,
  options?: MutationHookOptions<{ createTable: Table }, CreateTableArgs>,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const mutation = gql`
      mutation createTable ($input: TableInput!) {
        createTable(input: $input) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useMutation<{ createTable: Table }, CreateTableArgs>(
    mutation,
    options,
  );
};

export const useCreateTableType = (
  fields: GenFields<TableType>,
  options?: MutationHookOptions<
    { createTableType: TableType },
    CreateTableTypeArgs
  >,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const mutation = gql`
      mutation createTableType ($input: TableTypeInput!) {
        createTableType(input: $input) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useMutation<{ createTableType: TableType }, CreateTableTypeArgs>(
    mutation,
    options,
  );
};

export const useCreateTag = (
  fields: GenFields<Tag>,
  options?: MutationHookOptions<{ createTag: Tag }, CreateTagArgs>,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const mutation = gql`
      mutation createTag ($input: TagCreateInput!) {
        createTag(input: $input) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useMutation<{ createTag: Tag }, CreateTagArgs>(mutation, options);
};

export const useCreateTax = (
  fields: GenFields<Tax>,
  options?: MutationHookOptions<{ createTax: Tax }, CreateTaxArgs>,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const mutation = gql`
      mutation createTax ($input: TaxCreateInput!) {
        createTax(input: $input) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useMutation<{ createTax: Tax }, CreateTaxArgs>(mutation, options);
};

export const useCreateUser = (
  fields: GenFields<User>,
  options?: MutationHookOptions<{ createUser: User }, CreateUserArgs>,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const mutation = gql`
      mutation createUser ($input: CreateUserInput!) {
        createUser(input: $input) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useMutation<{ createUser: User }, CreateUserArgs>(mutation, options);
};

export const useCreateUserFolder = (
  fields: GenFields<UserFolder>,
  options?: MutationHookOptions<
    { createUserFolder: UserFolder },
    CreateUserFolderArgs
  >,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const mutation = gql`
      mutation createUserFolder ($input: UserFolderInput!) {
        createUserFolder(input: $input) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useMutation<{ createUserFolder: UserFolder }, CreateUserFolderArgs>(
    mutation,
    options,
  );
};

export const useCreateUserStore = (
  fields: GenFields<UserStore>,
  options?: MutationHookOptions<
    { createUserStore: UserStore },
    CreateUserStoreArgs
  >,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const mutation = gql`
      mutation createUserStore ($userStoreInput: UserStoreInput!) {
        createUserStore(userStoreInput: $userStoreInput) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useMutation<{ createUserStore: UserStore }, CreateUserStoreArgs>(
    mutation,
    options,
  );
};

export const useDeleteAds = (
  options?: MutationHookOptions<{ deleteAds: boolean }, DeleteAdsArgs>,
) => {
  const mutation = gql`
    mutation deleteAds($listId: [String!]!) {
      deleteAds(listId: $listId)
    }
  `;
  return useMutation<{ deleteAds: boolean }, DeleteAdsArgs>(mutation, options);
};

export const useDeleteBranch = (
  options?: MutationHookOptions<{ deleteBranch: boolean }, DeleteBranchArgs>,
) => {
  const mutation = gql`
    mutation deleteBranch($listId: [String!]!) {
      deleteBranch(listId: $listId)
    }
  `;
  return useMutation<{ deleteBranch: boolean }, DeleteBranchArgs>(
    mutation,
    options,
  );
};

export const useDeleteBusinessProductType = (
  options?: MutationHookOptions<
    { deleteBusinessProductType: boolean },
    DeleteBusinessProductTypeArgs
  >,
) => {
  const mutation = gql`
    mutation deleteBusinessProductType($listId: [String!]!) {
      deleteBusinessProductType(listId: $listId)
    }
  `;
  return useMutation<
    { deleteBusinessProductType: boolean },
    DeleteBusinessProductTypeArgs
  >(mutation, options);
};

export const useDeleteCategory = (
  options?: MutationHookOptions<
    { deleteCategory: boolean },
    DeleteCategoryArgs
  >,
) => {
  const mutation = gql`
    mutation deleteCategory($listId: [String!]!) {
      deleteCategory(listId: $listId)
    }
  `;
  return useMutation<{ deleteCategory: boolean }, DeleteCategoryArgs>(
    mutation,
    options,
  );
};

export const useDeleteCountry = (
  options?: MutationHookOptions<{ deleteCountry: boolean }, DeleteCountryArgs>,
) => {
  const mutation = gql`
    mutation deleteCountry($listId: [String!]!) {
      deleteCountry(listId: $listId)
    }
  `;
  return useMutation<{ deleteCountry: boolean }, DeleteCountryArgs>(
    mutation,
    options,
  );
};

export const useDeleteCoupon = (
  options?: MutationHookOptions<{ deleteCoupon: boolean }, DeleteCouponArgs>,
) => {
  const mutation = gql`
    mutation deleteCoupon($listId: [String!]!) {
      deleteCoupon(listId: $listId)
    }
  `;
  return useMutation<{ deleteCoupon: boolean }, DeleteCouponArgs>(
    mutation,
    options,
  );
};

export const useDeleteDistrict = (
  options?: MutationHookOptions<
    { deleteDistrict: boolean },
    DeleteDistrictArgs
  >,
) => {
  const mutation = gql`
    mutation deleteDistrict($listId: [String!]!) {
      deleteDistrict(listId: $listId)
    }
  `;
  return useMutation<{ deleteDistrict: boolean }, DeleteDistrictArgs>(
    mutation,
    options,
  );
};

export const useDeleteOrder = (
  options?: MutationHookOptions<{ deleteOrder: boolean }, DeleteOrderArgs>,
) => {
  const mutation = gql`
    mutation deleteOrder($listId: [String!]!) {
      deleteOrder(listId: $listId)
    }
  `;
  return useMutation<{ deleteOrder: boolean }, DeleteOrderArgs>(
    mutation,
    options,
  );
};

export const useDeleteOrderItem = (
  options?: MutationHookOptions<
    { deleteOrderItem: boolean },
    DeleteOrderItemArgs
  >,
) => {
  const mutation = gql`
    mutation deleteOrderItem($idOrder: String!, $listIdOrderItem: [String!]!) {
      deleteOrderItem(idOrder: $idOrder, listIdOrderItem: $listIdOrderItem)
    }
  `;
  return useMutation<{ deleteOrderItem: boolean }, DeleteOrderItemArgs>(
    mutation,
    options,
  );
};

export const useDeletePrinter = (
  options?: MutationHookOptions<{ deletePrinter: boolean }, DeletePrinterArgs>,
) => {
  const mutation = gql`
    mutation deletePrinter($listId: [String!]!) {
      deletePrinter(listId: $listId)
    }
  `;
  return useMutation<{ deletePrinter: boolean }, DeletePrinterArgs>(
    mutation,
    options,
  );
};

export const useDeleteProduct = (
  options?: MutationHookOptions<{ deleteProduct: boolean }, DeleteProductArgs>,
) => {
  const mutation = gql`
    mutation deleteProduct($listId: [String!]!) {
      deleteProduct(listId: $listId)
    }
  `;
  return useMutation<{ deleteProduct: boolean }, DeleteProductArgs>(
    mutation,
    options,
  );
};

export const useDeleteProductCategory = (
  options?: MutationHookOptions<
    { deleteProductCategory: boolean },
    DeleteProductCategoryArgs
  >,
) => {
  const mutation = gql`
    mutation deleteProductCategory($listId: [String!]!) {
      deleteProductCategory(listId: $listId)
    }
  `;
  return useMutation<
    { deleteProductCategory: boolean },
    DeleteProductCategoryArgs
  >(mutation, options);
};

export const useDeleteRoleById = (
  options?: MutationHookOptions<
    { deleteRoleById: boolean },
    DeleteRoleByIdArgs
  >,
) => {
  const mutation = gql`
    mutation deleteRoleById($id: String!) {
      deleteRoleById(id: $id)
    }
  `;
  return useMutation<{ deleteRoleById: boolean }, DeleteRoleByIdArgs>(
    mutation,
    options,
  );
};

export const useDeleteServiceType = (
  options?: MutationHookOptions<
    { deleteServiceType: boolean },
    DeleteServiceTypeArgs
  >,
) => {
  const mutation = gql`
    mutation deleteServiceType($listId: [String!]!) {
      deleteServiceType(listId: $listId)
    }
  `;
  return useMutation<{ deleteServiceType: boolean }, DeleteServiceTypeArgs>(
    mutation,
    options,
  );
};

export const useDeleteState = (
  options?: MutationHookOptions<{ deleteState: boolean }, DeleteStateArgs>,
) => {
  const mutation = gql`
    mutation deleteState($listId: [String!]!) {
      deleteState(listId: $listId)
    }
  `;
  return useMutation<{ deleteState: boolean }, DeleteStateArgs>(
    mutation,
    options,
  );
};

export const useDeleteStaticPage = (
  options?: MutationHookOptions<
    { deleteStaticPage: boolean },
    DeleteStaticPageArgs
  >,
) => {
  const mutation = gql`
    mutation deleteStaticPage($listId: [String!]!) {
      deleteStaticPage(listId: $listId)
    }
  `;
  return useMutation<{ deleteStaticPage: boolean }, DeleteStaticPageArgs>(
    mutation,
    options,
  );
};

export const useDeleteStoreImage = (
  options?: MutationHookOptions<
    { deleteStoreImage: boolean },
    DeleteStoreImageArgs
  >,
) => {
  const mutation = gql`
    mutation deleteStoreImage($listId: [String!]!) {
      deleteStoreImage(listId: $listId)
    }
  `;
  return useMutation<{ deleteStoreImage: boolean }, DeleteStoreImageArgs>(
    mutation,
    options,
  );
};

export const useDeleteTable = (
  options?: MutationHookOptions<{ deleteTable: boolean }, DeleteTableArgs>,
) => {
  const mutation = gql`
    mutation deleteTable($listId: [String!]!) {
      deleteTable(listId: $listId)
    }
  `;
  return useMutation<{ deleteTable: boolean }, DeleteTableArgs>(
    mutation,
    options,
  );
};

export const useDeleteTableType = (
  options?: MutationHookOptions<
    { deleteTableType: boolean },
    DeleteTableTypeArgs
  >,
) => {
  const mutation = gql`
    mutation deleteTableType($listId: [String!]!) {
      deleteTableType(listId: $listId)
    }
  `;
  return useMutation<{ deleteTableType: boolean }, DeleteTableTypeArgs>(
    mutation,
    options,
  );
};

export const useDeleteTag = (
  options?: MutationHookOptions<{ deleteTag: boolean }, DeleteTagArgs>,
) => {
  const mutation = gql`
    mutation deleteTag($listId: [String!]!) {
      deleteTag(listId: $listId)
    }
  `;
  return useMutation<{ deleteTag: boolean }, DeleteTagArgs>(mutation, options);
};

export const useDeleteTax = (
  options?: MutationHookOptions<{ deleteTax: boolean }, DeleteTaxArgs>,
) => {
  const mutation = gql`
    mutation deleteTax($listId: [String!]!) {
      deleteTax(listId: $listId)
    }
  `;
  return useMutation<{ deleteTax: boolean }, DeleteTaxArgs>(mutation, options);
};

export const useDeleteUser = (
  options?: MutationHookOptions<{ deleteUser: boolean }, DeleteUserArgs>,
) => {
  const mutation = gql`
    mutation deleteUser($id: String!) {
      deleteUser(id: $id)
    }
  `;
  return useMutation<{ deleteUser: boolean }, DeleteUserArgs>(
    mutation,
    options,
  );
};

export const useDeleteUserFolder = (
  options?: MutationHookOptions<
    { deleteUserFolder: boolean },
    DeleteUserFolderArgs
  >,
) => {
  const mutation = gql`
    mutation deleteUserFolder($listId: [String!]!) {
      deleteUserFolder(listId: $listId)
    }
  `;
  return useMutation<{ deleteUserFolder: boolean }, DeleteUserFolderArgs>(
    mutation,
    options,
  );
};

export const useDeleteUserStore = (
  options?: MutationHookOptions<
    { deleteUserStore: boolean },
    DeleteUserStoreArgs
  >,
) => {
  const mutation = gql`
    mutation deleteUserStore($id: [String!]!) {
      deleteUserStore(id: $id)
    }
  `;
  return useMutation<{ deleteUserStore: boolean }, DeleteUserStoreArgs>(
    mutation,
    options,
  );
};

export const useForgotPassword = (
  fields: GenFields<JwtPayload>,
  options?: MutationHookOptions<
    { forgotPassword: JwtPayload },
    ForgotPasswordArgs
  >,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const mutation = gql`
      mutation forgotPassword ($input: ForgotPasswordInput!) {
        forgotPassword(input: $input) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useMutation<{ forgotPassword: JwtPayload }, ForgotPasswordArgs>(
    mutation,
    options,
  );
};

export const useLogin = (
  fields: GenFields<JwtPayload>,
  options?: MutationHookOptions<{ login: JwtPayload }, LoginArgs>,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const mutation = gql`
      mutation login ($user: LoginUserInput!) {
        login(user: $user) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useMutation<{ login: JwtPayload }, LoginArgs>(mutation, options);
};

export const useLoginFirebase = (
  fields: GenFields<JwtPayload>,
  options?: MutationHookOptions<
    { loginFirebase: JwtPayload },
    LoginFirebaseArgs
  >,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const mutation = gql`
      mutation loginFirebase ($idToken: String!) {
        loginFirebase(idToken: $idToken) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useMutation<{ loginFirebase: JwtPayload }, LoginFirebaseArgs>(
    mutation,
    options,
  );
};

export const useRejectManager = (
  options?: MutationHookOptions<{ rejectManager: boolean }, RejectManagerArgs>,
) => {
  const mutation = gql`
    mutation rejectManager($id: String!) {
      rejectManager(id: $id)
    }
  `;
  return useMutation<{ rejectManager: boolean }, RejectManagerArgs>(
    mutation,
    options,
  );
};

export const useUpdateAds = (
  options?: MutationHookOptions<{ updateAds: boolean }, UpdateAdsArgs>,
) => {
  const mutation = gql`
    mutation updateAds($id: String!, $input: AdsUpdateInput!) {
      updateAds(id: $id, input: $input)
    }
  `;
  return useMutation<{ updateAds: boolean }, UpdateAdsArgs>(mutation, options);
};

export const useUpdateBranch = (
  fields: GenFields<Branch>,
  options?: MutationHookOptions<{ updateBranch: Branch }, UpdateBranchArgs>,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const mutation = gql`
      mutation updateBranch ($branchInput: UpdateBranchInput!,$id: String!) {
        updateBranch(branchInput: $branchInput,id: $id) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useMutation<{ updateBranch: Branch }, UpdateBranchArgs>(
    mutation,
    options,
  );
};

export const useUpdateBusinessProductType = (
  fields: GenFields<BusinessProductType>,
  options?: MutationHookOptions<
    { updateBusinessProductType: BusinessProductType },
    UpdateBusinessProductTypeArgs
  >,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const mutation = gql`
      mutation updateBusinessProductType ($businessProductTypeInput: BusinessProductTypeInput!,$id: String!) {
        updateBusinessProductType(businessProductTypeInput: $businessProductTypeInput,id: $id) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useMutation<
    { updateBusinessProductType: BusinessProductType },
    UpdateBusinessProductTypeArgs
  >(mutation, options);
};

export const useUpdateCategory = (
  fields: GenFields<Category>,
  options?: MutationHookOptions<
    { updateCategory: Category },
    UpdateCategoryArgs
  >,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const mutation = gql`
      mutation updateCategory ($fieldsToUpdate: UpdateCategoryInput!,$id: String!) {
        updateCategory(fieldsToUpdate: $fieldsToUpdate,id: $id) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useMutation<{ updateCategory: Category }, UpdateCategoryArgs>(
    mutation,
    options,
  );
};

export const useUpdateCompleteStatusOrder = (
  options?: MutationHookOptions<
    { updateCompleteStatusOrder: boolean },
    UpdateCompleteStatusOrderArgs
  >,
) => {
  const mutation = gql`
    mutation updateCompleteStatusOrder(
      $input: OrderUpdateStatusCompleteInput!
    ) {
      updateCompleteStatusOrder(input: $input)
    }
  `;
  return useMutation<
    { updateCompleteStatusOrder: boolean },
    UpdateCompleteStatusOrderArgs
  >(mutation, options);
};

export const useUpdateCountry = (
  options?: MutationHookOptions<{ updateCountry: boolean }, UpdateCountryArgs>,
) => {
  const mutation = gql`
    mutation updateCountry($id: String!, $input: CountryUpdateInput!) {
      updateCountry(id: $id, input: $input)
    }
  `;
  return useMutation<{ updateCountry: boolean }, UpdateCountryArgs>(
    mutation,
    options,
  );
};

export const useUpdateCoupon = (
  options?: MutationHookOptions<{ updateCoupon: boolean }, UpdateCouponArgs>,
) => {
  const mutation = gql`
    mutation updateCoupon($id: String!, $input: CouponUpdateInput!) {
      updateCoupon(id: $id, input: $input)
    }
  `;
  return useMutation<{ updateCoupon: boolean }, UpdateCouponArgs>(
    mutation,
    options,
  );
};

export const useUpdateDistrict = (
  options?: MutationHookOptions<
    { updateDistrict: boolean },
    UpdateDistrictArgs
  >,
) => {
  const mutation = gql`
    mutation updateDistrict($id: String!, $input: DistrictInput!) {
      updateDistrict(id: $id, input: $input)
    }
  `;
  return useMutation<{ updateDistrict: boolean }, UpdateDistrictArgs>(
    mutation,
    options,
  );
};

export const useUpdateOrder = (
  fields: GenFields<Order>,
  options?: MutationHookOptions<{ updateOrder: Order }, UpdateOrderArgs>,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const mutation = gql`
      mutation updateOrder ($id: String!,$input: OrderUpdateInput!) {
        updateOrder(id: $id,input: $input) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useMutation<{ updateOrder: Order }, UpdateOrderArgs>(
    mutation,
    options,
  );
};

export const useUpdateOrderItem = (
  options?: MutationHookOptions<
    { updateOrderItem: boolean },
    UpdateOrderItemArgs
  >,
) => {
  const mutation = gql`
    mutation updateOrderItem(
      $idOrder: String!
      $idOrderItem: String!
      $input: OrderItemInput!
    ) {
      updateOrderItem(
        idOrder: $idOrder
        idOrderItem: $idOrderItem
        input: $input
      )
    }
  `;
  return useMutation<{ updateOrderItem: boolean }, UpdateOrderItemArgs>(
    mutation,
    options,
  );
};

export const useUpdatePrinter = (
  options?: MutationHookOptions<{ updatePrinter: boolean }, UpdatePrinterArgs>,
) => {
  const mutation = gql`
    mutation updatePrinter($id: String!, $input: PrinterInput!) {
      updatePrinter(id: $id, input: $input)
    }
  `;
  return useMutation<{ updatePrinter: boolean }, UpdatePrinterArgs>(
    mutation,
    options,
  );
};

export const useUpdateProduct = (
  fields: GenFields<Product>,
  options?: MutationHookOptions<{ updateProduct: Product }, UpdateProductArgs>,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const mutation = gql`
      mutation updateProduct ($id: String!,$productUpdateInput: ProductUpdateInput!) {
        updateProduct(id: $id,productUpdateInput: $productUpdateInput) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useMutation<{ updateProduct: Product }, UpdateProductArgs>(
    mutation,
    options,
  );
};

export const useUpdateProductCategory = (
  fields: GenFields<ProductCategory>,
  options?: MutationHookOptions<
    { updateProductCategory: ProductCategory },
    UpdateProductCategoryArgs
  >,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const mutation = gql`
      mutation updateProductCategory ($id: String!,$productCategoryUpdateInput: ProductCategoryUpdateInput!) {
        updateProductCategory(id: $id,productCategoryUpdateInput: $productCategoryUpdateInput) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useMutation<
    { updateProductCategory: ProductCategory },
    UpdateProductCategoryArgs
  >(mutation, options);
};

export const useUpdateRole = (
  fields: GenFields<Role>,
  options?: MutationHookOptions<{ updateRole: Role }, UpdateRoleArgs>,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const mutation = gql`
      mutation updateRole ($id: String!,$roleInput: RoleInput!) {
        updateRole(id: $id,roleInput: $roleInput) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useMutation<{ updateRole: Role }, UpdateRoleArgs>(mutation, options);
};

export const useUpdateServiceType = (
  fields: GenFields<ServiceType>,
  options?: MutationHookOptions<
    { updateServiceType: ServiceType },
    UpdateServiceTypeArgs
  >,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const mutation = gql`
      mutation updateServiceType ($id: String!,$serviceTypeInput: ServiceTypeInput!) {
        updateServiceType(id: $id,serviceTypeInput: $serviceTypeInput) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useMutation<{ updateServiceType: ServiceType }, UpdateServiceTypeArgs>(
    mutation,
    options,
  );
};

export const useUpdateSettingGeneral = (
  fields: GenFields<SettingGeneral>,
  options?: MutationHookOptions<
    { updateSettingGeneral: SettingGeneral },
    UpdateSettingGeneralArgs
  >,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const mutation = gql`
      mutation updateSettingGeneral ($input: SettingGeneralInput!) {
        updateSettingGeneral(input: $input) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useMutation<
    { updateSettingGeneral: SettingGeneral },
    UpdateSettingGeneralArgs
  >(mutation, options);
};

export const useUpdateSettingSeo = (
  fields: GenFields<SettingSeo>,
  options?: MutationHookOptions<
    { updateSettingSeo: SettingSeo },
    UpdateSettingSeoArgs
  >,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const mutation = gql`
      mutation updateSettingSeo ($input: SettingSeoInput!) {
        updateSettingSeo(input: $input) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useMutation<{ updateSettingSeo: SettingSeo }, UpdateSettingSeoArgs>(
    mutation,
    options,
  );
};

export const useUpdateState = (
  options?: MutationHookOptions<{ updateState: boolean }, UpdateStateArgs>,
) => {
  const mutation = gql`
    mutation updateState($id: String!, $input: StateInput!) {
      updateState(id: $id, input: $input)
    }
  `;
  return useMutation<{ updateState: boolean }, UpdateStateArgs>(
    mutation,
    options,
  );
};

export const useUpdateStaticPage = (
  options?: MutationHookOptions<
    { updateStaticPage: boolean },
    UpdateStaticPageArgs
  >,
) => {
  const mutation = gql`
    mutation updateStaticPage($id: String!, $input: StaticPageUpdateInput!) {
      updateStaticPage(id: $id, input: $input)
    }
  `;
  return useMutation<{ updateStaticPage: boolean }, UpdateStaticPageArgs>(
    mutation,
    options,
  );
};

export const useUpdateStoreImage = (
  options?: MutationHookOptions<
    { updateStoreImage: boolean },
    UpdateStoreImageArgs
  >,
) => {
  const mutation = gql`
    mutation updateStoreImage($id: String!, $input: StoreImageInput!) {
      updateStoreImage(id: $id, input: $input)
    }
  `;
  return useMutation<{ updateStoreImage: boolean }, UpdateStoreImageArgs>(
    mutation,
    options,
  );
};

export const useUpdateTable = (
  fields: GenFields<Table>,
  options?: MutationHookOptions<{ updateTable: Table }, UpdateTableArgs>,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const mutation = gql`
      mutation updateTable ($id: String!,$input: TableInput!) {
        updateTable(id: $id,input: $input) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useMutation<{ updateTable: Table }, UpdateTableArgs>(
    mutation,
    options,
  );
};

export const useUpdateTableType = (
  fields: GenFields<TableType>,
  options?: MutationHookOptions<
    { updateTableType: TableType },
    UpdateTableTypeArgs
  >,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const mutation = gql`
      mutation updateTableType ($id: String!,$input: TableTypeInput!) {
        updateTableType(id: $id,input: $input) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useMutation<{ updateTableType: TableType }, UpdateTableTypeArgs>(
    mutation,
    options,
  );
};

export const useUpdateTag = (
  options?: MutationHookOptions<{ updateTag: boolean }, UpdateTagArgs>,
) => {
  const mutation = gql`
    mutation updateTag($id: String!, $input: TagUpdateInput!) {
      updateTag(id: $id, input: $input)
    }
  `;
  return useMutation<{ updateTag: boolean }, UpdateTagArgs>(mutation, options);
};

export const useUpdateTax = (
  options?: MutationHookOptions<{ updateTax: boolean }, UpdateTaxArgs>,
) => {
  const mutation = gql`
    mutation updateTax($id: String!, $input: TaxUpdateInput!) {
      updateTax(id: $id, input: $input)
    }
  `;
  return useMutation<{ updateTax: boolean }, UpdateTaxArgs>(mutation, options);
};

export const useUpdateUser = (
  fields: GenFields<User>,
  options?: MutationHookOptions<{ updateUser: User }, UpdateUserArgs>,
) => {
  const fragment = queryBuilder(fields);
  const { isString, isFragment, fragmentName } = guessFragmentType(fragment);
  const mutation = gql`
      mutation updateUser ($id: String!,$updateUserInput: UpdateUserInput!) {
        updateUser(id: $id,updateUserInput: $updateUserInput) {
          ${isString ? fragment : "..." + fragmentName}
        }
      } ${isFragment ? fragment : ""}
      `;

  return useMutation<{ updateUser: User }, UpdateUserArgs>(mutation, options);
};

export const useUpdateUserFolder = (
  options?: MutationHookOptions<
    { updateUserFolder: boolean },
    UpdateUserFolderArgs
  >,
) => {
  const mutation = gql`
    mutation updateUserFolder($id: String!, $input: UserFolderInput!) {
      updateUserFolder(id: $id, input: $input)
    }
  `;
  return useMutation<{ updateUserFolder: boolean }, UpdateUserFolderArgs>(
    mutation,
    options,
  );
};
