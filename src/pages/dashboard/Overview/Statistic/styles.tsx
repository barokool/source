import styled from "styled-components/macro";
import tw from "twin.macro";

export const StatisticContainer = styled.div`
  ${tw`grid phone:grid-cols-4 grid-cols-2 gap-2`}
`;

export const Items = {
  Wrapper: styled.div`
    ${tw`p-2 flex-col gap-[16px] shadow rounded-sm cursor-pointer `}
  `,
  Top: styled.div`
    ${tw`flex gap-2 items-center`}
  `,
  Title: styled.div`
    ${tw`font-medium text-16 text-black`}
  `,
  Bottom: styled.p`
    ${tw`font-bold text-secondary text-26 text-right`}
  `,
};
