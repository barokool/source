import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { User } from "apiCaller";
import { removeUserCookies } from "common/utils/auth";
import { IAccount } from "typings";

export interface IAuthStates {
  currentUser: IAccount | null;
  accountProfile: User | null;
  isLogoutAction: boolean;
  step: number;
}

const initialState: IAuthStates = {
  currentUser: null,
  accountProfile: null,
  isLogoutAction: false,
  step: 1,
};

export const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    removeCurrentUser: state => {
      state.currentUser = null;
      state.isLogoutAction = true;
      removeUserCookies();
    },
    setStep: (state, action: PayloadAction<number>) => {
      state.step = action.payload;
    },
  },
});

// Actions
export const { removeCurrentUser, setStep } = authSlice.actions;

export default authSlice.reducer;
