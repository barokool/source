import tw from "twin.macro";
import styled from "styled-components/macro";
import { Form as _Form } from "formik";
import _Button from "designs/Button";

export const CustomersManagementDialogContainer = styled.div`
  ${tw`bg-white p-1 phone:p-2 laptop:p-5 rounded-sm overflow-x-hidden `}
`;

export const ElementWrapper = styled.div`
  ${tw`cursor-pointer`}
`;

export const Form = styled(_Form)`
  ${tw`grid grid-cols-2 gap-4 `}
`;

export const ButtonWrapper = styled.div`
  ${tw`flex justify-end  w-full mt-3`}
`;

export const Button = styled(_Button)`
  ${tw`py-1 px-2 w-max flex justify-center ml-1`}
`;
