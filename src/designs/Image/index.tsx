import withErrorBoundary from "common/HOC/withErrorBoundary";
import React from "react";

/**
 * @example
 *  We have a folder tree like this
 *   + assets/image
 *     |-- dog.png    --> <Image name="dog.png" />
 *     |-- drawer
 *       |-- ant.jpg --> <Image name="animal/ant.jpg" />
 */
type ISVGProps = {
  alt?: string;
} & (
  | {
      name: string;
      readonly src?: undefined;
    }
  | {
      readonly name?: undefined;
      src: string;
    }
) &
  React.DetailedHTMLProps<
    React.ImgHTMLAttributes<HTMLImageElement>,
    HTMLImageElement
  >;

const Image: React.FC<ISVGProps> = props => {
  const { name = "", alt, src, ...rest } = props;

  if (!name && !src) return null;

  return (
    <img
      src={src || require(`assets/images/${name}`)}
      alt={alt || `${name} image`}
      {...(rest as any)}
    />
  );
};

export default withErrorBoundary(Image);
