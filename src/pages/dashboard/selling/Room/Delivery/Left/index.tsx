import React from "react";

//redux
import { useSelector } from "react-redux";
import { IRootState } from "typings";

//api
import { OrderTypeEnum, OrderStatusEnum } from "apiCaller";

//locals
import LeftReviewBill from "./ReviewBill";
import LeftCardBoard from "./CardBoard";
import { DeliveryLeftContainer } from "./styles";

interface IDeliveryLeftProps {}

const DeliveryLeft: React.FC<IDeliveryLeftProps> = props => {
  const { orderSelected } = useSelector((state: IRootState) => state.order);

  return (
    <DeliveryLeftContainer>
      {orderSelected?.status === OrderStatusEnum.Approving &&
      orderSelected.type === OrderTypeEnum.Delivery ? (
        <LeftReviewBill />
      ) : (
        <LeftCardBoard />
      )}
    </DeliveryLeftContainer>
  );
};

export default DeliveryLeft;
