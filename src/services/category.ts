import { Category, CategoryResult, GenFields } from "apiCaller";

export const fragmentGetAllCategories: GenFields<CategoryResult> = [
  "totalCount",
  {
    results: [
      "_id",
      "name",
      "description",
      { image: ["default", "small", "medium"] },
      "categoryCode",
    ],
  },
];

export const fragmentCreateCategory: GenFields<Category> = ["_id"];

export const fragmentUpdateCategory: GenFields<Category> = ["_id"];
