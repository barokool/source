import { TableLayoutContainer, SearchBoxWrapper, TableWrapper } from "./styles";

interface ITableLayoutProps {
  buttonMenu?: JSX.Element;
  search?: JSX.Element;
  className?: string;
}

const TableLayout: React.FC<ITableLayoutProps> = ({
  buttonMenu,
  search,
  children,
  className,
}) => {
  return (
    <TableLayoutContainer>
      <SearchBoxWrapper className={className}>
        {search}
        {buttonMenu}
      </SearchBoxWrapper>
      <TableWrapper>{children}</TableWrapper>
    </TableLayoutContainer>
  );
};

export default TableLayout;
