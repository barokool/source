import { t } from "language";
import React, { useState } from "react";
import { RouteComponentProps } from "react-router";

import { useBreadcrumb } from "hooks/useBreadcrumb";

import TwoSideLayout from "layouts/TwoSide";

import OrderServiceItem from "./components/OrderItem";
import { Button, ButtonWrapper, OrderServiceContainer } from "./styles";
import OrderServiceDetail from "./components/OrderServiceDetail";

interface OrderServiceProps extends RouteComponentProps {}

const OrderService: React.FC<OrderServiceProps> = ({ location }) => {
  const [selectDate, setSelectDate] = useState({ date: "12-12-2022" });
  const [selected, setSelected] = useState("#2307519TFC");

  useBreadcrumb([
    {
      name: t("orderService.order-service"),
    },
  ]);

  return (
    <OrderServiceContainer>
      <TwoSideLayout>
        <TwoSideLayout.LeftSide>
          <TwoSideLayout.ListBox>
            {initData.map(value =>
              value.code === selected ? (
                <OrderServiceItem
                  active
                  data={value}
                  onClick={() => {
                    console.log(value);
                    setSelected(value.code);
                  }}
                />
              ) : (
                <OrderServiceItem
                  data={value}
                  onClick={() => {
                    setSelected(value.code);
                  }}
                />
              ),
            )}
          </TwoSideLayout.ListBox>
        </TwoSideLayout.LeftSide>
        <TwoSideLayout.RightSide>
          <OrderServiceDetail />
          <ButtonWrapper>
            <Button outline>{t("orderService.cancel-order")}</Button>
            <Button primary>{t("orderService.accept-oder")}</Button>
          </ButtonWrapper>
        </TwoSideLayout.RightSide>
      </TwoSideLayout>
    </OrderServiceContainer>
  );

  // return (
  //   <Empty
  //     SVG={<SVG name="orderService/empty" width={200} height={200} />}
  //     Title={t("orderService.empty-title")}
  //     SubTitle={t("orderService.empty-message")}
  //     ButtonMenu={<div></div>}
  //   />
  // );
};

export default OrderService;

const initData: {
  code: string;
  name: string;
  phone: string;
  timeBooking: string;
  status: "new" | "cancel" | "complete";
}[] = [
  {
    code: "#2307519TFC",
    name: "Tram Huyen",
    phone: "0933333333",
    timeBooking: "15:56",
    status: "new",
  },
  {
    code: "#2307919TFC",
    name: "Tram Huyen",
    phone: "0933333334",
    timeBooking: "15:56",
    status: "cancel",
  },
  {
    code: "#2303919TFC",
    name: "Tram Huyen",
    phone: "0933333335",
    timeBooking: "15:56",
    status: "cancel",
  },
  {
    code: "#2317919TFC",
    name: "Tram Huyen",
    phone: "0933333336",
    timeBooking: "15:56",
    status: "complete",
  },
  {
    code: "#2407919TFC",
    name: "Tram Huyen",
    phone: "0933333337",
    timeBooking: "15:56",
    status: "new",
  },
];
