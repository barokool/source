import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Branch } from "apiCaller";

type ISellingPageType = "Room" | "Menu";

interface SellingPageState {
  pageSelected: ISellingPageType;
  branchSelected: Branch | null;
}

const initialState: SellingPageState = {
  pageSelected: "Room",
  branchSelected: null,
};

export const sellingSlice = createSlice({
  name: "selling",
  initialState,
  reducers: {
    setSellingPageSelected: (
      state: any,
      action: PayloadAction<ISellingPageType>,
    ) => {
      state.pageSelected = action.payload;
    },
    setBranchSelected: (state: any, action: PayloadAction<Branch | null>) => {
      state.branchSelected = action.payload;
    },
  },
});

// Actions
export const { setSellingPageSelected, setBranchSelected } =
  sellingSlice.actions;

export default sellingSlice.reducer;
