import { Branch, BranchResult, GenFields } from "apiCaller";

export const fragmentGetAllBranch: GenFields<BranchResult> = [
  {
    results: [
      "_id",
      "code",
      "name",
      "phoneNumber",

      { images: ["default", "medium", "small"] },
      "maxGuest",
      "introduction",
      "status",
      { operationTimes: ["day", { times: ["openingHours", "closeHours"] }] },
      {
        location: [
          { country: ["_id", "name"] },
          { state: ["_id", "name"] },
          { city: ["_id", "name"] },
          { district: ["_id", "name"] },
          "addressLine",
        ],
      },
      "isMainBranch",
      { serviceType: ["_id", "name"] },
      { businessProductTypes: ["_id", "name"] },
    ],
  },
];

export const fragmentGetBranchById: GenFields<Branch> = [
  "_id",
  "code",
  "name",
  "phoneNumber",

  { images: ["default", "medium", "small"] },
  "maxGuest",
  "introduction",
  "status",
  { operationTimes: ["day", { times: ["openingHours", "closeHours"] }] },
  {
    location: [
      { country: ["_id", "name"] },
      { state: ["_id", "name"] },
      { city: ["_id", "name"] },
      { district: ["_id", "name"] },
      "addressLine",
    ],
  },
  "isMainBranch",
  { serviceType: ["_id", "name"] },
  { businessProductTypes: ["_id", "name"] },
];

export const fragmentAddBranchManager: GenFields<Branch> = [
  "_id",
  "code",
  "name",
  "phoneNumber",

  { images: ["default", "medium", "small"] },
  "maxGuest",
  "introduction",
  "status",
  { operationTimes: ["day", { times: ["openingHours", "closeHours"] }] },
  {
    location: [
      { country: ["_id", "name"] },
      { state: ["_id", "name"] },
      { city: ["_id", "name"] },
      { district: ["_id", "name"] },
      "addressLine",
    ],
  },
  "isMainBranch",
  { serviceType: ["_id", "name"] },
  { businessProductTypes: ["_id", "name"] },
];

export const fragmentUpdateBranch: GenFields<Branch> = [
  "_id",
  "code",
  "name",
  "phoneNumber",

  { images: ["default", "medium", "small"] },
  "maxGuest",
  "introduction",
  "status",
  { operationTimes: ["day", { times: ["openingHours", "closeHours"] }] },
  {
    location: [
      { country: ["_id", "name"] },
      { state: ["_id", "name"] },
      { city: ["_id", "name"] },
      { district: ["_id", "name"] },
      "addressLine",
    ],
  },
  "isMainBranch",
  { serviceType: ["_id", "name"] },
  { businessProductTypes: ["_id", "name"] },
];
