import { useGetPrinterById } from "apiCaller";
import React, { useCallback, useEffect } from "react";
import { fragmentGetPrinterById } from "services/printer";

export const usePrinterById = (id: string) => {
  const [getPrinterById, { data }] = useGetPrinterById(fragmentGetPrinterById);

  useEffect(() => {
    invokeGettingPrinter();
  }, []);

  const invokeGettingPrinter = useCallback(() => {
    getPrinterById({
      variables: {
        id: id,
      },
    });
  }, []);

  return {
    PrinterById: data,
  };
};
