import React, { useState, useEffect } from "react";
import { FieldArray, FieldArrayRenderProps, useField } from "formik";
import FormControlErrorHelper from "common/styles/FormControlErrorHelper";
import SVG from "designs/SVG";

//languages
import { t } from "language";

//api
import { ToppingInput, Topping } from "apiCaller";

//locals
import { ElementWrapper, Other, Group } from "./styles";

interface IAddToppingProps {
  name: string;
  existedToppings?: Topping[];
}

const toppingItemDefault: ToppingInput = { name: "", price: 0, quantity: 0 };

const AddTopping: React.FC<IAddToppingProps> = ({ name, existedToppings }) => {
  const [field, meta, helpers] = useField<ToppingInput[]>(name);
  const { value } = field;
  const { setValue } = helpers;
  const existedToppingItems: ToppingInput[] = existedToppings as ToppingInput[];

  const handleCreateNewToppingItem = (): ToppingInput[] => {
    if (value && value?.length > 0) {
      return [...value, toppingItemDefault];
    }
    return [toppingItemDefault];
  };

  useEffect(() => {
    if (existedToppingItems) {
      value && setValue(existedToppingItems.concat([...value]));
    }
  }, []);

  return (
    <ElementWrapper className="flex flex-col gap-4">
      <FieldArray
        name={name}
        render={arrayHelperItem => (
          <Group.Wrapper>
            {value &&
              value.length > 0 &&
              value.map((item, indexItem) => (
                <Group.Wrapper key={indexItem}>
                  <ToppingItem
                    name={name}
                    indexItem={indexItem}
                    arrayHelperItem={arrayHelperItem}
                    initialValues={item}
                  />
                </Group.Wrapper>
              ))}
          </Group.Wrapper>
        )}
      />

      <Other.ButtonWrapper>
        <Other.Button
          outline
          onClick={e => {
            e.preventDefault();
            setValue(handleCreateNewToppingItem());
          }}
        >
          {t("product.product-list.add-topping")}
        </Other.Button>
      </Other.ButtonWrapper>
    </ElementWrapper>
  );
};

export default AddTopping;

interface IToppingItemProps {
  name: string;
  initialValues?: ToppingInput;
  indexItem: number;
  arrayHelperItem?: FieldArrayRenderProps;
}

export const ToppingItem: React.FC<IToppingItemProps> = ({
  name,
  indexItem,
  arrayHelperItem,
  initialValues,
}) => {
  const [, meta] = useField(`${name}.${indexItem}.toppings`);
  console.log(meta);
  const isError: boolean = !!meta.error;

  return (
    <ElementWrapper className="w-full flex justify-between mb-1">
      <Group.Right>
        <SVG name="product/humburger" height={24} width={24} />
        <Group.InputsWrapper>
          <Group.Input
            label={t("product.product-list.topping-name")}
            name={`${name}.${indexItem}.name`}
            placeholder={t("product.product-list.enter-topping-name")}
            value={initialValues && initialValues.name}
          />
          <Group.Input
            label={t("product.product-list.topping-price")}
            name={`${name}.${indexItem}.price`}
            placeholder={t("product.product-list.enter-price")}
            type="number"
            value={initialValues && initialValues.price}
          />
          <Group.Input
            label={t("product.product-list.topping-quantity")}
            name={`${name}.${indexItem}.quantity`}
            placeholder={t("product.product-list.enter-price")}
            type="number"
            value={initialValues && initialValues.quantity}
          />
          {isError && (
            <FormControlErrorHelper>{meta.error}</FormControlErrorHelper>
          )}
        </Group.InputsWrapper>
      </Group.Right>
      <SVG
        name="product/cancel"
        height={24}
        width={24}
        className="flex-shrink"
        onClick={() => arrayHelperItem?.remove(indexItem)}
      />
    </ElementWrapper>
  );
};
