import {
  Box,
  Code,
  LeftSide,
  Name,
  OrderServiceItemContainer,
  Phone,
  RightSide,
  Time,
  Status,
} from "./styles";
import { t } from "language";

interface OrderServiceItemProps {
  active?: boolean;
  onClick?: (value: any) => void;
  data: {
    code: string;
    name: string;
    phone: string;
    timeBooking: string;
    status: "new" | "cancel" | "complete";
  };
}
// dispatch(item.code) ->
const OrderServiceItem: React.FC<OrderServiceItemProps> = props => {
  const { active, onClick, data } = props;
  return (
    <OrderServiceItemContainer onClick={onClick} active={active}>
      <LeftSide>
        <Code>{data.code}</Code>
        <Name>{data.name}</Name>
        <Time>
          {t("orderService.booking-time")} {data.timeBooking}
        </Time>
        <Phone>{data.phone}</Phone>
      </LeftSide>
      <RightSide>
        <Box>
          <Status status={data.status}>{data.status}</Status>
        </Box>
      </RightSide>
    </OrderServiceItemContainer>
  );
};

export default OrderServiceItem;
