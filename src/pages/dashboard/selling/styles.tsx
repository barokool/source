import styled from "styled-components/macro";
import tw from "twin.macro";
import SearchBoxTable from "components/SearchBoxTable";

export const SellingLayoutContainer = styled.div`
  ${tw`py-1.5 px-2`}
`;

export const HeaderContainer = styled.div`
  ${tw`flex w-full justify-between items-center`}
`;

export const LeftHeaderContainer = styled.div`
  ${tw`flex w-full gap-x-3  justify-between items-center`}
`;

export const RightHeaderContainer = styled.div`
  ${tw`w-max flex justify-between items-center`}
`;

export const RightHeaderItem = {
  Container: styled.div`
    ${tw`flex items-center`}
  `,
  Content: styled.div`
    ${tw`text-16 text-black font-semibold`}
  `,
};

export const ChangePageButton = styled.button<{ active?: boolean }>`
  ${tw`px-3.5 py-2 text-black text-14 rounded-md w-30`}
  ${({ active }) => (active ? tw`bg-background ` : tw`bg-transparent `)}
`;

export const SearchBox = styled(SearchBoxTable)`
  ${tw`w-full`}
`;

export const PageSelectedContainer = styled.div`
  ${tw`mt-2 pb-10`}
`;
