import styled from "styled-components/macro";
import tw from "twin.macro";
import _Button from "designs/Button";

export const StoreSectionContainer = styled.div`
  ${tw`  `}
`;

export const HeaderContainer = styled.div`
  ${tw`flex justify-between`}
`;

export const LeftHeaderContainer = styled.div`
  ${tw`w-2/3 grid gap-y-0.5`}
`;

export const StoreName = styled.h1`
  ${tw`text-black text-16 font-bold`}
`;

export const StoreAddress = styled.p`
  ${tw`text-body-color text-12`}
`;

export const RightHeaderContainer = styled.div`
  ${tw`w-1/3`}
`;

export const RightCornerButton = styled(_Button)`
  ${tw`px-3 py-0.5  border-primary border-2 text-primary w-max float-right`}
`;

export const BodyContainer = styled.div`
  ${tw`w-full`}
`;

export const StoreImageContainer = styled.div`
  ${tw`my-2 `}
`;
