import styled from "styled-components/macro";
import tw from "twin.macro";
import _SearchBox from "components/SearchBoxTable";
import _Button from "designs/Button";

export const TableTypeContainer = styled.div`
  ${tw`  `}
`;

export const ActionWrapper = styled.div`
  ${tw`flex flex-col phone:flex-row justify-between mb-4`}
`;

export const SearchBox = styled(_SearchBox)`
  ${tw`w-full  phone:w-55 phone:mb-0`}
`;

export const Avatar = styled.img`
  ${tw`w-4 h-4 rounded-full`}
`;

export const AddButton = styled(_Button)`
  ${tw`px-3.5 py-[12.5px] font-medium bg-secondary text-white w-max`}
`;
