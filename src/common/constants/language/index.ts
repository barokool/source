import { IItemLanguage } from "typings";

export const LISTCOUNTRY: IItemLanguage[] = [
  {
    id: "1",
    name: "Eng",
    phonePrefix: "+01",
    lng: "en",
    flag: "register/american-flag",
  },
  {
    id: "2",
    name: "Ger",
    phonePrefix: "+49",
    lng: "de",
    flag: "register/german-flag",
  },
  {
    id: "3",
    name: "Vie",
    phonePrefix: "+84",
    lng: "vi",
    flag: "register/vietnam-flag",
  },
];
