import { useField } from "formik";
import CheckboxIcon from "icons/Checkbox";
import { useEffect } from "react";
import { CheckboxButton, GroupRadioContainer } from "./styles";

interface IGroupRadioProps {
  content: string;
  isChecked: boolean;
  onSelect: (isChecked: boolean) => void;
  className?: string;
  name?: string;
  id?: string;
}

const GroupRadioV3: React.FC<IGroupRadioProps> = ({
  content,
  isChecked,
  className,
  onSelect,
  name,
  id,
}) => {
  return (
    <label
      className={`flex gap-1 items-center hover:cursor-pointer + ${className}`}
      onClick={() => {
        onSelect(!isChecked);
      }}
      htmlFor={`${id}`}
    >
      <div className="relative w-1.5 h-1.5 rounded-full flex items-center justify-center  border-2 border-primary">
        <CheckboxButton checked={isChecked} />
      </div>
      {content}
    </label>
  );
};

export default GroupRadioV3;
