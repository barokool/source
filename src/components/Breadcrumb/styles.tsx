import styled from "styled-components/macro";
import tw from "twin.macro";

export const BreadcrumbContainer = styled.div`
  ${tw`w-full`}
`;

export const Container = styled.div`
  ${tw`flex flex-row items-center`}
`;

export const ItemWrapper = styled.div<{ lastItem: boolean }>`
  ${tw`text-14 text-neutral-1`}
  ${({ lastItem }) =>
    lastItem ? tw`text-opacity-100 font-regular` : tw`text-neutral-2`}
`;

export const Item = styled.p`
  ${tw`cursor-default`}
`;
