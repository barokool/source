import React, { useEffect, useState } from "react";
import * as yup from "yup";
import { isValidPhoneNumber } from "libphonenumber-js";
import { Formik } from "formik";
import { toast } from "react-toastify";

import { forceTextInputEnterNumber } from "common/functions/condition/forceTextInputEnterNumber";
import { PATH } from "common/constants/routes";
import Input from "designs/InputV2";
import Select from "designs/Select";
import MultipleSelect from "designs/MultipleSelect";

//hooks
import { useRedirect } from "hooks/useRedirect";
import { useServiceTypes } from "hooks/useServiceTypes";
import useAuth from "hooks/useAuth";
import { useBusinessProductType } from "hooks/useBusinessProductType";

//language
import { t } from "language";

//api
import {
  BusinessProductType,
  City,
  Country,
  CreateManagerRequestInput,
  District,
  ServiceType,
  State,
  useCreateManagerRequest,
  useGetAllCity,
  useGetAllCountry,
  useGetAllDistrict,
  useGetAllState,
} from "apiCaller";
import { fragmentCreateManagerRequest } from "services/managerRequest";

//locals
import {
  RegisterContainer,
  Heading,
  Form,
  ButtonWrapper,
  Button,
  FormContainer,
  ElementWrapper,
  RegisterLabel,
  RegisterContent,
} from "./styles";

import {
  fragmentGetAllCities,
  fragmentGetAllCountry,
  fragmentGetAllDistricts,
  fragmentGetAllStates,
} from "services/location";
import AuthLayout from "layouts/Auth";
import InputPhone from "designs/InputPhone";

interface IRegisterProps {}

interface IFormValue {
  nameStore: string;
  phoneNumber: string;
  email: string;
  country: string;
  state: string;
  city: string;
  district: string;
  serviceType: string;
  businessProductType: string[];
  password: string;
  confirmPassword: string;
}

const Register: React.FC<IRegisterProps> = props => {
  const redirect = useRedirect();

  // API
  const [getAllCountry, { data: allCountryData, error: countryError }] =
    useGetAllCountry(fragmentGetAllCountry);
  const [getAllCity, { data: allCityData }] =
    useGetAllCity(fragmentGetAllCities);
  const [getAllState, { data: allStateData }] =
    useGetAllState(fragmentGetAllStates);
  const [getAllDistrict, { data: allDistrictData }] = useGetAllDistrict(
    fragmentGetAllDistricts,
  );
  const [createManagerRequest, { data, loading, error }] =
    useCreateManagerRequest(fragmentCreateManagerRequest);

  //useState
  const [listBusinessProductTypeSelected, setListBusinessProductTypeSelected] =
    useState<BusinessProductType[]>([]);

  const [listCountryState, setListCountryState] = useState<Country[]>([]);
  const [listCity, setListCity] = useState<City[]>([]);
  const [listState, setListState] = useState<State[]>([]);
  const [listDistrict, setListDistrict] = useState<District[]>([]);

  const [countrySelected, setCountrySelected] = useState<Country | null>(null);
  const [citySelected, setCitySelected] = useState<City | null>(null);
  const [stateSelected, setStateSelected] = useState<State | null>(null);
  const [districtSelected, setDistrictSelected] = useState<District | null>(
    null,
  );

  const [serviceTypeSelected, setServiceTypeSelected] =
    useState<ServiceType | null>(null);

  const [initialValues] = useState<IFormValue>({
    nameStore: "",
    phoneNumber: "",
    email: "",
    country: "",
    state: "",
    city: "",
    district: "",
    serviceType: "",
    businessProductType: [],
    password: "",
    confirmPassword: "",
  });

  const { isAuth } = useAuth();

  //getAllState
  useEffect(() => {
    invokegetAllCountryAPI();
  }, []);

  useEffect(() => {
    allCountryData &&
      setListCountryState(allCountryData.getAllCountry.results || []);
  }, [allCountryData]);

  useEffect(() => {
    allStateData && setListState(allStateData.getAllState.results as State[]);
  }, [allStateData]);

  useEffect(() => {
    allDistrictData &&
      setListDistrict(allDistrictData.getAllDistrict.results as District[]);
  }, [allDistrictData]);

  useEffect(() => {
    allCityData && setListCity(allCityData.getAllCity.results as City[]);
  }, [allCityData]);

  useEffect(() => {
    countrySelected && invoketGetAllStatesAPI();
  }, [countrySelected]);

  useEffect(() => {
    stateSelected && invokeGetAllDistrictSAPI();
  }, [stateSelected]);

  useEffect(() => {
    districtSelected && invokeGetAllCitiAPI();
  }, [districtSelected]);

  useEffect(() => {
    if (data) {
      toast.success(t("register.register-success"));
      redirect(PATH.AUTH.LOGIN);
    }
  }, [data]);

  useEffect(() => {
    error && toast.error(error.message);
  }, [error]);

  useEffect(() => {
    if (isAuth) redirect(PATH.DASHBOARD);
  }, []);

  const invokegetAllCountryAPI = () => {
    getAllCountry({ variables: {} });
  };

  const invoketGetAllStatesAPI = () => {
    getAllState({
      variables: {
        filter: {
          country: countrySelected?._id || "",
        },
      },
    });
  };

  const invokeGetAllDistrictSAPI = () => {
    getAllDistrict({
      variables: {
        filter: { state: stateSelected?._id || "" },
      },
    });
  };

  const invokeGetAllCitiAPI = () => {
    getAllCity({
      variables: {
        filter: {
          district: districtSelected?._id || "",
        },
      },
    });
  };

  const { ServiceType } = useServiceTypes();
  const { productType } = useBusinessProductType();
  const { results } = ServiceType?.getAllServiceType || {};

  const validationSchema = yup
    .object()
    .shape<{ [key in keyof IFormValue]: any }>({
      phoneNumber: yup
        .string()
        .required(t("login.please-enter-phone-number"))
        .test(
          "is-valid",
          t("login.error-message.invalid-phone-number"),
          value => isValidPhoneNumber(`${value}`),
        ),
      country: yup.string().required(t("register.please-select-country")),
      state: yup.string().required("register.please-select-state"),
      district: yup.string().required(t("register.please-select-district")),
      city: yup.string().required(t("register.please-select-city")),
      nameStore: yup.string().required(t("register.please-enter-store-name")),
      password: yup.string().required(t("register.please-enter-password")),
      email: yup.string().required(t("register.please-enter-email")),
      confirmPassword: yup
        .string()
        .required(t("register.please-confirm-password"))
        .when("password", {
          is: (val: string) => (val && val.length > 0 ? true : false),
          then: yup
            .string()
            .oneOf(
              [yup.ref("password")],
              t("register.passwords-are-not-match"),
            ),
        }),

      serviceType: yup
        .string()
        .required(t("register.please-select-service-type")),
      businessProductType: yup
        .string()
        .required(t("register.please-select-business-product-type")),
    });

  const onHandleSubmit = (values: IFormValue) => {
    let input: CreateManagerRequestInput = {
      businessProductTypes: listBusinessProductTypeSelected.map(
        item => item._id,
      ) as string[],
      email: values.email,
      location: {
        country: countryAPISelected?._id as string,
        state: stateSelected?._id as string,
        district: districtSelected?._id as string,
        city: citySelected?._id as string,
      },
      password: values.password,
      nameBranch: values.nameStore,
      serviceType: serviceTypeSelected?._id as string,
      phoneNumber: values.phoneNumber,
    };
    createManagerRequest({
      variables: {
        input: input,
      },
    });
  };

  const onHandleCancel = () => {
    redirect(PATH.AUTH.LOGIN);
  };

  const handleCountryChange = (country: Country) => {
    if (country._id === countrySelected?._id) return;

    setCountrySelected(country);

    // Reset all state
    setListState([]);
    setListDistrict([]);
    setListCity([]);
    setStateSelected(null);
    setDistrictSelected(null);
    setCitySelected(null);
  };

  const handleStateChange = (state: State) => {
    setStateSelected(state);

    // Reset all state
    setListDistrict([]);
    setListCity([]);
    setDistrictSelected(null);
    setCitySelected(null);
  };

  const handleDistrictChange = (district: District) => {
    setDistrictSelected(district);

    // Reset all state
    setListCity([]);
    setCitySelected(null);
  };

  return (
    <AuthLayout>
      <RegisterContainer>
        <RegisterContent>
          <Heading.Title>{t("register.be-our-partner")}</Heading.Title>
          <Formik
            validationSchema={validationSchema}
            initialValues={initialValues}
            onSubmit={onHandleSubmit}
          >
            <Form>
              <ElementWrapper>
                <FormContainer>
                  <Input name="nameStore" label={t("register.name")} required />
                  <div className="flex items-center gap-2">
                    <Select
                      name="country"
                      label={t("register.country")}
                      onSelect={country => {
                        handleCountryChange(country as Country);
                      }}
                      optionSelected={countrySelected}
                      options={listCountryState}
                      className="max-w-[150px] w-full"
                    />
                    <Select
                      name="state"
                      label={
                        countrySelected?.alpha2Code === "de"
                          ? t("register.state")
                          : t("register.state-selectedVietnam")
                      }
                      required
                      onSelect={state => handleStateChange(state as State)}
                      optionSelected={stateSelected}
                      options={listState}
                      className="max-w-[300px] w-full"
                      disabled={!countrySelected}
                    />
                  </div>
                  <InputPhone
                    name="phoneNumber"
                    label={t("login.phone-number")}
                    placeholder={t("login.enter-phone-number")}
                    required
                    className="flex gap-1"
                  />
                  <Select
                    name="district"
                    label={
                      countrySelected?.alpha2Code === "de"
                        ? t("register.district")
                        : t("register.district-selectedVietnam")
                    }
                    required
                    onSelect={district =>
                      handleDistrictChange(district as District)
                    }
                    optionSelected={districtSelected}
                    options={listDistrict}
                    disabled={!stateSelected}
                  />
                  <Input name="email" label={t("register.email")} required />
                  <Select
                    name="city"
                    label={
                      countrySelected?.alpha2Code === "de"
                        ? t("register.city")
                        : t("register.city-selectedVietnam")
                    }
                    required
                    onSelect={city => setCitySelected(city)}
                    optionSelected={citySelected}
                    options={listCity}
                    disabled={!districtSelected}
                  />
                  <Select
                    name="serviceType"
                    label={t("register.business-type")}
                    required
                    onSelect={value => setServiceTypeSelected(value)}
                    optionSelected={serviceTypeSelected}
                    options={results || []}
                    optionTarget="name"
                  />
                  <MultipleSelect
                    name="businessProductType"
                    label={t("companyRegister.business-product-type")}
                    required
                    onSelect={options =>
                      setListBusinessProductTypeSelected(options)
                    }
                    options={
                      productType?.getAllBusinessProductType?.results || []
                    }
                    listOptionsSelected={listBusinessProductTypeSelected}
                  />
                </FormContainer>
                <ElementWrapper>
                  <RegisterLabel>{t("register.register-user")}</RegisterLabel>
                  <FormContainer>
                    <Input
                      label={t("register.password")}
                      required
                      name="password"
                      type="password"
                    />
                    <Input
                      label={t("register.confirm-password")}
                      required
                      name="confirmPassword"
                      type="password"
                    />
                  </FormContainer>
                </ElementWrapper>
              </ElementWrapper>

              <ButtonWrapper>
                <Button type="reset" outline onClick={onHandleCancel}>
                  {t("common.cancel")}
                </Button>
                <Button primary type="submit" loading={loading}>
                  {t("register.register-user")}
                </Button>
              </ButtonWrapper>
            </Form>
          </Formik>
        </RegisterContent>
      </RegisterContainer>
    </AuthLayout>
  );
};

export default Register;
