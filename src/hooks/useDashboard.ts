import { useGetDashboard } from "apiCaller";
import React, { useCallback, useEffect } from "react";
import { fragmentGetDashBoard } from "services/dashboard";
import { useCurrentBranch } from "./useCurrentBranch";

export const useDashboard = () => {
  const [getDashBoard, { data }] = useGetDashboard(fragmentGetDashBoard);
  const { branchSelected } = useCurrentBranch();
  useEffect(() => {
    if (branchSelected) invokingGetDashboard();
  }, [branchSelected]);

  const invokingGetDashboard = useCallback(() => {
    getDashBoard({
      variables: {
        idBranch: branchSelected?._id as string,
      },
    });
  }, [branchSelected]);

  return {
    dataDashboard: data,
  };
};
