import React, { useEffect, useState } from "react";
import { Formik } from "formik";
import * as yup from "yup";
import Input from "designs/InputV2";
import Select from "designs/Select";
import Dialog from "components/Dialog";
import { DialogTitle } from "common/styles/Title";
import { toast } from "react-toastify";

//hooks
import { useTableType } from "hooks/useTableType";

//api
import {
  Table,
  TableInput,
  TableStatus,
  TableType,
  useCreateTable,
  useUpdateTable,
} from "apiCaller";
import { fragmentCreateTable, fragmentUpdateTable } from "services/table";

//languages
import { t } from "language";

//locals
import {
  TableListDialogContainer,
  Form,
  ButtonWrapper,
  Button,
  ElementWrapper,
} from "./styles";

interface ITableListDialogProps {
  ButtonMenu?: React.ReactElement;
  editField?: Table;
  title?: string;
  onClose?: () => void;
  open?: boolean;
  onSuccess?: () => void;
}

interface IFormValue {
  code: number;
  tableType: string;
  status?: TableStatus;
}

const formKey = (key: keyof IFormValue) => key;

const validationSchema = yup
  .object()
  .shape<{ [key in keyof IFormValue]: any }>({
    tableType: yup
      .string()
      .required(t("table.table-list.please-select-table-category")),
    code: yup.string().required(t("table.table-list.please-enter-code")),
  });

const TableListDialog: React.FC<ITableListDialogProps> = ({
  ButtonMenu,
  editField,
  title,
  onClose,
  onSuccess,
  open = false,
}) => {
  const [initialValues, setInitialValues] = useState<IFormValue>({
    code: 0,
    tableType: "",
    status: TableStatus.Empty,
  });

  const { listTableType } = useTableType();
  const [isOpen, setOpen] = useState<boolean>(open);
  const [tableTypeSelected, setTableTypeSelected] = useState<TableType | null>(
    null,
  );

  const [
    updateTable,
    {
      loading: updateTableLoading,
      error: updateTableErrors,
      data: updateTableData,
    },
  ] = useUpdateTable(fragmentUpdateTable);
  const [
    createTable,
    {
      loading: createTableLoading,
      error: createTableErrors,
      data: createTableData,
    },
  ] = useCreateTable(fragmentCreateTable);

  useEffect(() => {
    if (editField) {
      setTableTypeSelected(editField?.tableType);
      setInitialValues({
        code: editField.code as number,
        tableType: editField?.tableType?.name as string,
        status: editField?.status as TableStatus,
      });
    }
  }, [editField]);

  useEffect(() => {
    createTableErrors && toast.error(createTableErrors.message);
    updateTableErrors && toast.error(updateTableErrors.message);
  }, [createTableErrors, updateTableErrors]);

  useEffect(() => {
    if (updateTableData && !updateTableErrors) {
      toast.success(t("table.table-list.update-success"));
      onSuccess?.();
      onHandleClose();
    }
    if (createTableData && !createTableErrors) {
      toast.success(t("table.table-list.create-success"));
      onSuccess?.();
      onHandleClose();
    }
  }, [updateTableData, createTableData]);

  const handleSubmit = (values: IFormValue) => {
    const input: TableInput = {
      tableType: tableTypeSelected?._id as string,
      status: values.status,
      code: values.code,
    };
    if (editField) {
      updateTable({
        variables: {
          id: editField._id as string,
          input: input,
        },
      });
    } else {
      createTable({
        variables: {
          input: { ...input },
        },
      });
    }
  };

  const onHandleClose = () => {
    setOpen(false);
    onClose && onClose();
  };

  return (
    <>
      <ElementWrapper
        onClick={() => setOpen(!isOpen)}
        className="flex justify-end w-full"
      >
        {ButtonMenu}
      </ElementWrapper>

      <Dialog
        open={isOpen}
        onClose={onHandleClose}
        className="overflow-auto"
        size="md"
      >
        <TableListDialogContainer>
          <DialogTitle>{title}</DialogTitle>
          <Formik
            enableReinitialize
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={handleSubmit}
          >
            <Form>
              <Input
                name={formKey("code")}
                label={t("table.table-list.code")}
                type="number"
                placeholder={t("table.table-list.enter-code")}
                required
              />
              <Select
                optionSelected={tableTypeSelected}
                onSelect={values => setTableTypeSelected(values)}
                options={listTableType}
                name={formKey("tableType")}
                label={t("table.table-list.table-category")}
                placeholder={t("table.table-list.select-table-category")}
                //formtarget
              />
              <ButtonWrapper>
                <Button outline type="button" onClick={onHandleClose}>
                  {t("common.cancel")}
                </Button>
                <Button
                  type="submit"
                  primary
                  loading={createTableLoading || updateTableLoading}
                >
                  {t("common.accept")}
                </Button>
              </ButtonWrapper>
            </Form>
          </Formik>
        </TableListDialogContainer>
      </Dialog>
    </>
  );
};
export default TableListDialog;
