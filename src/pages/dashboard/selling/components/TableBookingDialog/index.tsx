import React, { useState } from "react";

//styles
import * as yup from "yup";
import Select from "designs/SimpleSelect";
import Input from "designs/InputV2";
import Dialog from "components/Dialog";
import { DialogTitle } from "common/styles/Title";

//hooks
import { Order, useCreateOrder } from "apiCaller";

//api
import { fragmentCreateOrder } from "services/order";

import {
  TableBookingDialogContainer,
  Button,
  ButtonWrapper,
  ElementWrapper,
  Form,
} from "./styles";

import { t } from "language";

interface ITableListDialogProps {
  ButtonMenu?: React.ReactElement;
  title?: string;
  onClose?: () => void;
  open?: boolean;
}

const TableBookingDialog: React.FC<ITableListDialogProps> = ({
  ButtonMenu,
  title,
  onClose,
  open = false,
}) => {
  const [isOpen, setOpen] = useState<boolean>(open);
  const [loading, setLoading] = useState<boolean>(false);

  const onHandleClose = () => {
    setOpen(false);
    onClose && onClose();
  };
  return (
    <>
      <ElementWrapper
        onClick={() => setOpen(!isOpen)}
        className="flex justify-end w-full"
      >
        {ButtonMenu}
      </ElementWrapper>

      <Dialog
        open={isOpen}
        onClose={onHandleClose}
        className="overflow-auto"
        size="md"
      >
        <TableBookingDialogContainer>
          <DialogTitle>{title}</DialogTitle>
          Hello World
          <ButtonWrapper>
            <Button outline type="button" onClick={onHandleClose}>
              {t("common.cancel")}
            </Button>
            <Button
              type="submit"
              primary
              loading={loading}
              onClick={() => {
                setOpen(false);
              }}
            >
              {t("common.accept")}
            </Button>
          </ButtonWrapper>
        </TableBookingDialogContainer>
      </Dialog>
    </>
  );
};
export default TableBookingDialog;
