import { useEffect, useRef, useState } from "react";
import ReactToPrint from "react-to-print";
import Barcode from "react-barcode";
import Dialog from "components/Dialog";

//hooks
import { useClickOutSide } from "hooks/useClickOutSide";
import { useLoading } from "hooks/useLoading";
import { useVirtual } from "react-virtual";

//api
import { useGetAllProduct } from "apiCaller";
import { fragmentGetAllProduct } from "services/product";

//locals
import { PrintIcon } from "./icons";
import { Button } from "../styles";
import { ProductBarcodeItem, RenderAllBarcodeContainer } from "./styles";

const TOTAL = 100;
const LOAD_DATA = "LOAD_DATA";

const GroupPrintDialog: React.FC<{
  productId: string;
  onClose: () => void;
}> = ({ productId, onClose }) => {
  const { elementRef, isVisible, setElementVisible } = useClickOutSide(false);
  const { startLoading, stopLoading } = useLoading();
  const [allBarcode, setAllBarcode] = useState<{ barcode: string[] }>();
  const [getAllProduct, { data, loading }] = useGetAllProduct(
    fragmentGetAllProduct,
  );
  const parentRef = useRef<HTMLDivElement>(null);
  const rowVirtualizer = useVirtual({
    size: TOTAL,
    parentRef,
  });
  const printRef = useRef<any>();

  useEffect(() => {
    invokeGetProductBarcode();
  }, []);

  useEffect(() => {
    if (loading) {
      startLoading(LOAD_DATA);
    } else {
      stopLoading(LOAD_DATA);
    }
  }, [loading]);

  useEffect(() => {
    data &&
      setAllBarcode({
        barcode: data?.getAllProduct.results?.map(
          item => item.barcode as string,
        ) as string[],
      });
  }, [data]);

  const invokeGetProductBarcode = () => {
    getAllProduct({ variables: {} });
  };

  return (
    <>
      <Button onClick={() => setElementVisible(!isVisible)} outline>
        Print barcode
      </Button>
      <Dialog className="pb-2" size="lg" open={isVisible} onClose={() => {}}>
        <div ref={elementRef}>
          <Dialog.Content className="bg-white">
            <div className="flex justify-end w-full py-1">
              <ReactToPrint
                content={() => printRef.current as any}
                trigger={() => (
                  <Button>
                    <PrintIcon />
                    Print all
                  </Button>
                )}
              />
            </div>
            <div className="w-full text-md">
              <div className="text-lg flex justify-between font-bold border-b border-solid border-neutral-4">
                <p className="p-1">{`Order number (${TOTAL} items)`}</p>
                <p className="p-1">Status</p>
              </div>
              <div
                className="border overflow-y-auto h-40 border-solid border-neutral-4"
                ref={parentRef}
              >
                <div
                  style={{
                    height: rowVirtualizer.totalSize,
                    width: "100%",
                    position: "relative",
                  }}
                >
                  {rowVirtualizer.virtualItems.map(
                    ({ index, start, measureRef }) => {
                      return (
                        <div
                          key={index}
                          ref={measureRef}
                          style={{
                            position: "absolute",
                            top: 0,
                            left: 0,
                            width: "100%",
                            transform: `translateY(${start}px)`,
                          }}
                        >
                          <div className="w-full hover:bg-neutral-5 flex justify-between px-1 border-b border-solid border-neutral-3.5">
                            <p>{allBarcode?.barcode[index]}</p>
                          </div>
                        </div>
                      );
                    },
                  )}
                </div>
              </div>
            </div>
          </Dialog.Content>
          <div className="hidden">
            <div
              ref={ref => {
                printRef.current = ref;
              }}
            >
              <RenderAllBarcode listBarcode={allBarcode?.barcode as string[]} />
            </div>
          </div>
        </div>
      </Dialog>
    </>
  );
};

export default GroupPrintDialog;

const RenderAllBarcode: React.FC<{
  listBarcode: string[];
}> = ({ listBarcode }) => {
  return (
    <RenderAllBarcodeContainer>
      {listBarcode.map((value, index) => (
        <ProductBarcodeItem key={index}>
          <Barcode value={value} width={1.3} fontSize={16} />
        </ProductBarcodeItem>
      ))}
    </RenderAllBarcodeContainer>
  );
};
