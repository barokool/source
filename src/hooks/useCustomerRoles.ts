import { useState, useCallback, useEffect } from "react";
import { Role, RoleEnum, useGetAllRole } from "apiCaller";
import { fragmentGetAllRole } from "services/role";

import useAuth from "./useAuth";

export const useCustomerRoles = () => {
  const { accountInfo } = useAuth();
  const [getAllRoles, { data, loading, called }] =
    useGetAllRole(fragmentGetAllRole);

  useEffect(() => {
    invokeGetAllRoles();
  }, []);

  const invokeGetAllRoles = useCallback(async () => {
    await getAllRoles({
      variables: {
        filterRole: {
          idBranch: accountInfo?.userInfo?.branches?.[0]?._id as string,
          name: "CUSTOMER" as RoleEnum,
        },
      },
    });
  }, []);

  return {
    listCustomerRole: data,
    invokeGetAllRoles,
  };
};
