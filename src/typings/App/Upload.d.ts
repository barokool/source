import { IImage } from "typings";

export type IUpload = File;

export interface ICustomUploadInput {
  type: string;
  url: IImage;
  file: IUpload;
}

export type IBase64Image = string;

export interface IURLCustomSizeImages {
  default: string;
  medium: string;
  small: string;
}
