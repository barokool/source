import { Form, Formik } from "formik";
import { useEffect, useState } from "react";
import { t } from "language";
import * as yup from "yup";
import { toast } from "react-toastify";

import {
  AddPrintDialogContainer,
  Button,
  ButtonWrapper,
  ElementWrapper,
  FormGroup,
  TitleLv1,
  TitleLv2,
} from "./styles";

import Dialog from "components/Dialog";
import { DialogTitle } from "common/styles/Title";
import Input from "designs/InputV2";
import GroupRadioV3 from "designs/GroupRadioButtonV3";
import { cleanObject } from "common/functions/object/cleanObject";

import {
  Printer,
  PrinterConfig,
  PrinterConfigBillMethod,
  PrinterFontSize,
  PrinterInput,
  PrinterMethod,
  PrinterOrderInfoInput,
  useCreatePrinter,
  useUpdatePrinter,
} from "apiCaller";
import useAuth from "hooks/useAuth";
import { fragmentCreatePrinter } from "services/printer";

interface IAddPrintDialogProps {
  ButtonMenu?: React.ReactElement;
  editField?: Printer;
  title?: string;
  onClose?: () => void;
  onSuccess?: () => void;
  open?: boolean;
}

interface IFormValue {
  ipAddress: string;
  name: string;
  config?: PrinterConfig;
  methodPrint?: PrinterMethod;
  numberOfPrints?: number;
  methodBillPrint?: PrinterConfigBillMethod;
  fontSize?: PrinterFontSize;
  title?: string;
  branchName?: string;
  branchLocation?: string;
  customerName?: string;
  customerLocation?: string;
  customerPhoneNumber?: string;
  dateOfPayment?: string;
  branchNote?: string;
  footerContent?: string;
  sku?: boolean;
  discountProduct?: boolean;
  comboDetail?: boolean;
  discountOrder?: boolean;
  exCessCash?: boolean;
  shippingPrice?: boolean;
  tax?: boolean;
  totalAmount?: boolean;
  totalAmountReceived?: boolean;
  totalQuantity?: boolean;
  idBranch?: string;
}
interface ObjectInfo {
  name?: string;
  value?: string;
  isCheck?: boolean;
  id?: string;
}

interface CustomObject {
  id?: string;
  value?: string;
}

const validationSchema = yup
  .object()
  .shape<{ [key in keyof IFormValue]: any }>({
    ipAddress: yup
      .string()
      .required(t("configuration.general-settings.ip-address")),
    name: yup
      .string()
      .required(t("configuration.general-settings.printer-name")),
  });

const AddPrintDialog: React.FC<IAddPrintDialogProps> = ({
  ButtonMenu,
  editField,
  title,
  onClose,
  onSuccess,
  open = false,
}) => {
  const { accountInfo } = useAuth();
  const [isOpen, setOpen] = useState<boolean>(open);
  const [loading, setLoading] = useState<boolean>(false);
  const [
    createPrinter,
    { data: createData, error: createError, loading: createLoading },
  ] = useCreatePrinter(fragmentCreatePrinter);
  const [initialValues, setInitialValues] = useState<IFormValue>({
    ipAddress: "",
    name: "",
  });

  const [listProductInfo, setListProductInfo] = useState<CustomObject[]>([]);
  const [listOrderInfo, setListOrderInfo] = useState<CustomObject[]>([]);
  const [printerConfigSelected, setPrinterConfigSelected] =
    useState<string>("");
  const [methodPrintSelected, setMethodPrintSelected] = useState<string>("");
  const [printBillSelected, setPrintBillSelected] = useState<string>("");
  const [fontSizeSelected, setFontSizeSelected] = useState<string>("");

  const [
    updatePrinter,
    { data: dataPrinter, error: errorPrinter, loading: updateLoading },
  ] = useUpdatePrinter();

  useEffect(() => {
    setListProductInfo(
      productInfo.filter(item =>
        Object.keys(
          cleanObject(
            { ...editField?.productInfo },
            { isRemoveFalseValue: true },
          ),
        ).find(item2 => item2 === item.value),
      ),
    );

    const fillOrderInfo = Object.keys(
      cleanObject({ ...editField?.orderInfo }, { isRemoveFalseValue: true }),
    );

    let newListOrderInfo = orderInfo.filter(value =>
      fillOrderInfo.find(item => item === value.value),
    );

    setListOrderInfo(newListOrderInfo);
    setPrinterConfigSelected(editField?.config as string);
    setPrintBillSelected(editField?.configBill?.method as string);
    setFontSizeSelected(editField?.fontSize as string);
    setMethodPrintSelected(editField?.method as string);

    if (editField) {
      setInitialValues({
        ipAddress: editField?.ipAddress as string,
        name: editField?.name as string,
        branchLocation: editField?.branchLocation as string,
        branchName: editField?.branchName as string,
        branchNote: editField?.branchNote as string,
        numberOfPrints: editField?.configBill?.numberOfPrints as number,
        customerLocation: editField?.customerLocation as string,
        customerName: editField?.customerName as string,
        customerPhoneNumber: editField?.customerPhoneNumber as string,
        dateOfPayment: editField?.dateOfPayment as string,
        footerContent: editField?.footerContent as string,
        title: editField?.title as string,
      });
    }
  }, [editField]);

  useEffect(() => {
    if (createData && !createError) {
      toast.success(t("configuration.general-settings.add-notify"));
      onSuccess && onSuccess();
      handleClose();
    }
    if (!createData && createError) {
      toast.error(t("configuration.general-settings.add-error"));
      handleClose();
    }
  }, [createData, createError]);

  const formKey = (key: keyof IFormValue) => key;
  const handleSubmit = (value: IFormValue) => {
    const input: PrinterInput = {
      branch: accountInfo?.userInfo?.branches?.[0]?._id as string,
      branchLocation: value.branchLocation,
      branchName: value.branchName,
      branchNote: value.branchNote,
      config: printerConfigSelected as PrinterConfig,
      configBill: {
        method: printBillSelected as PrinterConfigBillMethod,
        numberOfPrints: value.numberOfPrints,
      },
      customerLocation: value.customerLocation,
      customerName: value.customerName,
      customerPhoneNumber: value.customerPhoneNumber,
      dateOfPayment: value.dateOfPayment,
      fontSize: fontSizeSelected as PrinterFontSize,
      footerContent: value.footerContent,
      ipAddress: value.ipAddress,
      method: methodPrintSelected as PrinterMethod,
      name: value.name,
      orderInfo: {
        totalQuantity: listOrderInfo.some(
          item => item.value === "totalQuantity",
        ),
        shippingPrice: listOrderInfo.some(
          item => item.value === "shippingPrice",
        ),
        discountOrder: listOrderInfo.some(
          item => item.value === "discountOrder",
        ),
        totalAmount: listOrderInfo.some(item => item.value === "totalAmount"),
        tax: listOrderInfo.some(item => item.value === "tax"),
        totalAmountReceived: listOrderInfo.some(
          item => item.value === "totalAmountReceived",
        ),
        excessCash: listOrderInfo.some(item => item.value === "excessCash"),
      },
      productInfo: {
        comboDetail: listProductInfo.some(item => item.value === "comboDetail"),
        discountProduct: listProductInfo.some(
          item => item.value === "discountProduct",
        ),
        sku: listProductInfo.some(item => item.value === "sku"),
      },
      title: value.title,
    };

    if (editField) {
      updatePrinter({
        variables: {
          id: editField?._id as string,
          input: input,
        },
      });
    } else
      createPrinter({
        variables: {
          input: input,
        },
      });
  };

  const FormGroupWrapper: React.FC<
    {
      title: string;
      col?: number;
    } & React.HTMLAttributes<HTMLDivElement>
  > = ({ title, children, col = 0, className }) => {
    return (
      <FormGroup>
        <TitleLv2>{title}</TitleLv2>
        <ElementWrapper
          className={`mt-3 ${className} ${
            col !== 0 && `grid grid-cols-${col} gap-y-4 gap-x-2`
          } `}
        >
          {children}
        </ElementWrapper>
      </FormGroup>
    );
  };

  const handleClose = () => {
    setOpen(false);
    onClose && onClose();
  };

  useEffect(() => {
    if (dataPrinter && !errorPrinter) {
      toast.success(t("common.edit-success"));
      onSuccess && onSuccess();
      handleClose();
    }

    if (errorPrinter && !dataPrinter) {
      toast.error(t("common.edit-fail"));
      onSuccess && onSuccess();
      handleClose();
    }
  }, [dataPrinter, errorPrinter]);

  return (
    <>
      <ElementWrapper onClick={() => setOpen(!isOpen)}>
        {ButtonMenu}
      </ElementWrapper>

      <Dialog open={isOpen} onClose={handleClose} size="lg">
        <AddPrintDialogContainer>
          <DialogTitle>{title}</DialogTitle>
          <Formik
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={handleSubmit}
            enableReinitialize
          >
            <Form>
              <ElementWrapper>
                <FormGroupWrapper
                  title={t("configuration.general-settings.general-info")}
                  col={2}
                >
                  <Input
                    name={formKey("ipAddress")}
                    label={t("configuration.general-settings.ip-address")}
                    type="text"
                    placeholder={t(
                      "configuration.general-settings.enter-ip-address",
                    )}
                    required
                  />
                  <Input
                    name={formKey("name")}
                    label={t("configuration.general-settings.printer-name")}
                    type="text"
                    placeholder="USUM Software"
                  />
                </FormGroupWrapper>
                <FormGroupWrapper
                  title={t("configuration.general-settings.printer-settings")}
                  col={2}
                >
                  {config.map((item, index) => {
                    return (
                      <GroupRadioV3
                        key={index}
                        content={item.name as string}
                        isChecked={printerConfigSelected === item.value}
                        onSelect={checked => {
                          if (checked) {
                            setPrinterConfigSelected(item.value as string);
                          } else {
                            setPrinterConfigSelected("");
                          }
                        }}
                      />
                    );
                  })}
                </FormGroupWrapper>
                <FormGroupWrapper
                  title={t(
                    "configuration.general-settings.select-default-printting-method",
                  )}
                >
                  {methodPrints.map((item, index) => {
                    return (
                      <GroupRadioV3
                        key={index}
                        content={item.name as string}
                        isChecked={methodPrintSelected === item.value}
                        onSelect={checked => {
                          if (checked) {
                            setMethodPrintSelected(item.value as string);
                          } else {
                            setMethodPrintSelected("");
                          }
                        }}
                      />
                    );
                  })}
                </FormGroupWrapper>
                <FormGroupWrapper
                  title={t(
                    "configuration.general-settings.set-printing-invoices",
                  )}
                  col={2}
                >
                  <div className="col-span-2">
                    {printBill.map((item, index) => {
                      return (
                        <GroupRadioV3
                          content={item.name as string}
                          isChecked={printBillSelected === item.value}
                          onSelect={checked => {
                            if (checked) {
                              setPrintBillSelected(item.value as string);
                            } else setPrintBillSelected("");
                          }}
                        />
                      );
                    })}
                  </div>

                  <Input
                    name={formKey("numberOfPrints")}
                    label={t(
                      "configuration.general-settings.number-of-printing-record",
                    )}
                    type="number"
                    placeholder=""
                  />
                </FormGroupWrapper>
              </ElementWrapper>
              <ElementWrapper>
                <TitleLv1>
                  {t("configuration.general-settings.edit-printting-form")}
                </TitleLv1>
                <FormGroupWrapper
                  title={t(
                    "configuration.general-settings.general-information",
                  )}
                  col={2}
                >
                  <ElementWrapper className="col-span-2">
                    {fontSize.map((item, index) => {
                      return (
                        <GroupRadioV3
                          key={index}
                          content={item.name as string}
                          isChecked={fontSizeSelected === item.value}
                          onSelect={checked => {
                            if (checked) {
                              setFontSizeSelected(item.value as string);
                            } else setFontSizeSelected("");
                          }}
                        />
                      );
                    })}
                  </ElementWrapper>
                  <Input
                    name={formKey("title")}
                    label={t("configuration.general-settings.title")}
                    type="text"
                    placeholder={t(
                      "configuration.general-settings.enter-the-invoice-title-here",
                    )}
                    required
                  />
                  <Input
                    name={formKey("branchName")}
                    label={t("configuration.general-settings.store-name")}
                    type="text"
                    placeholder="USUM Software"
                  />
                  <Input
                    name={formKey("branchLocation")}
                    label={t("configuration.general-settings.address")}
                    type="text"
                    placeholder={t(
                      "configuration.general-settings.enter-address",
                    )}
                  />
                </FormGroupWrapper>
                <FormGroupWrapper
                  title={t("configuration.general-settings.order-information")}
                  col={2}
                >
                  <Input
                    name={formKey("customerName")}
                    label={t("configuration.general-settings.customer-name")}
                    type="text"
                    placeholder={t(
                      "configuration.general-settings.enter-customer-name",
                    )}
                    required
                  />
                  <Input
                    name={formKey("customerLocation")}
                    label={t("configuration.general-settings.address")}
                    type="text"
                    placeholder={t(
                      "configuration.general-settings.enter-address",
                    )}
                  />
                  <Input
                    name={formKey("customerPhoneNumber")}
                    label={t("configuration.general-settings.phone-number")}
                    type="text"
                    placeholder={t(
                      "configuration.general-settings.enter-customer-name",
                    )}
                  />
                  <Input
                    name={formKey("dateOfPayment")}
                    label={t(
                      "configuration.general-settings.payment-date-and-time",
                    )}
                    type="text"
                    placeholder={""}
                    required
                  />
                  <Input
                    name={formKey("branchNote")}
                    label={t("configuration.general-settings.store-notes")}
                    type="text"
                    placeholder={""}
                  />
                  <Input
                    name={formKey("footerContent")}
                    label={t("configuration.general-settings.content-of-bills")}
                    type="text"
                    placeholder=""
                  />
                </FormGroupWrapper>
                <FormGroupWrapper
                  title={t(
                    "configuration.general-settings.product-information",
                  )}
                >
                  <>
                    {productInfo.map((item, index) => {
                      return (
                        <>
                          <GroupRadioV3
                            key={item.id}
                            id={item.value as string}
                            content={item.name as string}
                            isChecked={listProductInfo.some(
                              value => value.value === item.value,
                            )}
                            onSelect={checked => {
                              if (checked) {
                                if (listProductInfo?.length == 0) {
                                  const copyItem: CustomObject = {
                                    id: item.id,
                                    value: item.value,
                                  };
                                  listProductInfo.push(copyItem);
                                }
                                if (listProductInfo?.length > 0) {
                                  setListProductInfo([
                                    ...listProductInfo,
                                    { id: item.id, value: item.value },
                                  ]);
                                }
                              } else {
                                setListProductInfo(
                                  listProductInfo.filter(
                                    product => product.value !== item.value,
                                  ),
                                );
                              }
                            }}
                          />
                        </>
                      );
                    })}
                  </>
                </FormGroupWrapper>
                <FormGroupWrapper
                  title={t(
                    "configuration.general-settings.order-value-information",
                  )}
                >
                  {orderInfo.map((item, index) => {
                    return (
                      <GroupRadioV3
                        key={index}
                        id={item.value as string}
                        content={item.name as string}
                        isChecked={listOrderInfo.some(
                          value => value.value === item.value,
                        )}
                        onSelect={checked => {
                          if (checked) {
                            if (listOrderInfo.length == 0) {
                              let newValue: CustomObject = {
                                id: item.id,
                                value: item.value,
                              };
                              listOrderInfo.push(newValue);
                            }
                            if (listOrderInfo.length > 0) {
                              setListOrderInfo([
                                ...listOrderInfo,
                                { id: item.id, value: item.value },
                              ]);
                            }
                          } else {
                            setListOrderInfo(
                              listOrderInfo.filter(
                                value => value.value !== item.value,
                              ),
                            );
                          }
                        }}
                      />
                    );
                  })}
                </FormGroupWrapper>
              </ElementWrapper>
              <FormGroup>
                <ButtonWrapper>
                  <Button outline type="button" onClick={() => setOpen(false)}>
                    {t("common.cancel")}
                  </Button>
                  <Button type="submit" primary loading={loading}>
                    {t("common.accept")}
                  </Button>
                </ButtonWrapper>
              </FormGroup>
            </Form>
          </Formik>
        </AddPrintDialogContainer>
      </Dialog>
    </>
  );
};

export default AddPrintDialog;
const fontSize: ObjectInfo[] = [
  {
    name: t("configuration.general-settings.small-font-size"),
    id: "1",
    value: PrinterFontSize.Small,
  },
  {
    name: t("configuration.general-settings.medium-font-size"),
    id: "2",
    value: PrinterFontSize.Medium,
  },
  {
    name: t("configuration.general-settings.big-font-size"),
    id: "3",
    value: PrinterFontSize.Large,
  },
];

const printBill: ObjectInfo[] = [
  {
    name: t("configuration.general-settings.print-bill-when-create-order"),
    id: "1",
    value: PrinterConfigBillMethod.Printwhencreatingorder,
  },
  {
    name: t("configuration.general-settings.review-before-print"),
    id: "2",
    value: PrinterConfigBillMethod.Reviewbeforeprint,
  },
];
const methodPrints: ObjectInfo[] = [
  {
    name: t("configuration.general-settings.wifi-printting"),
    id: "1",
    value: PrinterMethod.Wifi,
  },
  {
    name: t("configuration.general-settings.print-by-a4-a5-printer"),
    id: "2",
    value: PrinterMethod.A4printer,
  },
  {
    name: t("configuration.general-settings.bluetooth-printing"),
    id: "3",
    value: PrinterMethod.Bluetooth,
  },
];
const config: ObjectInfo[] = [
  {
    name: t("configuration.general-settings.set-up-invoice-printing"),
    id: "1",
    value: PrinterConfig.Billprinting,
  },
  {
    name: t("configuration.general-settings.printer-stamps-labels"),
    id: "2",
    value: PrinterConfig.Stampprinting,
  },
];
const productInfo: ObjectInfo[] = [
  {
    name: t("configuration.general-settings.sku-code"),
    value: "sku",
    id: "1",
  },
  {
    name: t("configuration.general-settings.product-discount"),
    value: "discountProduct",
    id: "2",
  },
  {
    name: t("configuration.general-settings.reveal-combo-detail"),
    value: "comboDetail",
    id: "3",
  },
];

const formKeyOrder = (key: keyof PrinterOrderInfoInput) => key;
const orderInfo: ObjectInfo[] = [
  {
    id: "1",
    name: t("configuration.general-settings.total-quantity"),
    value: formKeyOrder("totalQuantity"),
  },
  {
    id: "2",
    name: t("configuration.general-settings.delivery-charge"),
    value: formKeyOrder("shippingPrice"),
  },
  {
    id: "3",
    name: t("configuration.general-settings.discount"),
    value: "discountOrder",
  },
  {
    id: "4",
    name: t("configuration.general-settings.order-value-information"),
    value: formKeyOrder("totalAmount"),
  },
  {
    id: "5",
    name: t("configuration.general-settings.tax"),
    value: formKeyOrder("tax"),
  },
  {
    id: "6",
    name: t("configuration.general-settings.totalAmountReceived"),
    value: formKeyOrder("totalAmountReceived"),
  },
  {
    id: "7",
    name: t("configuration.general-settings.excessCash"),
    value: formKeyOrder("excessCash"),
  },
];
