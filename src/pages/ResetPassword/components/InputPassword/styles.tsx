import styled from "styled-components/macro";
import tw from "twin.macro";
import _Button from "designs/Button";
import { Form as _Form } from "formik";

export const Form = styled(_Form)`
  ${tw`flex flex-col `}
`;

export const InputWrapper = styled.div`
  ${tw`flex flex-col gap-y-3`}
`;

export const Button = styled(_Button)`
  ${tw`w-full flex items-center justify-center text-16 font-medium text-white py-1 mt-4`}
`;

export const InputPasswordContainer = styled.div`
  ${tw``}
`;
