import {
  ChangeEvent,
  DetailedHTMLProps,
  TextareaHTMLAttributes,
  useEffect,
  useState,
} from "react";

import { useField } from "formik";
import FormControlLabel from "common/styles/FormControlLabel";
import FormControlErrorHelper from "common/styles/FormControlErrorHelper";

import { TextAreaWrapper, TextAreaContainer, Label } from "./styles";

interface IInputProps
  extends DetailedHTMLProps<
    TextareaHTMLAttributes<HTMLTextAreaElement>,
    HTMLTextAreaElement
  > {
  name: string;
  classNameInput?: string;
  error?: any;
  touched?: any;
  ref?: any;
  rows?: number;
  label: string;
  required?: boolean;
  value?: string;
  placeholder?: string;
  id?: string;
  onChangeValue?: (value: string | number) => void;
  small?: boolean;
  readonly onChange?: (e: ChangeEvent<HTMLTextAreaElement>) => void;
}

const TextArea: React.FC<IInputProps> = props => {
  const {
    className = "",
    classNameInput = "",
    error,
    ref,
    rows = 4,
    onChangeValue,
    onChange,
    label = "",
    required,
    touched,
    placeholder,
    id,
    value,
    ...restProps
  } = props;

  const [field, meta] = useField(props);

  useEffect(() => {
    onChangeValue && onChangeValue(field.value || "");
  }, [field.value]);
  const isError: boolean = !!meta.touched && !!meta.error;
  return (
    <TextAreaWrapper className={className}>
      <Label>
        {label && (
          <FormControlLabel required={required} isError={isError} htmlFor={id}>
            {label}
          </FormControlLabel>
        )}
      </Label>

      <TextAreaContainer
        error={isError}
        {...field}
        {...(restProps as any)}
        placeholder={placeholder}
      />
      {isError && <FormControlErrorHelper>{meta.error}</FormControlErrorHelper>}
    </TextAreaWrapper>
  );
};

export default TextArea;
