import React from "react";

//api
import { Order } from "apiCaller";

//languages
import { t } from "language";

//locals
import { OrderHistoryContainer } from "./styles";
import Section from "./Section";

interface IOrderHistoryProps {
  recentDelivery?: Order[];
  recentOrder?: Order[];
}

const OrderHistory: React.FC<IOrderHistoryProps> = props => {
  const { recentDelivery, recentOrder } = props;

  return (
    <OrderHistoryContainer>
      <Section
        title={t("overview.recent-delivery")}
        data={recentDelivery as Order[]}
      />
      <Section
        title={t("overview.recent-order")}
        data={recentOrder as Order[]}
      />
    </OrderHistoryContainer>
  );
};

export default OrderHistory;
