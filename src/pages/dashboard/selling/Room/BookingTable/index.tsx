import React from "react";

//api
import { OrderStatusEnum, OrderTypeEnum } from "apiCaller";

//redux
import { useSelector } from "react-redux";
import { IRootState } from "typings";

//locals
import { DeliveryTabContainer, NarrowerWrapper, WiderWrapper } from "./styles";
import DeliveryLeft from "./Left";
import DeliveryRight from "./Right";

interface IDeliveryTab {}

const DeliveryTab: React.FC<IDeliveryTab> = props => {
  const { orderSelected } = useSelector((state: IRootState) => state.order);
  return (
    <>
      {orderSelected?.status === OrderStatusEnum.Approving &&
      orderSelected.type === OrderTypeEnum.Delivery ? (
        <DeliveryTabContainer>
          <WiderWrapper>
            <DeliveryLeft />
          </WiderWrapper>
          <NarrowerWrapper>
            <DeliveryRight />
          </NarrowerWrapper>
        </DeliveryTabContainer>
      ) : (
        <DeliveryTabContainer>
          <NarrowerWrapper>
            <DeliveryLeft />
          </NarrowerWrapper>
          <WiderWrapper>
            <DeliveryRight />
          </WiderWrapper>
        </DeliveryTabContainer>
      )}
    </>
  );
};

export default DeliveryTab;
