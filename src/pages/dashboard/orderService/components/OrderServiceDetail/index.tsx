import {
  CustomerInfoBox,
  Line,
  OrderServiceDetailContainer,
  PayInfoBox,
  TableBookingInfoBox,
  Text,
  Title,
} from "./styles";
import { t } from "language";

interface OrderServiceDetailProps {}

const OrderServiceDetail: React.FC<OrderServiceDetailProps> = props => {
  const CustomerInfo = () => (
    <CustomerInfoBox>
      <Title>{t("orderService.customer-info.customer-info")}</Title>
      <Line>
        <Text>{t("orderService.customer-info.full-name")}</Text>
        <Text>Huynh Ngoc Duc</Text>
      </Line>
      <Line>
        <Text>{t("orderService.customer-info.phone-number")}</Text>
        <Text>0933333333</Text>
      </Line>
      <Line>
        <Text>{t("orderService.customer-info.email")}</Text>
        <Text>duchuynh@gmail.com</Text>
      </Line>
    </CustomerInfoBox>
  );

  const TableBookingInfo = () => (
    <TableBookingInfoBox>
      <Title>{t("orderService.order-info.order-info")}</Title>
      <Line>
        <Text>{t("orderService.order-info.amount-of-customers")}</Text>
        <Text>4</Text>
      </Line>
      <Line>
        <Text>{t("orderService.order-info.order-date")}</Text>
        <Text>12/12/2020</Text>
      </Line>
      <Line>
        <Text>{t("orderService.order-info.received-date")}</Text>
        <Text>11:00</Text>
      </Line>
      <Line>
        <Text>{t("orderService.order-info.category-table")}</Text>
        <Text>Bàn 4 người</Text>
      </Line>
    </TableBookingInfoBox>
  );

  const PayInfo = () => (
    <PayInfoBox>
      <Title>{t("orderService.payment-info.payment-info")}</Title>
      <Line>
        <Text>{t("orderService.payment-info.payment-method")}</Text>
        <Text>Trả tại quầy</Text>
      </Line>
      <Line>
        <Text>{t("orderService.payment-info.Promotion-code")}</Text>
        <Text>Giảm giá 10% cho đơn hàng trên 300k</Text>
      </Line>
    </PayInfoBox>
  );

  return (
    <OrderServiceDetailContainer>
      <CustomerInfo />
      <TableBookingInfo />
      <PayInfo />
    </OrderServiceDetailContainer>
  );
};

export default OrderServiceDetail;
