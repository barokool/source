import styled from "styled-components/macro";
import tw from "twin.macro";

export const MenuTabContainer = styled.div`
  ${tw``}
`;

export const ListProductContainer = styled.div`
  ${tw`min-h-[500px]`}
`;

export const ListProductWrapper = styled.div`
  ${tw`grid grid-cols-4 gap-2 h-full`}
`;
