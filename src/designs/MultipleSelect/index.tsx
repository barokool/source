/**
 * @name Multiple_Select
 * @return_value_of_formik {String} a string which was joined of all selected option
 * @example
 *  - valueKey = "_id"
 *  - optionSelected = [{_id: "12EQ"}, {_id: "32UI"}, {_id: "7U21"}]
 * --> "12EQ|32UI|7u21"
 */

import { IIconSVGProps } from "typings";
import { Listbox, Transition } from "@headlessui/react";
import { useField, useFormikContext } from "formik";
import { useEffect, useState } from "react";
import { Fragment } from "react";
import {
  MultipleSelectContainer,
  HiddenInput,
  MenuButton,
  MenuItem,
  ListboxButton,
  ListboxOptionsContainer,
  Tag,
  Placeholder,
  TagContainer,
  TagText,
  RemoveIconWrapper,
  LabelWrapper,
} from "./styles";
import DropdownArrowIcon from "icons/Arrows/SelectArrow";
import FormControlErrorHelper from "common/styles/FormControlErrorHelper";
import FormControlLabel from "common/styles/FormControlLabel";

export type IOption = {
  /**
   * @param {String} name - This will display in option
   */
  name?: string;
  // Value will be passed to form values
  value?: string;
  [key: string]: any;
};

interface IMultipleSelectProps<T> {
  name: string;
  className?: string;
  listOptionsSelected: T[];
  label: string;
  options: T[];
  onSelect: (option: T[]) => void;
  disabled?: boolean;
  placeholder?: string;
  /**
   * @description targetForm is key of optionSelected. When form is submitted,
   * value of name will be optionSelected[targetForm]
   * @default targetForm = "_id"
   */
  formTarget?: string;
  /**
   * @description optionTarget is key of option which will be displayed in feature
   * @default optionTarget = "name"
   */
  optionTarget?: string;
  splitSign?: string; // ex: splitSign = "|" --> "abc|erf"
  required?: boolean;
}

const MultipleSelect = <T,>(props: IMultipleSelectProps<T>) => {
  const {
    name,
    className,
    label,
    options = [],
    listOptionsSelected = [],
    placeholder = "",
    disabled = false,
    formTarget = "_id",
    optionTarget = "name",
    required = false,
    splitSign = "|",
    onSelect,
  } = props;
  const { setFieldValue } = useFormikContext();
  const [field, meta] = useField(name);
  const isError: boolean = Boolean(!!meta.error && !!meta.touched);
  const [displayOptions, setDisplayOptions] = useState<T[]>(options);

  useEffect(() => {
    setDisplayOptions(options);
    removeAllOptionSelectedOutOfListOptions();
  }, [options]);

  useEffect(() => {
    setTimeout(() => {
      const value = (listOptionsSelected || [])
        .map(item => (item as any)[formTarget])
        .join(splitSign);
      let newOption = value;
      // setFieldValue cannot set immediately after first render
      setFieldValue(name, newOption);
    }, 300);
    removeAllOptionSelectedOutOfListOptions();
  }, [listOptionsSelected]);

  const handleSelect = (option: T) => {
    onSelect && onSelect([...listOptionsSelected, option]);
  };

  const removeAllOptionSelectedOutOfListOptions = () => {
    if (!listOptionsSelected?.length) return;
    const filteredOptions = displayOptions.filter(option => {
      if (
        listOptionsSelected.find(
          item =>
            (item as any)?.[optionTarget] === (option as any)?.[optionTarget],
        )
      )
        return false;
      return true;
    });
    setDisplayOptions(filteredOptions);
  };

  const removeItem = (removedItem: T) => {
    if (!listOptionsSelected?.length) return;
    const newListOptionsSelected = listOptionsSelected.filter(
      item =>
        (item as any)?.[optionTarget] !== (removedItem as any)?.[optionTarget],
    );
    setDisplayOptions([...displayOptions, removedItem]);
    onSelect && onSelect(newListOptionsSelected);
  };

  return (
    <MultipleSelectContainer className={className}>
      <LabelWrapper>
        <FormControlLabel isError={isError} required={required}>
          {label}
        </FormControlLabel>
      </LabelWrapper>

      <div>
        <Listbox
          value={listOptionsSelected}
          onChange={handleSelect as any}
          disabled={disabled}
        >
          <div className={`relative text-black ${className}`}>
            <ListboxButton>
              <MenuButton isError={isError} disabled={disabled}>
                {listOptionsSelected?.length > 0 ? (
                  <TagContainer>
                    {listOptionsSelected?.map((option: any, index) => (
                      <Tag key={index}>
                        <TagText>{option?.[optionTarget]}</TagText>
                        <RemoveIconWrapper
                          onClick={e => {
                            e.stopPropagation();
                            removeItem(option);
                          }}
                        >
                          <RemoveIcon />
                        </RemoveIconWrapper>
                      </Tag>
                    ))}
                  </TagContainer>
                ) : (
                  <Placeholder>{placeholder}</Placeholder>
                )}
                <DropdownArrowIcon />
              </MenuButton>
            </ListboxButton>
            <Transition
              as={Fragment}
              leave="transition ease-in duration-100"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <ListboxOptionsContainer>
                {displayOptions.map((option: any, index) => (
                  <Listbox.Option key={index} value={option}>
                    {({ selected, active }) => (
                      <MenuItem active={active || selected}>
                        {option?.[optionTarget]}
                      </MenuItem>
                    )}
                  </Listbox.Option>
                ))}
              </ListboxOptionsContainer>
            </Transition>
          </div>
        </Listbox>
      </div>
      {isError && <FormControlErrorHelper>{meta.error}</FormControlErrorHelper>}
      <HiddenInput {...field} />
    </MultipleSelectContainer>
  );
};

export default MultipleSelect;

const RemoveIcon: React.FC<IIconSVGProps> = props => {
  return (
    <svg
      width={12}
      height={12}
      viewBox="0 0 12 12"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g clipPath="url(#clip0)">
        <path
          d="M10.2445 1.75545C7.90345 -0.585151 4.09616 -0.585151 1.75507 1.75545C-0.585022 4.09606 -0.585022 7.90433 1.75507 10.2449C2.92562 11.415 4.46283 11.9997 6.00006 11.9997C7.5373 11.9997 9.074 11.415 10.2445 10.2449C12.5852 7.90435 12.5852 4.09606 10.2445 1.75545ZM8.47571 7.76877C8.67129 7.96436 8.67129 8.28051 8.47571 8.4761C8.37816 8.57364 8.25011 8.62267 8.12204 8.62267C7.99398 8.62267 7.86591 8.57364 7.76836 8.4761L6.00004 6.70726L4.23221 8.47558C4.13417 8.57313 4.0061 8.62216 3.87854 8.62216C3.75049 8.62216 3.62241 8.57313 3.52487 8.47558C3.32928 8.28 3.32928 7.96335 3.52487 7.76826L5.29269 5.99994L3.52437 4.23161C3.32879 4.03603 3.32879 3.71938 3.52437 3.52429C3.71946 3.32871 4.03611 3.32871 4.2317 3.52429L6.00002 5.29261L7.76834 3.52429C7.96392 3.32871 8.28008 3.32871 8.47566 3.52429C8.67125 3.71938 8.67125 4.03603 8.47566 4.23161L6.70734 5.99994L8.47571 7.76877Z"
          fill="white"
        />
      </g>
      <defs>
        <clipPath id="clip0">
          <rect width={12} height={12} fill="white" />
        </clipPath>
      </defs>
    </svg>
  );
};
