import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { IBreadcrumb } from "typings";

export interface ISystemConfig {
  isExtendDrawer: boolean;
  isUpdateLocalStorage: boolean;
  breadcrumb: IBreadcrumb;
  loadingTasks: string[];
  isCollapse: boolean;
}

const initialState: ISystemConfig = {
  isExtendDrawer: true,
  isUpdateLocalStorage: false,
  breadcrumb: [],
  loadingTasks: [],
  isCollapse: false,
};

export const _configSlice = createSlice({
  name: "_config",
  initialState,
  reducers: {
    setExtendDrawer: (state, action: PayloadAction<boolean>) => {
      state.isExtendDrawer = action.payload;
    },
    toggleExtendDrawer: state => {
      state.isExtendDrawer = !state.isExtendDrawer;
    },
    setIsUpdateLocalStorage: (state, action: PayloadAction<boolean>) => {
      state.isUpdateLocalStorage = action.payload;
    },
    setBreadcrumb: (state, action: PayloadAction<IBreadcrumb>) => {
      state.breadcrumb = action.payload;
    },
    startLoading: (state, action: PayloadAction<string>) => {
      state.loadingTasks.push(action.payload);
    },
    stopLoading: (state, action: PayloadAction<string>) => {
      state.loadingTasks = state.loadingTasks.filter(
        task => task !== action.payload,
      );
    },
    clearLoadingTasks: state => {
      state.loadingTasks = [];
    },
    setCollapseDrawer: (state, action: PayloadAction<boolean>) => {
      state.isCollapse = action.payload;
    },
  },
});

// Actions
export const {
  setExtendDrawer,
  toggleExtendDrawer,
  setBreadcrumb,
  startLoading,
  stopLoading,
  clearLoadingTasks,
  setCollapseDrawer,
  setIsUpdateLocalStorage,
} = _configSlice.actions;

export default _configSlice.reducer;
