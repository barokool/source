import React, { useState, useEffect } from "react";
import { lazy } from "react";

//redux
import { useSelector, useDispatch } from "react-redux";
import { IRootState } from "typings";
import { setSellingPageSelected } from "redux/slices/sellingPage";

//api
import { Product, useGetProductByBarcode } from "apiCaller";
import { fragmentGetProductByBarcode } from "services/product";

//languages
import { t } from "language";

//locals
import {
  ChangePageButton,
  HeaderContainer,
  LeftHeaderContainer,
  RightHeaderContainer,
  SellingLayoutContainer,
  SearchBox,
  RightHeaderItem,
  PageSelectedContainer,
} from "./styles";
import ProductDialog from "./components/ProductDialog";

interface ISellingProps {}

const SellingMenu = lazy(() => import("pages/dashboard/selling/Menu"));
const SellingRoom = lazy(() => import("pages/dashboard/selling/Room"));

const Selling: React.FC<ISellingProps> = props => {
  const [product, setProduct] = useState<Product | null>();
  const [search, setSearch] = useState<string>("");
  const [getProductByBarCode, { data, loading, called }] =
    useGetProductByBarcode(fragmentGetProductByBarcode);
  const [open, setOpen] = useState<boolean>(false);
  const { pageSelected } = useSelector(
    (state: IRootState) => state.sellingPage,
  );
  const dispatch = useDispatch();

  const onSearch = (text: string) => {
    setSearch(text);
  };

  const handleScanBarcode = async () => {
    try {
      await getProductByBarCode({ variables: { barcode: search } });
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    if (search.length > 5) {
      handleScanBarcode();
    }
  }, [search]);

  useEffect(() => {
    if (data) {
      setProduct(data.getProductByBarcode as Product);
    }
  }, [data]);

  useEffect(() => {
    setOpen(true);
  }, [product]);

  return (
    <SellingLayoutContainer>
      <HeaderContainer>
        <LeftHeaderContainer>
          <ChangePageButton
            onClick={() => dispatch(setSellingPageSelected("Room"))}
            active={pageSelected === "Room"}
          >
            {t("selling.room")}
          </ChangePageButton>
          <ChangePageButton
            onClick={() => dispatch(setSellingPageSelected("Menu"))}
            active={pageSelected === "Menu"}
          >
            {t("selling.menu")}
          </ChangePageButton>
          <SearchBox onFetchData={onSearch} />

          <ProductDialog product={product as Product} open={open} />
        </LeftHeaderContainer>
        <RightHeaderContainer>
          <RightHeaderItem.Container></RightHeaderItem.Container>
        </RightHeaderContainer>
      </HeaderContainer>
      <PageSelectedContainer>
        <React.Suspense fallback={<PageSellingLoading />}>
          {pageSelected === "Menu" ? <SellingMenu /> : <SellingRoom />}
        </React.Suspense>
      </PageSelectedContainer>
    </SellingLayoutContainer>
  );
};

const PageSellingLoading: React.FC = () => {
  return (
    <div className="w-full grid grid-cols-7  gap-x-4">
      <div className="col-span-3 animate-pulse h-screen bg-line shadow-md"></div>
      <div className="col-span-4 animate-pulse h-screen bg-line shadow-md"></div>
    </div>
  );
};

export default Selling;
