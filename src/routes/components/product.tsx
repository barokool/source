import { IRoutes } from "typings";
import { PATH } from "common/constants/routes";
import { lazy } from "react";
import { t } from "language";

import ProductIcon from "icons/Dashboard/Product";

const ProductCategory = lazy(() => import("pages/dashboard/product/Category"));
const ProductList = lazy(() => import("pages/dashboard/product/List"));
// const Combo = lazy(() => import("pages/dashboard/product/combo"));

export const productRoutes: IRoutes = {
  name: t("dashboard.product-management"),
  path: PATH.PRODUCT.SELF,
  exact: true,
  isPrivate: true,
  Icon: ProductIcon,
  children: [
    {
      name: t("product.product-category.product-category"),
      path: PATH.PRODUCT.CATEGORY,
      isPrivate: true,
      Component: ProductCategory,
      exact: true,
    },
    {
      name: t("product.product-list.product-list"),
      path: PATH.PRODUCT.LIST,
      isPrivate: true,
      Component: ProductList,
      exact: true,
    },
    // {
    //   name: "Combo sản phẩm",
    //   path: PATH.PRODUCT.COMBO,
    //   isPrivate: true,
    //   Component: Combo,
    //   exact: true,
    // },
  ],
};
