import React, { useState, useEffect } from "react";

//redux
import { useSelector } from "react-redux";
import { IRootState } from "redux/storeToolkit";

//api
import { OrderTypeEnum, OrderStatusEnum } from "apiCaller";

//locals
import { TakeAwayRightContainer } from "./styles";
import RightCheckOut from "./CheckOut";
import RightProcessing from "./Processing";
import RightPayment from "./Payment";
import RightCompleted from "./Completed";
import Empty from "./Empty";

interface IInPlaceRightProps {}

const InPlaceRight: React.FC<IInPlaceRightProps> = props => {
  const { orderSelected } = useSelector((state: IRootState) => state.order);
  const [initComponent, setInitComponent] = useState<React.ReactNode>(<></>);

  useEffect(() => {
    if (orderSelected?.type === OrderTypeEnum.Take_away) {
      if (orderSelected.orderItems?.length === 0) setInitComponent(<Empty />);
      else {
        switch (orderSelected?.status) {
          case OrderStatusEnum.New:
            setInitComponent(<RightCheckOut />);
            break;
          case OrderStatusEnum.Approving:
            setInitComponent(<RightPayment />);
            break;
          case OrderStatusEnum.Processing:
            setInitComponent(<RightProcessing />);
            break;
          case OrderStatusEnum.Complete:
            setInitComponent(<RightCompleted />);
        }
      }
    }
  }, [orderSelected]);

  return <TakeAwayRightContainer>{initComponent}</TakeAwayRightContainer>;
};

export default InPlaceRight;
