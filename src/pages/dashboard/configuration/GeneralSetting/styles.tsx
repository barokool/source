import tw from "twin.macro";
import styled, { css } from "styled-components/macro";
import _SearchBox from "components/SearchBoxTable";
import _Button from "designs/Button";

export const GeneralSettingContainer = styled.div`
  ${tw` flex w-full justify-between`}
`;

export const ButtonCustom = styled(_Button)<{ select?: boolean }>`
  ${tw`px-2 py-1  bg-secondary text-white`}

  & span {
    ${tw`h-1 w-1 border-t-2 border-l-2 right-0`}
    transform: rotate(-135deg);
  }
`;

export const RenderItemContainer = styled.div`
  ${tw``}
`;
