import React, { useState } from "react";
import Dialog from "components/Dialog";
import { DialogTitle } from "common/styles/Title";

//hooks
import { useReactToPrint } from "react-to-print";

//locals
import {
  PrintBillDialogContainer,
  ButtonWrapper,
  Button,
  ElementWrapper,
} from "./styles";
import Bill from "../Bill";

//languages
import { t } from "language";

interface ITableListDialogProps {
  ButtonMenu?: React.ReactElement;
  title?: string;
  onClose?: () => void;
  open?: boolean;
}

const PrintBillDialog: React.FC<ITableListDialogProps> = ({
  ButtonMenu,
  title,
  onClose,
  open = false,
}) => {
  const billRef = React.createRef();
  const handlePrint = useReactToPrint({
    content: () => billRef.current as React.ReactInstance,
  });

  const [isOpen, setOpen] = useState<boolean>(open);
  const [loading, setLoading] = useState<boolean>(false);

  const onHandleClose = () => {
    setOpen(false);
    onClose && onClose();
  };
  return (
    <>
      <ElementWrapper
        onClick={() => setOpen(!isOpen)}
        className="flex justify-end w-full"
      >
        {ButtonMenu}
      </ElementWrapper>

      <Dialog
        open={isOpen}
        onClose={onHandleClose}
        className="overflow-auto"
        size="md"
      >
        <PrintBillDialogContainer>
          <DialogTitle>{title}</DialogTitle>
          <Bill ref={billRef} />
          <ButtonWrapper>
            <Button outline type="button" onClick={onHandleClose}>
              {t("common.cancel")}
            </Button>
            <Button
              type="submit"
              primary
              loading={loading}
              onClick={() => {
                handlePrint();
                setOpen(false);
              }}
            >
              {t("common.accept")}
            </Button>
          </ButtonWrapper>
        </PrintBillDialogContainer>
      </Dialog>
    </>
  );
};
export default PrintBillDialog;
