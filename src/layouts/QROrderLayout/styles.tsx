import styled from "styled-components/macro";
import tw from "twin.macro";

export const QROrderLayoutContainer = styled.div`
  ${tw`flex overflow-auto  h-screen`}
`;

export const QROrderPageWrapper = styled.div`
  ${tw`max-w-[600px] w-full mx-auto`}
`;
