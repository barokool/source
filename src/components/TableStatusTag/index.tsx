import { TableStatus } from "apiCaller";
import { TableStatusTagContainer } from "./styles";

interface ProductStatusTagProps {
  active: TableStatus;
}

const TableStatusTag: React.FC<ProductStatusTagProps> = ({
  active,
  children,
}) => {
  return (
    <TableStatusTagContainer active={active}>
      {children}
    </TableStatusTagContainer>
  );
};

export default TableStatusTag;
