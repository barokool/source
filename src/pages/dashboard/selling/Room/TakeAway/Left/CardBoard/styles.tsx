import styled from "styled-components/macro";
import tw from "twin.macro";

export const LeftCardBoardContainer = styled.div`
  ${tw`w-full h-screen shadow-lg `}
`;

export const LeftCardBoardWrapper = styled.div`
  ${tw`min-h-[630px]`}
`;

export const CardListContainer = styled.div`
  ${tw`grid grid-cols-4 gap-2 p-2 h-full`}
`;
