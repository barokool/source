import React, { useState, useEffect } from "react";

import {
  AddNote,
  Card,
  OrderDetailContainer,
  RatioGroup,
  CustomRadio,
  Point,
  BoxPoint,
  RightContent,
  RadioItem,
  CheckOutButtonWrapper,
} from "./styles";

import DishImage from "assets/images/qr-order/dish-image.png";

import GroupRadioButton, { IOptions } from "designs/GroupRadioButton";
import TextArea from "designs/TextArea";
import CheckOutButton from "components/CheckOutButton";
import { Form, Formik } from "formik";
import CustomGroupRadio from "components/RadioGroup";
import { Radio } from "designs/GroupRadioButton/styles";

interface OrderDetailProps {}

interface IFormValue {
  note?: string;
}

const validationSchema = {};

const OrderDetail: React.FC<OrderDetailProps> = props => {
  const [listOption, setListOption] = useState<IOptions[]>([
    {
      _id: "1",
      label: "Sốt HS",
    },
    {
      _id: "2",
      label: "Sốt đậu",
    },
    {
      _id: "3",
      label: "Sốt hàu",
    },
  ]);
  const [optionSelected, setOptionSelected] = useState<IOptions>();

  const [listTopping, setListTopping] = useState<IOptions[]>([
    {
      _id: "1",
      label: "Salad",
    },
    {
      _id: "2",
      label: "Salad",
    },
    {
      _id: "3",
      label: "Salad",
    },
  ]);
  const [toppingSelected, setToppingSelected] = useState<IOptions>();
  const [initialValues, setInitialValues] = useState<IFormValue>({
    note: "",
  });

  const handleSubmit = () => console.log("Submitted");

  return (
    <OrderDetailContainer>
      <Card.Container>
        <img src={DishImage} alt="/dish-image" className="w-full h-full" />
        <Card.ContentContainer>
          <Card.LeftContentContainer>
            <Card.Label>Gà sốt phần</Card.Label>
            <Card.Description>
              2 gà sốt + 1 khoai tây chiên + 1 Pepsi{" "}
            </Card.Description>
          </Card.LeftContentContainer>
          <Card.Price>đ 155,000</Card.Price>
        </Card.ContentContainer>
      </Card.Container>

      <RatioGroup.Container>
        <RatioGroup.Label>Chọn sốt</RatioGroup.Label>
        <CustomGroupRadio
          options={listOption}
          onChange={option => setOptionSelected(option)}
          optionSelected={optionSelected}
          renderOption={({ option, checked }) => (
            <RadioItem.Container>
              <RadioItem.LeftWrapper>
                <BoxPoint>
                  <Point>
                    <CustomRadio active={checked} />
                  </Point>
                </BoxPoint>
                <RadioItem.Label>{option?.label}</RadioItem.Label>
              </RadioItem.LeftWrapper>
              <RightContent>1</RightContent>
            </RadioItem.Container>
          )}
        />
      </RatioGroup.Container>

      <RatioGroup.Container>
        <RatioGroup.Label>Món ăn kèm</RatioGroup.Label>
        <CustomGroupRadio
          options={listTopping}
          onChange={option => setOptionSelected(option)}
          optionSelected={toppingSelected}
          renderOption={({ option, checked }) => (
            <RadioItem.Container>
              <RadioItem.LeftWrapper>
                <BoxPoint>
                  <Point>
                    <CustomRadio active={checked} />
                  </Point>
                </BoxPoint>
                <RadioItem.Label>{option?.label}</RadioItem.Label>
              </RadioItem.LeftWrapper>
              <RightContent>15,000 đ</RightContent>
            </RadioItem.Container>
          )}
        />
      </RatioGroup.Container>

      <AddNote.Container>
        <AddNote.Label>Thêm ghi chú món</AddNote.Label>
        <Formik initialValues={initialValues} onSubmit={handleSubmit}>
          <Form>
            <TextArea
              name="name"
              label=""
              placeholder="Việc thực hiện yêu cầu tùy thuộc vào khả năng của quán"
            />
          </Form>
        </Formik>
      </AddNote.Container>

      <CheckOutButtonWrapper>
        <CheckOutButton />
      </CheckOutButtonWrapper>
    </OrderDetailContainer>
  );
};

export default OrderDetail;
