import styled from "styled-components/macro";
import tw from "twin.macro";

export const FavouriteDishesContainer = styled.div`
  ${tw`my-4.5 px-2 py-1.5 `}
`;

export const HeaderContainer = styled.div`
  ${tw`flex justify-between`}
`;

export const LeftHeaderContainer = styled.div`
  ${tw`flex items-center`}
`;

export const LeftHeaderLabel = styled.h1`
  ${tw`text-16 text-black ml-1 font-semibold `}
`;

export const DishCardContainer = styled.div`
  ${tw`mt-2`}
`;

export const LeftButtonContent = styled.h3`
  ${tw`text-white text-13 mr-1`}
`;

export const RightButtonContent = styled.h3`
  ${tw`text-white text-16 font-bold`}
`;
export const QuantityContent = styled.button`
  ${tw`text-white border-2 border-white px-0.5`}
`;

export const AddButtonWrapper = styled.div`
  ${tw`flex items-center`}
`;

export const DishQuantity = styled.p`
  ${tw`text-black text-14 mr-1`}
`;
