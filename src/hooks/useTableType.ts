import { useCallback, useEffect, useState } from "react";
import { useGetAllTableType } from "apiCaller";
import { fragmentGetAllTableType } from "services/table";
import { toast } from "react-toastify";

import { TableType } from "apiCaller";
import { useCurrentBranch } from "./useCurrentBranch";

export const useTableType = () => {
  const { branchSelected } = useCurrentBranch();
  const [listTableType, setListTableType] = useState<TableType[]>([]);
  const [getAllTableType, { data, error }] = useGetAllTableType(
    fragmentGetAllTableType,
  );
  useEffect(() => {
    branchSelected && invokeGetAllTableType();
  }, [branchSelected]);

  useEffect(() => {
    data && setListTableType(data.getAllTableType.results as TableType[]);
  }, [data]);

  useEffect(() => {
    error && toast.error(error.message);
  }, [error]);

  const invokeGetAllTableType = () => {
    getAllTableType({
      variables: {
        filter: {
          branch: branchSelected?._id as string,
        },
      },
    });
  };

  return {
    listTableType,
  };
};
