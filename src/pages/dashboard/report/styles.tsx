import tw from "twin.macro";
import styled from "styled-components/macro";

export const ReportPageContainer = styled.div`
  ${tw``}
`;

export const ReportChildrenContainer = styled.div`
  ${tw`grid grid-cols-2 gap-2`}
`;
