import styled from "styled-components/macro";
import tw from "twin.macro";

export const SearchBoxTableContainer = styled.label`
  ${tw`relative grid items-center w-full h-4 gap-1 px-1 rounded-sm cursor-text text-neutral-3 `}
  ${tw`border border-solid border-border-color focus-within:border-primary focus-within:text-primary-1`}
  grid-template-columns: 25px 1fr;
`;

export const Input = styled.input`
  ${tw`w-full p-0 outline-none text-16 text-neutral-1 placeholder:text-neutral-3 bg-transparent `}
`;
