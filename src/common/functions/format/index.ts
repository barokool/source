export const formatMoney = (n: string) => {
  return n.replace(/\B(?=(\d{3})+(?!\d))/g, ".");
};

export const formKey = <T>(key: keyof T) => key;
