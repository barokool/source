export const cleanObject = <T>(
  obj: T,
  option?: {
    isRemoveFalseValue: boolean;
  },
): Partial<T> => {
  let object = obj as any;
  if (!object) return object;

  for (let key in object) {
    if (
      object[key] == undefined ||
      object[key] == null ||
      (option?.isRemoveFalseValue && object[key] == false)
    ) {
      delete object[key];
    }
    if (typeof object[key] == "object") cleanObject(object[key], option);
  }
  return object;
};
