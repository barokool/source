import styled from "styled-components/macro";
import tw from "twin.macro";

export const MenuLeftContainer = styled.div`
  ${tw`w-full shadow-lg p-2 h-screen`}
`;
