import { ButtonHTMLAttributes, DetailedHTMLProps, useRef } from "react";
import { useHistory } from "react-router-dom";

import { ButtonWrapper } from "./styles";
import Spinner from "icons/Spinner";
interface IButtonProps {
  to?: string;
  className?: string;
  primary?: boolean;
  outline?: boolean;
  loading?: boolean;
  onSuccess?: () => void;
  handleClick?: () => void;
  type: "button" | "reset" | "submit";
}

const AddButton: React.FC<IButtonProps> = props => {
  const history = useHistory();
  const {
    children,
    className = "",
    to,
    primary,
    loading = false,
    outline,
    onSuccess,
    handleClick,
    type,
  } = props;
  const buttonRef = useRef<HTMLButtonElement>(null);
  console.log(type);

  return (
    <ButtonWrapper
      type={type}
      ref={buttonRef}
      outline={outline}
      primary={primary}
      loading={loading}
      className={className}
      onClick={handleClick}
    >
      {loading ? <Spinner /> : ""}
      {children}
    </ButtonWrapper>
  );
};

export default AddButton;
