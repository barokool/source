import styled from "styled-components/macro";
import tw from "twin.macro";
import _Button from "designs/Button";

export const PaymentPageContainer = styled.div`
  ${tw`my-4.5 px-2 py-1.5 `}
`;

export const HeaderContainer = styled.div`
  ${tw`flex w-1/3 justify-between`}
`;

export const HeaderLabel = styled.h3`
  ${tw`text-16 text-black font-bold`}
`;

export const BodyContainer = styled.div`
  ${tw`mt-3.5`}
`;

export const ButtonWrapper = styled.div`
  ${tw`w-full mt-50`}
`;

export const Button = styled(_Button)`
  ${tw`px-3.5 py-[12.5px] font-medium bg-primary text-white justify-center text-center w-full mb-4`}
`;
