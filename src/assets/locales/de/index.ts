import login from "./login.json";
import companyRegister from "./companyRegister.json";
import configuration from "./configuration.json";
import dashboard from "./dashboard.json";
import delivery from "./delivery.json";
import order from "./order.json";
import orderService from "./orderService.json";
import overview from "./overview.json";
import product from "./product.json";
import coupon from "./coupon.json";
import report from "./report.json";
import table from "./table.json";
import common from "./common.json";
import register from "./register.json";
import selling from "./selling.json";
import resetPassword from "./resetPassword.json";
import shipping from "./shipping.json";
import account from "./account.json";
import notfound from "./notfound.json";
import settings from "./settings.json";

const languageGer = {
  login,
  companyRegister,
  configuration,
  dashboard,
  delivery,
  order,
  orderService,
  overview,
  product,
  coupon,
  report,
  table,
  common,
  register,
  selling,
  resetPassword,
  shipping,
  account,
  notfound,
  settings,
};

export default languageGer;
