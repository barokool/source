import { OrderInfoContainer } from "./styles";
interface OrderInfoProps {
  data: Array<{
    title: string;
    content: string;
  }>;
}

import styled from "styled-components/macro";
import tw from "twin.macro";

export const Box = styled.div`
  ${tw`  `}
`;

export const Title = styled.div`
  ${tw`text-13 font-regular leading-4 mb-1`}
`;
export const Content = styled.div`
  ${tw`text-20 font-bold leading-6`}
`;

const OrderInfo: React.FC<OrderInfoProps> = props => {
  console.log(props.data);
  const { data } = props;
  return (
    <OrderInfoContainer>
      {data.map(({ content, title }, index) => (
        <Box key={title}>
          <Title>{title}</Title>
          <Content>
            {content === "NaN" ? 0 : content} {index === 0 ? "đ" : "đơn hàng"}
          </Content>
        </Box>
      ))}
    </OrderInfoContainer>
  );
};

export default OrderInfo;
