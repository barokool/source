import styled from "styled-components/macro";
import tw from "twin.macro";

export const OrderServiceDetailContainer = styled.div`
  ${tw`  `}
`;
export const Box = styled.div`
  ${tw`border-b border-neutral-4 border-dashed p-2`}
`;
export const CustomerInfoBox = styled(Box)`
  ${tw`  `}
`;
export const TableBookingInfoBox = styled(Box)`
  ${tw`  `}
`;
export const PayInfoBox = styled(Box)`
  ${tw`  `}
`;
export const Title = styled.h4`
  ${tw`text-20 font-medium pb-3`}
`;
export const Line = styled.div`
  ${tw`flex justify-between`}
`;
export const Text = styled.p`
  ${tw`text-14`}
`;
export const TextBold = styled.p`
  ${tw`text-14 font-medium`}
`;
