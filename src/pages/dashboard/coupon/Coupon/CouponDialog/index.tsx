import React, { useState, useEffect } from "react";
import { Formik } from "formik";
import * as yup from "yup";
import { toast } from "react-toastify";
import Input from "designs/InputV2";
import Select from "designs/Select";
import Dialog from "components/Dialog";
import MultiAutoComplete from "designs/MultipleAutoComplete";
import { DialogTitle } from "common/styles/Title";
import DatePicker from "designs/DatePicker";

//hooks
import useAuth from "hooks/useAuth";
import { useCurrentBranch } from "hooks/useCurrentBranch";

//languages
import { t } from "language";

//api
import {
  Branch,
  Product,
  CouponCreateInput,
  CouponMethodEnum,
  Coupon,
  useCreateCoupon,
  useUpdateCoupon,
  useGetAllBranch,
  useGetAllProduct,
  CouponStatusEnum,
} from "apiCaller";
import { fragmentCreateCoupon } from "services/coupon";
import { fragmentGetAllBranch } from "services/branch";
import { fragmentGetAllProduct } from "services/product";

//locals
import {
  Button,
  Form,
  ElementWrapper,
  ButtonWrapper,
  CouponDialogContainer,
  BlockHolder,
} from "./styles";

interface ICouponDialogProps {
  ButtonMenu?: React.ReactElement;
  editField?: Coupon;
  open?: boolean;
  onSuccess?: () => void;
  title: string;
}

interface IFormValue {
  applyForBooking?: boolean;
  applyForShipping?: boolean;
  branches: string[];
  closeDate: string;
  code: string;
  description?: string;
  method?: CouponMethodEnum;
  name: string;
  openingDate: string;
  products: string[];
  quantity: number;
  value: number;
}

interface IPaymentMethodOptions {
  id: CouponMethodEnum;
  name: string;
}

const validationSchema = yup
  .object()
  .shape<{ [key in keyof IFormValue]: any }>({
    name: yup.string().required(t("coupon.coupon-code.please-enter-name")),
    branches: yup.string().required("Please select branch"),
    closeDate: yup.string().required("Please select close date"),
    code: yup.string().required("Please enter code"),
    openingDate: yup.string().required("Please select opening date"),
    products: yup.string().required("Please select products"),
    quantity: yup.number().required("Please enter quantity"),
    value: yup.number().required("Please enter value"),
  });

const CouponDialog: React.FC<ICouponDialogProps> = ({
  ButtonMenu,
  editField,
  open = false,
  onSuccess,
  title,
}) => {
  const { accountInfo } = useAuth();
  const { branchSelected } = useCurrentBranch();
  //local states
  const [isOpen, setOpen] = useState<boolean>(open || false);
  const [initialValues, setInitialValues] = useState<IFormValue>({
    name: "",
    branches: [],
    code: "",
    closeDate: "",
    openingDate: "",
    method: "cash" as CouponMethodEnum,
    products: [],
    quantity: 1,
    value: 1,
  });
  const [listBranch, setListBranch] = useState<Branch[]>([]);
  const [listBranchSelected, setListBranchSelected] = useState<Branch[]>([]);
  const [listProduct, setListProduct] = useState<Product[]>([]);
  const [listProductSelected, setListProductSelected] = useState<Product[]>([]);
  const [methodSelected, setMethodSelected] =
    useState<IPaymentMethodOptions | null>(null);
  const [listMethod] = useState<IPaymentMethodOptions[]>([
    { id: CouponMethodEnum.Cash, name: t("coupon.coupon-code.cash") },
    { id: CouponMethodEnum.Percent, name: t("coupon.coupon-code.percentage") },
  ]);

  //api
  const [getAllBranch, { data: getAllBranchData }] =
    useGetAllBranch(fragmentGetAllBranch);
  const [getAllProduct, { data: getAllProductData }] = useGetAllProduct(
    fragmentGetAllProduct,
  );
  const [
    createCoupon,
    {
      data: createCouponData,
      loading: createCouponLoading,
      error: createCouponErrors,
    },
  ] = useCreateCoupon(fragmentCreateCoupon);
  const [
    updateCoupon,
    {
      data: updateCouponData,
      loading: updateCouponLoading,
      error: updateCouponErrors,
    },
  ] = useUpdateCoupon();

  useEffect(() => {
    getAllBranchData &&
      setListBranch(getAllBranchData.getAllBranch.results as Branch[]);
  }, [getAllBranchData]);

  useEffect(() => {
    getAllProductData &&
      setListProduct(getAllProductData.getAllProduct.results as Product[]);
  }, [getAllProductData]);

  useEffect(() => {
    if (createCouponData && !createCouponErrors) {
      toast.success(t("coupon.coupon-code.create-success"));
      onSuccess?.();
      setOpen(false);
    }
    if (updateCouponData && !updateCouponErrors) {
      toast.success(t("coupon.coupon-code.update-success"));
      onSuccess?.();
      setOpen(false);
    }
  }, [createCouponData, updateCouponData]);

  useEffect(() => {
    branchSelected && invokeGetAllData();
  }, [branchSelected]);

  useEffect(() => {
    if (editField) {
      setListBranchSelected(editField.branches as Branch[]);
      editField.method === CouponMethodEnum.Cash
        ? setMethodSelected(listMethod[0])
        : setMethodSelected(listMethod[1]);
      setListProductSelected(editField.products as Product[]);
      setInitialValues({
        name: editField.name as string,
        code: editField.code as string,
        openingDate: editField.openingDate as any,
        closeDate: editField.closeDate as any,
        quantity: editField.quantity as number,
        branches: editField.branches?.map(
          branch => branch._id as string,
        ) as string[],
        value: editField.value as number,
        method: editField.method as CouponMethodEnum,
        products: editField.products?.map(
          product => product._id as string,
        ) as string[],
      });
    }
  }, [editField]);

  const invokeGetAllData = () => {
    getAllBranch({
      variables: {
        filterBranch: { userId: accountInfo?.userInfo?._id as any },
      },
    });
    getAllProduct({
      variables: { filterProduct: { branch: branchSelected?._id || "" } },
    });
  };

  const handleSubmit = (values: IFormValue) => {
    const input: CouponCreateInput = {
      name: values.name,
      code: values.code as string,
      branches: listBranchSelected.map(
        branch => branch._id as string,
      ) as string[],
      quantity: values.quantity,
      value: values.value,
      method: methodSelected?.id as CouponMethodEnum,
      closeDate: values.closeDate?.toString() as string,
      openingDate: values.openingDate?.toString() as string,
      products: listProductSelected.map(
        product => product._id as string,
      ) as string[],
    };
    if (editField) {
      updateCoupon({
        variables: {
          id: editField._id as string,
          input: { status: CouponStatusEnum.Instock, ...input },
        },
      });
    } else {
      console.log({ input });
      createCoupon({
        variables: {
          input,
        },
      });
    }
  };

  return (
    <>
      <ElementWrapper onClick={() => setOpen(!isOpen)}>
        {ButtonMenu}
      </ElementWrapper>

      <Dialog
        open={isOpen}
        onClose={() => setOpen(false)}
        className="overflow-auto"
        size="lg"
      >
        <CouponDialogContainer>
          <DialogTitle>{title}</DialogTitle>
          <Formik
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={handleSubmit}
            enableReinitialize
          >
            <Form>
              <Input
                name="name"
                label={t("coupon.coupon-code.name")}
                placeholder={t("coupon.coupon-code.enter-name")}
                required
              />
              <Input
                name="code"
                label={t("coupon.coupon-code.code")}
                placeholder={t("coupon.coupon-code.enter-code")}
              />
              <MultiAutoComplete
                name="branches"
                label={t("coupon.coupon-code.branch")}
                required
                placeholder={t("coupon.coupon-code.select-branch")}
                options={listBranch}
                onSelect={values => setListBranchSelected(values)}
                listOptionsSelected={listBranchSelected}
              />

              <MultiAutoComplete
                name="products"
                label={t("coupon.coupon-code.applicable-product")}
                required
                placeholder={t("coupon.coupon-code.select-applicable-product")}
                options={listProduct}
                onSelect={values => setListProductSelected(values)}
                listOptionsSelected={listProductSelected}
              />
              <DatePicker
                name="openingDate"
                label={t("coupon.coupon-code.start-date")}
                placeholder={t("coupon.coupon-code.select-start-date")}
                formTarget="openingDate"
              />
              <DatePicker
                name="closeDate"
                label={t("coupon.coupon-code.end-date")}
                placeholder={t("coupon.coupon-code.select-end-date")}
                formTarget="closeDate"
              />
              <Select
                name="method"
                optionSelected={methodSelected}
                label={t("coupon.coupon-code.select-method")}
                options={listMethod}
                onSelect={method => setMethodSelected(method)}
                formTarget="_id"
              />

              <Input
                name="value"
                label={t("coupon.coupon-code.reduced-value")}
                placeholder={
                  initialValues.method == "cash"
                    ? initialValues.method.toString()
                    : ""
                }
                type="number"
              />
              <Input
                name="quantity"
                type="number"
                label={t("coupon.coupon-code.quantity")}
                placeholder={t("coupon.coupon-code.enter-quantity")}
              />

              <ButtonWrapper>
                <Button outline type="button" onClick={() => setOpen(false)}>
                  {t("common.cancel")}
                </Button>
                <Button
                  type="submit"
                  primary
                  loading={createCouponLoading || updateCouponLoading}
                >
                  {t("common.accept")}
                </Button>
              </ButtonWrapper>
            </Form>
          </Formik>
        </CouponDialogContainer>
      </Dialog>
    </>
  );
};

export default CouponDialog;
