import React, { useState, useEffect, useMemo, useCallback } from "react";
import { RouteComponentProps } from "react-router";

import { usePage } from "hooks/usePage";
import { useLoading } from "hooks/useLoading";
import { useBreadcrumb } from "hooks/useBreadcrumb";
import useAuth from "hooks/useAuth";

import Table, { IColumns } from "designs/Table";
import TableLayout from "layouts/Table";
import {
  StaffReportContainer,
  SearchBox,
  FilterButton,
  RightButtonWrapper,
} from "./styles";

import { PATH } from "common/constants/routes";
import { getQueryFromLocation } from "common/functions";
import ListDataLayout from "layouts/ListDataLayout";
import { t } from "language";

interface IStaffReportRoute extends RouteComponentProps {}

const LOAD_DATA = "LOAD_DATA";
const SIZE_PER_PAGE = 10;

const StaffReport: React.FC<IStaffReportRoute> = ({ location }) => {
  const [page, setPage] = usePage(getQueryFromLocation(location)?.page);
  const [totalSize, setTotalSize] = useState<number>(0);
  const [search, setSearch] = useState<string>("");
  const { startLoading, stopLoading } = useLoading();
  const { accountInfo } = useAuth();
  // const [listReport, setListReport] = useState<IEmployeeReport[]>([]);

  useBreadcrumb([
    {
      name: t("report.report"),
    },
  ]);

  useEffect(() => {
    invokeGetAllDataAPI();
  }, [page, search]);

  const invokeGetAllDataAPI = async () => {
    try {
      startLoading(LOAD_DATA);
      // const result = await getEmployeeReportAPI({
      //   branch: accountInfo?.userInfo?.branches?.[0]._id as string,
      //   filterDayReport: "",
      // });
      // setListReport(result);
      // setTotalSize(result.length);
    } catch (error) {
      console.log(error);
    } finally {
      stopLoading(LOAD_DATA);
    }
  };

  const handlePageChange = useCallback((nextPage: number) => {
    setPage(nextPage);
  }, []);

  const onSearch = (text: string) => setSearch(text);

  const columns: IColumns = useMemo(
    () => [
      {
        text: t("report.staffs-report.code"),
        dataField: "code",
      },
      {
        text: t("report.staffs-report.name"),
        dataField: "name",
      },
      {
        text: t("report.staffs-report.revenue"),
        dataField: "totalMoney",
      },
      {
        text: "",
        dataField: "actions",
        headerStyle: () => ({ display: "none" }),
        formatter: (_: string) => <div className="w-0"></div>,
      },
    ],
    [page],
  );
  return (
    <ListDataLayout
      title={t("report.staffs-report.staffs-report")}
      leftTopBar={
        <SearchBox
          onFetchData={onSearch}
          placeholder={t("report.staffs-report.search-by-staff-name")}
        ></SearchBox>
      }
      rightTopBar={
        <RightButtonWrapper>
          <FilterButton>{t("common.this-month")}</FilterButton>
          <FilterButton>{t("common.branch")}</FilterButton>
        </RightButtonWrapper>
      }
    >
      <Table
        columns={columns}
        // data={listReport}
        data={[]}
        totalSize={totalSize}
        sizePerPage={SIZE_PER_PAGE}
        onPageChange={handlePageChange}
        page={page}
        isRemote
      />
    </ListDataLayout>
  );
};

export default StaffReport;
