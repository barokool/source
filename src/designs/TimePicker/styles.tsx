import { formControlCommon } from "common/styles/FormControl";
import { Field } from "formik";
import styled from "styled-components/macro";
import tw from "twin.macro";

export const Container = styled.div`
  ${tw`w-full `}
`;

export const DateInputContainer = styled.label<{
  isError: boolean;
  disabled?: boolean;
}>`
  ${tw`grid items-center`}
  ${({ isError, disabled }) => formControlCommon(isError, disabled)}
  ${({ isError }) => isError && tw`text-black`}
  grid-template-columns: 1fr 24px;
`;

export const InputContainer = styled.div<{ isError: boolean }>`
  ${tw`w-full relative`}
  ${({ isError }) => (isError ? tw`text-danger` : tw`text-gray`)}
`;

export const FieldWrapper = styled.div`
  ${tw`relative flex flex-row items-center w-full `}
`;

export const InputField = styled(Field)<{ isError: boolean }>`
  ${tw` focus:outline-none`}
  ${({ isError, disabled }) => formControlCommon(isError, disabled)}
`;

export const IconWrapper = styled.div`
  ${tw`absolute cursor-pointer select-none right-1`}
`;
export const LabelWrapper = styled.div`
  ${tw`px-[4px] py-0 flex items-center justify-between absolute -top-2 left-[20px] z-10 bg-white rounded-sm`}
`;
export const EventChange = styled.div`
  ${tw`text-16 duration-300 cursor-default hover:text-gray text-primary`}
`;
