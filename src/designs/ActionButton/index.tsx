import Dropdown from "designs/Dropdown";
import SVG from "designs/SVG";
import { OptionDropdown } from "./styles";
import { ReactNode, useCallback, useState } from "react";
import AlertDialogV2 from "components/AlertDialogV2";

import { t } from "language";
import IconButton from "designs/IconButton";

type IActionType = "edit" | "delete" | "updateStatus" | "watch";

interface IActionButtonsProps {
  buttons: {
    delete?: {
      title: string;
      message: string;
      onDelete: () => void;
    };
    edit?: {
      DialogContent: React.FC<{ onClose: () => void }>;
    };
    updateStatus?: {
      DialogContent: React.FC<{ onClose: () => void }>;
    };
    watch?: {
      DialogContent: React.FC<{ onClose: () => void }>;
    };
  };
}

type IOption = {
  type: IActionType;
  iconName: string;
  name: string;
};

const options: {
  [k in IActionType]: IOption;
} = {
  updateStatus: {
    type: "updateStatus",
    iconName: "common/update",
    name: t("common.update-status"),
  },
  edit: {
    type: "edit",
    iconName: "common/edit",
    name: t("common.edit"),
  },
  delete: {
    type: "delete",
    iconName: "common/delete",
    name: t("common.delete"),
  },
  watch: {
    type: "watch",
    iconName: "common/edit",
    name: t("common.watch"),
  },
};

const ActionButtons: React.FC<IActionButtonsProps> = ({ buttons }) => {
  const [optionSelected, setOptionSelected] = useState<IOption | null>(null);
  const [loading, setLoading] = useState(false);
  const [selectedOptions] = useState(() => {
    const selectedOptions: IOption[] = [];

    for (let actionType in buttons) {
      selectedOptions.push(options[actionType as IActionType]);
    }
    return selectedOptions;
  });

  const handleClose = useCallback(() => {
    setOptionSelected(null);
  }, []);

  const handleOnClickEditButton = (option: IOption) => {
    setOptionSelected(option);
  };

  return (
    <>
      <div className="flex flex-shrink-0 flex-row items-center space-x-1.5 justify-end">
        {selectedOptions.map(option => {
          const { iconName, name, type } = option;
          return (
            <IconButton
              onClick={() => handleOnClickEditButton(option)}
              tooltip={name}
              key={name}
            >
              <SVG className="flex-shrink-0 w-2 h-2" name={iconName} />
            </IconButton>
          );
        })}
      </div>
      {buttons.delete && (
        <AlertDialogV2
          loading={loading}
          isOpen={optionSelected?.type === "delete"}
          onClose={() => setOptionSelected(null)}
          title={buttons.delete?.title || ""}
          message={buttons?.delete?.message}
          onConfirm={() => {
            try {
              setLoading(true);
              buttons.delete?.onDelete();
              setLoading(false);
              setOptionSelected(null);
            } catch (error) {
              setOptionSelected(null);
              setLoading(false);
            }
          }}
        />
      )}
      {optionSelected?.type === "edit" && buttons.edit && (
        <buttons.edit.DialogContent onClose={handleClose} />
      )}
      {optionSelected?.type === "updateStatus" && buttons.updateStatus && (
        <buttons.updateStatus.DialogContent onClose={handleClose} />
      )}
      {optionSelected?.type === "watch" && buttons.watch && (
        <buttons.watch.DialogContent onClose={handleClose} />
      )}
    </>
  );
};

export default ActionButtons;
