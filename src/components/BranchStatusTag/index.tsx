import { BranchStatus } from "apiCaller";
import { BranchStatusTagContainer } from "./styles";

interface ProductStatusTagProps {
  active: BranchStatus;
}

const BranchStatusTag: React.FC<ProductStatusTagProps> = ({
  active,
  children,
}) => {
  return (
    <BranchStatusTagContainer active={active}>
      {children}
    </BranchStatusTagContainer>
  );
};

export default BranchStatusTag;
