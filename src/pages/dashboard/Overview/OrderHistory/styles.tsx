import styled from "styled-components/macro";
import tw from "twin.macro";

export const OrderHistoryContainer = styled.div`
  ${tw`grid phone:grid-cols-2 grid-cols-1 gap-2 
  desktop:mt-4 laptop:mt-3 mt-2
  `}
`;
