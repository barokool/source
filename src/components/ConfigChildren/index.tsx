import React from "react";
import {
  ConfigChildrenContainer,
  ContentContainer,
  Description,
  Title,
} from "./styles";

export interface IChildrenConfig {
  Icon?: React.ReactNode;
  title?: string;
  description?: string;
  href?: string;
}

const ConfigChildren: React.FC<IChildrenConfig> = ({
  Icon,
  title,
  description,
  href,
}) => {
  return (
    <ConfigChildrenContainer>
      <div>{Icon}</div>
      <ContentContainer>
        <Title>{title}</Title>
        <Description>{description}</Description>
      </ContentContainer>
    </ConfigChildrenContainer>
  );
};

export default ConfigChildren;
