import { IRoute, IRoutes } from "typings";

import { rootRoute, authRoutes, notFoundRoute } from "./components/publics";
import { dashboardRoute } from "./components/dashboard";
import { productRoutes } from "./components/product";
import { orderServiceRoute } from "./components/orderService";
import {
  productReportRoutes,
  reportRoutes,
  sellingReportRoutes,
  customerReportRoutes,
  staffReportRoutes,
} from "./components/report";
import { sellingRoutes } from "./components/selling";
import {
  configurationRoute,
  branchManagementRoute,
  staffsManagementRoute,
  generalSettingRoute,
  customersManagementRoute,
  accountInfoRoute,
  roleDelegationRoute,
} from "./components/configuration";
import { couponRoutes } from "./components/coupon";
import { tableRoutes } from "./components/table";
import { shippingRoutes } from "./components/shipping";
import { orderRoutes } from "./components/order";
import * as listQrOrderRoutes from "./components/qrOrder";
const flattenRoutes = (routes: IRoutes[]): IRoute[] => {
  let flatRoutes: IRoute[] = [];
  routes = routes || [];
  routes.forEach(item => {
    flatRoutes.push(item);
    if (typeof item.children !== "undefined") {
      flatRoutes = [...flatRoutes, ...flattenRoutes(item.children)];
    }
  });
  return flatRoutes;
};

const dashboardRoutes: IRoutes[] = [
  dashboardRoute,
  // settingRoutes,
  // orderServiceRoute,
  shippingRoutes,
  orderRoutes,
  productRoutes,
  tableRoutes,
  couponRoutes,
  reportRoutes,
  configurationRoute,
  sellingRoutes,

  //hidden routes
  branchManagementRoute,
  staffsManagementRoute,
  accountInfoRoute,
  generalSettingRoute,
  customersManagementRoute,
  sellingReportRoutes,
  productReportRoutes,
  customerReportRoutes,
  staffReportRoutes,
  roleDelegationRoute,
];

const otherRoutes: IRoutes[] = [
  rootRoute,
  authRoutes,
  notFoundRoute,
  ...Object.values(listQrOrderRoutes),
];

const allRoutes = [...otherRoutes, ...dashboardRoutes];

const flattenedRoutes = flattenRoutes(allRoutes);

export { allRoutes, flattenedRoutes, notFoundRoute, dashboardRoutes };
