import styled from "styled-components/macro";
import tw from "twin.macro";

export const OrderLayoutContainer = styled.div`
  ${tw` flex justify-between`}
`;

export const RightSideContainer = styled.div`
  ${tw`w-7/12 ml-2 mt-3.5`}
`;

export const LeftSideContainer = styled.div`
  ${tw`w-5/12`}
`;
