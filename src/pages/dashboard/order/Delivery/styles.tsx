import Button from "designs/Button";
import styled from "styled-components/macro";
import tw from "twin.macro";
import _Pagination from "components/Pagination";

export const DeliveryOrderContainer = styled.div`
  ${tw`h-full`}
`;

export const DeleteButton = styled(Button)`
  ${tw`p-1 float-right mt-2`}
`;

export const Pagination = styled(_Pagination)`
  ${tw`desktop:mt-4 laptop:mt-3 mt-1 ml-auto mr-0`}
`;
