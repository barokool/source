import React, { useEffect, useState } from "react";
import OrderProcessingImage from "assets/images/selling/order-processing.png";
import { toast } from "react-toastify";
import PrintBillDialog from "pages/dashboard/selling/components/PrintBillDialog";

//redux
import { IRootState } from "typings";
import { useSelector, useDispatch } from "react-redux";
import { setActionSuccess } from "redux/slices/common";

//api
import {
  useUpdateOrder,
  Order,
  OrderStatusEnum,
  useGetOrderById,
} from "apiCaller";
import { fragmentUpdateOrder, fragmentGetOrderById } from "services/order";
import { setOrderSelected } from "redux/slices/order";

//languages
import { t } from "language";

//locals
import {
  RightProcessingContainer,
  CompleteButton,
  Content,
  Image,
  RightProcessingWrapper,
  HeaderContainer,
  OrderCodeTag,
  PrintBillButton,
} from "./styles";

interface IRightProcessing {}

const RightProcessing: React.FC<IRightProcessing> = props => {
  const { orderSelected } = useSelector(
    (rootState: IRootState) => rootState.order,
  );
  const dispatch = useDispatch();
  const [order, setOrder] = useState<Order | null>(null);
  const [updateOrder, { data: orderUpdated, loading: updateOrderLoading }] =
    useUpdateOrder(fragmentUpdateOrder);
  const [getOrderById] = useGetOrderById(fragmentGetOrderById);

  useEffect(() => {
    order && dispatch(setOrderSelected(order));
  }, [order]);

  useEffect(() => {
    if (orderUpdated) {
      toast.success(t("selling.complete-order-successfully"));
      setOrder(orderUpdated?.updateOrder as Order);
      handleRefreshOrder();
      dispatch(setActionSuccess());
    }
  }, [orderUpdated]);

  const handleCompleteOrder = () => {
    updateOrder({
      variables: {
        id: orderSelected?._id as string,
        input: {
          status: OrderStatusEnum.Complete,
        },
      },
    });
  };

  const handleRefreshOrder = () => {
    getOrderById({
      variables: { id: orderSelected?._id as string },
    });
  };

  return (
    <RightProcessingContainer>
      <HeaderContainer>
        <OrderCodeTag>#{orderSelected?.barcode}</OrderCodeTag>
        <PrintBillDialog
          ButtonMenu={
            <PrintBillButton>{t("selling.bill.print-bill")}</PrintBillButton>
          }
        />
      </HeaderContainer>
      <RightProcessingWrapper>
        <Image src={OrderProcessingImage} alt="order-processing" />
        <Content.Wrapper>
          <Content.Title>{t("selling.order-in-processing")}</Content.Title>
          <Content.Description>
            {t("selling.order-in-processing-message")}
          </Content.Description>
        </Content.Wrapper>
        <CompleteButton
          primary
          loading={updateOrderLoading}
          type="button"
          onClick={() => handleCompleteOrder()}
        >
          {t("selling.complete-order")}
        </CompleteButton>
      </RightProcessingWrapper>
    </RightProcessingContainer>
  );
};

export default RightProcessing;
