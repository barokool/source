import React, { useState, useEffect } from "react";
import OrderStatusTag from "components/OrderStatusTag";
import { OrderCardContainer } from "./styles";

import { t } from "language";
import { Order, OrderStatusEnum } from "apiCaller";

interface OrderServiceCardProps {
  order?: Order;
  isSelected?: boolean;
  onClick?: () => void;
}

const OrderCard: React.FC<OrderServiceCardProps> = ({
  order,
  isSelected,
  onClick,
}) => {
  return (
    <OrderCardContainer
      onClick={onClick}
      className={`${
        isSelected
          ? "border-dashed cursor-pointer border-secondary"
          : "border-transparent"
      }`}
    >
      <p className="text-13 text-secondary">{order?._id}</p>
      <p className="text-16 text-secondary font-semibold mt-1">{"User name"}</p>

      <div className="flex justify-between w-full mt-1">
        <p className="text-14 text-black">{"0819190221"}</p>
        <OrderStatusTag active={order?.status as OrderStatusEnum}>
          {order?.status === "new" && t("orderService.status.new")}
          {order?.status === "shipping" && t("orderService.status.shipping")}
          {order?.status === "complete" && t("orderService.status.complete")}
        </OrderStatusTag>
      </div>
    </OrderCardContainer>
  );
};

export default OrderCard;
