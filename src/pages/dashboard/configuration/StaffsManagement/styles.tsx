import tw from "twin.macro";
import styled from "styled-components/macro";
import _SearchBox from "components/SearchBoxTable";
import _Button from "designs/Button";

export const StaffsManagementContainer = styled.div`
  ${tw``}
`;

export const ActionWrapper = styled.div`
  ${tw`flex flex-col phone:flex-row justify-between mb-4`}
`;

export const SearchBox = styled(_SearchBox)`
  ${tw`w-full phone:w-55 phone:mb-0`}
`;

export const AddButton = styled(_Button)`
  ${tw`px-3.5 py-[12.5px] font-medium w-max`}
`;

export const Avatar = styled.img`
  ${tw`w-4 h-4 rounded-full`}
`;

export const ArrayFieldWrapper = styled.div`
  ${tw`flex flex-wrap max-w-sm`}
`;

export const ArrayFieldContent = styled.p`
  ${tw`desktop:mr-0.5`}
`;

export const RenderActionContainer = styled.div`
  ${tw`flex justify-end gap-2`}
`;
