import styled from "styled-components/macro";
import tw from "twin.macro";
import { Listbox } from "@headlessui/react";
import { formControlCommon } from "common/styles/FormControl";

export const SelectContainer = styled.div`
  ${tw`w-full relative `}
`;

export const HiddenInput = styled.input`
  ${tw`absolute w-1 h-1 opacity-0 `}
`;

export const ListboxButton = styled(Listbox.Button)`
  ${tw`relative w-full text-left cursor-pointer focus:outline-none`}
`;

export const ListboxOptionsContainer = styled(Listbox.Options)`
  ${tw`absolute z-30 w-full py-0.5 mt-1 overflow-auto bg-white  rounded-sm shadow-drop-down max-h-[300px] focus:outline-none border-gray text-left`}
`;

export const MenuButton = styled.div<{
  isError: boolean;
  disabled: boolean;
}>`
  ${tw`grid items-center`}
  ${({ isError, disabled }) => formControlCommon(isError, disabled)}
  grid-template-columns: 1fr 25px;
`;

export const Text = styled.p`
  ${tw`truncate`}
`;

export const Placeholder = styled.p`
  ${tw` text-gray`}
`;

export const MenuItem = styled.div<{ active?: boolean }>`
  ${tw` w-full px-1.5 py-0.5 text-14 font-regular cursor-pointer`}
  ${({ active }) => active && tw`bg-line`}
`;

export const LabelWrapper = styled.div`
  ${tw`px-[4px] py-0 flex items-center justify-between absolute -top-2 left-[20px] z-10 bg-white rounded-sm`}
`;
