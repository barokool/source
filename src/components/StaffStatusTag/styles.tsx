import { UserStatus } from "apiCaller";
import styled from "styled-components/macro";
import tw, { TwStyle } from "twin.macro";

const listStatus: {
  [k in UserStatus]: TwStyle;
} = {
  blocked: tw`bg-[#06083d0d] text-black`,
  isActive: tw`bg-[#ffc1071a] text-primary`,
  waitForApproval: tw`bg-danger text-white`,
};
export const StaffStatusTagContainer = styled.div<{
  active: UserStatus;
}>`
  ${tw`flex justify-center p-1 rounded-sm text-14 font-regular whitespace-nowrap w-max`}
  ${({ active }) => listStatus[active]}
`;
