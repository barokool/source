import styled from "styled-components/macro";
import tw from "twin.macro";
import { Form as _Form } from "formik";
import _Button from "designs/Button";

export const OTPFormContainer = styled.div`
  ${tw`flex items-center`}
`;

export const Form = styled(_Form)`
  ${tw``}
`;

export const PhoneValidate = styled.p`
  ${tw`text-16 text-black font-medium text-center mb-0.5`}
`;

export const Instruction = styled.p`
  ${tw`text-secondary mb-2 font-semibold text-12 italic`}
`;

export const Notification = styled.p`
  ${tw`text-14 text-black text-left mt-2 mb-3`}
`;

export const PhoneNumber = styled.span`
  ${tw`font-bold text-primary`}
`;

export const CountDown = styled.span`
  ${tw`font-bold text-primary`}
`;

export const Resend = styled(_Button)`
  ${tw`w-max px-2 py-1 bg-white text-secondary border-solid`}
`;

export const Button = styled(_Button)`
  ${tw`w-full px-2 py-1 justify-center bg-secondary border-transparent text-center`}
`;

export const ButtonWrapper = styled.div`
  ${tw`w-max grid grid-cols-2 gap-x-1 justify-end mt-3`}
`;
