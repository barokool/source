import React, { useCallback, useEffect, useMemo, useState } from "react";
import { RouteComponentProps } from "react-router";
import { toast } from "react-toastify";

import { getQueryFromLocation } from "common/functions";
import { PATH } from "common/constants/routes";
import StaffsManagementDialog from "./StaffsManagementDialog";

import { usePage } from "hooks/usePage";
import { useBreadcrumb } from "hooks/useBreadcrumb";
import { useLoading } from "hooks/useLoading";
import useAuth from "hooks/useAuth";

import StaffStatusTag from "components/StaffStatusTag";
import Table, { IColumns } from "designs/Table";
import { SearchBox, AddButton } from "./styles";

import ListDataLayout from "layouts/ListDataLayout";
import i18n, { t } from "language";
import ActionButtons from "designs/ActionButton";

import {
  Branch,
  Role,
  useDeleteUser,
  useGetAllUser,
  User,
  UserStatus,
} from "apiCaller";
import { fragmentGetAllUser } from "services/user";
import { useDebouncedState } from "hooks/useDebouncedState";

const SIZE_PER_PAGE = 10;
const LOAD_DATA = "LOAD_DATA";
const DEBOUNCE_TIME = 300;

interface IStaffManagementProps extends RouteComponentProps {}
interface ISearchDependencies {
  branchSelected: Branch | null;
  text: string;
}

const StaffManagementPage: React.FC<IStaffManagementProps> = ({ location }) => {
  const { startLoading, stopLoading } = useLoading();
  const { accountInfo } = useAuth();
  const [page, setPage] = usePage(getQueryFromLocation(location)?.page);

  const [getAllUser, { data, called, error }] =
    useGetAllUser(fragmentGetAllUser);

  const [deleteUser, { data: deleteData, error: deleteError }] =
    useDeleteUser();
  const { results, totalCount } = data?.getAllUser || {};

  const [search, setSearch] = useDebouncedState<ISearchDependencies>(
    {
      branchSelected: null,
      text: "",
    },
    objectSearch => {
      invokeGetAllDataAPI(objectSearch.text);
    },
    DEBOUNCE_TIME,
  );

  const invokeGetAllDataAPI = (search: string = "") => {
    try {
      startLoading(LOAD_DATA);
      getAllUser({
        variables: {
          filterUser: {
            branches: [accountInfo.userInfo?.branches?.[0]._id as string],
            displayName: search,
          },
          size: SIZE_PER_PAGE,
          page: page - 1,
        },
      });
    } catch (error) {
    } finally {
      stopLoading(LOAD_DATA);
    }
  };
  useBreadcrumb([
    {
      name: t("configuration.configuration"),
      href: PATH.CONFIGURATION.SELF,
    },
    { name: t("configuration.staff-management.staff-management") },
  ]);

  useEffect(() => {
    invokeGetAllDataAPI();
  }, [page]);

  useEffect(() => {
    invokeGetAllDataAPI();
  }, []);

  useEffect(() => {
    if (deleteData && !deleteError) {
      toast.success(t("common.delete-success"));
      invokeGetAllDataAPI();
    }
    if (!deleteData && deleteError) {
      toast.error(t("common.delete-fail"));
    }
  }, [deleteError, deleteData]);

  const handleChangePage = useCallback((nextPage: number) => {
    setPage(nextPage);
  }, []);

  const handleDelete = (record: User) => {
    try {
      deleteUser({
        variables: {
          id: record?._id as string,
        },
      });
      startLoading(LOAD_DATA);
    } catch (error) {
      console.log(error);
    } finally {
      stopLoading(LOAD_DATA);
      invokeGetAllDataAPI();
    }
  };

  const renderAction = (record: User) => {
    return (
      <ActionButtons
        buttons={{
          edit: {
            DialogContent: ({ onClose }) => {
              return (
                <StaffsManagementDialog
                  editField={record}
                  onSuccess={() => invokeGetAllDataAPI()}
                  onClose={onClose}
                  title={t("common.edit")}
                  open
                  ButtonMenu={<></>}
                />
              );
            },
          },
          delete: {
            title: t("configuration.staff-management.delete"),
            message: t("configuration.staff-management.confirm-delete"),
            onDelete: () => handleDelete(record),
          },
        }}
      />
    );
  };

  const columns: IColumns = useMemo(
    () => [
      {
        text: t("configuration.staff-management.name"),
        dataField: "displayName",
      },
      {
        text: t("configuration.staff-management.staff-phone-number"),
        dataField: "phoneNumber",
      },
      {
        text: t("configuration.staff-management.role"),
        dataField: "role",
        formatter: (role: Role) => role?.position,
      },
      {
        text: t("configuration.staff-management.branch"),
        dataField: "branches",
        formatter: (branches: Branch[]) => (
          <div>
            {branches?.map(branch => (
              <p className="ml-1">{branch?.name}</p>
            ))}
          </div>
        ),
      },
      {
        text: t("configuration.staff-management.statuss"),
        dataField: "status",
        formatter: (status: UserStatus) => (
          <>
            {status === "blocked" && (
              <StaffStatusTag active={status}>
                {status === "blocked" &&
                  t("configuration.staff-management.status.blocked")}
              </StaffStatusTag>
            )}
            {status === "isActive" && (
              <StaffStatusTag active={status}>
                {status === "isActive" &&
                  t("configuration.staff-management.status.isActive")}
              </StaffStatusTag>
            )}
          </>
        ),
      },
      {
        text: t("configuration.general-settings.actions"),
        dataField: "",
        formatter: (_: string, record: User) => renderAction(record),
      },
    ],
    [page, i18n.language],
  );

  return (
    <ListDataLayout
      title={t("configuration.staff-management.staff")}
      leftTopBar={
        <SearchBox
          onFetchData={text =>
            setSearch({
              ...search,
              text,
            })
          }
          placeholder={t("configuration.staff-management.search-by-staff-name")}
        ></SearchBox>
      }
      rightTopBar={
        <StaffsManagementDialog
          onSuccess={() => {
            invokeGetAllDataAPI();
          }}
          title={t("configuration.staff-management.add-new-staff")}
          ButtonMenu={
            <AddButton className="w-1/5" primary>
              {t("configuration.staff-management.add-new-staff")}
            </AddButton>
          }
        />
      }
    >
      <Table
        columns={columns}
        data={results || []}
        totalSize={totalCount as number}
        sizePerPage={SIZE_PER_PAGE}
        onPageChange={handleChangePage}
        page={page}
        isRemote
      />
    </ListDataLayout>
  );
};

export default StaffManagementPage;
