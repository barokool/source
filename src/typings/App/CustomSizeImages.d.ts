export interface IImageSizeInput {
  width?: number;
  height?: number | null;
}
export interface IImage {
  default?: string;
  small?: string;
  medium?: string;
}
export interface IImageInput {
  small?: IImageSizeInput;
  medium?: IImageSizeInput;
}
