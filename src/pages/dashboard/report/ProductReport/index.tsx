import React, { useState, useEffect, useMemo, useCallback } from "react";

import { useLoading } from "hooks/useLoading";
import { usePage } from "hooks/usePage";
import { useBreadcrumb } from "hooks/useBreadcrumb";
import useAuth from "hooks/useAuth";

import Table, { IColumns } from "designs/Table";
import TableLayout from "layouts/Table";
import AlertDialog from "components/AlertDialog";
import SVG from "designs/SVG";
import ListDataLayout from "layouts/ListDataLayout";

import { RouteComponentProps } from "react-router";
import {
  ProductReportContainer,
  SearchBox,
  RightButtonWrapper,
  FilterButton,
} from "./styles";

import { PATH } from "common/constants/routes";
import { getQueryFromLocation } from "common/functions";
import { t } from "language";

interface IProductReportProps extends RouteComponentProps {}

const SIZE_PER_PAGE = 10;
const LOAD_DATA = "LOAD_DATA";

const ProductReport: React.FC<IProductReportProps> = ({ location }) => {
  const [page, setPage] = usePage(getQueryFromLocation(location)?.page);
  const { startLoading, stopLoading } = useLoading();
  const [search, setSearch] = useState<string>("");
  // const [listReport, setListReport] = useState<IProductReport[]>([]);
  const [totalSize, setTotalSize] = useState<number>(0);
  const { accountInfo } = useAuth();

  useBreadcrumb([
    {
      name: t("report.report"),
      href: PATH.CONFIGURATION.SELF,
    },
  ]);

  useEffect(() => {
    invokeGetAllDataAPI();
  }, [page, search]);

  const invokeGetAllDataAPI = async () => {
    try {
      startLoading(LOAD_DATA);
      // const result = await getProductReportAPI({
      //   branch: accountInfo.userInfo?.branches?.[0]._id as string,
      //   filterDayReport: "",
      // });
      // setListReport(result);
      // setTotalSize(result.length);
    } catch (error) {
      console.log(error);
    } finally {
      stopLoading(LOAD_DATA);
    }
  };

  const onSearch = (text: string) => setSearch(text);

  const handleChangePage = useCallback((nextPage: number) => {
    setPage(nextPage);
  }, []);

  const columns: IColumns = useMemo(
    () => [
      {
        text: t("report.product-report.code"),
        dataField: "code",
      },
      {
        text: t("report.product-report.name"),
        dataField: "name",
      },
      {
        text: t("report.product-report.revenue"),
        dataField: "totalMoney",
      },
      {
        text: "",
        dataField: "actions",
        headerStyle: () => ({ display: "none" }),
        formatter: (_: string) => <div className="w-0"></div>,
      },
    ],
    [page],
  );

  return (
    <ProductReportContainer>
      <ListDataLayout
        rightTopBar={
          <RightButtonWrapper>
            <FilterButton>{t("common.this-month")}</FilterButton>
            <FilterButton>{t("common.this-week")} </FilterButton>
          </RightButtonWrapper>
        }
        leftTopBar={
          <SearchBox
            onFetchData={onSearch}
            placeholder={t("report.customer-report.search-by-customer-name")}
          ></SearchBox>
        }
        title={t("report.product-report.product-report")}
      >
        <Table
          columns={columns}
          // data={listReport}
          data={[]}
          totalSize={totalSize}
          sizePerPage={SIZE_PER_PAGE}
          onPageChange={handleChangePage}
          page={page}
          isRemote
        />
      </ListDataLayout>
    </ProductReportContainer>
  );
};

export default ProductReport;
