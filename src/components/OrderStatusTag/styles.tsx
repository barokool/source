import { OrderStatusEnum } from "apiCaller";
import styled from "styled-components/macro";
import tw, { TwStyle } from "twin.macro";

const listStatus: {
  [k in OrderStatusEnum]: TwStyle;
} = {
  processing: tw`text-[#F88F01]`,
  shipping: tw`text-[#F88F01]`,
  canceled: tw`text-[#CE1611]`,
  complete: tw`text-[#007637]`,
  new: tw`text-[#007637]`,
  approving: tw`text-[#007637]`,
  ready: tw`text-[#007637]`,
};
export const OrdertatusTagContainer = styled.div<{
  active: OrderStatusEnum;
}>`
  ${tw` text-14 font-regular whitespace-nowrap w-max`}
  ${({ active }) => listStatus[active]}
`;
