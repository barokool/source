import InPlaceRight from "pages/dashboard/selling/Room/InPlace/Right";
import TakeAwayRight from "pages/dashboard/selling/Room/TakeAway/Right";
import DeliveryRight from "pages/dashboard/selling/Room/Delivery/Right";
import BookingTableRight from "pages/dashboard/selling/Room/BookingTable/Right";

//api
import { OrderTypeEnum } from "apiCaller";

//redux
import { useSelector } from "react-redux";
import { IRootState } from "redux/storeToolkit";

//locals
import { MenuRightContainer } from "./styles";

interface IMenuRightProps {}

const MenuRight: React.FC<IMenuRightProps> = props => {
  const { orderSelected } = useSelector((state: IRootState) => state.order);
  return (
    <MenuRightContainer>
      {orderSelected?.type === OrderTypeEnum.In_place && <InPlaceRight />}
      {orderSelected?.type === OrderTypeEnum.Take_away && <TakeAwayRight />}
      {orderSelected?.type === OrderTypeEnum.Delivery && <DeliveryRight />}
      {orderSelected?.type === OrderTypeEnum.Booking_table && (
        <BookingTableRight />
      )}
    </MenuRightContainer>
  );
};

export default MenuRight;
