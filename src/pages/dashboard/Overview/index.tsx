import React, { useEffect, useState } from "react";
import { PATH } from "common/constants/routes";
import SVG from "designs/SVG";

//hooks
import useAuth from "hooks/useAuth";
import { useRedirect } from "hooks/useRedirect";
import { useBreadcrumb } from "hooks/useBreadcrumb";
import { useCurrentBranch } from "hooks/useCurrentBranch";
import { useLoading } from "hooks/useLoading";

//locals
import Statistic from "./Statistic";
import OrderHistory from "./OrderHistory";

//languages
import { t } from "language";

//api
import { Dashboard, Order, useGetDashboard } from "apiCaller";
import { fragmentGetDashBoard } from "services/dashboard";

//locals
import { Heading, OverviewContainer } from "./styles";

const LOAD_DATA = "LOAD_DATA";

const Overview: React.FC = () => {
  const redirect = useRedirect();
  const { accountInfo } = useAuth();
  const { branchSelected } = useCurrentBranch();
  const { startLoading, stopLoading } = useLoading();

  //locals states
  const [dashBoardData, setDashBoardData] = useState<Dashboard | null>(null);
  const [getDashboard, { data, loading }] =
    useGetDashboard(fragmentGetDashBoard);

  useBreadcrumb([
    {
      name: t("dashboard.dashboard"),
      href: "",
    },
    {
      name: t("dashboard.overview"),
      href: PATH.DASHBOARD,
    },
  ]);

  useEffect(() => {
    branchSelected && invokeGetDataAPI();
  }, [branchSelected]);

  useEffect(() => {
    data && setDashBoardData(data.getDashboard);
  }, [data]);

  useEffect(() => {
    loading ? startLoading(LOAD_DATA) : stopLoading(LOAD_DATA);
  }, [loading]);

  const invokeGetDataAPI = () => {
    getDashboard({
      variables: { idBranch: branchSelected?._id || "" },
    });
  };

  const onHandleClick = () => {
    redirect(PATH.SELLING.SELF);
  };

  return (
    <OverviewContainer>
      <Heading.Wrapper>
        <Heading.TextWrapper>
          <Heading.Title>
            {t("common.hello")} {accountInfo?.userInfo?.displayName}
          </Heading.Title>
          <Heading.Desc>{t("common.greeting")}</Heading.Desc>
        </Heading.TextWrapper>
        <SVG
          name="dashboard/store"
          height={47}
          width={47}
          className="cursor-pointer"
          onClick={onHandleClick}
        />
      </Heading.Wrapper>
      <Statistic
        revenue={dashBoardData?.revenueTotal || 0}
        inPlace={dashBoardData?.totalOrderInPlace || 0}
        delivery={dashBoardData?.totalOrderDelivery || 0}
        orderTable={dashBoardData?.totalOrderBookingTable || 0}
      />
      <OrderHistory
        recentDelivery={dashBoardData?.recentOrderDelivery as Order[]}
        recentOrder={dashBoardData?.recentOrderBookingTable as Order[]}
      />
    </OverviewContainer>
  );
};

export default Overview;
