import styled from "styled-components/macro";
import tw from "twin.macro";
import _Button from "designs/Button";

export const RightCompletedContainer = styled.div`
  ${tw`w-full h-screen shadow-lg flex flex-col`}
`;

export const RightCompleteWrapper = styled.div`
  ${tw`my-auto`}
`;

export const AddButton = styled(_Button)`
  ${tw`mt-4 text-center rounded-md mx-auto px-2 py-1`}
`;

export const Image = styled.img`
  ${tw`mx-auto w-30 h-25`}
`;

export const Content = {
  Wrapper: styled.div`
    ${tw`mt-4 w-full`}
  `,
  Title: styled.p`
    ${tw`text-black text-20 font-semibold text-center`}
  `,
  Description: styled.p`
    ${tw`text-black text-16 text-center`}
  `,
};
