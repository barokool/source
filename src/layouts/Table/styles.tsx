import styled from "styled-components/macro";
import tw from "twin.macro";

export const TableLayoutContainer = styled.div`
  ${tw`flex flex-col gap-3 pb-4 pt-2`}
`;

export const SearchBoxWrapper = styled.div`
  ${tw``}
`;

export const TableWrapper = styled.div`
  ${tw``}
`;
