import React from "react";
import { PATH } from "common/constants/routes";

//hooks
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { useBreadcrumb } from "hooks/useBreadcrumb";

import { t } from "language";

//styles
import { AdminContainer } from "./styles";

interface IAdminProps {}

const Admin: React.FC<IAdminProps> = props => {
  const dispatch = useDispatch();

  useEffect(() => {
    setUpBreadcrumb();
  }, []);

  const setUpBreadcrumb = () => {
    dispatch(
      useBreadcrumb([
        {
          name: t("account.admin.account"),
          href: PATH.DASHBOARD,
        },
        {
          name: t("account.admin.account"),
        },
      ]),
    );
  };
  return <AdminContainer>{t("account.admin.admin-page")}</AdminContainer>;
};

export default Admin;
