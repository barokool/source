import styled from "styled-components/macro";
import tw from "twin.macro";
import _Button from "designs/Button";
import _SVG from "designs/SVG";

export const UserInfoContainer = styled.div`
  ${tw`relative flex items-center gap-2`}
`;

export const ButtonContainer = styled(_Button)`
  ${tw`flex justify-end items-center px-1 py-0.5`}
`;

export const AvatarWrapper = styled.div`
  ${tw`w-3 h-3 laptop:w-4 laptop:h-4 rounded-full overflow-hidden`}
`;

export const Avatar = styled.img`
  ${tw`w-full h-full`}
`;

export const Name = styled.p`
  ${tw`text-black text-14 laptop:text-16 font-medium whitespace-nowrap`}
`;

export const Icon = styled(_SVG)`
  ${tw`w-2 h-2 duration-200`}
`;

export const Dropdown = {
  Container: styled.ul<{ isVisible?: boolean }>`
    ${tw`absolute phone:right-0 bg-white p-1 rounded-sm shadow-md z-50 duration-200 w-max text-black`}
    ${({ isVisible }) =>
      isVisible ? tw`visible opacity-100 top-6` : tw`invisible opacity-0 top-4`}
  `,
  Item: styled.li``,
  Button: styled(_Button)`
    ${tw`flex items-center px-1 py-0.5 w-full text-14 whitespace-nowrap hover:bg-primary hover:bg-opacity-10`}
  `,
  Icon: styled(_SVG)`
    ${tw`w-2 h-2 inline-block mr-1`}
  `,
};

export const ElementWrapper = styled.div`
  ${tw`w-full`}
`;
export const Address = styled.p`
  ${tw`text-14 text-black font-regular`}
`;
