import React, { useState, useEffect, useCallback, useMemo, lazy } from "react";
import { RouteComponentProps } from "react-router";

import { usePage } from "hooks/usePage";
import { useBreadcrumb } from "hooks/useBreadcrumb";
import { useLoading } from "hooks/useLoading";

import { getQueryFromLocation } from "common/functions";
import { PATH } from "common/constants/routes";
import EmptyImage from "assets/images/product/ProductListEmpty.png";

import Table, { IColumns } from "designs/Table";
import { SearchBox, AddButton } from "./styles";
import ActionButtons from "designs/ActionButton";

import ListDataLayout from "layouts/ListDataLayout";
import { t } from "language";
import {
  RoleEnum,
  useDeleteUser,
  useGetAllUser,
  User,
  UserRanking,
} from "apiCaller";
import { fragmentGetAllUser } from "services/user";
import { toast } from "react-toastify";
import Empty from "components/Empty";
const CustomersManagementDialog = lazy(
  () => import("./CustomersManagementDialog"),
);
const LOAD_DATA = "LOAD_DATA";
const SIZE_PER_PAGE = 10;

const CustomersManagementPage: React.FC<RouteComponentProps> = ({
  location,
}) => {
  const { startLoading, stopLoading } = useLoading();
  const [page, setPage] = usePage(getQueryFromLocation(location)?.page);
  const [getAllUser, { data: getData, error: getError }] =
    useGetAllUser(fragmentGetAllUser);
  const [deleteUser, { data: deleteData, error: deleteError }] =
    useDeleteUser();
  const { results, totalCount } = getData?.getAllUser || {};

  useBreadcrumb([
    {
      name: t("configuration.configuration"),
      href: PATH.CONFIGURATION.SELF,
    },
    {
      name: t("configuration.customer-management.customer-management"),
    },
  ]);

  useEffect(() => {
    invokeGetAllDataAPI();
  }, [page]);

  useEffect(() => {
    if (!getData && getError) {
      toast.error(t("common.error"));
    }
  }, [getData, getError]);

  useEffect(() => {
    if (deleteData && deleteError) {
      toast.success(t("common.delete-success"));
    }
    if (!deleteData && deleteError) {
      toast.error("You dont have authorize to delete");
    }
  }, [deleteData, deleteError]);

  const invokeGetAllDataAPI = (searchText: string = "") => {
    try {
      startLoading(LOAD_DATA);
      getAllUser({
        variables: {
          page: page - 1,
          filterUser: {
            role: "CUSTOMER" as RoleEnum,
            displayName: searchText,
          },
        },
      });
    } catch (error) {
    } finally {
      stopLoading(LOAD_DATA);
    }
  };

  const handleChangePage = useCallback((nextPage: number) => {
    setPage(nextPage);
  }, []);

  const renderAction = (record: User) => {
    return (
      <ActionButtons
        buttons={{
          watch: {
            DialogContent: ({ onClose }) => {
              return (
                <CustomersManagementDialog
                  editField={record}
                  onSuccess={() => invokeGetAllDataAPI()}
                  onClose={onClose}
                  title={t("common.watch")}
                  open
                />
              );
            },
          },
          delete: {
            title: t("configuration.staff-management.delete"),
            message: t("configuration.staff-management.confirm-delete"),
            onDelete: () => {
              deleteUser({
                variables: {
                  id: record._id as string,
                },
              });
              invokeGetAllDataAPI();
            },
          },
        }}
      />
    );
  };

  const columns: IColumns = useMemo(
    () => [
      {
        text: t("configuration.customer-management.customer-code"),
        dataField: "customerCode",
      },
      {
        text: t("configuration.customer-management.name"),
        dataField: "displayName",
      },
      {
        text: t("configuration.customer-management.customer-group"),
        dataField: "ranking",
        formatter: (rank: UserRanking) => (
          <>
            {rank === UserRanking.Bronze && (
              <h1>
                {UserRanking.Bronze.toString().charAt(0).toUpperCase() +
                  UserRanking.Bronze.slice(1)}
              </h1>
            )}
            {rank === UserRanking.Diamond && (
              <h1>
                {UserRanking.Diamond.toString().charAt(0).toUpperCase() +
                  UserRanking.Diamond.slice(1)}
              </h1>
            )}
            {rank === UserRanking.Sliver && (
              <h1>
                {UserRanking.Sliver.toString().charAt(0).toUpperCase() +
                  UserRanking.Sliver.slice(1)}
              </h1>
            )}
            {rank === UserRanking.Gold && (
              <h1>
                {UserRanking.Gold.toString().charAt(0).toUpperCase() +
                  UserRanking.Gold.slice(1)}
              </h1>
            )}
          </>
        ),
      },
      {
        text: t("configuration.customer-management.email"),
        dataField: "email",
      },
      {
        text: t("configuration.customer-management.phone-number"),
        dataField: "phoneNumber",
        headerStyle: () => ({ width: "150px" }),
      },
      {
        text: t("common.actions"),
        dataField: "actions",
        formatter: (_: string, record: User) => renderAction(record),
      },
    ],
    [page],
  );

  return totalCount === 0 ? (
    <Empty
      SVG={<img src={EmptyImage} width={200} height={200} />}
      Title={t("configuration.customer-management.empty.title")}
      SubTitle={t("configuration.customer-management.empty.subtitle")}
      ButtonMenu={
        <CustomersManagementDialog
          onSuccess={() => {
            invokeGetAllDataAPI();
          }}
          title={t("configuration.customer-management.add")}
          ButtonMenu={
            <AddButton className="w-1/5">
              {t("configuration.customer-management.add")}
            </AddButton>
          }
          isAdd
        />
      }
    />
  ) : (
    <ListDataLayout
      leftTopBar={
        <SearchBox
          onFetchData={invokeGetAllDataAPI}
          placeholder={t(
            "configuration.customer-management.search-by-customer-name",
          )}
        ></SearchBox>
      }
      rightTopBar={
        <CustomersManagementDialog
          onSuccess={() => {
            invokeGetAllDataAPI();
          }}
          title={t("configuration.customer-management.add")}
          ButtonMenu={
            <AddButton className="w-1/5">
              {t("configuration.customer-management.add")}
            </AddButton>
          }
          isAdd
        />
      }
      title={t("configuration.customer-management.customer")}
    >
      <Table
        columns={columns}
        data={results || []}
        totalSize={totalCount as number}
        sizePerPage={SIZE_PER_PAGE}
        onPageChange={handleChangePage}
        page={page}
        isRemote
      />
    </ListDataLayout>
  );
};

export default CustomersManagementPage;
