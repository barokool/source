import styled from "styled-components/macro";
import tw from "twin.macro";

export const SingleImageUploaderContainer = styled.div`
  ${tw``}
`;

export const HiddenInput = styled.input`
  ${tw`absolute w-1 h-1 opacity-0`}
`;

export const ImageUploadContainer = styled.div<{ isError: boolean }>`
  ${tw`
   border-dashed border rounded-sm p-1.5 cursor-pointer w-full min-h-[150px] flex justify-center items-center focus:outline-none`}
  ${({ isError }) =>
    isError ? tw`border-danger text-danger` : tw` border-gray text-gray`}
`;

export const PreviewImage = styled.img`
  ${tw` block w-auto m-auto h-10 `}
`;

export const SkeletonContainer = styled.div`
  ${tw` text-center flex flex-col items-center`}
`;

export const SkeletonMessage = styled.p`
  ${tw` mt-1 text-12 font-semibold`}
`;
