import React, { useState, useEffect } from "react";

//redux
import { useSelector } from "react-redux";
import { IRootState } from "redux/storeToolkit";

//api
import { OrderStatusEnum, OrderTypeEnum } from "apiCaller";

//locals
import { DeliveryRightContainer } from "./styles";
import RightCheckOut from "./CheckOut";
import RightProcessing from "./Processing";
import RightPayment from "./Payment";
import RightCompleted from "./Completed";
import RightShipping from "./Shipping";
import Empty from "./Empty";

interface IDeliveryRightProps {}

const DeliveryRight: React.FC<IDeliveryRightProps> = props => {
  const { orderSelected } = useSelector((state: IRootState) => state.order);
  const [initComponent, setInitComponent] = useState<React.ReactNode>(<></>);

  useEffect(() => {
    if (orderSelected?.type === OrderTypeEnum.Booking_table) {
      if (orderSelected.orderItems?.length === 0) setInitComponent(<Empty />);
      else {
        switch (orderSelected?.status) {
          case OrderStatusEnum.New:
            if ((orderSelected?.orderItems?.length as number) > 0)
              setInitComponent(<RightCheckOut />);
            break;
          case OrderStatusEnum.Approving:
            setInitComponent(<RightPayment />);
            break;
          case OrderStatusEnum.Processing:
            setInitComponent(<RightProcessing />);
            break;
          case OrderStatusEnum.Complete:
            setInitComponent(<RightCompleted />);
            break;
          case OrderStatusEnum.Shipping:
            setInitComponent(<RightShipping />);
        }
      }
    }
  }, [orderSelected]);

  return <DeliveryRightContainer>{initComponent}</DeliveryRightContainer>;
};

export default DeliveryRight;
