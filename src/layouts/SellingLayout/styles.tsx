import styled from "styled-components/macro";
import tw from "twin.macro";

export const SellingLayoutContainer = styled.div`
  ${tw`overflow-hidden`}
`;
