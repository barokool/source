import { useState, useCallback, useMemo, useEffect } from "react";
import { toast } from "react-toastify";
import { PATH } from "common/constants/routes";
import { getQueryFromLocation } from "common/functions/route";
import { RouteComponentProps } from "react-router";
import Table, { IColumns } from "designs/Table";
import SVG from "designs/SVG";
import ActionButtons from "designs/ActionButton";
import Empty from "components/Empty";
import ProductStatusTag from "components/ProductStatusTag";
import ListDataLayout from "layouts/ListDataLayout";
import EmptyImage from "assets/images/product/ProductListEmpty.png";

//hooks
import { usePage } from "hooks/usePage";
import { useBreadcrumb } from "hooks/useBreadcrumb";
import { useCurrentBranch } from "hooks/useCurrentBranch";
import { useLoading } from "hooks/useLoading";
import { useDebouncedState } from "hooks/useDebouncedState";

//redux
import { useSelector } from "react-redux";
import { IRootState } from "typings";

//languages
import i18n, { t } from "language";

//api
import {
  Product,
  ProductStatus,
  ProductCategory,
  Tag,
  useGetAllProduct,
  useDeleteProduct,
  Branch,
  Topping,
} from "apiCaller";
import { fragmentGetAllProduct } from "services/product";

//locals
import {
  Button,
  Description,
  RightTopBarWrapper,
  SearchBox,
  TagContent,
  TagWrapper,
  ProductImage,
} from "./styles";
import ProductListDialog from "./Dialog";
import UpdateStatusDialog from "./UpdateStatus";
import GroupPrintDialog from "./GroupPrintDialog";

const SIZE_PER_PAGE = 10;
const LOAD_DATA = "LOAD_DATA";
const DEBOUNCE_TIME = 1000;

interface ISearchDependencies {
  branchSelected: Branch | null;
  text: string;
}

interface IListProps extends RouteComponentProps {}

const ProductList: React.FC<IListProps> = ({ location }) => {
  const { branchSelected } = useCurrentBranch();
  const { languageSelected } = useSelector(
    (state: IRootState) => state.language,
  );
  const { startLoading, stopLoading } = useLoading();

  //local state
  const [listProduct, setListProduct] = useState<Product[]>([]);
  const [page, setPage] = usePage(getQueryFromLocation(location)?.page);
  const [totalSize, setTotalSize] = useState(0);
  const [actionSuccess, setActionSuccess] = useState<boolean>(false);

  const [search, setSearch] = useDebouncedState<ISearchDependencies>(
    {
      branchSelected: null,
      text: "",
    },
    objectSearch => {
      if (objectSearch.branchSelected) {
        invokeGetAllDataAPI(objectSearch.text, objectSearch.branchSelected);
      }
    },
    DEBOUNCE_TIME,
  );

  //api
  const [
    getAllProduct,
    {
      data: getAllProductData,
      error: getAllProductErrors,
      loading: getAllProductLoading,
    },
  ] = useGetAllProduct(fragmentGetAllProduct);
  const [
    deleteProduct,
    {
      loading: deleteProductLoading,
      error: deleteProductErrors,
      data: deleteProductData,
    },
  ] = useDeleteProduct();

  useBreadcrumb([
    {
      name: t("product.product"),
    },
    {
      name: t("product.product-list.product-list"),
      href: PATH.PRODUCT.LIST,
    },
  ]);

  useEffect(() => {
    branchSelected && invokeGetAllDataAPI("", branchSelected);
  }, [page, branchSelected]);

  useEffect(() => {
    if (actionSuccess === true && branchSelected) {
      invokeGetAllDataAPI("", branchSelected);
    }
    setActionSuccess(false);
  }, [actionSuccess]);

  useEffect(() => {
    if (getAllProductLoading || deleteProductLoading) {
      startLoading(LOAD_DATA);
    } else {
      stopLoading(LOAD_DATA);
    }
  }, [getAllProductLoading, deleteProductLoading]);

  useEffect(() => {
    branchSelected &&
      setSearch({
        ...search,
        branchSelected,
      });
  }, [branchSelected]);

  useEffect(() => {
    if (getAllProductData) {
      setListProduct(getAllProductData.getAllProduct.results || []);
      setTotalSize(getAllProductData.getAllProduct.totalCount as number);
    }
  }, [getAllProductData]);

  //toast errors when they occur
  useEffect(() => {
    getAllProductErrors && toast.error(getAllProductErrors.message);
  }, [getAllProductErrors]);

  useEffect(() => {
    if (deleteProductData && !deleteProductErrors) {
      setActionSuccess(true);
      toast.success(t("product.product-list.delete-success"));
    }
  }, [deleteProductData]);

  const invokeGetAllDataAPI = (searchText: string = "", branch: Branch) => {
    getAllProduct({
      variables: {
        page: page - 1,
        size: SIZE_PER_PAGE,
        filterProduct: {
          branch: branch._id as string,
          name: searchText,
        },
      },
    });
  };

  const onPageChange = useCallback((nextPage: number) => {
    setPage(nextPage);
  }, []);

  const renderAction = (record: Product) => {
    return (
      <ActionButtons
        buttons={{
          updateStatus: {
            DialogContent: ({ onClose }) => {
              return (
                <UpdateStatusDialog
                  onClose={onClose}
                  open
                  editField={record}
                  title={t("product.product-list.update-status")}
                  onSuccess={() => setActionSuccess(true)}
                />
              );
            },
          },
          edit: {
            DialogContent: ({ onClose }) => {
              return (
                <ProductListDialog
                  onClose={onClose}
                  open
                  editField={record}
                  onSuccess={() => setActionSuccess(true)}
                  title={t("product.product-list.edit-product")}
                />
              );
            },
          },
          delete: {
            title: t("product.product-list.delete-product"),
            message: t("product.product-list.confirm-delete-product"),
            onDelete: () => {
              deleteProduct({
                variables: {
                  listId: [record._id as string],
                },
              });
              setActionSuccess(true);
            },
          },
        }}
      />
    );
  };

  const columns: IColumns = useMemo(
    () => [
      {
        text: t("product.product-list.image"),
        dataField: "",
        formatter: (_: string, record: Product) => (
          <ProductImage
            src={
              (record.thumbnail?.small as string) ||
              (record.thumbnail?.medium as string) ||
              (record.thumbnail?.default as string)
            }
            width={50}
            height={50}
            alt="product-image"
          />
        ),
      },
      {
        text: t("product.product-list.name"),
        dataField: "name",
      },
      {
        text: t("product.product-list.category"),
        dataField: "productCategory",
        formatter: (category: ProductCategory) =>
          (category?.name as string) || t("common.no-info"),
      },
      {
        text: t("product.product-list.price"),
        dataField: "price",
        formatter: (price: number) =>
          price?.toString().prettyMoney() || price?.toString().prettyMoney(),
      },
      {
        text: t("product.product-list.variant"),
        dataField: "toppings",
        formatter: (toppings: Topping[]) =>
          toppings?.map(item => item.name + ", "),
      },
      {
        text: t("product.product-list.status.status"),
        dataField: "status",
        formatter: (status: ProductStatus) => (
          <ProductStatusTag active={status}>
            {status === ProductStatus.Deleted &&
              t("product.product-list.status.deleted")}
            {status === ProductStatus.Outofstock &&
              t("product.product-list.status.out-of-stock")}
            {status === ProductStatus.Instock &&
              t("product.product-list.status.in-stock")}
          </ProductStatusTag>
        ),
      },
      {
        text: t("product.product-list.tags"),
        dataField: "tags",
        formatter: (tags: Tag[]) => {
          return (
            <TagWrapper>
              {tags.map((tag, index) => (
                <TagContent key={index}>{tag.name}</TagContent>
              ))}
            </TagWrapper>
          );
        },
      },
      {
        text: t("product.product-list.actions"),
        dataField: "",
        formatter: (_: string, record: Product) => renderAction(record),
      },
    ],
    [page, languageSelected, i18n.language, actionSuccess],
  );

  return totalSize === 0 && !search.text ? (
    <Empty
      SVG={<img src={EmptyImage} width={200} height={200} />}
      Title={t("product.product-list.empty-title")}
      SubTitle={t("product.product-list.empty-message")}
      ButtonMenu={
        <ProductListDialog
          onSuccess={() => setActionSuccess(true)}
          title={t("product.product-list.add-product")}
          ButtonMenu={
            <Button primary className="mx-auto">
              {t("product.product-list.add-new-product")}
            </Button>
          }
        />
      }
    />
  ) : (
    <ListDataLayout
      rightTopBar={
        <RightTopBarWrapper>
          <ProductListDialog
            onSuccess={() => setActionSuccess(true)}
            title={t("product.product-list.add-new-product")}
            ButtonMenu={
              <Button primary>
                {t("product.product-list.add-new-product")}
              </Button>
            }
          />
        </RightTopBarWrapper>
      }
      leftTopBar={
        <SearchBox
          onFetchData={text =>
            setSearch({
              ...search,
              text,
            })
          }
          placeholder={t("product.product-list.search-by-name")}
        />
      }
      title={t("product.product-list.product-list")}
    >
      <Table
        columns={columns}
        data={listProduct || []}
        totalSize={totalSize}
        sizePerPage={SIZE_PER_PAGE}
        onPageChange={onPageChange}
        page={page}
        isRemote
      />
    </ListDataLayout>
  );
};
export default ProductList;
