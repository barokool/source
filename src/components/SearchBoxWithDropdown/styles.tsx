import styled from "styled-components/macro";
import tw from "twin.macro";

export const SearchBoxContainer = styled.div`
  ${tw`relative font-medium rounded-lg bg-neutral-5  text-md`}
`;

export const Form = styled.form`
  ${tw`grid w-full h-6 grid-cols-8 `}
`;

export const TextField = styled.div`
  ${tw`relative flex flex-row items-center w-full h-full col-start-1 col-end-8 gap-1 px-2 phone:col-end-7 cursor-text`}
`;

export const Input = styled.input`
  ${tw`relative flex-1 w-11/12 h-full py-2 text-neutral-1  placeholder-neutral-3 bg-neutral-5`}
`;

export const Dropdown = styled.div`
  ${tw` absolute w-full top-[100%] left-0 max-h-60 shadow-lg bg-primary-3 z-10 rounded-lg cursor-auto overflow-y-auto mt-0.5`}
`;

export const DropdownItem = styled.div<{ active?: boolean }>`
  ${tw`w-full text-neutral-1 cursor-pointer text-md`}
  ${({ active }) => active && tw`bg-neutral-5`}
`;
