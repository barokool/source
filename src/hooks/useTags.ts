import { GetAllTagArgs, Tag, useGetAllTag } from "apiCaller";
import { useState, useCallback, useEffect } from "react";
import { fragmentGetAllTag } from "services/tag";
import useAuth from "./useAuth";

export const useTags = (value?: GetAllTagArgs) => {
  const [listTag, setListTag] = useState<Tag[]>([]);
  const [totalCount, setTotalCount] = useState(0);
  const { accountInfo } = useAuth();
  useEffect(() => {
    invokeSetAllTags({
      filter: {},
    });
  }, []);
  const invokeSetAllTags = useCallback(async (arg: GetAllTagArgs) => {
    const [getAllTag, { data }] = useGetAllTag(fragmentGetAllTag);
    await getAllTag({
      variables: {},
    });
    setTotalCount(data?.getAllTag?.totalCount as number);
    setListTag(data?.getAllTag?.results || []);
  }, []);

  return {
    listTag,
    setListTag,
    invokeSetAllTags,
    totalCount,
  };
};
