import { useState } from "react";

import Dialog from "components/Dialog";
import Tooltip from "designs/Tooltip";

import {
  ElementButton,
  AlertDialogContainer,
  Title,
  Message,
  ButtonWrapper,
  Button,
} from "./styles";

interface IAlertDialogProps {
  ButtonMenu?: React.ReactElement;
  title: string;
  message?: string;
  onConfirm?: (data: any) => void;
  data?: any;
  tooltip: string;
  loading?: boolean;
}

const AlertDialog: React.FC<IAlertDialogProps> = props => {
  const { ButtonMenu, title, message, onConfirm, data, tooltip, loading } =
    props;
  const [isOpen, setIsOpen] = useState(false);

  const handleConfirm = () => {
    onConfirm && onConfirm(data);
    setIsOpen(false);
  };
  console.log(loading);
  return (
    <>
      <Tooltip text={tooltip}>
        <ElementButton onClick={() => setIsOpen(!isOpen)}>
          {ButtonMenu}
        </ElementButton>
      </Tooltip>
      <Dialog open={isOpen} onClose={() => setIsOpen(false)}>
        <AlertDialogContainer>
          <Title>{title}</Title>
          <Message>{message}</Message>
          <ButtonWrapper>
            <Button outline onClick={() => setIsOpen(false)}>
              Huỷ bỏ
            </Button>
            <Button primary loading={loading} onClick={handleConfirm}>
              Đồng ý
            </Button>
          </ButtonWrapper>
        </AlertDialogContainer>
      </Dialog>
    </>
  );
};

export default AlertDialog;
