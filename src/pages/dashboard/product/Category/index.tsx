import { useState, useCallback, useMemo, useEffect } from "react";
import { PATH } from "common/constants/routes";
import { getQueryFromLocation } from "common/functions/route";
import { RouteComponentProps } from "react-router";
import { toast } from "react-toastify";
import Empty from "components/Empty";
import AlertDialog from "components/AlertDialog";
import Table, { IColumns } from "designs/Table";
import SVG from "designs/SVG";
import ListDataLayout from "layouts/ListDataLayout";
import EmptyImage from "assets/images/product/ProductCategoryEmpty.png";

//hooks
import { useLoading } from "hooks/useLoading";
import { usePage } from "hooks/usePage";
import { useBreadcrumb } from "hooks/useBreadcrumb";
import { useDebouncedState } from "hooks/useDebouncedState";

//language
import i18n, { t } from "language";

//api
import {
  useGetAllProductCategory,
  useDeleteProductCategory,
  ProductCategory,
  BusinessProductType,
} from "apiCaller";
import { fragmentGetAllProductCategory } from "services/product";

//locals
import { Button, RenderActionContainer, SearchBox } from "./styles";
import ProductCategoryDialog from "./Dialog";

const SIZE_PER_PAGE = 10;
const LOAD_DATA = "LOAD_DATA";
const DEBOUNCE_TIME = 1000;

interface IProductCategoryProps extends RouteComponentProps {}

const ProductCategoryComponent: React.FC<IProductCategoryProps> = ({
  location,
}) => {
  const { startLoading, stopLoading } = useLoading();

  //local states
  const [search, setSearch] = useDebouncedState<string>(
    "",
    search => invokeGetAllDataAPI(search),
    DEBOUNCE_TIME,
  );
  const [page, setPage] = usePage(getQueryFromLocation(location)?.page);
  const [listProductCategory, setListProductCategory] = useState<
    ProductCategory[]
  >([]);
  const [totalSize, setTotalSize] = useState<number>();
  const [actionSuccess, setActionSuccess] = useState<boolean>(false);

  //api
  const [
    getAllProductCategory,
    {
      data: getAllProductCategoryData,
      loading: getAllProductCategoryLoading,
      error: getAllProductCategoryErrors,
    },
  ] = useGetAllProductCategory(fragmentGetAllProductCategory);
  const [
    deleteProductCategory,
    {
      data: deleteProductCategoryData,
      loading: deleteProductCategoryLoading,
      error: deleteProductCategoryErrors,
    },
  ] = useDeleteProductCategory();

  useBreadcrumb([
    {
      name: t("product.product"),
    },
    {
      name: t("product.product-category.product-category"),
      href: PATH.PRODUCT.CATEGORY,
    },
  ]);

  useEffect(() => {
    if (getAllProductCategoryData) {
      setListProductCategory(
        getAllProductCategoryData.getAllProductCategory
          .results as ProductCategory[],
      );
      setTotalSize(
        getAllProductCategoryData.getAllProductCategory.totalCount as number,
      );
    }
  }, [getAllProductCategoryData]);

  useEffect(() => {
    if (deleteProductCategoryData && !deleteProductCategoryErrors) {
      invokeGetAllDataAPI();
      toast.success(t("product.product-list.delete-success"));
      setActionSuccess(true);
    }
  }, [deleteProductCategoryData]);

  useEffect(() => {
    if (getAllProductCategoryLoading || deleteProductCategoryLoading) {
      startLoading(LOAD_DATA);
    } else {
      stopLoading(LOAD_DATA);
    }
  }, [getAllProductCategoryLoading, deleteProductCategoryLoading]);

  useEffect(() => {
    getAllProductCategoryErrors &&
      toast.error(getAllProductCategoryErrors.message);
    deleteProductCategoryErrors &&
      toast.error(deleteProductCategoryErrors.message);
  }, [getAllProductCategoryErrors, deleteProductCategoryErrors]);

  useEffect(() => {
    invokeGetAllDataAPI(search);
  }, [page]);

  useEffect(() => {
    if (actionSuccess) {
      invokeGetAllDataAPI(search);
    }
    setActionSuccess(false);
  }, [actionSuccess]);

  const invokeGetAllDataAPI = (searchText: string = "") => {
    getAllProductCategory({
      variables: {
        page: page - 1,
        size: SIZE_PER_PAGE,
        filterProductCategory: {
          name: searchText,
        },
      },
    });
  };

  const onPageChange = useCallback((nextPage: number) => {
    setPage(nextPage);
  }, []);

  const renderAction = (record: ProductCategory) => {
    return (
      <RenderActionContainer>
        <ProductCategoryDialog
          onSuccess={() => {
            invokeGetAllDataAPI();
          }}
          title={t("product.product-category.edit-product-category")}
          editField={record}
          ButtonMenu={<SVG name="common/edit" width={20} height={20} />}
        />
        <AlertDialog
          tooltip={t("common.delete")}
          ButtonMenu={<SVG name="common/delete" width={20} height={20} />}
          title={t("product.product-category.delete-product-category")}
          message={t(
            "product.product-category.confirm-delete-product-category",
          )}
          onConfirm={() => {
            deleteProductCategory({
              variables: { listId: [record?._id as string] },
            });
          }}
        />
      </RenderActionContainer>
    );
  };

  const columns: IColumns = useMemo(
    () => [
      {
        text: t("product.product-category.name"),
        dataField: "name",
      },
      {
        text: t("product.product-category.original-product-category"),
        dataField: "businessProductType",
        formatter: (productType: BusinessProductType) => productType?.name,
      },
      {
        text: t("product.product-category.actions"),
        dataField: "actions",
        formatter: (_: string, record: ProductCategory) => renderAction(record),
      },
    ],
    [page, i18n.language, actionSuccess],
  );

  return totalSize === 0 && !search.text ? (
    <Empty
      SVG={
        <img
          alt="product-category-empty"
          src={EmptyImage}
          width={200}
          height={200}
        />
      }
      Title={t("product.product-category.empty-title")}
      SubTitle={t("product.product-category.empty-message")}
      ButtonMenu={
        <ProductCategoryDialog
          onSuccess={() => setActionSuccess(true)}
          title={t("product.product-category.add-new-product-category")}
          ButtonMenu={
            <Button className="mx-auto">
              {t("product.product-category.add-new-product-category")}
            </Button>
          }
        />
      }
    />
  ) : (
    <ListDataLayout
      title={t("product.product-category.product-category")}
      leftTopBar={
        <SearchBox
          onFetchData={searchText => setSearch(searchText)}
          placeholder={t("product.product-category.search-by-name")}
        />
      }
      rightTopBar={
        <ProductCategoryDialog
          onSuccess={() => setActionSuccess(true)}
          title={t("product.product-category.add-new-product-category")}
          ButtonMenu={
            <Button primary>
              {t("product.product-category.add-new-product-category")}
            </Button>
          }
        />
      }
    >
      <Table
        columns={columns}
        data={listProductCategory || []}
        totalSize={totalSize as number}
        sizePerPage={SIZE_PER_PAGE}
        onPageChange={onPageChange}
        page={page}
        isRemote
      />
    </ListDataLayout>
  );
};

export default ProductCategoryComponent;
