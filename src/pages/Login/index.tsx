import { Fragment, useEffect, useState } from "react";
import { PATH } from "common/constants/routes";
import * as Yup from "yup";
import { Formik } from "formik";
import SelectBranchDialog from "./components/SelectBranchDialog";
import Input from "designs/InputV2";

//languages
import { t } from "language";

//hooks
import useLogin from "hooks/useLogin";
import useAuth from "hooks/useAuth";
import { useRedirect } from "hooks/useRedirect";
import { useCurrentBranch } from "hooks/useCurrentBranch";

//locals
import {
  FormContainer,
  Heading,
  Button,
  Form,
  ElementWrapper,
  Other,
  TextDesc,
} from "./styles";
import AuthLayout from "layouts/Auth";
import InputPhone from "designs/InputPhone";
import { isValidPhoneNumber } from "react-phone-number-input";

interface IFormValue {
  phoneNumber: string;
  password: string;
}

const initialValues: IFormValue = {
  password: "",
  phoneNumber: "",
};

const LoginPage = () => {
  const validationSchema = Yup.object().shape<{
    [key in keyof IFormValue]: any;
  }>({
    phoneNumber: Yup.string()
      .required(t("login.please-enter-phone-number"))
      .test("is-valid", t("login.error-message.invalid-phone-number"), value =>
        isValidPhoneNumber(`${value}`),
      ),
    password: Yup.string().required(t("login.please-enter-password")),
  });

  const { branchSelected } = useCurrentBranch();

  const redirect = useRedirect();
  const [openDialog, setOpenDialog] = useState<boolean>(false);
  const { isAuth } = useAuth();
  const { login, loading, isLoginSuccess, data } = useLogin();

  useEffect(() => {
    isLoginSuccess && setOpenDialog(true);
  }, [isLoginSuccess]);

  useEffect(() => {
    if (isAuth && branchSelected) {
      redirect(PATH.DASHBOARD);
    }
  }, []);

  const onHandleSubmit = (values: IFormValue) => {
    login({
      user: {
        password: values.password,
        phoneNumber: values.phoneNumber,
      },
    });
  };

  return (
    <Fragment>
      <SelectBranchDialog
        open={openDialog}
        title={t("login.which-branch-are-you-in")}
        onSuccess={() => {
          redirect(PATH.DASHBOARD);
        }}
        listBranch={data?.login.userInfo?.branches || []}
      />
      <AuthLayout>
        <FormContainer>
          <Heading.Wrapper>
            <Heading.Title>{t("login.title")}</Heading.Title>
            <Heading.SubTitle>{t("login.subtitle")}</Heading.SubTitle>
          </Heading.Wrapper>
          <Formik
            enableReinitialize
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={onHandleSubmit}
          >
            <Form>
              <ElementWrapper>
                <InputPhone
                  name="phoneNumber"
                  label={t("login.phone-number")}
                  placeholder={t("login.enter-phone-number")}
                  required
                  className="flex gap-1 col-span-8"
                />

                <Input
                  name="password"
                  label={t("login.password")}
                  placeholder={t("login.enter-password")}
                  required
                  type="password"
                />
              </ElementWrapper>
              <Button primary type="submit" loading={loading}>
                {t("login.login")}
              </Button>
            </Form>
          </Formik>
          <Other>
            <TextDesc onClick={() => redirect(PATH.AUTH.RESET_PASSWORD)}>
              {t("login.forget-password")}
            </TextDesc>
            <TextDesc onClick={() => redirect(PATH.AUTH.REGISTER)}>
              {t("login.register")}
            </TextDesc>
          </Other>
        </FormContainer>
      </AuthLayout>
    </Fragment>
  );
};

export default LoginPage;
