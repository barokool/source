import axios from "axios";
import { useCallback, useEffect, useState } from "react";

const useLocationFree = () => {
  const [location, setLocation] = useState();

  useEffect(() => {
    handleGetLocation();
  }, []);

  const handleGetLocation = useCallback(async () => {
    const result = await axios("https://api.positionstack.com/v1/forward", {
      data: {
        access_key: "09649691f2c1cb42f0fd1dfe572ab5c9",
        query: "1600 Pennsylvania Ave NW - Washington",
        limit: 1,
      },
    });
    console.log(result);

    // setLocation(result);
  }, []);

  return [location, setLocation];
};

export default useLocationFree;
