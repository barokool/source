import { TypedUseSelectorHook, useDispatch, useSelector } from "react-redux";
import store, { IRootState } from "redux/storeToolkit";

export type AppDispatch = typeof store.dispatch;
export const useAppDispatch = () => useDispatch<AppDispatch>();
export const useAppSelector: TypedUseSelectorHook<IRootState> = useSelector;
