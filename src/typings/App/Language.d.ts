export type ILanguage = "vi" | "en";

export interface IItemLanguage {
  id: string;
  name: string;
  lng: string;
  phonePrefix: string;
  flag: string;
}
