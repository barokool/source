import { GenFields, Tag, TagResult } from "apiCaller";

export const fragmentGetAllTag: GenFields<TagResult> = [
  { results: ["_id", "name", "description", "createdAt", "updatedAt"] },
  "totalCount",
];
export const fragmentCreateTag: GenFields<Tag> = [
  "_id",
  "name",
  "description",
  "createdAt",
  "updatedAt",
];
export const fragmentUpdateTag: GenFields<Tag> = ["_id", "name", "description"];
