import { resizeImage } from "common/functions/image/resize";
import { uploadImageToS3AWS } from "common/utils/s3Handler";
import { IImageInput, IImage } from "typings";

export const resizeImageThenUpload = async ({
  file,
  width,
  height = 0,
}: {
  file: File;
  width: number;
  height?: number;
}): Promise<string> => {
  const resizedBlob = await resizeImage(file, width, height);

  if (!resizedBlob) {
    console.log("Cannot resize!!!");
    return "";
  }

  console.log({ resizedBlob });

  const url = await uploadImageToS3AWS({
    file: new File([resizedBlob], file.name),
  });
  return url;
};

export const getCustomSizeImagesFromFile = async (
  file: File,
  customSize: IImageInput,
): Promise<IImage> => {
  const listResizePromises: (Promise<string> | undefined)[] = [
    undefined,
    undefined,
    undefined,
  ];

  if (!customSize) {
    listResizePromises[0] = uploadImageToS3AWS({ file });
  }

  if (customSize?.small) {
    listResizePromises[1] = resizeImageThenUpload({
      file,
      width: customSize?.small?.width || 0,
      height: customSize?.small?.height || 0,
    });
  }

  if (customSize?.medium) {
    listResizePromises[2] = resizeImageThenUpload({
      file,
      width: customSize?.medium?.width || 0,
      height: customSize?.medium?.height || 0,
    });
  }

  let [defaultSrc, small, medium] = await Promise.all(listResizePromises);

  console.log({ defaultSrc, small, medium });

  return { default: defaultSrc, small, medium };
};
