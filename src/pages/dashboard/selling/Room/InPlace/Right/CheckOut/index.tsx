import React, { useEffect, useMemo, useState, useCallback } from "react";
import { toast } from "react-toastify";
import UserDefaultAvatar from "assets/images/selling/user-default-avatar.jpg";
import Table, { IColumns } from "designs/Table";
import SVG from "designs/SVG";
import SimpleSelect from "designs/SimpleSelect";
import AlertDialog from "components/AlertDialog";

//hooks
import useAuth from "hooks/useAuth";

//api
import { IRootState } from "typings";
import { fragmentGetOrderById, fragmentUpdateOrder } from "services/order";
import { fragmentGetAllTableType, fragmentGetAllTable } from "services/table";
import { fragmentGetAllUser } from "services/user";
import { fragmentUpdateTable } from "services/table";
import {
  Order,
  OrderItem,
  Product,
  User,
  TableType,
  Table as ITable,
  ProductUnit,
  useUpdateOrder,
  useDeleteOrderItem,
  useGetOrderById,
  OrderStatusEnum,
  useGetAllUser,
  useGetAllTableType,
  useGetAllTable,
  useUpdateTable,
  RoleEnum,
  Branch,
  TableStatus,
} from "apiCaller";

//redux
import { useSelector, useDispatch } from "react-redux";
import { setOrderSelected } from "redux/slices/order";
import { setActionSuccess } from "redux/slices/common";

//languages
import { t } from "language";
import { setSellingPageSelected } from "redux/slices/sellingPage";

//local
import {
  FooterContainer,
  HeaderContainer,
  RightCheckOutContainer,
  OrderCodeTag,
  TemporaryCalculatedContainer,
  TemporaryCalculatedContent,
  TemporaryCalculatedPrice,
  ButtonWrapper,
  FooterItemsContainer,
  LeftButton,
  RightButton,
  FooterItemWrapper,
  RenderActionContainer,
  OptionAvatar,
  OptionLabel,
  OptionWrapper,
  ProductThumbnail,
  ProductContent,
} from "./styles";
import OrderItemDialog from "../../../../components/OrderItemDialog";

interface IRightCheckOutProps {}

const SIZE_PER_PAGE = 100;

const RightCheckOut: React.FC<IRightCheckOutProps> = props => {
  //redux
  const { orderSelected } = useSelector((state: IRootState) => state.order);
  const dispatch = useDispatch();
  const { accountInfo } = useAuth();
  const listOrderItem = (orderSelected && orderSelected.orderItems) || [];
  const [branchSelected, setBranchSelected] = useState<Branch | null>(
    accountInfo.userInfo.branches?.[0] as Branch,
  );
  const [listStaff, setListStaff] = useState<User[]>([]);
  const [staffSelected, setStaffSelected] = useState<User | null>(null);
  const [listTable, setListTable] = useState<ITable[]>([]);
  const [tableSelected, setTableSelected] = useState<ITable | null>(null);
  const [listTableType, setListTableType] = useState<TableType[]>([]);
  const [tableTypeSelected, setTableTypeSelected] = useState<TableType | null>(
    null,
  );

  const [order, setOrder] = useState<Order | null>(null);
  const [page, setPage] = useState<number>(0);

  //api
  const [getOrderById, { data: orderReturned, error: getOrderByIdErrors }] =
    useGetOrderById(fragmentGetOrderById);
  const [
    deleteOrderItem,
    { loading: deleteOrderItemLoading, error: deleteOrderItemErrors },
  ] = useDeleteOrderItem();
  const [
    updateOrder,
    {
      data: orderUpdated,
      error: updateOrderErrors,
      loading: updateOrderLoading,
    },
  ] = useUpdateOrder(fragmentUpdateOrder);
  const [getAllStaff, { data: getAllStaffData, error: getAllStaffErrors }] =
    useGetAllUser(fragmentGetAllUser);
  const [
    getAllTableType,
    { data: getAllTableTypeData, error: getAllTableTypeErrors },
  ] = useGetAllTableType(fragmentGetAllTableType);
  const [getAllTable, { data: getAllTableData, error: getAllTableErrors }] =
    useGetAllTable(fragmentGetAllTable);
  const [
    updateTable,
    { error: updateTableErrors, loading: updateTableLoading },
  ] = useUpdateTable(fragmentUpdateTable);

  //catch data returned from API
  useEffect(() => {
    branchSelected && invokeGetAllDataAPI();
  }, [branchSelected]);

  useEffect(() => {
    orderReturned && setOrder(orderReturned?.getOrderById as Order);
    orderUpdated && setOrder(orderUpdated?.updateOrder as Order);
  }, [orderReturned, orderUpdated]);

  useEffect(() => {
    order && dispatch(setOrderSelected(order));
  }, [order]);

  useEffect(() => {
    getAllStaffData &&
      setListStaff(getAllStaffData.getAllUser?.results as User[]);
  }, [getAllStaffData]);

  useEffect(() => {
    getAllTableTypeData &&
      setListTableType(
        getAllTableTypeData?.getAllTableType.results as TableType[],
      );
  }, [getAllTableTypeData]);

  useEffect(() => {
    getAllTableData &&
      setListTable(getAllTableData?.getAllTable.results as ITable[]);
  }, [getAllTableData]);

  useEffect(() => {
    if (tableTypeSelected) {
      setTableSelected(null);
      invokeGetAllTable();
    }
  }, [tableTypeSelected]);

  useEffect(() => {
    if (orderUpdated) {
      toast.success(t("selling.save-order-success"));
      handleRefreshOrder();
      dispatch(setActionSuccess());
      dispatch(setOrderSelected(orderUpdated.updateOrder as Order));
      dispatch(setSellingPageSelected("Room"));
    }
  }, [orderUpdated]);

  //toast errors when they occur from API
  useEffect(() => {
    getOrderByIdErrors && toast.error(getOrderByIdErrors.message);
  }, [getOrderByIdErrors]);

  useEffect(() => {
    deleteOrderItemErrors && toast.error(deleteOrderItemErrors.message);
  }, [deleteOrderItemErrors]);

  useEffect(() => {
    updateOrderErrors && toast.error(updateOrderErrors.message);
  }, [updateOrderErrors]);

  useEffect(() => {
    getAllStaffErrors && toast.error(getAllStaffErrors.message);
  }, [getAllStaffErrors]);

  useEffect(() => {
    getAllTableErrors && toast.error(getAllTableErrors.message);
  }, [getAllTableTypeErrors]);

  useEffect(() => {
    updateTableErrors && toast.error(updateTableErrors.message);
  }, [updateTableErrors]);

  //functions
  const invokeGetAllDataAPI = () => {
    getAllStaff({
      variables: {
        filterUser: {
          role: RoleEnum.Staff,
          branches: [branchSelected?._id as string],
        },
      },
    });
    getAllTableType({
      variables: {
        filter: {
          branch: branchSelected?._id as string,
        },
      },
    });
  };

  const invokeGetAllTable = async () => {
    getAllTable({
      variables: {
        filter: {
          status: TableStatus.Empty,
          tableType: tableTypeSelected?._id as string,
        },
      },
    });
  };

  const handleRefreshOrder = () => {
    getOrderById({
      variables: { id: orderSelected?._id as string },
    });
  };

  const handleApprovingOrder = async () => {
    if (
      !tableSelected ||
      !branchSelected ||
      !staffSelected ||
      !tableTypeSelected
    ) {
      toast.error(t("selling.please-fill-all-fields"));
    } else {
      updateOrder({
        variables: {
          id: orderSelected?._id as string,
          input: {
            status: OrderStatusEnum.Approving,
            table: tableSelected?._id as string,
            fromBranch: accountInfo?.userInfo.branches?.[0]._id as string,
          },
        },
      });
      updateTable({
        variables: {
          id: tableSelected?._id as string,
          input: {
            status: TableStatus.Using,
          },
        },
      });
    }
  };

  const renderAction = (record: OrderItem) => {
    return (
      <RenderActionContainer>
        <OrderItemDialog
          onSuccess={() => {
            handleRefreshOrder();
          }}
          title={t("common.edit")}
          editFields={record}
          ButtonMenu={<SVG name="common/edit" width={20} height={20} />}
        />
        <AlertDialog
          tooltip={t("common.delete")}
          ButtonMenu={<SVG name="common/delete" width={20} height={20} />}
          title={t("product.product-category.delete-product-category")}
          message={t(
            "product.product-category.confirm-delete-product-category",
          )}
          loading={deleteOrderItemLoading}
          onConfirm={() => {
            deleteOrderItem({
              variables: {
                idOrder: orderSelected?._id as string,
                listIdOrderItem: [record._id as string],
              },
            });
            handleRefreshOrder();
            dispatch(setActionSuccess());
            toast.success(t("selling.delete-item-success"));
          }}
        />
      </RenderActionContainer>
    );
  };

  const columns: IColumns = useMemo(
    () => [
      {
        text: t("selling.product.code"),
        dataField: "product",
        formatter: (product: Product) => product.barcode,
      },
      {
        text: t("selling.product.name"),
        dataField: "product",
        formatter: (product: Product, record: OrderItem) => (
          <ProductContent.Wrapper>
            <ProductContent.Name>{product.name} </ProductContent.Name>
            <ProductContent.ToppingWrapper>
              {record.toppings?.map(item => item.name + ", ")}
            </ProductContent.ToppingWrapper>
          </ProductContent.Wrapper>
        ),
      },
      {
        text: t("selling.product.image"),
        dataField: "product",
        formatter: (product: Product) => (
          <ProductThumbnail
            src={
              (product.thumbnail?.small as string) ||
              (product.thumbnail?.medium as string) ||
              (product.thumbnail?.default as string)
            }
          />
        ),
      },
      {
        text: t("selling.unit"),
        dataField: "unit",
        formatter: (unit: ProductUnit) => unit?.name || t("selling.none"),
      },
      {
        text: t("selling.product.quantity"),
        dataField: "quantity",
      },
      {
        text: t("selling.product.price"),
        dataField: "product",
        formatter: (product: Product) =>
          product.price?.toString().prettyMoney(),
      },
      {
        text: t("selling.total"),
        dataField: "totalPrice",
        formatter: (totalPrice: number) => totalPrice?.toString().prettyMoney(),
      },
      {
        text: t("common.actions"),
        dataField: "actions",
        formatter: (_: string, record: OrderItem) => renderAction(record),
      },
    ],
    [page],
  );

  const handleChangePage = useCallback((newPage: number) => {
    setPage(newPage);
  }, []);

  return (
    <>
      <RightCheckOutContainer>
        <HeaderContainer>
          <OrderCodeTag>#{orderSelected?.barcode}</OrderCodeTag>
        </HeaderContainer>
        <Table
          columns={columns}
          data={listOrderItem as OrderItem[]}
          totalSize={orderReturned?.getOrderById.orderItems?.length as number}
          sizePerPage={SIZE_PER_PAGE}
          onPageChange={handleChangePage}
          isRemote
          page={page}
          className="mt-0 h-40"
        />
        <FooterContainer>
          <TemporaryCalculatedContainer>
            <TemporaryCalculatedContent>
              {orderSelected?.price?.finalPrice ? `${t("selling.total")}` : ""}
            </TemporaryCalculatedContent>
            <TemporaryCalculatedPrice>
              {orderSelected?.price?.finalPrice
                ? orderSelected?.price?.finalPrice + "€"
                : ""}
            </TemporaryCalculatedPrice>
          </TemporaryCalculatedContainer>
        </FooterContainer>
        <FooterItemsContainer>
          <FooterItemWrapper>
            <SimpleSelect
              options={accountInfo.userInfo.branches as Branch[]}
              onSelect={branch => setBranchSelected(branch)}
              optionSelected={branchSelected}
              placeholder={t("selling.select-branch")}
            />
          </FooterItemWrapper>
          <FooterItemWrapper>
            <SimpleSelect
              options={listStaff}
              onSelect={staff => setStaffSelected(staff)}
              optionSelected={staffSelected}
              placeholder={t("selling.select-staff")}
              optionTarget="displayName"
              renderOption={staff => (
                <OptionWrapper>
                  <OptionAvatar
                    src={(staff?.avatar?.medium as string) || UserDefaultAvatar}
                  />
                  <OptionLabel>{staff?.displayName}</OptionLabel>
                </OptionWrapper>
              )}
            />
          </FooterItemWrapper>
          <FooterItemWrapper>
            <SimpleSelect
              options={listTableType}
              onSelect={tableType => setTableTypeSelected(tableType)}
              optionSelected={tableTypeSelected}
              placeholder={t("selling.select-table-type")}
            />
          </FooterItemWrapper>
          <FooterItemWrapper>
            <SimpleSelect
              options={listTable}
              onSelect={table => setTableSelected(table)}
              optionSelected={tableSelected}
              placeholder={t("selling.select-table")}
              optionTarget="code"
            />
          </FooterItemWrapper>
        </FooterItemsContainer>
        <ButtonWrapper>
          <LeftButton onClick={() => dispatch(setOrderSelected(null))}>
            {t("common.cancel")}
          </LeftButton>
          <RightButton
            onClick={() => {
              handleApprovingOrder();
            }}
            loading={updateOrderLoading || updateTableLoading}
          >
            {t("selling.check-out")}
          </RightButton>
        </ButtonWrapper>
      </RightCheckOutContainer>
    </>
  );
};

export default RightCheckOut;
