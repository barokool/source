import styled from "styled-components/macro";
import tw from "twin.macro";
export const DRAWER_WIDTH = "250px";
export const AUTO_CLOSE_POINT = 1024;
export const HEIGHT_HEADER = 60;
export const HEIGHT_FOOTER = 120;
// export const Overlay = styled.div<{ isExtendDrawer: boolean }>`
//   ${tw`absolute top-0 h-screen w-screen bg-black bg-opacity-20 z-40 transform duration-200`}
//   ${({ isExtendDrawer }) =>
//     isExtendDrawer
//       ? tw`visible opacity-100 laptop:invisible laptop:opacity-0`
//       : tw`invisible opacity-0`}
// `;

// export const DashboardLayoutContainer = styled.div`
//   ${tw` w-screen h-screen`}/* laptop:grid laptop:grid-cols-auto-1fr */
// `;

// export const MainLayout = styled.div`
//   ${tw`w-full h-full bg-background overflow-y-auto`}
// `;

// export const MainContainer = styled.main`
//   ${tw`m-auto max-w-[1170px] px-1.5`}
// `;
// export const MainContent = styled.main`
//   ${tw`relative p-2 bg-white-color flex flex-col gap-2`}
//   min-height: calc(100vh - 60px);
// `;

export const DashboardContainer = styled.div`
  ${tw`relative w-full h-full bg-neutral-4`}
`;

export const Viewpoint = styled.div<{ isExtendDrawer: boolean }>`
  ${tw`z-0 h-screen overflow-auto transition-all duration-300 `}
  @media screen and (min-width: ${AUTO_CLOSE_POINT}px) {
    padding-left: ${({ isExtendDrawer }) =>
      isExtendDrawer ? DRAWER_WIDTH : "0"};
  }
`;

export const ContentContainer = styled.div`
  ${tw`m-auto max-w-[1170px] px-1.5`}
`;

export const MainContent = styled.main`
  ${tw`relative p-2 bg-white-color flex flex-col gap-2`}
  min-height: calc(100vh - 60px);
`;
