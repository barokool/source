import { useCallback, useEffect } from "react";
import { toast } from "react-toastify";

//api
import { Branch, useGetAllBranch } from "apiCaller";
import { fragmentGetAllBranch } from "services/branch";

import useAuth from "hooks/useAuth";

export const useBranches = () => {
  const [getAllBranch, { data, loading, called, error }] =
    useGetAllBranch(fragmentGetAllBranch);
  const { accountInfo } = useAuth();

  useEffect(() => {
    accountInfo && invokeSetAllBranchesAPI();
  }, [accountInfo]);

  useEffect(() => {
    toast.error(error?.message);
  }, [error]);

  const invokeSetAllBranchesAPI = useCallback(() => {
    accountInfo &&
      getAllBranch({
        variables: {
          filterBranch: { userId: accountInfo?.userInfo?._id as string },
        },
      });
  }, []);

  return {
    listBranch: data,
    loading,
    called,
  };
};
