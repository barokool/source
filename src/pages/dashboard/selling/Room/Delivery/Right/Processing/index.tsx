import React, { useEffect, useState } from "react";
import OrderProcessingImage from "assets/images/selling/order-processing.png";
import TableListDialog from "pages/dashboard/table/TableList/TableListDialog";
import { toast } from "react-toastify";

//redux
import { IRootState } from "typings";
import { useSelector, useDispatch } from "react-redux";
import { setActionSuccess } from "redux/slices/common";

//api
import {
  useUpdateOrder,
  Order,
  OrderStatusEnum,
  useGetOrderById,
} from "apiCaller";
import { fragmentUpdateOrder, fragmentGetOrderById } from "services/order";
import { setOrderSelected } from "redux/slices/order";

//languages
import { t } from "language";

//locals
import {
  RightProcessingContainer,
  CompleteButton,
  Content,
  Image,
  RightProcessingWrapper,
  HeaderContainer,
  OrderCodeTag,
  PrintBillButton,
} from "./styles";

interface IRightProcessing {}

const RightProcessing: React.FC<IRightProcessing> = props => {
  //redux
  const { orderSelected } = useSelector(
    (rootState: IRootState) => rootState.order,
  );
  const dispatch = useDispatch();

  //local state
  const [order, setOrder] = useState<Order | null>(null);
  const [
    updateOrder,
    { data: orderUpdated, loading, error: updateOrderErrors },
  ] = useUpdateOrder(fragmentUpdateOrder);
  const [getOrderById, { error: getOrderByIdErrors }] =
    useGetOrderById(fragmentGetOrderById);

  //catch data returned from api
  useEffect(() => {
    if (order) {
      dispatch(setOrderSelected(order));
    }
  }, [order]);

  useEffect(() => {
    if (orderUpdated) {
      toast.success(t("selling.complete-order-successfully"));
      setOrder(orderUpdated?.updateOrder as Order);
      handleRefreshOrder();
      dispatch(setActionSuccess());
    }
  }, [orderUpdated]);

  //toast errors when they occur
  useEffect(() => {
    if (updateOrderErrors) {
      toast.error(updateOrderErrors.message);
    }
  }, [updateOrderErrors]);

  useEffect(() => {
    if (getOrderByIdErrors) {
      toast.error(getOrderByIdErrors.message);
    }
  }, []);

  //functions
  const handleCompleteOrder = async () => {
    try {
      await updateOrder({
        variables: {
          id: orderSelected?._id as string,
          input: {
            status: OrderStatusEnum.Shipping,
          },
        },
      });
    } catch (error) {
      console.log(error);
    }
  };

  const handleRefreshOrder = async () => {
    try {
      await getOrderById({
        variables: { id: orderSelected?._id as string },
      });
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <RightProcessingContainer>
      <HeaderContainer>
        <OrderCodeTag>#{orderSelected?.barcode}</OrderCodeTag>
        <TableListDialog
          ButtonMenu={
            <PrintBillButton>{t("selling.bill.print-bill")}</PrintBillButton>
          }
          onSuccess={() => {}}
        />
      </HeaderContainer>
      <RightProcessingWrapper>
        <Image src={OrderProcessingImage} alt="order-processing" />
        <Content.Wrapper>
          <Content.Title>{t("selling.order-in-processing")}</Content.Title>
          <Content.Description>
            {t("selling.order-in-processing-message")}
          </Content.Description>
        </Content.Wrapper>

        <CompleteButton
          primary
          onClick={() => {
            handleCompleteOrder();
          }}
          loading={loading}
        >
          {t("selling.move-to-shipping")}
        </CompleteButton>
      </RightProcessingWrapper>
    </RightProcessingContainer>
  );
};

export default RightProcessing;
