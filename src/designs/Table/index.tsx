import React, { memo, useEffect, useState, useRef, ReactNode } from "react";
import ToolkitProvider from "react-bootstrap-table2-toolkit/dist/react-bootstrap-table2-toolkit";
import BootstrapTable, { ColumnDescription } from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";

import Pagination from "components/Pagination";

import { shouldDecreasePageIndex } from "common/functions";

import { ITableProps } from "./interfaces";
import { TableContainer, Header, PaginationWrapper } from "./styles";

export type IColumns<T extends Object = any> = (Omit<
  ColumnDescription<any, any>,
  "dataField" | "formatter"
> & {
  dataField: NestedObjectLessPaths<T> | "actions" | "none";
  formatter?: (cell: any, record: T) => ReactNode;
})[];

const isJustDeleteAnItem = (prev: number, curr: number) => {
  return prev - curr === 1;
};

const TableCustom: React.FC<ITableProps> = ({
  className = "",
  data,
  columns,
  headerElement,
  sizePerPage = 10,
  onPageChange,
  page = 1,
  isRemote = false,
  totalSize = 0,
  onClickRow,
  onTableChange,
}) => {
  const prevTotalSize = useRef<number>(totalSize);
  useEffect(() => {
    if (
      isJustDeleteAnItem(prevTotalSize.current, totalSize) &&
      shouldDecreasePageIndex(page, totalSize, sizePerPage)
    ) {
      onPageChange?.(page - 1);
    }
    prevTotalSize.current = totalSize;
  }, [totalSize, page]);

  const handleChangePage = (nextPage: number) => {
    onPageChange && onPageChange(nextPage + 1);
  };
  const handleTableChange = (
    type: string,
    { page, sizePerPage }: Record<any, any>,
  ) => {
    const newPage = page;

    onTableChange && onTableChange(newPage);
  };

  const handleEmptyData = () => {
    return <div className="empty-data">{}</div>;
  };

  const rowEvents = {
    onClick: (rowIndex: number) => {
      onClickRow && onClickRow(rowIndex);
    },
  };

  const totalPage = Math.ceil(totalSize / sizePerPage);

  return (
    <TableContainer className={`custom-table__container ${className}`}>
      <ToolkitProvider
        bootstrap4
        keyField="id"
        data={data}
        columns={columns}
        search
      >
        {(props: any) => (
          <>
            <Header>{headerElement}</Header>
            <BootstrapTable
              {...props.baseProps}
              bordered={false}
              wrapperClasses="table-responsive col-span-12  lg:overflow-visible"
              noDataIndication={handleEmptyData}
              rowEvents={rowEvents}
              pagination={paginationFactory({
                hideSizePerPage: true,
                sizePerPage,
                page,
                totalSize,
                custom: true,
              })}
              remote={
                isRemote && {
                  pagination: true,
                  filter: false,
                  sort: false,
                }
              }
              onTableChange={handleTableChange}
            />
            {totalSize > 0 && (
              <PaginationWrapper>
                <Pagination
                  page={page - 1}
                  sizePerPage={sizePerPage}
                  totalSize={totalSize}
                  onPageChange={handleChangePage}
                />
              </PaginationWrapper>
            )}
          </>
        )}
      </ToolkitProvider>
    </TableContainer>
  );
};

export default memo(TableCustom);
