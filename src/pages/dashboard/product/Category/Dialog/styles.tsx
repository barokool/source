import styled from "styled-components/macro";
import tw from "twin.macro";
import { Form as _Form } from "formik";
import _Button from "designs/Button";

export const ElementWrapper = styled.div`
  ${tw`cursor-pointer flex justify-end w-full`}
`;

export const ProductCategoryDialogContainer = styled.div`
  ${tw`bg-white p-1 phone:p-2 laptop:p-5 rounded-sm w-full`}
`;

export const Form = styled(_Form)`
  ${tw`grid gap-2`}
`;

export const ButtonWrapper = styled.div`
  ${tw`flex justify-end mt-3`}
`;

export const Button = styled(_Button)`
  ${tw`py-1 w-max px-2 flex justify-center ml-1`}
`;

export const FormGroup = styled.div`
  ${tw``}
`;
