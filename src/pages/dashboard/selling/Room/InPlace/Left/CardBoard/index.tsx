import React, { useState, useEffect, useCallback } from "react";
import { toast } from "react-toastify";
import OrderCard from "pages/dashboard/selling/components/OrderCard";
import AddOrderButton from "designs/AddOrderButton";
import Pagination from "components/Pagination";

//redux
import { setOrderSelected } from "redux/slices/order";
import { setBranchSelected } from "redux/slices/sellingPage";
import { resetAction } from "redux/slices/common";
import { useDispatch, useSelector } from "react-redux";
import { IRootState } from "typings";

//hooks
import useAuth from "hooks/useAuth";

//api
import { fragmentCreateOrder, fragmentGetAllOrder } from "services/order";
import {
  Order,
  useGetAllOrder,
  useCreateOrder,
  OrderTypeEnum,
  Branch,
} from "apiCaller";

//languages
import { t } from "language";

//locals
import {
  CardListContainer,
  LeftCardBoardContainer,
  LeftCardBoardWrapper,
} from "./styles";

interface ILeftCardBoardProps {}

const SIZE_PER_PAGE = 16;

const LeftCardBoard: React.FC<ILeftCardBoardProps> = props => {
  //redux
  const dispatch = useDispatch();
  const { orderSelected } = useSelector((state: IRootState) => state.order);

  const [page, setPage] = useState<number>();
  const { accountInfo } = useAuth();
  const { actionSuccess } = useSelector((state: IRootState) => state.common);
  const { branchSelected } = useSelector(
    (state: IRootState) => state.sellingPage,
  );
  const { branches: listBranch } = accountInfo.userInfo;
  const [listOrder, setListOrder] = useState<Order[]>([]);

  const [getAllOrder, { data: listOrderReturned, loading }] =
    useGetAllOrder(fragmentGetAllOrder);
  const [createOrder, { data: orderReturned }] =
    useCreateOrder(fragmentCreateOrder);

  useEffect(() => {
    dispatch(setBranchSelected(listBranch?.[0] as Branch));
  }, []);

  //get data from api
  useEffect(() => {
    invokeSetAllDataAPI();
  }, [branchSelected, page]);

  useEffect(() => {
    if (actionSuccess === true) {
      invokeSetAllDataAPI();
      dispatch(resetAction());
    }
  }, [actionSuccess]);

  useEffect(() => {
    if (listOrderReturned) {
      setListOrder(listOrderReturned?.getAllOrder.results as Order[]);
    }
  }, [listOrderReturned]);

  useEffect(() => {
    if (orderReturned) {
      invokeSetAllDataAPI();
      dispatch(setOrderSelected(orderReturned?.createOrder as Order));
      toast.success(t("selling.create-order-success"));
    }
  }, [orderReturned]);

  //set page equal to the final page

  //functions
  const invokeSetAllDataAPI = () => {
    getAllOrder({
      variables: {
        filter: {
          type: OrderTypeEnum.In_place,
          fromBranch: branchSelected?._id as string,
        },
        page: page,
        size: SIZE_PER_PAGE,
      },
    });
  };

  const handleCreateOrder = () => {
    createOrder({
      variables: {
        input: {
          type: OrderTypeEnum.In_place,
          fromBranch: branchSelected?._id as string,
        },
      },
    });
    dispatch(resetAction());
  };

  const handleChangePage = useCallback((newPage: number) => {
    setPage(newPage);
  }, []);

  return (
    <LeftCardBoardContainer>
      <LeftCardBoardWrapper>
        <CardListContainer>
          {loading ? (
            new Array(10)
              .fill(0)
              .map((_, index) => <OrderCard key={index} loading />)
          ) : (
            <>
              {listOrder.map(order => (
                <OrderCard
                  key={order._id}
                  data={order}
                  onClick={() => dispatch(setOrderSelected(order))}
                  isActive={order._id === orderSelected?._id}
                />
              ))}
            </>
          )}
          {listOrder.length < SIZE_PER_PAGE || listOrder.length === 0 ? (
            <AddOrderButton
              onClick={() => {
                handleCreateOrder();
              }}
            ></AddOrderButton>
          ) : (
            <></>
          )}
        </CardListContainer>
      </LeftCardBoardWrapper>
      <Pagination
        onPageChange={handleChangePage}
        page={page}
        sizePerPage={SIZE_PER_PAGE}
        totalSize={(listOrderReturned?.getAllOrder.totalCount as number) + 1}
        className="float-right"
        displayRanges={1}
      />
    </LeftCardBoardContainer>
  );
};

export default LeftCardBoard;
