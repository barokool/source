//redux
import { useSelector } from "react-redux";
import { IRootState } from "typings";

//apis

import { OrderStatusEnum, OrderTypeEnum } from "apiCaller";

//locals
import LeftReviewBill from "./ReviewBill";
import LeftCardBoard from "./CardBoard";
import { InPlaceLeftContainer } from "./styles";

interface IInPlaceLeftProps {}

const InPlaceLeft: React.FC<IInPlaceLeftProps> = props => {
  const { orderSelected } = useSelector((state: IRootState) => state.order);

  return (
    <InPlaceLeftContainer>
      {orderSelected?.status === OrderStatusEnum.Approving &&
      orderSelected.type === OrderTypeEnum.In_place ? (
        <LeftReviewBill />
      ) : (
        <LeftCardBoard />
      )}
    </InPlaceLeftContainer>
  );
};

export default InPlaceLeft;
