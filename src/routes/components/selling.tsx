import { IRoutes } from "typings";
import { PATH } from "common/constants/routes";
import { lazy } from "react";
import { t } from "language";
import { ProductIcon } from "designs/icons/Drawer";

import Selling from "pages/dashboard/selling";

export const sellingRoutes: IRoutes = {
  name: t("dashboard.selling.selling"),
  path: PATH.SELLING.SELF,
  exact: true,
  isPrivate: true,
  Icon: ProductIcon,
  isSellRoute: true,
  hiddenRoute: true,
  Component: Selling,
};
