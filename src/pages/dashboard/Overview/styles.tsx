import styled from "styled-components/macro";
import tw from "twin.macro";

export const OverviewContainer = styled.div`
  ${tw`w-full`}
`;

export const Content = styled.article`
  ${tw`grid w-full grid-cols-2 gap-2 `}
`;

export const Section = styled.section`
  ${tw`p-3 border border-solid rounded-lg border-neutral-4`}
`;

export const Heading = {
  Wrapper: styled.div`
    ${tw`flex flex-col-reverse gap-2 phone:flex-row justify-between items-center`}
  `,
  TextWrapper: styled.div`
    ${tw`flex flex-col gap-1`}
  `,
  Title: styled.p`
    ${tw`font-semibold text-black text-16`}
  `,
  Desc: styled.p`
    ${tw`font-regular text-body-color text-14`}
  `,
};
