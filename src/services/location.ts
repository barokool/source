import {
  CityResult,
  CountryResult,
  District,
  DistrictResult,
  GenFields,
  State,
  StateResult,
} from "apiCaller";

export const fragmentGetAllDistricts: GenFields<DistrictResult> = [
  { results: ["_id", "name", { state: ["_id", "name"] }] },
];

export const fragmentGetDistrictsByProvince: GenFields<DistrictResult> = [
  { results: ["_id", "name"] },
];

export const fragmentGetAllStates: GenFields<StateResult> = [
  "totalCount",
  { results: ["_id", "name"] },
];

export const fragmentUpdateState: GenFields<State> = ["_id", "name"];

export const fragmentCreateState: GenFields<State> = ["_id", "name"];

export const fragmentGetAllCities: GenFields<CityResult> = [
  "totalCount",
  {
    results: ["_id", "name", { geoLocation: ["type"] }],
  },
];

export const fragmentGetAllCountry: GenFields<CountryResult> = [
  {
    results: ["_id", "name", "prefixPhone", "alpha2Code", "icon"],
  },
];
