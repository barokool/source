import styled from "styled-components/macro";
import tw from "twin.macro";

export const ButtonWrapper = styled.button<{
  primary?: boolean;
  outline?: boolean;
  loading?: boolean;
}>`
  ${tw`font-medium rounded-sm flex gap-2 items-center overflow-hidden focus:outline-none 
  border border-solid border-transparent duration-200`}
  ${({ primary }) =>
    primary &&
    tw`bg-black text-white hover:text-black hover:bg-white hover:border-black`}
  ${({ outline }) =>
    outline && tw`text-black border-black hover:text-white hover:bg-black`}
  ${({ loading }) => loading && tw`bg-opacity-60 cursor-wait`}
`;
