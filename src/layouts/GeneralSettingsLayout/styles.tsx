import styled from "styled-components/macro";
import tw from "twin.macro";

export const GeneralSettingsLayoutContainer = styled.div`
  ${tw`  `}
`;

export const ChangeButtonWrapper = styled.div`
  ${tw`my-4 flex justify-between w-2/5`}
`;
