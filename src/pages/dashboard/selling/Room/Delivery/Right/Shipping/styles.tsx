import styled from "styled-components/macro";
import tw from "twin.macro";
import _Button from "designs/Button";

export const RightShippingContainer = styled.div`
  ${tw`w-full h-screen shadow-lg flex flex-col`}
`;

export const RightShippingWrapper = styled.div`
  ${tw`my-auto`}
`;

export const Image = styled.img`
  ${tw`mx-auto w-30 h-25`}
`;

export const Content = {
  Wrapper: styled.div`
    ${tw`mt-4 w-full`}
  `,
  Title: styled.p`
    ${tw`text-black text-20 font-semibold text-center`}
  `,
  Description: styled.p`
    ${tw`text-black text-16 text-center`}
  `,
};

export const TrackingButton = styled(_Button)`
  ${tw`mt-4 text-center mx-auto px-2 py-1 rounded-md`}
`;
