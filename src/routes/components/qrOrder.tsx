import { IRoutes } from "typings";
import { PATH } from "common/constants/routes";
import { lazy } from "react";
import { t } from "language";

const HomePage = lazy(() => import("pages/qrOrder/Home"));
const MenuPage = lazy(() => import("pages/qrOrder/Menu"));
const FavouriteDishesPage = lazy(() => import("pages/qrOrder/FavouriteDishes"));
const PaymentPage = lazy(() => import("pages/qrOrder/Payment"));
const OrderDetailPage = lazy(() => import("pages/qrOrder/OrderDetail"));
const CheckOutDishesPage = lazy(() => import("pages/qrOrder/CheckOutDishes"));

export const qrOrderHomeRoute: IRoutes = {
  name: "Home",
  path: PATH.QR_ORDER.HOME,
  exact: true,
  isPrivate: false,
  Component: HomePage,
};

export const qrOrderMenuRoute: IRoutes = {
  name: "Menu",
  path: PATH.QR_ORDER.MENU,
  exact: true,
  isPrivate: false,
  Component: MenuPage,
};

export const qrOrderFavouriteDishesRoute: IRoutes = {
  name: "Favourite Dishes",
  path: PATH.QR_ORDER.FAVOURITE_DISHES,
  exact: true,
  isPrivate: false,
  Component: FavouriteDishesPage,
};

export const qrOrderPaymentRoute: IRoutes = {
  name: "Payment",
  path: PATH.QR_ORDER.PAYMENT,
  exact: true,
  isPrivate: false,
  Component: PaymentPage,
};

export const qrOrderDetailRoute: IRoutes = {
  name: "Order detail",
  path: PATH.QR_ORDER.DETAIL,
  exact: true,
  isPrivate: false,
  Component: OrderDetailPage,
};

export const qrOrderCheckOutDishesRoute: IRoutes = {
  name: "Checkout dishes",
  path: PATH.QR_ORDER.CHECK_OUT_DISHES,
  exact: true,
  isPrivate: false,
  Component: CheckOutDishesPage,
};
