import React, { useState, useEffect, useRef, ChangeEvent } from "react";
import ImageCropper from "components/ImageCropper";
import { IBase64Image, IImage, IImageInput } from "typings";
import ImageUploadLayout from "layouts/ImageUpload";
import SVG from "designs/SVG";
import {
  MultipleImageUploaderContainer,
  HiddenInput,
  ImageUploadContainer,
  PreviewImagesContainer,
  Image,
  SkeletonContainer,
  SkeletonMessage,
  ImageContainer,
} from "./styles";
import { useField, useFormikContext } from "formik";
import FormControlLabel from "common/styles/FormControlLabel";
import FormControlErrorHelper from "common/styles/FormControlErrorHelper";

import Spinner from "icons/Spinner";
import IconButton from "designs/IconButton";

import { toast } from "react-toastify";
import { getCustomSizeImagesFromFile } from "services/uploader";
interface IMultipleImageUploaderProps {
  className?: string;
  name: string;
  images: IImage[];
  label: string;
  required?: boolean;
  customSize: IImageInput;
  onChange?: (images: IImage[]) => void;
  text?: string;
  imageType?: "image/jpeg" | "image/png";
  /**
   * @example aspect={16/9}
   */
  aspect?: number;
}

const MultipleImageUploader: React.FC<IMultipleImageUploaderProps> = props => {
  const {
    images = [],
    name = "",
    label = "",
    text = "Drag your image here",
    aspect = 1,
    required = false,
    customSize,
    onChange,
    className = "",
    imageType = "image/jpeg",
  } = props;
  const [fileSelected, setFileSelected] = useState<File | undefined>();
  const [displayImages, setDisplayImages] = useState<IBase64Image[] | string[]>(
    [],
  );
  const [files, setFiles] = useState<File[]>([]);
  const [croppedFile, setCroppedFile] = useState<File | null>(null);
  const [openCropImage, setOpenCropImage] = useState(false);

  const [isEdited, setIsEdited] = useState(false);
  const { setFieldValue } = useFormikContext();
  const [field, meta] = useField(name);
  const isError: boolean = Boolean(!!meta.error && !!meta.touched);

  useEffect(() => {
    if (!croppedFile && !isEdited) {
      setFieldValue(name, images?.length || "");
      setFiles(new Array(images.length).fill(null));
      console.log("Set files ");
    }
  }, [images]);

  const handleUploadRawImage = (files: File[]) => {
    if (!files) return;

    const file = files[0];
    setFileSelected(file);
    setOpenCropImage(true);
  };

  const handleCloseImageCropper = () => {
    setOpenCropImage(false);
  };

  const handleCroppedImage = (file: File) => {
    try {
      setIsEdited(true);

      const newFiles = [...files, file];
      onChange && onChange([...images, {}]);
      setFiles(newFiles);
      setCroppedFile(file);
      setOpenCropImage(false);
    } catch (error) {
      console.error(error);
    }
  };

  const removeImage = (index: number) => {
    setIsEdited(true);

    files.splice(index, 1);

    setFiles([...files]);

    const newImage = images.filter(
      image => image?.small !== images[index]?.small,
    );

    onChange && onChange(newImage);
    setFieldValue(name, newImage.length);
  };

  return (
    <>
      <MultipleImageUploaderContainer className={className}>
        <FormControlLabel isError={isError} required={required}>
          {label}
        </FormControlLabel>
        <ImageUploadLayout onUpload={handleUploadRawImage}>
          <ImageUploadContainer isError={isError}>
            <SkeletonContainer>
              <SVG
                name="common/upload"
                className="block m-auto"
                width="61"
                height="60"
              />
              <SkeletonMessage>{text}</SkeletonMessage>
            </SkeletonContainer>
          </ImageUploadContainer>
        </ImageUploadLayout>
        {isError && (
          <FormControlErrorHelper>{meta?.error}</FormControlErrorHelper>
        )}
        <PreviewImagesContainer>
          {images?.map((image, index) => (
            <ImageUploaderCard
              file={files[index]}
              key={image?.small}
              image={image}
              customSize={customSize}
              onLoaded={image => {
                images[index] = image;
                onChange?.([...images]);
                setFieldValue(name, images?.length);
              }}
              onRemove={() => removeImage(index)}
            />
          ))}
          {/* {displayImages?.map((image, index) => (
            <ImageContainer key={index}>
              <Image
                src={image || ""}
                alt="Image uploader"
                width="auto"
                height="80"
              />
              <SVG
                className="absolute top-0.5 right-0.5 cursor-pointer"
                name="common/remove-image"
                width="24"
                height="24"
                onClick={() => removeImage(index)}
              />
            </ImageContainer>
          ))} */}
        </PreviewImagesContainer>
        <HiddenInput {...field} />
      </MultipleImageUploaderContainer>

      <ImageCropper
        aspect={aspect}
        image={fileSelected}
        isOpen={openCropImage}
        onClose={handleCloseImageCropper}
        onConfirm={handleCroppedImage}
      />
    </>
  );
};

export default MultipleImageUploader;

const ImageUploaderCard: React.FC<{
  image: IImage;
  file: File | null;
  onLoaded: (customSizeImage: IImage) => void;
  onRemove: () => void;
  customSize: IImageInput;
}> = ({ image, file, customSize, onLoaded, onRemove }) => {
  const imageSrc = image?.small || image?.medium || image?.default;
  const isLoading = !imageSrc;

  useEffect(() => {
    if (isLoading && file) {
      handleUploadImage();
    }
  }, [isLoading, file]);

  // console.log({ file, imageSrc });

  const handleUploadImage = async () => {
    if (!file) return;
    try {
      const customSizeImage = await getCustomSizeImagesFromFile(
        file,
        customSize,
      );
      onLoaded(customSizeImage);
      console.log({ customSizeImage });
    } catch (error) {
      console.error(error);
      toast.error("Upload image error!");
    }
  };

  return (
    <div className="relative">
      <ImageContainer>
        {isLoading ? (
          <div className="flex items-center justify-center bg-neutral-4 animate-pulse">
            <div className="w-5 h-5 ">
              <Spinner className="w-5 h-5 " />
            </div>
          </div>
        ) : (
          <Image
            src={imageSrc}
            alt="Image uploader"
            width="auto"
            height="200"
          />
        )}
      </ImageContainer>
      <IconButton
        tooltip="Remove"
        className="absolute top-0.5 right-0.5 cursor-pointer w-2.5 h-2.5"
      >
        <SVG
          name="common/remove-image"
          width="24"
          height="24"
          onClick={onRemove}
        />
      </IconButton>
    </div>
  );
};
