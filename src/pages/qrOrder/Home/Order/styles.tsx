import styled from "styled-components/macro";
import tw from "twin.macro";

export const OrderSectionContainer = styled.div`
  ${tw` `}
`;

export const OrderTitle = styled.h1`
  ${tw`text-14 text-black font-bold`}
`;

export const ListDishContainer = styled.div`
  ${tw`flex justify-between`}
`;

export const DishItem = {
  Container: styled.div`
    ${tw`w-1/3 mt-2 cursor-pointer`}
  `,

  Label: styled.h3`
    ${tw`text-center mt-1`}
  `,
};
