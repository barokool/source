import React, { useState, useEffect } from "react";

import {
  OrderDetailContainer,
  Detail,
  ButtonWrapper,
  Button,
  OrderDetailTopContainer,
  TotalBill,
  Driver,
} from "./styles";
import { t } from "language";
import {
  Order,
  OrderStatusEnum,
  OrderTypeEnum,
  useUpdateOrder,
} from "apiCaller";
import { fragmentUpdateOrder } from "services/order";

const nextOrderStatus: Record<OrderStatusEnum, any> = {
  approving: "approving",
  canceled: "canceled",
  complete: "complete",
  new: "new",
  processing: "processing",
  ready: "ready",
  shipping: "shipping",
};

export interface OrderDetailItem {
  title?: string;
  contents: { key?: string; value?: string }[];
}

interface OrderDetailProps {
  order?: Order;
  items?: OrderDetailItem[];
  isApproval?: boolean;
  onSuccess?: () => void;
  invokeCloseDetail: () => void;
}

const OrderServiceDetail: React.FC<OrderDetailProps> = ({
  order,
  items,
  isApproval,
  onSuccess,
  invokeCloseDetail,
}) => {
  const [isLoading, setLoading] = useState(false);
  const [isOpen, setOpen] = useState<boolean>(true);
  const [updateOrder] = useUpdateOrder(fragmentUpdateOrder);
  const handleSubmit = async () => {
    try {
      setLoading(true);
      await updateOrder({
        variables: {
          id: order?._id as string,
          input: {
            status: nextOrderStatus[order?.status as OrderStatusEnum],
            fromBranch: order?.fromBranch?._id as string,
          },
        },
      });
      onSuccess?.();
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  };

  if (order) {
    return (
      <OrderDetailContainer>
        <OrderDetailTopContainer>
          <TotalBill.Container>
            <TotalBill.Label>
              {t("shipping.shipping.total-bill")}
            </TotalBill.Label>
            <TotalBill.Tag>{order.price?.finalPrice || 1500000}</TotalBill.Tag>
          </TotalBill.Container>
          <Driver.Container>
            <Driver.Label>{t("shipping.shipping.driver-name")}</Driver.Label>
            <Driver.Content>
              {order?.driver?.displayName || "Huynh Ngoc Duc"}
            </Driver.Content>
          </Driver.Container>
        </OrderDetailTopContainer>
        {items?.map(item => (
          <Detail.Wrapper>
            <Detail.Title>{item.title}</Detail.Title>
            {item.contents.map(content => {
              return (
                <Detail.ChildrenWrapper>
                  <Detail.LeftContent>{content.key}</Detail.LeftContent>
                  <Detail.RightContent>{content.value}</Detail.RightContent>
                </Detail.ChildrenWrapper>
              );
            })}
          </Detail.Wrapper>
        ))}
        <div className="flex justify-between">
          <h1 className="text-secondary text-20 font-semibold">
            {t("shipping.shipping.total-bill")}
          </h1>
          <h1 className="text-secondary text-20 font-semibold">{1500000}</h1>
        </div>
        {isApproval ? (
          <ButtonWrapper>
            <Button outline type="button" onClick={() => invokeCloseDetail()}>
              {t("common.cancel")}
            </Button>
            <Button
              primary
              type="button"
              onClick={() => handleSubmit()}
              loading={isLoading}
            >
              {t("common.accept")}
            </Button>
          </ButtonWrapper>
        ) : (
          <></>
        )}
      </OrderDetailContainer>
    );
  } else return <></>;
};

export default OrderServiceDetail;
