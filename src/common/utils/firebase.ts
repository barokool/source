import firebase from "firebase/app";

const firebaseConfig = {
  apiKey: "AIzaSyBot7bnOqtmlCqxIT2ZsBeAKr-6hR5zTak",
  authDomain: "gogobei-7bfab.firebaseapp.com",
  projectId: "gogobei-7bfab",
  storageBucket: "gogobei-7bfab.appspot.com",
  messagingSenderId: "361364070651",
  appId: "1:361364070651:web:06e7313e2bd183c23c905e",
  measurementId: "G-BTPQQLSB6Z",
};

firebase.initializeApp(firebaseConfig);

export { firebase };
