import React, { useState, useEffect } from "react";
import * as Yup from "yup";
import * as InputPhoneDesign from "designs/InputPhone";
import { Formik } from "formik";
import { toast } from "react-toastify";
import "firebase/auth";
import { formatPhoneNumber } from "common/functions";

//languages
import { t } from "language";

//api
import { useIsExistingPhoneNumber } from "apiCaller";
import { fragmentIsExistingPhoneNumber } from "services/user";

//redux
import { useDispatch } from "react-redux";
import { setPhoneNumber, setUserId } from "redux/slices/resetPassword";

//locals
import { InputPhoneContainer, Button, Form } from "./styles";

interface IInputPhoneProps {
  onVerifyPhoneNumberSuccess?: () => void;
}

interface IFormValue {
  phoneNumber: string;
}

const validationSchemaPhoneNumber = Yup.object().shape<{
  [key in keyof IFormValue]: any;
}>({
  phoneNumber: Yup.string().required(
    t("resetPassword.please-enter-phone-number"),
  ),
});

const InputPhone: React.FC<IInputPhoneProps> = ({
  onVerifyPhoneNumberSuccess,
}) => {
  //redux
  const dispatch = useDispatch();

  //local state
  const [initialValues] = useState<IFormValue>({
    phoneNumber: "",
  });

  //api
  const [isExistingPhoneNumber, { data, loading, error }] =
    useIsExistingPhoneNumber(fragmentIsExistingPhoneNumber);

  const handleSubmit = (values: IFormValue) => {
    const payload = { phoneNumber: formatPhoneNumber(values.phoneNumber) };
    console.log(payload);
    isExistingPhoneNumber({ variables: payload });
  };

  useEffect(() => {
    if (data && !error) {
      dispatch(
        setPhoneNumber(data.isExistingPhoneNumber.phoneNumber as string),
      );
      dispatch(setUserId(data.isExistingPhoneNumber._id as string));
      onVerifyPhoneNumberSuccess?.();
      toast.success(t("resetPassword.verify-phone-number-success"));
    }
  }, [data]);

  useEffect(() => {
    error && toast.error(t("resetPassword.invalid-phone-number"));
  }, [error]);

  return (
    <InputPhoneContainer>
      <Formik
        validationSchema={validationSchemaPhoneNumber}
        initialValues={initialValues}
        onSubmit={handleSubmit}
        enableReinitialize
      >
        <Form>
          <InputPhoneDesign.default
            name="phoneNumber"
            label={t("login.phone-number")}
            placeholder={t("login.enter-phone-number")}
            required
            className="flex gap-1 col-span-8"
          />
          <Button primary type="submit" loading={loading}>
            {t("resetPassword.confirm")}
          </Button>
        </Form>
      </Formik>
    </InputPhoneContainer>
  );
};

export default InputPhone;
