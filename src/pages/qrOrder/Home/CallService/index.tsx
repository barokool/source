import {
  CallServiceContainer,
  CallServiceTitle,
  PressBellContainer,
  PressBellMessage,
  NoteInput,
} from "./styles";
import SVG from "designs/SVG";

interface ICallServiceProps {}

const CallService: React.FC<ICallServiceProps> = props => {
  return (
    <CallServiceContainer>
      <CallServiceTitle>Gọi phục vụ</CallServiceTitle>
      <PressBellContainer>
        <SVG name="qrOrder/bell" width={65} height={80} className="mx-auto" />
        <PressBellMessage>Chạm vào chuông để gọi nhân viên</PressBellMessage>
      </PressBellContainer>
      <NoteInput name="" placeholder="Thêm chú thích..." />
    </CallServiceContainer>
  );
};

export default CallService;
