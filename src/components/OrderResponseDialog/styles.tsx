import styled from "styled-components/macro";
import tw, { TwStyle } from "twin.macro";
import { Form as _Form } from "formik";
import _Button from "designs/Button";
import { OrderResponseStatus } from "./index";

const listStatus: {
  [k in OrderResponseStatus]: TwStyle;
} = {
  failed: tw`bg-erorr-color`,
  success: tw`bg-primary`,
};

const listStatusForButton: {
  [k in OrderResponseStatus]: TwStyle;
} = {
  failed: tw`text-erorr-color`,
  success: tw`text-primary`,
};

export const OrderResponseDialogContainer = styled.div<{
  isDanger: OrderResponseStatus;
}>`
  ${tw`rounded-sm py-3 px-2`}
  ${({ isDanger }) => listStatus[isDanger]}
`;

export const ElementWrapper = styled.div`
  ${tw`cursor-pointer`}
`;

export const Label = styled.h1`
  ${tw`text-16 text-white font-bold mt-2.5 mb-0.5`}
`;

export const Description = styled.div`
  ${tw`text-13 text-white mb-2`}
`;

export const Button = styled(_Button)<{ isDanger: OrderResponseStatus }>`
  ${tw`bg-white px-5.5 py-1.5 mx-auto `}
  ${({ isDanger }) => listStatusForButton[isDanger]}
`;
