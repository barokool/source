import { useState, useCallback, useMemo, useEffect } from "react";
import { toast } from "react-toastify";
import { PATH } from "common/constants/routes";
import { getQueryFromLocation } from "common/functions/route";
import { RouteComponentProps } from "react-router";
import Table, { IColumns } from "designs/Table";
import ActionButtons from "designs/ActionButton";
import Empty from "components/Empty";
import ListDataLayout from "layouts/ListDataLayout";
import EmptyImage from "assets/images/product/ProductListEmpty.png";

//hooks
import { usePage } from "hooks/usePage";
import { useBreadcrumb } from "hooks/useBreadcrumb";
import { useCurrentBranch } from "hooks/useCurrentBranch";
import { useLoading } from "hooks/useLoading";
import { useDebouncedState } from "hooks/useDebouncedState";

//languages
import i18n, { t } from "language";

//api
import {
  TableType,
  useGetAllTableType,
  useDeleteTableType,
  Branch,
} from "apiCaller";
import { fragmentGetAllTableType } from "services/table";

//locals
import { AddButton, SearchBox, TableTypeContainer } from "./styles";
import TableTypeDialog from "./TableTypeDialog";

const SIZE_PER_PAGE = 10;
const LOAD_DATA = "LOAD_DATA";
const DEBOUNCE_TIME = 1000;

interface ISearchDependencies {
  branchSelected: Branch | null;
  text: string;
}

interface ITableTypeProps extends RouteComponentProps {}

const TableTypeList: React.FC<ITableTypeProps> = ({ location }) => {
  const { branchSelected } = useCurrentBranch();
  const { startLoading, stopLoading } = useLoading();

  //local state
  const [listTableType, setListTableType] = useState<TableType[]>([]);
  const [page, setPage] = usePage(getQueryFromLocation(location)?.page);
  const [totalSize, setTotalSize] = useState(0);
  const [actionSuccess, setActionSuccess] = useState<boolean>(false);
  const [search, setSearch] = useDebouncedState<ISearchDependencies>(
    {
      branchSelected: null,
      text: "",
    },
    objectSearch => {
      if (objectSearch.branchSelected) {
        invokeGetAllDataAPI(objectSearch.text, objectSearch.branchSelected);
      }
    },
    DEBOUNCE_TIME,
  );

  //api
  const [
    getAllTableType,
    {
      data: getAllTableTypeData,
      error: getAllTableTypeErrors,
      loading: getAllTableTypeLoading,
    },
  ] = useGetAllTableType(fragmentGetAllTableType);
  const [
    deleteTableType,
    {
      loading: deleteTableTypeLoading,
      error: deleteTableTypeErrors,
      data: deleteTableTypeData,
    },
  ] = useDeleteTableType();

  useBreadcrumb([
    {
      name: t("table.table-management"),
    },
    {
      name: t("table.table-type.table-type"),
      href: PATH.TABLE_MANAGEMENT.TYPE,
    },
  ]);

  useEffect(() => {
    branchSelected && invokeGetAllDataAPI("", branchSelected);
  }, [page, branchSelected]);

  useEffect(() => {
    if (actionSuccess === true && branchSelected) {
      invokeGetAllDataAPI("", branchSelected);
    }
    setActionSuccess(false);
  }, [actionSuccess]);

  useEffect(() => {
    if (getAllTableTypeLoading || deleteTableTypeLoading) {
      startLoading(LOAD_DATA);
    } else {
      stopLoading(LOAD_DATA);
    }
  }, [getAllTableTypeLoading, deleteTableTypeLoading]);

  useEffect(() => {
    branchSelected &&
      setSearch({
        ...search,
        branchSelected,
      });
  }, [branchSelected]);

  useEffect(() => {
    if (getAllTableTypeData) {
      setListTableType(getAllTableTypeData.getAllTableType.results || []);
      setTotalSize(getAllTableTypeData.getAllTableType.totalCount as number);
    }
  }, [getAllTableTypeData]);

  //toast errors when they occur
  useEffect(() => {
    getAllTableTypeErrors && toast.error(getAllTableTypeErrors.message);
    deleteTableTypeErrors && toast.error(deleteTableTypeErrors.message);
  }, [getAllTableTypeErrors, deleteTableTypeErrors]);

  useEffect(() => {
    if (deleteTableTypeData && !deleteTableTypeErrors) {
      toast.success(t("table.table-type.delete-success"));
      setActionSuccess(true);
    }
  }, [deleteTableTypeData]);

  //get table types filter on branch and name
  const invokeGetAllDataAPI = (searchText: string = "", branch: Branch) => {
    getAllTableType({
      variables: {
        page: page - 1,
        size: SIZE_PER_PAGE,
        filter: {
          branch: branch._id as string,
          name: searchText,
        },
      },
    });
  };

  const onPageChange = useCallback((nextPage: number) => {
    setPage(nextPage);
  }, []);

  const renderAction = (record: TableType) => {
    return (
      <ActionButtons
        buttons={{
          edit: {
            DialogContent: ({ onClose }) => {
              return (
                <TableTypeDialog
                  onClose={onClose}
                  open
                  editField={record}
                  title={t("table.table-type.edit")}
                  onSuccess={() => setActionSuccess(true)}
                />
              );
            },
          },
          delete: {
            title: t("table.table-type.delete"),
            message: t("table.table-type.confirm-delete"),
            onDelete: () =>
              deleteTableType({
                variables: { listId: [record._id as string] },
              }),
          },
        }}
      />
    );
  };

  const columns = useMemo(
    (): IColumns<TableType> => [
      {
        text: t("table.table-type.table-type"),
        dataField: "name",
      },
      {
        text: t("table.table-type.max-of-seat"),
        dataField: "maxGuest",
        formatter: (maxGuest: string) => maxGuest,
      },
      {
        text: t("table.table-type.quantity"),
        dataField: "quantity",
        formatter: (quantity: number) => quantity.toString(),
      },
      {
        text: t("table.table-type.actions"),
        dataField: "actions",
        formatter: (_, actions: TableType) => renderAction(actions),
      },
    ],
    [page, actionSuccess, i18n.language],
  );

  return totalSize === 0 && !search.text ? (
    <Empty
      SVG={<img src={EmptyImage} width={200} height={200} />}
      Title={t("table.table-type.empty.title")}
      SubTitle={t("table.table-type.empty.message")}
      ButtonMenu={
        <TableTypeDialog
          onSuccess={() => setActionSuccess(true)}
          title={t("table.table-type.add")}
          ButtonMenu={
            <AddButton primary className="mx-auto mt-2">
              {t("table.table-type.add")}
            </AddButton>
          }
        />
      }
    />
  ) : (
    <TableTypeContainer>
      <ListDataLayout
        rightTopBar={
          <TableTypeDialog
            onSuccess={() => setActionSuccess(true)}
            title={t("table.table-type.add")}
            ButtonMenu={
              <AddButton primary>{t("table.table-type.add")}</AddButton>
            }
          />
        }
        leftTopBar={
          <SearchBox
            onFetchData={text =>
              setSearch({
                ...search,
                text,
              })
            }
            placeholder={t("table.table-type.searching")}
          />
        }
        title={t("table.table-type.table-type")}
      >
        <Table
          columns={columns}
          data={listTableType || []}
          totalSize={totalSize}
          sizePerPage={SIZE_PER_PAGE}
          onPageChange={onPageChange}
          page={page}
          isRemote
        />
      </ListDataLayout>
    </TableTypeContainer>
  );
};
export default TableTypeList;
