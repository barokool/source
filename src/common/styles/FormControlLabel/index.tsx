import styled from "styled-components/macro";
import tw from "twin.macro";

interface IFormLabelProps {
  isError: boolean;
  required: boolean | undefined;
  htmlFor?: string;
  subTitle?: string | null;
}

const FormControlLabel: React.FC<IFormLabelProps> = ({
  children,
  required,
  htmlFor,
  isError,
  subTitle,
}) => {
  if (!children) return null;

  return (
    <FormLabelContainer htmlFor={htmlFor} isError={isError}>
      {children}
      {required && <RequireLabel>*</RequireLabel>}
      {subTitle && <SubTitle>{subTitle}</SubTitle>}
    </FormLabelContainer>
  );
};

export default FormControlLabel;

const FormLabelContainer = styled.label<{ isError: boolean }>`
  ${tw`font-medium text-16 mb-0.5 flex flex-row items-center`}
  ${({ isError }) => (isError ? tw`text-danger` : tw`text-black`)}
`;

const RequireLabel = styled.p`
  ${tw`text-12 font-semibold text-danger ml-0.5`}
`;

const SubTitle = styled.span`
  ${tw`text-14 ml-0.5 text-neutral-3 font-regular`}
`;
