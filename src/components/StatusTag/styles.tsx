import styled from "styled-components/macro";
import tw from "twin.macro";

export const StatusTagContainer = styled.div<{ active?: boolean }>`
  ${tw`flex justify-center px-3 py-1 rounded-sm text-14 font-regular whitespace-nowrap w-max`}
  ${({ active }) =>
    active ? tw`bg-successfull text-white` : tw`bg-gray text-line`}
`;
