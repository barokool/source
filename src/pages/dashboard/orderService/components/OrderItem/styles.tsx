import styled from "styled-components/macro";
import tw from "twin.macro";

export const OrderServiceItemContainer = styled.div<{ active?: boolean }>`
  ${tw`grid grid-cols-2 px-2 border-2 border-dashed rounded border-transparent py-1.5 cursor-pointer`}
  ${({ active }) => active && tw` border-neutral-3`}
`;

export const Box = styled.div`
  ${tw`  `}
`;
export const LeftSide = styled.div`
  ${tw`  `}
`;
export const RightSide = styled.div`
  ${tw`flex flex-col-reverse justify-between items-end`}
`;
export const Code = styled.p`
  ${tw`text-13`}
`;
export const Name = styled.p`
  ${tw`text-16 font-medium`}
`;
export const Time = styled.p`
  ${tw`text-14 text-body-color`}
`;
export const Phone = styled.p`
  ${tw`text-14`}
`;
export const Status = styled.p<{ status?: "new" | "cancel" | "complete" }>`
  ${tw`text-14 font-medium text-right`}
  ${({ status }) => status === "new" && tw`text-primary`}
  ${({ status }) => status === "cancel" && tw`text-danger`}
  ${({ status }) => status === "complete" && tw`text-secondary`}
`;
