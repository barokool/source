import { useCallback, useEffect, useRef } from "react";
import { useDispatch } from "react-redux";
import { setBreadcrumb as setBreadcrumbAction } from "redux/slices/_config";
import { IBreadcrumb } from "typings";

import i18n from "language";

export const useBreadcrumb = (initBreadcrumb: IBreadcrumb) => {
  const dispatch = useDispatch();
  const breadcrumb = useRef(initBreadcrumb);

  useEffect(() => {
    breadcrumb.current = initBreadcrumb;
    setBreadcrumb(initBreadcrumb);
  }, [i18n.language]);

  const setBreadcrumb = useCallback((breadcrumb: IBreadcrumb) => {
    dispatch(setBreadcrumbAction(breadcrumb));
  }, []);

  return { breadcrumb, setBreadcrumb };
};
