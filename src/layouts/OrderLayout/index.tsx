import React, { useState, useEffect } from "react";
import {
  LeftSideContainer,
  OrderLayoutContainer,
  RightSideContainer,
} from "./styles";

import { useLoading } from "hooks/useLoading";
import OrderCard from "components/OrderCard";
import { setOrderSelected } from "redux/slices/order";
import { useDispatch, useSelector } from "react-redux";
import { IRootState } from "redux/storeToolkit";
import OrderProgressBar from "components/OrderProgressBar";
import { useOrders } from "hooks/useOrders";
import { OrderStatusEnum } from "apiCaller";

interface OrderLayoutProps {}

const LOAD_DATA = "LOAD_DATA";
const SIZE_PER_PAGE = 5;

const OrderLayout: React.FC<OrderLayoutProps> = ({ children }) => {
  const [totalSize, setTotalSize] = useState<number>();
  const { startLoading, stopLoading } = useLoading();
  const { listOrders } = useOrders();
  const dispatch = useDispatch();
  const { orderSelected } = useSelector((state: IRootState) => state.order);

  return (
    <OrderLayoutContainer>
      <LeftSideContainer>
        {listOrders?.getAllOrder?.results?.map(item => (
          <OrderCard
            order={item}
            onClick={() => dispatch(setOrderSelected(item))}
            isSelected={item._id === orderSelected?._id}
          />
        ))}
      </LeftSideContainer>
      <RightSideContainer>
        <OrderProgressBar status={orderSelected?.status as OrderStatusEnum} />
        {children}
      </RightSideContainer>
    </OrderLayoutContainer>
  );
};

export default OrderLayout;
