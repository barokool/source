import tw from "twin.macro";
import styled from "styled-components/macro";

export const ConfigChildrenContainer = styled.div`
  ${tw`flex items-center w-full bg-white p-0.5 phone:p-1 laptop:p-2 cursor-pointer`}
`;

export const ContentContainer = styled.div`
  ${tw`flex flex-col justify-between ml-1 phone:ml-2 laptop:ml-3`}
`;

export const Title = styled.p`
  ${tw`text-secondary leading-4 text-14 laptop:leading-6 laptop:text-20`}
`;

export const Description = styled.p`
  ${tw`text-secondary font-bold text-12 laptop:text-14 mt-0.5 laptop:mt-1`}
`;
