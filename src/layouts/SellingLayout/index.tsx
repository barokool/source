import { SellingLayoutContainer } from "./styles";

interface ISellingLayoutPrpos {}

const SellingLayout: React.FC<ISellingLayoutPrpos> = ({ children }) => {
  return <SellingLayoutContainer>{children}</SellingLayoutContainer>;
};

export default SellingLayout;
