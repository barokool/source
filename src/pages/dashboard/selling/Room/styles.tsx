import styled from "styled-components/macro";
import tw from "twin.macro";

export const SellingRoomContainer = styled.div`
  ${tw`flex justify-between h-screen`}
`;
export const GroupTabWrapper = styled.div`
  ${tw`w-full`}
`;
