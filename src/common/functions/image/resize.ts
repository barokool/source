export const resizeImage = (
  file: File,
  toWidth: number | null,
  toHeight: number | null,
): Promise<Blob | null> => {
  return new Promise((resolve, reject) => {
    let image = new Image();
    image.src = URL.createObjectURL(file);
    image.onload = () => {
      let width = image.width;
      let height = image.height;

      let newWidth = width;
      let newHeight = height;

      if (toWidth) {
        newHeight = height * (toWidth / width);
        newWidth = toWidth;
      } else if (toHeight) {
        newWidth = width * (toHeight / height);
        newHeight = toHeight;
      }

      let canvas = document.createElement("canvas");
      canvas.width = newWidth;
      canvas.height = newHeight;

      let context = canvas.getContext("2d");

      (context as any).drawImage(image, 0, 0, newWidth, newHeight);

      canvas.toBlob(bold => resolve(bold), file.type);
    };
    image.onerror = reject;
  });
};
