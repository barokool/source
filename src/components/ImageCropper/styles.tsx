import styled from "styled-components/macro";
import tw from "twin.macro";
import _Button from "designs/Button";

export const DialogContainer = styled.div`
  ${tw`w-full h-full bg-white`}
`;

export const Content = styled.div`
  ${tw`flex flex-col items-center `}
`;

export const CropperImageContainer = styled.div`
  ${tw`relative w-full h-40 bg-line `}
`;

export const RangeContainer = styled.div`
  ${tw`w-3/4 p-1 py-2`}
`;

export const ActionButtons = styled.div`
  ${tw`flex flex-row justify-end gap-1 p-2 ml-auto mr-0 w-30 `}
`;

export const Button = styled(_Button)`
  ${tw`flex justify-center py-1 w-15`}
`;
