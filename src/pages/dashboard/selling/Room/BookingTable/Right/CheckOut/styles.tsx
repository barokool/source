import styled from "styled-components/macro";
import tw from "twin.macro";
import _Button from "designs/Button";

export const RightCheckOutContainer = styled.div`
  ${tw`w-full h-screen relative shadow-lg`}
`;

export const HeaderContainer = styled.div`
  ${tw`p-2 border-b-2 border-line w-full`}
`;

export const OrderCodeTag = styled.div`
  ${tw`p-1 text-black text-16 font-semibold rounded-sm border-2 border-black w-max`}
`;

export const FooterContainer = styled.div`
  ${tw`py-1.5 px-2`}
`;

export const TemporaryCalculatedContainer = styled.div`
  ${tw`w-1/5 float-right flex items-center justify-between mb-4`}
`;

export const TemporaryCalculatedContent = styled.h3`
  ${tw`text-20 text-black`}
`;

export const TemporaryCalculatedPrice = styled.h2`
  ${tw`text-20 text-black font-bold`}
`;

export const ButtonWrapper = styled.div`
  ${tw`w-full flex items-center mt-3 absolute bottom-0 `}
`;

export const RightButton = styled(_Button)`
  ${tw`py-1.5 bg-black text-16 text-white justify-center w-1/2`}
`;

export const LeftButton = styled(_Button)`
  ${tw`py-1.5 bg-line text-black justify-center text-16 w-1/2`}
`;

export const RenderActionContainer = styled.div`
  ${tw`flex justify-end gap-2`}
`;

export const FooterItemsContainer = styled.div`
  ${tw`w-full grid grid-cols-2 gap-x-4 mt-8 px-2`}
`;

export const FooterItemWrapper = styled.div`
  ${tw`flex w-full justify-between`}
`;

export const OptionWrapper = styled.div`
  ${tw`w-full flex gap-x-0.5 items-center`}
`;

export const OptionLabel = styled.p`
  ${tw``}
`;

export const OptionAvatar = styled.img`
  ${tw`w-3 h-3 rounded-full mr-2`}
`;

export const ProductThumbnail = styled.img`
  ${tw`rounded-full w-3 h-3`}
`;

export const PrintBillButton = styled(_Button)`
  ${tw`px-3.5 py-[12.5px] font-medium bg-secondary text-white w-max `}
`;
