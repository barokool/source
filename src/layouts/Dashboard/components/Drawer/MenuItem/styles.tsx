import styled from "styled-components/macro";
import tw from "twin.macro";
import _Button from "designs/Button";
import _ArrowIcon from "designs/icons/Arrow";

export const MenuItemContainer = styled.div`
  ${tw`relative z-50 `}
`;

export const Button = styled(_Button)`
  ${tw`w-full mb-1`}
`;

export const ButtonContainer = styled.div<{ active: boolean }>`
  ${tw`relative w-full h-full flex items-center border-r-[5px] gap-[16px] text-white text-14  z-50 overflow-hidden hover:bg-hover-color `}
  ${({ active }) =>
    active
      ? tw`bg-hover-color border-primary-1 text-neutral-1`
      : tw`bg-white hover:bg-hover-color`}
`;

export const ButtonIconWrapper = styled.div`
  ${tw`flex items-center justify-center w-4 h-4`}
`;

export const Text = styled.p<{ active: boolean; isCollapse?: boolean }>`
  ${tw`pl-5 pr-0.5 absolute flex items-center justify-between w-full whitespace-nowrap z-50 duration-200`}
  ${({ active }) => (active ? tw`text-secondary ` : tw`text-secondary`)}
`;

export const ArrowIcon = styled(_ArrowIcon)`
  ${tw`w-2 h-2`}
`;

export const Dropdown = styled.ul<{
  isVisible?: boolean;
  isCollapse?: boolean;
}>`
  ${tw`text-white mb-1 rounded-sm`}
  &:before {
    content: "";
    ${tw`absolute top-0 -left-2 h-full w-2`}
  }
  ${({ isVisible }) => (isVisible ? tw`block` : tw`hidden`)}
  ${({ isCollapse }) =>
    isCollapse
      ? tw`absolute -top-1/2 left-full hidden shadow-lg z-40 min-w-full bg-secondary`
      : tw`pl-2 w-full`}
`;

export const DropdownItem = styled.li`
  ${tw`flex items-center w-full text-left`}
`;

export const SubmenuButton = styled(_Button)<{
  isActive?: boolean;
}>`
  ${tw`p-1 w-full flex items-center text-secondary text-left text-13 whitespace-nowrap  `}
  ${({ isActive }) =>
    isActive ? tw` bg-hover-color  outline-none` : tw` hover:bg-hover-color`}
`;

export const ListStyle = styled.span<{
  isActive?: boolean;
}>`
  ${tw`block w-0.5 h-0.5 rounded-full mr-1 border`}
  ${({ isActive }) => (isActive ? tw`bg-secondary ` : tw`bg-secondary `)}
`;

export const ButtonNoDropdown = styled.div`
  ${tw`absolute top-0 left-full z-50 shadow-2xl px-1 py-0.5 rounded-sm bg-white text-left hidden cursor-default select-none ml-2`}
  &:before {
    content: "";
    ${tw`absolute top-0 -left-2 h-full w-2`}
  }
`;

export const ButtonNoDropdownText = styled.p<{ active?: boolean }>`
  ${tw`whitespace-nowrap text-14 `}
  ${({ active }) => (active ? tw`text-black` : tw`text-black text-opacity-50`)}
`;
