import React, { useState, useEffect, useMemo } from "react";

//languages
import i18n, { t } from "language";

//api
import { OrderStatusEnum } from "apiCaller";

//languages
import {
  ActiveContainer,
  ActiveContent,
  ActivePoint,
  OrderProgressBarContainer,
} from "./styles";

interface IOrderProgressBarProps {
  status?: OrderStatusEnum;
  className?: string;
}

const OrderProgressBar: React.FC<IOrderProgressBarProps> = ({
  status,
  className,
}) => {
  const [step, setStep] = useState<number>(0);

  const progress = useMemo(
    () => [
      {
        id: 0,
        value: t("order.sell-at-store.status.new"),
      },
      {
        id: 1,
        value: t("order.sell-at-store.status.prepare"),
      },
      {
        id: 2,
        value: t("order.sell-at-store.status.shipping"),
      },
      {
        id: 3,
        value: t("order.sell-at-store.status.complete"),
      },
    ],
    [i18n.language],
  );

  useEffect(() => {
    if (status === OrderStatusEnum.New) {
      setStep(0);
    }
    if (status === OrderStatusEnum.Processing) {
      setStep(1);
    }
    if (status === OrderStatusEnum.Shipping) {
      setStep(2);
    }
    if (status === OrderStatusEnum.Complete) {
      setStep(3);
    }
  }, [status]);

  return (
    <OrderProgressBarContainer className={className}>
      {progress.map((item, index) => (
        <ActiveContainer>
          <ActivePoint isActive={step >= (item.id as number)}>
            {step >= (item.id as number) ? "✓" : ""}
          </ActivePoint>
          <ActiveContent isActive={step >= (item.id as number)}>
            {item.value}
          </ActiveContent>
        </ActiveContainer>
      ))}
    </OrderProgressBarContainer>
  );
};

export default OrderProgressBar;
