import { useCallback, useEffect } from "react";
import { useGetAllRole } from "apiCaller";
import { fragmentGetAllRole } from "services/role";
import { useCurrentBranch } from "./useCurrentBranch";

export const useRoles = () => {
  const [getAllRoles, { data }] = useGetAllRole(fragmentGetAllRole);
  const { branchSelected } = useCurrentBranch();

  useEffect(() => {
    branchSelected && invokeSetAllRoles();
  }, [branchSelected]);

  const invokeSetAllRoles = useCallback(async () => {
    getAllRoles({
      variables: {
        filterRole: {
          idBranch: branchSelected?._id as string,
        },
      },
    });
  }, []);

  return {
    listRole: data,
    invokeSetAllRoles,
  };
};
