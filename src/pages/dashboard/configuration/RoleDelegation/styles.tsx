import styled from "styled-components/macro";
import tw from "twin.macro";
import _Button from "designs/Button";
import _SearchBox from "components/SearchBoxTable";

export const RoleDelegationContainer = styled.div`
  ${tw`  `}
`;

export const Button = styled(_Button)`
  ${tw`px-2 py-1`}
`;

export const Title = styled.span`
  ${tw`font-medium text-16`}
`;

export const SearchBox = styled(_SearchBox)`
  ${tw`w-full phone:w-55 phone:mb-0`}
`;

export const Description = styled.p`
  ${tw`text-13 text-gray py-1`}
`;

export const TableBox = styled.div`
  ${tw`  `}
`;
