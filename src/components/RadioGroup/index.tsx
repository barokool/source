import { RadioGroup } from "@headlessui/react";
import { useEffect, useState } from "react";

interface ITabProps<T = any> {
  className?: string;
  innerClassName?: string;
  options: T[];
  optionSelected: T | null;
  onChange: (option: T) => void;
  renderOption: (props: { option: T; checked: boolean }) => JSX.Element;
}

const CustomGroupRadio = <T,>(props: ITabProps<T>) => {
  const {
    onChange,
    className = "",
    options,
    optionSelected,
    innerClassName = "flex flex-col gap-1",
    renderOption,
  } = props;

  const [value, setValue] = useState<T | null>();

  useEffect(() => {
    if (optionSelected && Object.keys(optionSelected as any).length > 0) {
      setValue(optionSelected);
    } else {
      options?.length > 0 && setValue(options[0]);
    }
  }, [optionSelected]);

  const handleChange = (option: any) => {
    onChange && onChange(option);
  };
  return (
    <div className={className}>
      <RadioGroup
        value={value}
        className={innerClassName}
        onChange={handleChange}
      >
        {options?.map((option, index) => (
          <RadioGroup.Option key={index} value={option}>
            {({ checked }) => renderOption({ option, checked })}
          </RadioGroup.Option>
        ))}
      </RadioGroup>
    </div>
  );
};

export default CustomGroupRadio;
