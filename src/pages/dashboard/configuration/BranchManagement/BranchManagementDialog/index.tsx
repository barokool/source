import React, { useEffect, useState } from "react";
import { Formik } from "formik";
import * as yup from "yup";

//hooks

//styles
import Input from "designs/InputV2";
import Select from "designs/Select";
import Dialog from "components/Dialog";
import { DialogTitle } from "common/styles/Title";
import {
  ElementWrapper,
  BranchManagementDialogContainer,
  Form,
  FormGroup,
  Title,
  ButtonWrapper,
  Button,
} from "./styles";
import Schedule from "./Schedule";
import RichTextEditor from "designs/RichTextEditor";

//api
import {
  Branch,
  BranchOperationTimeInput,
  BranchStatus,
  BranchTimeInput,
  BusinessProductType,
  City,
  Country,
  CreateManagerRequestInput,
  DayOfWeek,
  District,
  ServiceType,
  State,
  UpdateBranchInput,
  useAddBranchManager,
  useGetAllBusinessProductType,
  useGetAllCity,
  useGetAllCountry,
  useGetAllDistrict,
  useGetAllState,
  useUpdateBranch,
} from "apiCaller";
import {
  fragmentAddBranchManager,
  fragmentUpdateBranch,
} from "services/branch";

import { t } from "language";
import { toast } from "react-toastify";
import {
  fragmentGetAllCities,
  fragmentGetAllCountry,
  fragmentGetAllDistricts,
  fragmentGetAllStates,
} from "services/location";
import { useServiceTypes } from "hooks/useServiceTypes";
import { fragmentGetAllBusinessProductType } from "services/businessProductType";
import MultipleAutocomplete from "designs/MultipleAutoComplete";

interface BranchManagementDialogProps {
  ButtonMenu?: React.ReactElement;
  editField?: Branch;
  title?: string;
  onClose?: () => void;
  onSuccess?: () => void;
  open?: boolean;
}

interface IFormValue {
  name: string;
  phoneNumber: string;
  introduction?: string;
  maxGuest?: number;
  code?: string;
  businessProductTypes: string[];
  status: string;
  country?: string;
  state?: string;
  district?: string;
  city?: string;
  address?: string;
  time?: BranchOperationTimeInput[];
  password?: string;
  serviceType: string;
  tables?: string[];
}

const validationSchema = yup
  .object()
  .shape<{ [key in keyof IFormValue]: any }>({
    name: yup
      .string()
      .required(
        t("configuration.branch-management.general-info.please-enter-name"),
      ),
    phoneNumber: yup
      .string()
      .required(
        t(
          "configuration.branch-management.general-info.please-enter-phone-number",
        ),
      ),
    status: yup
      .string()
      .required(
        t("configuration.branch-management.general-info.please-select-status"),
      ),
    time: yup.array().of(
      yup.object().shape({
        times: yup.array().of(
          yup.object().shape({
            openingHours: yup
              .string()
              .required(
                t(
                  "configuration.branch-management.general-info.please-select-start-time",
                ),
              ),
            closeHours: yup
              .string()
              .required(
                t(
                  "configuration.branch-management.general-info.please-select-end-time",
                ),
              ),
          }),
        ),
      }),
    ),
    businessProductTypes: yup.string().required(),
    serviceType: yup.string().required(),
  });

const BranchManagementDialog: React.FC<BranchManagementDialogProps> = ({
  ButtonMenu,
  editField,
  title,
  onSuccess,
  onClose,
  open = false,
}) => {
  const [isOpen, setOpen] = useState<boolean>(open);
  const [timeSelected, setTimeSelected] = useState<BranchTimeInput[]>([]);
  //hooks
  const { ServiceType } = useServiceTypes();
  const [getAllBusinessProductType, { data: businessData }] =
    useGetAllBusinessProductType(fragmentGetAllBusinessProductType);

  const [isChangeCountry, setIsChangeCountry] = useState(false);
  const [oldStateSelected, setOldStateSelected] = useState<State | null>(null);
  const [oldDistrictSelected, setOldDistrictSelected] =
    useState<District | null>(null);

  const [getAllCountry, { data: countryData, error: countryError }] =
    useGetAllCountry(fragmentGetAllCountry);
  const [getAllState, { data: getAllStateData }] =
    useGetAllState(fragmentGetAllStates);
  const [getAllDistrict, { data: getAllDistrictData }] = useGetAllDistrict(
    fragmentGetAllDistricts,
  );
  const [getAllCity, { data: getAllCityData }] =
    useGetAllCity(fragmentGetAllCities);
  const [listCountryState, setListCountryState] = useState<Country[]>([]);
  const [listState, setListState] = useState<State[]>([]);
  const [listDistrict, setListDistrict] = useState<District[]>([]);
  const [listCity, setListCity] = useState<City[]>([]);
  const [countryAPISelected, setCountryAPISelected] = useState<Country | null>(
    null,
  );
  const [stateSelected, setStateSelected] = useState<State | null>(null);
  const [districtSelected, setDistrictSelected] = useState<District | null>(
    null,
  );
  const [citySelected, setCitySelected] = useState<City | null>(null);

  const [statusSelected, setStatusSelected] = useState<{
    name: string;
    status: BranchStatus;
  } | null>(null);
  const [businessTypeSelected, setBusinessTypeSelected] = useState<
    BusinessProductType[]
  >([]);
  const [serviceTypeSelected, setServiceTypeSelected] =
    useState<ServiceType | null>(null);
  //apis
  const [addBranchManager, { data: createData, error: createError }] =
    useAddBranchManager(fragmentAddBranchManager);
  const [updateBranch, { data: updateData, error: updateError }] =
    useUpdateBranch(fragmentUpdateBranch);

  //useEffect catch data

  useEffect(() => {
    if (createData?.addBranchManager !== undefined && !createError?.message) {
      toast.success(t("common.create-success"));
      onSuccess && onSuccess();
      setOpen(false);
    }
    if (!createData && createError) {
      toast.error(t("common.create-fail"));
      setOpen(false);
    }
  }, [createData, createError]);

  useEffect(() => {
    if (updateData && !updateError) {
      toast.success(t("common.edit-success"));
      onSuccess && onSuccess();
      handleClose();
    }
    if (!updateData && updateError) {
      toast.error(t("common.edit-fail"));
      handleClose();
    }
  }, [updateData, updateError]);

  useEffect(() => {
    invokegetAllBusinessProductType();
  }, []);

  useEffect(() => {
    invokegetAllCountryAPI();
  }, []);

  useEffect(() => {
    if (isChangeCountry) {
      setListDistrict([]);
      setListCity([]);
    }
  }, [isChangeCountry]); // dung

  useEffect(() => {
    if (stateSelected != oldStateSelected) {
      setListDistrict([]);
      setListCity([]);
      setDistrictSelected(null);
      setCitySelected(null);
    }
  }, [stateSelected]);

  useEffect(() => {
    if (districtSelected != oldDistrictSelected) {
      setListCity([]);
      setCitySelected(null);
    }
  }, [districtSelected]);

  useEffect(() => {
    countryData && setListCountryState(countryData.getAllCountry.results || []);
  }, [countryData]);

  useEffect(() => {
    getAllStateData &&
      setListState(getAllStateData.getAllState.results as State[]);
  }, [getAllStateData]);

  useEffect(() => {
    getAllDistrictData &&
      setListDistrict(getAllDistrictData.getAllDistrict.results as District[]);
  }, [getAllDistrictData]);

  useEffect(() => {
    getAllCityData && setListCity(getAllCityData.getAllCity.results as City[]);
  }, [getAllCityData]);

  useEffect(() => {
    if (isChangeCountry) {
      setStateSelected(null);
    }
    countryAPISelected && invoketGetAllStatesAPI();
  }, [countryAPISelected, isChangeCountry]);

  useEffect(() => {
    if (isChangeCountry) setDistrictSelected(null);
    stateSelected && invokeGetAllDistrictSAPI();
  }, [stateSelected, isChangeCountry]);

  useEffect(() => {
    if (isChangeCountry) setCitySelected(null);
    districtSelected && invokeGetAllCitiAPI();
  }, [districtSelected, isChangeCountry]);

  const invokegetAllBusinessProductType = () => {
    getAllBusinessProductType({ variables: {} });
  };

  const invokegetAllCountryAPI = () => {
    getAllCountry({ variables: {} });
  };

  const invoketGetAllStatesAPI = () => {
    getAllState({
      variables: {
        filter: {
          country: countryAPISelected?._id || "",
        },
      },
    });
  };

  const invokeGetAllDistrictSAPI = () => {
    getAllDistrict({
      variables: {
        filter: { state: stateSelected?._id || "" },
      },
    });
  };

  const invokeGetAllCitiAPI = () => {
    getAllCity({
      variables: {
        filter: {
          district: districtSelected?._id || "",
        },
      },
    });
  };

  useEffect(() => {
    //when add new branch but dont create, close form => then set null all fields
    if (!isOpen) {
      setCountryAPISelected(null);
      setStateSelected(null);
      setDistrictSelected(null);
      setCitySelected(null);
    }
  }, [
    countryAPISelected,
    stateSelected,
    districtSelected,
    citySelected,
    isOpen,
  ]);

  useEffect(() => {
    setCountryAPISelected(editField?.location?.country as Country);
    setStateSelected(editField?.location?.state as State);
    setOldStateSelected(editField?.location?.state as State);
    setDistrictSelected(editField?.location?.district as District);
    setOldDistrictSelected(editField?.location?.district as District);
    setCitySelected(editField?.location?.city as City);
    //end location
    setStatusSelected({
      name: editField?.status as string,
      status: editField?.status as BranchStatus,
    });
    setBusinessTypeSelected(
      editField?.businessProductTypes as BusinessProductType[],
    );
    setServiceTypeSelected(editField?.serviceType as ServiceType);
    //a
    if (editField) {
      setInitialValues({
        name: editField?.name as string,
        phoneNumber: editField?.phoneNumber as string,
        status: editField?.status as string,
        code: editField?.code as string,
        maxGuest: editField?.maxGuest as number,
        introduction: editField?.introduction as string,
        country: countryAPISelected?._id as string,
        state: stateSelected?._id as string,
        district: districtSelected?._id as string,
        city: citySelected?._id as string,
        address: editField?.location?.addressLine as string,
        businessProductTypes: businessTypeSelected?.map(
          item => item._id as string,
        ) as string[],
        password: "",
        serviceType: serviceTypeSelected?._id as string,
      });
    }
  }, [editField]);

  const handleSubmit = (values: IFormValue) => {
    const input: CreateManagerRequestInput = {
      nameBranch: values.name,
      phoneNumber: values.name,
      location: {
        country: countryAPISelected?._id as string,
        state: stateSelected?._id as string,
        district: districtSelected?._id as string,
        city: citySelected?._id as string,
        addressLine: values.address,
      },
      introduction: values.introduction,
      maxGuest: values.maxGuest,
      operationTimes: initTimeData as BranchOperationTimeInput[],
      statusBranch: statusSelected?.status as BranchStatus,
      businessProductTypes: businessTypeSelected?.map(
        item => item._id as string,
      ) as string[],
      password: values.password,
      serviceType: serviceTypeSelected?._id as string,
    };

    if (editField) {
      const input: UpdateBranchInput = {
        name: values.name,
        phoneNumber: values.phoneNumber,
        location: {
          country: countryAPISelected?._id as string,
          state: stateSelected?._id as string,
          district: districtSelected?._id as string,
          city: citySelected?._id as string,
          addressLine: values.address,
        },
        introduction: values.introduction,
        maxGuest: values.maxGuest,
        status: statusSelected?.status as BranchStatus,
      };

      updateBranch({
        variables: {
          id: editField?._id as string,
          branchInput: input,
        },
      });
      return;
    }
    addBranchManager({
      variables: {
        branchInput: input,
      },
    });
  };

  const handleClose = () => {
    setOpen(false);
    onClose && onClose();
  };

  useEffect(() => {
    timeSelected && console.log(timeSelected);
  }, [timeSelected]);

  const initTimeData: BranchOperationTimeInput[] = [
    {
      day: DayOfWeek.Monday,
      times: [
        {
          closeHours: timeSelected?.[0]?.closeHours,
          openingHours: timeSelected?.[0]?.openingHours,
        },
      ],
    },
    {
      day: DayOfWeek.Tuesday,
      times: [
        {
          closeHours: timeSelected?.[1]?.closeHours,
          openingHours: timeSelected?.[1]?.openingHours,
        },
      ],
    },
    {
      day: DayOfWeek.Wednesday,
      times: [
        {
          closeHours: timeSelected?.[2]?.closeHours,
          openingHours: timeSelected?.[2]?.openingHours,
        },
      ],
    },
    {
      day: DayOfWeek.Thursday,
      times: [
        {
          closeHours: timeSelected?.[3]?.closeHours,
          openingHours: timeSelected?.[3]?.openingHours,
        },
      ],
    },
    {
      day: DayOfWeek.Friday,
      times: [
        {
          closeHours: timeSelected?.[4]?.closeHours,
          openingHours: timeSelected?.[4]?.openingHours,
        },
      ],
    },
    {
      day: DayOfWeek.Saturday,
      times: [
        {
          closeHours: timeSelected?.[5]?.closeHours,
          openingHours: timeSelected?.[5]?.openingHours,
        },
      ],
    },
    {
      day: DayOfWeek.Sunday,
      times: [
        {
          closeHours: timeSelected?.[6]?.closeHours,
          openingHours: timeSelected?.[6]?.openingHours,
        },
      ],
    },
  ];
  const [initialValues, setInitialValues] = useState<IFormValue>({
    name: "",
    phoneNumber: "",
    status: BranchStatus.Isactive,
    time: initTimeData,
    introduction: "",
    country: "",
    businessProductTypes: [""],
    password: "",
    serviceType: "",
  });
  return (
    <>
      <ElementWrapper
        onClick={() => setOpen(!isOpen)}
        className="flex justify-end w-full"
      >
        {ButtonMenu}
      </ElementWrapper>
      <Dialog open={isOpen} onClose={handleClose} size="lg">
        <BranchManagementDialogContainer>
          <DialogTitle>{title}</DialogTitle>
          <Formik
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={handleSubmit}
            enableReinitialize
          >
            <Form>
              <FormGroup>
                <Title>
                  {t(
                    "configuration.branch-management.general-info.general-info",
                  )}
                </Title>
                <ElementWrapper className="grid grid-cols-2 gap-3">
                  <ElementWrapper className="space-y-3">
                    <Input
                      name="name"
                      label={t("configuration.branch-management.name")}
                      placeholder={t(
                        "configuration.branch-management.general-info.enter-name",
                      )}
                      required
                      autoComplete="off"
                    />
                    <Input
                      name="phoneNumber"
                      label={t(
                        "configuration.branch-management.general-info.phone-number",
                      )}
                      placeholder={t(
                        "configuration.branch-management.general-info.please-enter-phone-number",
                      )}
                      required
                    />

                    <Input
                      name="password"
                      label={t("configuration.customer-management.password")}
                      placeholder={t(
                        "configuration.customer-management.please-enter-password",
                      )}
                      required={editField ? false : true}
                      disabled={editField ? true : false}
                    />
                    <Input
                      name="maxGuest"
                      label={t(
                        "configuration.branch-management.general-info.max-customer",
                      )}
                      placeholder={t(
                        "configuration.branch-management.general-info.enter-max-customer",
                      )}
                      type="number"
                    />
                    <Select
                      name="status"
                      label={t(
                        "configuration.branch-management.general-info.status",
                      )}
                      formTarget="name"
                      options={initStatus}
                      optionSelected={statusSelected}
                      onSelect={status => setStatusSelected(status)}
                      optionTarget="name"
                    />
                  </ElementWrapper>
                  <ElementWrapper className="space-y-3">
                    <div className="flex items-center gap-2">
                      <Select
                        name="country"
                        label={t("register.country")}
                        onSelect={country => {
                          setCountryAPISelected(country);
                          setIsChangeCountry(true);
                        }}
                        optionSelected={countryAPISelected}
                        options={listCountryState}
                        className="max-w-[150px] w-full"
                      />
                      <Select
                        name="state"
                        label={
                          countryAPISelected?.alpha2Code === "de"
                            ? t("register.state")
                            : t("register.state-selectedVietnam")
                        }
                        optionSelected={stateSelected}
                        options={listState}
                        onSelect={state => setStateSelected(state)}
                        className="max-w-[300px] w-full"
                      />
                    </div>
                    <Select
                      name="district"
                      label={
                        countryAPISelected?.alpha2Code === "de"
                          ? t("register.district")
                          : t("register.district-selectedVietnam")
                      }
                      required
                      onSelect={value => setDistrictSelected(value)}
                      optionSelected={districtSelected}
                      options={listDistrict}
                    />
                    <Select
                      name="city"
                      label={
                        countryAPISelected?.alpha2Code === "de"
                          ? t("register.city")
                          : t("register.city-selectedVietnam")
                      }
                      required
                      onSelect={city => setCitySelected(city)}
                      optionSelected={citySelected}
                      options={listCity}
                    />
                    <Input
                      name="address"
                      label={t(
                        "configuration.branch-management.general-info.street",
                      )}
                      placeholder={t(
                        "configuration.branch-management.general-info.enter-street",
                      )}
                    />
                    <MultipleAutocomplete
                      label={t(
                        "configuration.branch-management.business-product",
                      )}
                      name="businessProductTypes"
                      options={
                        businessData?.getAllBusinessProductType?.results || []
                      }
                      listOptionsSelected={businessTypeSelected}
                      onSelect={items => setBusinessTypeSelected(items)}
                    />
                    <Select
                      label={t("configuration.branch-management.service-type")}
                      name="serviceType"
                      onSelect={setServiceTypeSelected}
                      optionSelected={serviceTypeSelected}
                      options={ServiceType?.getAllServiceType?.results || []}
                    />
                  </ElementWrapper>
                  <ElementWrapper className="space-y-3 col-span-2">
                    <RichTextEditor
                      name="introduction"
                      label={t(
                        "configuration.branch-management.general-info.restaurant-description",
                      )}
                      defaultValue={initialValues.introduction}
                    />
                  </ElementWrapper>
                </ElementWrapper>
              </FormGroup>
              {!editField && (
                <FormGroup>
                  <Title>
                    {t(
                      "configuration.branch-management.general-info.active-time",
                    )}
                  </Title>
                  <ElementWrapper className="grid grid-cols-5 gap-3 col">
                    <ElementWrapper className="col-span-3">
                      <Schedule
                        onChange={setTimeSelected}
                        name="time"
                        maxField={2}
                      />
                    </ElementWrapper>
                  </ElementWrapper>
                </FormGroup>
              )}

              <ButtonWrapper>
                <Button outline type="button" onClick={() => setOpen(false)}>
                  {t("common.cancel")}
                </Button>
                <Button type="submit" primary>
                  {t("common.accept")}
                </Button>
              </ButtonWrapper>
            </Form>
          </Formik>
        </BranchManagementDialogContainer>
      </Dialog>
    </>
  );
};
export default BranchManagementDialog;

const initStatus: { name: string; status: BranchStatus }[] = [
  {
    name: "Closed",
    status: BranchStatus.Closed,
  },
  {
    name: "Is Active",
    status: BranchStatus.Isactive,
  },
  {
    status: BranchStatus.Opening,
    name: "Opening",
  },
  {
    status: BranchStatus.Stopworking,
    name: "Stop working",
  },
  {
    status: BranchStatus.Pause,
    name: "Pause",
  },
];
