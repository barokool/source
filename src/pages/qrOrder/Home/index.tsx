import { HomePageContainer } from "./styles";
import OrderSection from "./Order";
import CallService from "./CallService";
import StoreSection from "./StoreSection";

interface IHomePageProps {}

const HomePage: React.FC<IHomePageProps> = props => {
  return (
    <HomePageContainer>
      <StoreSection />
      <OrderSection />
      <CallService />
    </HomePageContainer>
  );
};

export default HomePage;
