import { useGetAllPrinter } from "apiCaller";
import React, { useEffect } from "react";
import { fragmentGetAllPrinterTemplate } from "services/printer";

export const usePrinter = (filterBranch = "") => {
  const [getAllPrinter, { data, loading }] = useGetAllPrinter(
    fragmentGetAllPrinterTemplate,
  );
  useEffect(() => {
    invokeGetPrinter();
  }, []);
  const invokeGetPrinter = () => {
    getAllPrinter({
      variables: {
        filter: {
          branch: filterBranch,
        },
      },
    });
  };
  return {
    listPrinter: data,
    loading,
  };
};
