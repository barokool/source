/**
 * @TODO
 *  [ ] convert svg to jsx - Come here: https://magic.reactjs.net/htmltojsx.html
 *  [ ] replace all fill="..." to  fill="currentColor" (except fill=none)
 *  [ ] add {...props} to <svg >
 */

import { IIconSVGProps } from "typings";

const StarIcon: React.FC<IIconSVGProps> = props => {
  return (
    <svg
      width={21}
      height={20}
      viewBox="0 0 21 20"
      fill="currentColor"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M20.4479 7.67191C20.3169 7.26693 19.9577 6.97929 19.5328 6.94099L13.7602 6.41684L11.4776 1.07414C11.3093 0.682593 10.926 0.429138 10.5001 0.429138C10.0742 0.429138 9.69091 0.682593 9.5226 1.07506L7.23998 6.41684L1.46651 6.94099C1.04231 6.98021 0.684023 7.26693 0.552336 7.67191C0.42065 8.07689 0.542265 8.52108 0.863166 8.80109L5.22653 12.6278L3.93987 18.2955C3.84573 18.7122 4.00747 19.143 4.35325 19.3929C4.5391 19.5272 4.75655 19.5956 4.97582 19.5956C5.16488 19.5956 5.35242 19.5446 5.52073 19.4439L10.5001 16.4679L15.4776 19.4439C15.8419 19.663 16.301 19.643 16.646 19.3929C16.992 19.1422 17.1536 18.7113 17.0594 18.2955L15.7728 12.6278L20.1361 8.80185C20.457 8.52108 20.5796 8.07765 20.4479 7.67191Z"
        fill="currentColor"
      />
    </svg>
  );
};

export default StarIcon;
