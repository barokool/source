import styled from "styled-components/macro";
import tw from "twin.macro";
import { Form as _Form } from "formik";
import _Button from "designs/Button";

export const ElementWrapper = styled.div`
  ${tw`cursor-pointer `}
`;

export const UserDialogContainer = styled.div`
  ${tw`bg-white p-1 phone:p-2 laptop:p-5 rounded-sm`}
`;

export const Form = styled(_Form)`
  ${tw`grid gap-2`}
`;

export const ButtonWrapper = styled.div`
  ${tw`flex justify-end mt-3`}
`;

export const Button = styled(_Button)`
  ${tw`py-1 w-max px-2 flex justify-center ml-1`}
`;

export const FormGroup = styled.div`
  ${tw`grid grid-cols-1 phone:grid-cols-2 gap-2`}
`;

export const Other = {
  ButtonWrapper: styled.div`
    ${tw`flex gap-1`}
  `,
  Button: styled(_Button)`
    ${tw`py-1 w-20 flex justify-center ml-1`}
  `,
};

export const Group = {
  Wrapper: styled.div`
    ${tw`flex flex-col gap-2 pb-2 border-b border-solid border-neutral-4`}
  `,
  Right: styled.div`
    ${tw`flex gap-2`}
  `,
  TextWrapper: styled.div`
    ${tw`flex flex-col gap-[6px]`}
  `,
  Text: styled.p`
    ${tw`font-regular  text-black text-14 text-left`}
  `,
  Input: styled.input`
    ${tw`border-none w-full outline-none`}
  `,
};
