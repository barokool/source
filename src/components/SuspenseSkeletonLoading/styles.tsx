import styled from "styled-components/macro";
import tw from "twin.macro";

export const SuspenseSkeletonLoadingContainer = styled.div`
  ${tw``}
`;
