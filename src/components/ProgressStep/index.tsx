import { ProgressStepsContainer, Step } from "./styles";

interface IProgressStepsProps {
  steps: string[];
  // Count from 1
  currentStep: number;
  // step count from 1
  onClickStep?: (step: number) => void;
}

const ProgressSteps: React.FC<IProgressStepsProps> = ({
  steps,
  currentStep,
  onClickStep,
}) => {
  return (
    <ProgressStepsContainer>
      {steps.map((step, index) => {
        const active = index === currentStep - 1;
        const isPassed = index < currentStep - 1;

        return (
          <Step.Wrapper
            onClick={() => isPassed && onClickStep?.(index + 1)}
            isLast={index === steps.length - 1}
          >
            <Step.Container isPassed={isPassed} active={active}>
              <Step.Click className="step-number" active={active}>
                +
              </Step.Click>
              <p>{step}</p>
            </Step.Container>
          </Step.Wrapper>
        );
      })}
    </ProgressStepsContainer>
  );
};

export default ProgressSteps;
