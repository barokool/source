import { TableStatus } from "apiCaller";
import styled from "styled-components/macro";
import tw, { TwStyle } from "twin.macro";

const listStatus: {
  [k in TableStatus]: TwStyle;
} = {
  using: tw`bg-[#f1c9c8] text-[#CE1611]`,
  empty: tw`bg-[#d6f2d8] text-[#007637]`,
  waiting: tw`bg-[#b2e9cd] text-[#007637]`,
  ordered: tw`bg-[#b2e9cd] text-[#007637]`,
  available: tw`bg-[#b2e9cd] text-[#007637]`,
  unavailable: tw`bg-[#b2e9cd] text-[#007637]`,
};
export const TableStatusTagContainer = styled.div<{
  active: TableStatus;
}>`
  ${tw`flex justify-center p-1 rounded-sm text-14 font-regular whitespace-nowrap w-10`}
  ${({ active }) => listStatus[active]}
`;
