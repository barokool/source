import { ProductStatus } from "apiCaller";
import { ProductStatusTagContainer } from "./styles";

interface ProductStatusTagProps {
  active: ProductStatus;
}

const ProductStatusTag: React.FC<ProductStatusTagProps> = ({
  active,
  children,
}) => {
  return (
    <ProductStatusTagContainer active={active}>
      {children}
    </ProductStatusTagContainer>
  );
};

export default ProductStatusTag;
