import { Field } from "formik";
import styled from "styled-components/macro";
import tw from "twin.macro";

export const ScheduleContainer = styled.div`
  ${tw`space-y-3`}
`;

export const ItemContainer = styled.div`
  ${tw`flex gap-x-3  items-start`}
`;
export const Label = styled.label`
  ${tw`w-[120px] h-5 flex-shrink-0 font-medium flex items-center gap-1`}
`;
export const ElementWrapper = styled.div`
  ${tw`  `}
`;

export const Input = styled(Field)`
  ${tw`hidden`}
`;

export const StyleRadioButton = styled.div<{
  checked: boolean;
  disabled?: boolean;
}>`
  ${tw`relative w-[20px] h-[20px] rounded flex items-center justify-center border-[2px] border-primary`}
  ${({ disabled }) => disabled && tw`border-neutral-3`}
  & svg {
    ${tw`absolute w-full h-full duration-300`}
    ${({ disabled }) => (disabled ? tw`bg-neutral-3` : tw`bg-primary`)}
    ${({ checked }) =>
      checked ? tw`visible opacity-100` : tw`invisible opacity-0`}
  }
`;
