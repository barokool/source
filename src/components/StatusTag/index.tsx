import { StatusTagContainer } from "./styles";

interface IStatusTagProps {
  active: boolean;
  activeLabel: string;
  inactiveLabel: string;
}

const StatusTag: React.FC<IStatusTagProps> = props => {
  const { active, activeLabel, inactiveLabel } = props;
  return (
    <StatusTagContainer active={active}>
      {active ? activeLabel : inactiveLabel}
    </StatusTagContainer>
  );
};

export default StatusTag;
