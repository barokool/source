import { useState } from "react";
import { RadioButtonContainer, Icon, Label } from "./styles";

interface IRadioButtonProps {
  label: string;
  value: boolean;
  onChange: (value: boolean) => void;
}

const RadioButton: React.FC<IRadioButtonProps> = props => {
  const { label, value, onChange } = props;
  const [enable, setEnable] = useState(value);
  const handleChange = () => {
    setEnable(!enable);
    onChange(!enable);
  };
  return (
    <RadioButtonContainer>
      <Icon
        onClick={handleChange}
        name={`common/${enable ? "checked-box" : "un-checked-box"}`}
        width={24}
        height={24}
      />
      <Label>{label}</Label>
    </RadioButtonContainer>
  );
};

export default RadioButton;
