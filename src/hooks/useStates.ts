import { GetAllStateArgs, State, useGetAllState } from "apiCaller";
import { useState, useCallback, useEffect } from "react";
import { fragmentGetAllStates } from "services/location";
export const useStates = () => {
  const [getAllState, { data, called, loading }] =
    useGetAllState(fragmentGetAllStates);
  useEffect(() => {
    invokeSetAllStates();
  }, []);

  const invokeSetAllStates = useCallback(() => {
    getAllState({
      variables: {},
    });
  }, []);

  return {
    listState: data,
    invokeSetAllStates,
  };
};
