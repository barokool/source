import React, { useState, useEffect } from "react";

import {
  HeaderContainer,
  HeaderLabel,
  PaymentPageContainer,
  BodyContainer,
  ButtonWrapper,
  Button,
} from "./styles";
import SVG from "designs/SVG";
import GroupRadioButton from "designs/GroupRadioButton";

import { IOptions } from "designs/GroupRadioButton";

export interface IBrandOption extends IOptions {
  name?: string;
  logo?: string;
}

interface IPaymentPageProps {}

const PaymentPage: React.FC<IPaymentPageProps> = props => {
  const [listBrand, setListBrand] = useState<IBrandOption[]>([
    {
      _id: "1",
      label: "Momo E-Wallet",
      svg: "qrOrder/momo-wallet",
    },
    {
      _id: "2",
      label: "Zalo pay",
      svg: "qrOrder/zalo-pay",
    },
  ]);
  const [brandSelected, setBrandSelected] = useState<IBrandOption>();
  return (
    <PaymentPageContainer>
      <HeaderContainer>
        <SVG name="common/arrow-left" />
        <HeaderLabel>Quay lại</HeaderLabel>
      </HeaderContainer>
      <BodyContainer>
        <GroupRadioButton
          options={listBrand}
          optionSelected={brandSelected as IBrandOption}
          onChange={value => setBrandSelected(value)}
        />
      </BodyContainer>

      <ButtonWrapper>
        <Button>Xác nhận</Button>
      </ButtonWrapper>
    </PaymentPageContainer>
  );
};

export default PaymentPage;
