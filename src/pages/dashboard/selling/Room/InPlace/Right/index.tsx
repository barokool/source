import React, { useState, useEffect } from "react";

//redux
import { useSelector } from "react-redux";
import { IRootState } from "redux/storeToolkit";

import { OrderTypeEnum, OrderStatusEnum } from "apiCaller";

//local
import { InPlaceRightContainer } from "./styles";
import RightCheckOut from "./CheckOut";
import RightProcessing from "./Processing";
import RightPayment from "./Payment";
import RightCompleted from "./Completed";
import Empty from "./Empty";

interface IInPlaceRightProps {}

const InPlaceRight: React.FC<IInPlaceRightProps> = props => {
  const { orderSelected } = useSelector((state: IRootState) => state.order);
  const [initComponent, setInitComponent] = useState<React.ReactNode>(<></>);

  useEffect(() => {
    if (orderSelected?.type === OrderTypeEnum.In_place) {
      if (orderSelected.orderItems?.length === 0) {
        setInitComponent(<Empty />);
      } else {
        switch (orderSelected?.status) {
          case OrderStatusEnum.New:
            if ((orderSelected?.orderItems?.length as number) > 0)
              setInitComponent(<RightCheckOut />);
            break;
          case OrderStatusEnum.Approving:
            setInitComponent(<RightPayment />);
            break;
          case OrderStatusEnum.Processing:
            setInitComponent(<RightProcessing />);
            break;
          case OrderStatusEnum.Complete:
            setInitComponent(<RightCompleted />);
        }
      }
    }
  }, [orderSelected]);

  return <InPlaceRightContainer>{initComponent}</InPlaceRightContainer>;
};

export default InPlaceRight;
