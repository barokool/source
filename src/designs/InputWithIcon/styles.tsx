import styled from "styled-components/macro";
import tw from "twin.macro";

export const InputContainer = styled.div`
  ${tw``}
`;

export const Label = styled.p`
  ${tw`mb-0.5 text-16 text-black font-medium`}
`;

export const InputLabel = styled.label`
  ${tw`p-1 bg-white w-full grid grid-cols-auto-1fr items-center border rounded-sm overflow-hidden`}
`;

export const IconWrapper = styled.div`
  ${tw`pr-1.5 flex items-center justify-center border-r`}
`;

export const Input = styled.input`
  ${tw`w-full h-full font-medium text-16 px-1 focus:outline-none`}
`;

export const Error = styled.p`
  ${tw`flex items-center text-16 text-warning leading-5 mt-1`}
`;
