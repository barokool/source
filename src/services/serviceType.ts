import { GenFields, ServiceType, ServiceTypeResult } from "apiCaller";

export const fragmentGetAllServiceType: GenFields<ServiceTypeResult> = [
  { results: ["_id", "name", { businessProductTypes: ["_id", "name"] }] },
  "totalCount",
];
export const fragmentCreateServiceType: GenFields<ServiceType> = [
  "_id",
  "name",
];
export const fragmentUpdateServiceType: GenFields<ServiceType> = [
  "_id",
  "name",
];
