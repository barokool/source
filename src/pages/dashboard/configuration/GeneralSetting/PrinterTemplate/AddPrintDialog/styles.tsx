import styled from "styled-components/macro";
import tw from "twin.macro";
import { Form as _Form } from "formik";
import _Button from "designs/Button";

export const AddPrintDialogContainer = styled.div`
  ${tw`bg-white overflow-x-hidden p-1 phone:p-2 laptop:p-5`}
`;

export const ElementWrapper = styled.div`
  ${tw``}
`;

export const Form = styled(_Form)`
  ${tw`space-y-1`}
`;

export const ButtonWrapper = styled.div`
  ${tw`flex justify-end mt-3`}
`;

export const Button = styled(_Button)`
  ${tw`py-1 px-2 w-max flex justify-center ml-1`}
`;

export const FormGroup = styled.div`
  ${tw`mb-3`}
`;

export const TitleLv1 = styled.span`
  ${tw`text-20 font-bold`}
`;

export const TitleLv2 = styled.div`
  ${tw`relative text-16 z-10 font-medium py-1`}
  &::after {
    content: "";
    z-index: -1;
    ${tw`absolute  h-full w-[100vw] bg-neutral-5`}
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }
`;
