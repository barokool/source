import { GenFields, Topping } from "apiCaller";

export const fragmentGetAllTopping: GenFields<Topping> = [
  "totalCount",
  {
    results: [
      "_id",
      "name",
      "description",
      "price",
      { image: ["default", "medium", "small"] },
      { restaurant: ["_id", "name"] },
      "status",
    ],
  },
];
