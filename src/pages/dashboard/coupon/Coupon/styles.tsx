import styled from "styled-components/macro";
import tw from "twin.macro";
import _SearchBox from "components/SearchBoxTable";
import _Button from "designs/Button";

export const CouponContainer = styled.div`
  ${tw`  `}
`;

export const SearchBox = styled(_SearchBox)`
  ${tw`w-full phone:w-55 phone:mb-0`}
`;

export const AddButton = styled(_Button)`
  ${tw`px-3.5 py-[12.5px] font-medium  text-white w-max`}
`;

export const RenderActionContainer = styled.div`
  ${tw`flex justify-end gap-2`}
`;
