import tw from "twin.macro";
import styled from "styled-components/macro";
import _SearchBox from "components/SearchBoxTable";
import _Button from "designs/Button";

export const CustomerReportContainer = styled.div`
  ${tw``}
`;

export const ActionWrapper = styled.div`
  ${tw`flex flex-col phone:flex-row justify-between mb-4`}
`;
export const SearchBox = styled(_SearchBox)`
  ${tw`w-full phone:w-55 phone:mb-0`}
`;

export const AddButton = styled(_Button)`
  ${tw`px-3.5 py-[12.5px] font-medium bg-primary text-white w-max`}
`;

export const FilterButton = styled(_Button)`
  ${tw`px-3.5 py-[12.5px] font-medium bg-white text-primary w-max border-2 border-primary`}
`;

export const RightButtonWrapper = styled.div`
  ${tw`w-[310px] flex justify-between`}
`;
