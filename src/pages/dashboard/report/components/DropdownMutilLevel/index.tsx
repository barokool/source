import { Listbox, Transition } from "@headlessui/react";
import _Button from "designs/Button";
import { Fragment, useEffect, useState } from "react";

import {
  Box,
  ContentLevel1,
  ContentLevel2,
  DropdownMultiLevelContainer,
  FilterButton,
  List,
  Main,
} from "./styles";

export type IChildren = {
  key: number;
  data: string;
  value?: string;
  childrens?: IChildren[];
  active?: boolean;
};

interface IDropdownMultiLevelProps {
  defaultValue: IChildren;
  options: IChildren[];
  onSelected: (value: IChildren) => void;
}

const DropdownMultiLevel = (props: IDropdownMultiLevelProps) => {
  const { options, defaultValue, onSelected } = props;

  const [valueSelected, setValueSelected] = useState<IChildren>(defaultValue);
  const [list, setList] = useState<IChildren[]>([]);

  useEffect(() => {
    const newList = handleCreateNewList(options);
    setList(newList);
  }, []);

  const handleCreateNewList = (list: IChildren[]) => {
    const newList = options.map(value =>
      value && value.childrens && value.childrens?.length > 0
        ? { ...value, active: false }
        : { ...value },
    );

    return newList;
  };

  const handleClickLevel1 = (select: IChildren) => {
    const newList = list.map(value =>
      value.data === select.data
        ? {
            ...value,
            active: !value.active,
          }
        : {
            ...value,
            active: false,
          },
    );
    setList(newList);
  };

  const handleSelected = (value: IChildren) => {
    const newList = list.map(value => ({
      ...value,
      active: false,
    }));
    console.log(value);

    setList(newList);
    setValueSelected(value);
    onSelected(value);
  };

  return (
    <DropdownMultiLevelContainer>
      <Listbox as="div" value={valueSelected} onChange={handleSelected}>
        <Listbox.Button>
          <FilterButton>{valueSelected.data}</FilterButton>
        </Listbox.Button>
        <Transition
          enter="transition duration-100 ease-out"
          enterFrom="transform scale-95 opacity-0"
          enterTo="transform scale-100 opacity-100"
          leave="transition duration-75 ease-out"
          leaveFrom="transform scale-100 opacity-100"
          leaveTo="transform scale-95 opacity-0"
        >
          <List>
            <Listbox.Options static>
              {list.map((item, index) => (
                <Box key={item.data} onClick={() => handleClickLevel1(item)}>
                  <ContentLevel1 active={!!item.active}>
                    {item.data}
                  </ContentLevel1>

                  {item.active && (
                    <Main>
                      {item &&
                        item.childrens &&
                        item.childrens.map((value: IChildren) => (
                          <Listbox.Option key={value.key} value={value}>
                            <ContentLevel2>{value.data}</ContentLevel2>
                          </Listbox.Option>
                        ))}
                    </Main>
                  )}
                </Box>
              ))}
            </Listbox.Options>
          </List>
        </Transition>
      </Listbox>
    </DropdownMultiLevelContainer>
  );
};

export default DropdownMultiLevel;
