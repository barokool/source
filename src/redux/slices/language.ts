import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { IItemLanguage } from "typings";
interface ILanguageState {
  languageSelected: IItemLanguage;
}

const initialState: ILanguageState = {
  languageSelected: {
    id: "1",
    name: "Eng",
    lng: "en",
    phonePrefix: "+84",
    flag: "register/american-flag",
  },
};

export const languageSlice = createSlice({
  name: "language",
  initialState,
  reducers: {
    setLanguageSelected: (state, action: PayloadAction<IItemLanguage>) => {
      state.languageSelected = action.payload;
    },
  },
});

// Actions
export const { setLanguageSelected } = languageSlice.actions;

export default languageSlice.reducer;
