import { IRoutes } from "typings";
import { PATH } from "common/constants/routes";
import { lazy } from "react";
import { t } from "language";

import ShippingIcon from "icons/Dashboard/Shipping";

const Order = lazy(() => import("pages/dashboard/shipping/Shipping"));
const Driver = lazy(() => import("pages/dashboard/shipping/Driver"));

export const shippingRoutes: IRoutes = {
  name: t("shipping.shipping.shipping"),
  path: PATH.SHIPPING.SELF,
  exact: true,
  isPrivate: true,
  Icon: ShippingIcon,
  children: [
    {
      name: t("shipping.shipping.shipping"),
      path: PATH.SHIPPING.SHIPPING,
      isPrivate: true,
      Component: Order,
      exact: true,
    },
    {
      name: t("shipping.driver.driver"),
      path: PATH.SHIPPING.DRIVER,
      isPrivate: true,
      Component: Driver,
      exact: true,
    },
  ],
};
