import React, { useState, useEffect } from "react";
import Input from "designs/InputV2";
import * as Yup from "yup";
import { Formik } from "formik";
import { toast } from "react-toastify";
import { PATH } from "common/constants/routes";

//redux
import { useSelector } from "react-redux";
import { IRootState } from "typings";

//hooks
import { useRedirect } from "hooks/useRedirect";

//languages
import { t } from "language";

//locals
import { InputPasswordContainer, Button, Form, InputWrapper } from "./styles";

//api
import { useForgotPassword } from "apiCaller";
import { fragmentForgotPassword } from "services/user";

interface IInputPasswordProps {}

interface IFormValue {
  password?: string;
  confirmPassword?: string;
}

const validationSchemaPassword = Yup.object().shape<{
  [key in keyof IFormValue]: any;
}>({
  password: Yup.string().required(t("resetPassword.please-enter-password")),
  confirmPassword: Yup.string()
    .required(t("resetPassword.please-confirm-password"))
    .when("password", {
      is: (val: string) => (val && val.length > 0 ? true : false),
      then: Yup.string().oneOf(
        [Yup.ref("password")],
        t("resetPassword.password-must-match"),
      ),
    }),
});

const InputPassword: React.FC<IInputPasswordProps> = props => {
  //redux
  const { phoneNumber, token } = useSelector(
    (state: IRootState) => state.resetPassword,
  );
  //local states
  const [initialValues, setInitialValues] = useState<IFormValue>({
    confirmPassword: "",
    password: "",
  });

  //api
  const [resetPassword, { data, loading, error }] = useForgotPassword(
    fragmentForgotPassword,
  );

  const redirect = useRedirect();

  const handleSubmit = (values: IFormValue) => {
    resetPassword({
      variables: {
        input: {
          newPassword: values.password as string,
          idToken: token as string,
          phoneNumber: phoneNumber as string,
        },
      },
    });
  };

  useEffect(() => {
    if (data && !error) {
      toast.success(t("resetPassword.update-password-success"));
      redirect(PATH.AUTH.LOGIN);
    }
  }, [data, error]);

  return (
    <InputPasswordContainer>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchemaPassword}
        enableReinitialize
        onSubmit={handleSubmit}
      >
        <Form>
          <InputWrapper>
            <Input
              name="password"
              value={initialValues.password}
              label={t("resetPassword.password")}
              placeholder={t("resetPassword.enter-password")}
              required
              type="password"
            />
            <Input
              name="confirmPassword"
              value={initialValues.confirmPassword}
              label={t("resetPassword.confirm-password")}
              placeholder={t("resetPassword.enter-confirm-password")}
              required
              type="password"
            />
          </InputWrapper>
          <Button primary type="submit" loading={loading}>
            {t("resetPassword.confirm")}
          </Button>
        </Form>
      </Formik>
    </InputPasswordContainer>
  );
};

export default InputPassword;
