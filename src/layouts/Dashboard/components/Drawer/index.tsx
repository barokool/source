import { Fragment, useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

import { IRootState } from "redux/storeToolkit";
import { dashboardRoutes } from "routes/Routes";

import MenuItem from "./MenuItem";

interface IShowDropDown {
  Item: number;
  status: boolean;
}

import { DrawerContainer, Icon } from "./styles";

const Drawer: React.FC = () => {
  const history = useHistory();
  const [activeMenu, setActiveMenu] = useState<number>(0);
  const [showDropDown, setShowDropDown] = useState<IShowDropDown>({
    Item: 0,
    status: true,
  });

  const [currentPath, setCurrentPath] = useState("");
  const { isExtendDrawer, isCollapse } = useSelector((state: IRootState) => {
    return state._config;
  });

  useEffect(() => {
    setCurrentPath(history.location.pathname);
  }, [history.location.pathname]);

  useEffect(() => {
    let isActive = 0;
    dashboardRoutes.forEach((item, index) => {
      if (item.children) {
        if (
          item.children.find(item => item.path === history.location.pathname)
        ) {
          isActive = index;
        }
      } else {
        if (item.path === history.location.pathname) {
          isActive = index;
        }
      }
    });
    setActiveMenu(isActive);
    setShowDropDown({ Item: isActive, status: true });
  }, [history.location.pathname]);

  const handleActiveMenu = (index: number) => {
    setActiveMenu(index);
  };

  const handleShowDropDown = (index: number) => {
    if (showDropDown.Item === index) {
      setShowDropDown(pre => {
        return { ...pre, status: !pre.status };
      });
    } else {
      setShowDropDown({ Item: index, status: true });
    }
  };
  return (
    <DrawerContainer isCollapse={isCollapse} isExtendDrawer={isExtendDrawer}>
      {dashboardRoutes.map((route, index) => {
        if (!route.hiddenRoute) {
          return (
            <Fragment key={`menu-with-sub-${index}`}>
              <MenuItem
                isCollapse={isCollapse}
                route={route}
                setCurrentPath={value => setCurrentPath(value)}
                currentPath={currentPath}
                onActiveMenu={() => handleActiveMenu(index)}
                onShowDropDown={() => handleShowDropDown(index)}
                isShowDropDown={
                  showDropDown.Item === index && showDropDown.status === true
                }
                active={activeMenu === index}
              />
            </Fragment>
          );
        }
      })}
    </DrawerContainer>
  );
};

export default Drawer;
