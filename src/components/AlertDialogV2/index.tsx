import { useState } from "react";

import Dialog from "components/Dialog";
import Tooltip from "designs/Tooltip";

import {
  ElementButton,
  AlertDialogContainer,
  Title,
  Message,
  ButtonWrapper,
  Button,
} from "./styles";

interface IAlertDialogProps {
  loading?: boolean;
  ButtonMenu?: React.ReactElement;
  title: string;
  message?: string;
  data?: any;
  tooltip?: string;
  onConfirm: () => void;
  onClose?: () => void;
  isOpen: boolean;
}

const AlertDialog: React.FC<IAlertDialogProps> = props => {
  const {
    ButtonMenu,
    isOpen,
    title,
    loading,
    message,
    onConfirm,
    data,
    tooltip,
    onClose,
  } = props;

  const handleConfirm = () => {
    onConfirm();
  };

  const handleClose = () => {
    !loading && onClose?.();
  };

  return (
    <>
      {ButtonMenu ? ButtonMenu : null}
      <Dialog open={isOpen} onClose={handleClose}>
        <AlertDialogContainer>
          <Title>{title}</Title>
          <Message>{message}</Message>
          <ButtonWrapper>
            <Button outline onClick={handleClose}>
              Huỷ bỏ
            </Button>
            <Button primary loading={loading} onClick={handleConfirm}>
              Đồng ý
            </Button>
          </ButtonWrapper>
        </AlertDialogContainer>
      </Dialog>
    </>
  );
};

export default AlertDialog;
