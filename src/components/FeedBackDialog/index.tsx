import React, { useState, useEffect } from "react";

import {
  FeedBackDialogContainer,
  Button,
  ButtonWrapper,
  ElementWrapper,
  Form,
  MidContentContainer,
  RateTitle,
  DialogTitle,
  StarsContainer,
} from "./styles";

import { Formik } from "formik";

import Input from "designs/InputV2";
import SVG from "designs/SVG";
import Dialog from "components/Dialog";
import StarIcon from "icons/Star";

import { t } from "language";
import IconButton from "designs/IconButton";

interface IFeedBackDialogProps {
  ButtonMenu: React.ReactElement;
  onClose?: () => void;
  onSuccess?: () => void;
}

interface IFormValue {
  review?: string;
}

const FeedBackDialog: React.FC<IFeedBackDialogProps> = ({
  ButtonMenu,
  onClose,
  onSuccess,
}) => {
  const [loading, setLoading] = useState<boolean>(false);
  const [isOpen, setOpen] = useState<boolean>(false);
  const [rateOptions, setRateOptions] = useState<
    { id: string; title: string; icon: string; star: number }[]
  >([
    {
      id: "1",
      title: "Rất tệ",
      icon: "qrOrder/feedback-very-bad",
      star: 1,
    },
    {
      id: "2",
      title: "Tệ",
      icon: "qrOrder/feedback-bad",
      star: 2,
    },
    {
      id: "3",
      title: "Không hài lòng",
      icon: "qrOrder/feedback-unsatisfy",
      star: 3,
    },
    {
      id: "4",
      title: "Tốt",
      icon: "qrOrder/feedback-normal",
      star: 4,
    },
    {
      id: "5",
      title: "Hoàn hảo",
      icon: "qrOrder/feedback-perfect",
      star: 5,
    },
  ]);
  const [rateSelected, setRateSelected] = useState<{
    id: string;
    title: string;
    icon: string;
    star: number;
  }>(rateOptions[4]);

  const handleSubmit = () => {
    setOpen(false);
  };
  const initialValues = useState<IFormValue>({
    review: "",
  });

  return (
    <>
      <ElementWrapper
        className="flex justify-end w-full"
        onClick={() => setOpen(!isOpen)}
      >
        {ButtonMenu}
      </ElementWrapper>

      <Dialog
        open={isOpen}
        onClose={() => setOpen(false)}
        className="overflow-auto"
        size="md"
      >
        <FeedBackDialogContainer>
          <Formik initialValues={initialValues} onSubmit={handleSubmit}>
            <Form>
              <DialogTitle>
                Xin mời quý khách đánh giá dịch vụ khách hàng
              </DialogTitle>
              <MidContentContainer>
                <RateTitle>{rateSelected.title}</RateTitle>
                <StarsContainer>
                  {[...Array(5)].map((e, i) => (
                    <IconButton key={i}>
                      <StarIcon
                        className={`${
                          i + 1 <= rateSelected.star
                            ? "text-primary cursor-pointer"
                            : "cursor-pointer text-tertiary"
                        }`}
                        onClick={() => setRateSelected(rateOptions[i])}
                      />
                    </IconButton>
                  ))}
                </StarsContainer>
                <SVG
                  name={rateSelected.icon}
                  width={60}
                  height={60}
                  className="mx-auto"
                />
              </MidContentContainer>
              <Input
                name="review"
                label=""
                placeholder="Thêm đánh giá"
                type="text"
              />
              <ButtonWrapper>
                <Button type="submit" primary loading={loading}>
                  {t("common.accept")}
                </Button>
              </ButtonWrapper>
            </Form>
          </Formik>
        </FeedBackDialogContainer>
      </Dialog>
    </>
  );
};

export default FeedBackDialog;
