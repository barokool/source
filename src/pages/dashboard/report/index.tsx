import React, { useState, useEffect, useMemo } from "react";
import { ReportChildrenContainer, ReportPageContainer } from "./styles";
import { RouteComponentProps } from "react-router";
import { PATH } from "common/constants/routes";
//icons
import BranchIcon from "icons/Branch";
import SettingIcon from "icons/Setting";
import CustomerIcon from "icons/Customer";
// import MultUsersIcon from "icons/MultUsers";

//hooks
import { useBreadcrumb } from "hooks/useBreadcrumb";
import { Link } from "react-router-dom";
import { t } from "language";
import ConfigChildren, { IChildrenConfig } from "components/ConfigChildren";

interface IReportPageProps extends RouteComponentProps {}

const ReportPage: React.FC<IReportPageProps> = props => {
  const [reportChildren, setReportChildren] = useState<IChildrenConfig[]>([
    {
      Icon: (
        <>
          <BranchIcon />
        </>
      ),
      title: t("report.selling-report.selling-report"),
      description: t("report.selling-report.detail"),
      href: "/selling-report",
    },
    {
      Icon: (
        <>
          <SettingIcon />
        </>
      ),
      title: t("report.product-report.product-report"),
      description: t("report.product-report.detail"),
      href: "product-report",
    },
    {
      Icon: (
        <>
          <CustomerIcon />
        </>
      ),
      title: t("report.customer-report.customer-report"),
      description: t("report.customer-report.detail"),
      href: "customer-report",
    },
    {
      Icon: (
        <>
          {/* <MultUsersIcon /> */}
          <CustomerIcon />
        </>
      ),
      title: t("report.staffs-report.staffs-report"),
      description: t("report.staffs-report.detail"),
      href: "staff-report",
    },
  ]);

  useBreadcrumb([
    {
      name: t("report.report"),
      href: PATH.REPORT.SELF,
    },
  ]);
  return (
    <ReportPageContainer>
      <ReportChildrenContainer>
        {reportChildren.map((item, index) => {
          return (
            <Link to={item.href as string}>
              <ConfigChildren
                key={index}
                Icon={item.Icon}
                title={item.title}
                description={item.description}
                href={item.href}
              />
            </Link>
          );
        })}
      </ReportChildrenContainer>
    </ReportPageContainer>
  );
};

export default ReportPage;
