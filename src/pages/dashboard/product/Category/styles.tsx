import styled from "styled-components/macro";
import tw from "twin.macro";
import _Button from "designs/Button";
import _SearchBox from "components/SearchBoxTable";

export const CategoryContainer = styled.div`
  ${tw`  `}
`;

export const Button = styled(_Button)`
  ${tw`flex items-center  justify-center text-16 font-medium text-white py-1 px-2 mt-2`}
`;

export const SearchBox = styled(_SearchBox)`
  ${tw`w-full phone:w-55 phone:mb-0`}
`;

export const RenderActionContainer = styled.div`
  ${tw`flex justify-end gap-2`}
`;
