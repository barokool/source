export const randomInt = (min: number, max: number) =>
  parseInt(String(Math.random() * (max - min + 1)), 10) + min;
