import {
  Box,
  Code,
  Content,
  LeftSide,
  Name,
  OrderItemContainer,
  Phone,
  RightSide,
  Time,
  Title,
} from "./styles";
import { t } from "language";
import order from "redux/slices/order";
import { Order } from "apiCaller";

interface OrderItemProps {
  orderItem: Order;
  active?: boolean;
  onClick?: (value: any) => void;
}

const OrderItem: React.FC<OrderItemProps> = props => {
  const { orderItem, active, onClick } = props;
  return (
    <OrderItemContainer onClick={onClick} active={active}>
      <LeftSide>
        <Code>{orderItem.code}</Code>
        <Name>
          {!orderItem.customerName ? "Customer" : orderItem.customerName}
        </Name>
        <Time>{!orderItem.createdAt ? "15:36" : orderItem.createdAt}</Time>
        <Phone>
          {!orderItem.phoneNumber ? "1900100000" : orderItem.phoneNumber}{" "}
        </Phone>
      </LeftSide>
      <RightSide>
        <Box>
          <Title>{t("order.common.order-value")}</Title>
          <Content>{orderItem.price?.finalPrice || 0}</Content>
        </Box>
        <Box>
          <Title>{t("order.common.staff-in-charge")}</Title>
          <Content>
            {orderItem.createdBy?.displayName || "Huỳnh Ngọc Đức"}
          </Content>
        </Box>
      </RightSide>
    </OrderItemContainer>
  );
};

export default OrderItem;
