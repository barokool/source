import React, { ChangeEvent, useState } from "react";
import AwesomeDebouncePromise from "awesome-debounce-promise";
import useConstant from "use-constant";
import { SearchBoxTableContainer, Input } from "./styles";
import SearchIcon from "icons/Search";

const DELAY = 300;

interface ISearchBoxTableProps {
  className?: string;
  placeholder?: string;
  onFetchData: (text: string) => void;
  onFetchDataNumber?: (text: number) => void;
}

const SearchBoxTable: React.FC<ISearchBoxTableProps> = ({
  className = "",
  placeholder = "Search...",
  onFetchData,
}) => {
  const [text, setText] = useState<string>("");

  const searchAPIDebounced = useConstant(() =>
    AwesomeDebouncePromise(onFetchData, DELAY),
  );

  const handleTextChange = async (e: ChangeEvent<HTMLInputElement>) => {
    const newText = e.target.value;
    searchAPIDebounced(newText);
    setText(newText);
  };

  return (
    <SearchBoxTableContainer className={className}>
      <SearchIcon />
      <Input
        name="search"
        type="text"
        placeholder={placeholder}
        value={text}
        onChange={handleTextChange}
        className="bg-background"
      />
    </SearchBoxTableContainer>
  );
};

export default SearchBoxTable;
