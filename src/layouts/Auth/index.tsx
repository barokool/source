import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import i18n from "language";

import SimpleSelect from "designs/SimpleSelect";
import SVG from "designs/SVG";
import { IRootState } from "typings";
import { setLanguageSelected } from "redux/slices/language";
import { LISTCOUNTRY } from "common/constants/language";
import {
  BackgroundContent,
  BackgroundWrapper,
  ContentWrapper,
  LayoutContainer,
  OptionTitle,
  OptionWrapper,
  SelectLanguageWrapper,
} from "./styles";

const AuthLayout: React.FC = ({ children }) => {
  const { languageSelected } = useSelector(
    (state: IRootState) => state.language,
  );
  const dispatch = useDispatch();

  useEffect(() => {
    if (languageSelected) {
      i18n.changeLanguage(languageSelected.lng);
    }
  }, [languageSelected]);

  return (
    <LayoutContainer>
      <BackgroundWrapper>
        <BackgroundContent />
      </BackgroundWrapper>
      <ContentWrapper>
        <SelectLanguageWrapper>
          <SimpleSelect
            options={LISTCOUNTRY}
            optionSelected={languageSelected}
            onSelect={option => dispatch(setLanguageSelected(option))}
            renderOption={option => {
              return (
                <OptionWrapper>
                  <SVG name={option?.flag as string} />
                  <OptionTitle>{option?.name as string}</OptionTitle>
                </OptionWrapper>
              );
            }}
          />
        </SelectLanguageWrapper>
        {children}
      </ContentWrapper>
    </LayoutContainer>
  );
};

export default AuthLayout;
