import { useEffect, useState, useCallback } from "react";
import { PATH } from "common/constants/routes";
import TwoSideLayout from "layouts/TwoSide";
import SimpleDatePicker from "designs/SimpleDatePicker";
import Empty from "components/Empty";
import EmptyImage from "assets/images/shipping/OrderListEmpty.png";
import OrderInfo from "pages/dashboard/order/components/OrderInfo";
import OrderItem from "pages/dashboard/order/components/OrderItem";
import OrderDetail from "pages/dashboard/order/components/OrderDetail";
import SimpleSelect from "pages/dashboard/order/components/SimpleSelect";
import DeleteOrderDialog from "pages/dashboard/order/components/DeleteOrderDialog";
import OrderProgressBar from "components/OrderProgressBar";

//languages
import { t } from "language";

//hooks
import { useBreadcrumb } from "hooks/useBreadcrumb";
import { useLoading } from "hooks/useLoading";
import { useBranches } from "hooks/useBranches";
import { useCurrentBranch } from "hooks/useCurrentBranch";

//api
import { fragmentGetAllOrder } from "services/order";
import {
  Branch,
  Order,
  OrderStatusEnum,
  OrderTypeEnum,
  useGetAllOrder,
} from "apiCaller";

//locals
import {
  DeliveryOrderContainer,
  DeleteButton,
  Pagination,
  RightSideWrapper,
} from "./styles";

interface IShippingOrderProps {}

const LOAD_DATA = "LOAD_DATA";
const SIZE_PER_PAGE = 4;

const ShippingOrder: React.FC<IShippingOrderProps> = () => {
  const { listBranch } = useBranches();
  const { startLoading, stopLoading } = useLoading();
  const [orderSelected, setOrderSelected] = useState<Order | null>();
  const [getAllOrder, { data, loading }] = useGetAllOrder(fragmentGetAllOrder);
  const { branchSelected: defaultBranch } = useCurrentBranch();

  //local states
  const [page, setPage] = useState<number>(0);
  const [dateSelected, setDateSelected] = useState<string>();
  const [branchSelected, setBranchSelected] = useState<Branch>();
  const [listOrder, setListOrder] = useState<Order[]>([]);
  const [totalCount, setTotalCount] = useState<number>();

  useBreadcrumb([
    {
      name: t("shipping.shipping.shipping"),
      href: PATH.SHIPPING.SELF,
    },
    {
      name: t("shipping.shipping.order"),
      href: PATH.SHIPPING.SHIPPING,
    },
  ]);

  useEffect(() => {
    defaultBranch && setBranchSelected(defaultBranch);
  }, [defaultBranch]);

  useEffect(() => {
    defaultBranch && invokeGetAllData();
  }, [dateSelected, branchSelected, page, defaultBranch]);

  useEffect(() => {
    if (data) {
      setListOrder(data.getAllOrder.results as Order[]);
      setTotalCount(data.getAllOrder.totalCount as number);
    }
  }, [data]);

  useEffect(() => {
    loading ? startLoading(LOAD_DATA) : stopLoading(LOAD_DATA);
  }, [loading]);

  const invokeGetAllData = () => {
    getAllOrder({
      variables: {
        filter: {
          orderDate: dateSelected ? dateSelected : "",
          fromBranch: branchSelected
            ? (branchSelected._id as string)
            : (defaultBranch?._id as string),
          type: OrderTypeEnum.Delivery,
          status: OrderStatusEnum.Shipping,
        },
        size: SIZE_PER_PAGE,
        page: page,
      },
    });
  };
  const totalRevenueOfBill = () => {
    let total = 0;
    listOrder?.map(order => {
      total += order.price?.finalPrice || 0;
    });
    return total / (totalCount as number);
  };

  const totalRevenue = totalRevenueOfBill();

  const onPageChange = useCallback((nextPage: number) => {
    setPage(nextPage);
  }, []);

  return (
    <DeliveryOrderContainer>
      {totalCount === 0 && dateSelected == null ? (
        <Empty
          SVG={<img src={EmptyImage} className="my-auto" />}
          Title={t("order.common.empty-title")}
          SubTitle={t("order.common.empty-message")}
        />
      ) : (
        <TwoSideLayout>
          <TwoSideLayout.LeftSide>
            <TwoSideLayout.ButtonBox>
              <SimpleDatePicker
                label=""
                classNameSVG="absolute right-1"
                className="col-span-1"
                onDateChange={(date: Date) =>
                  setDateSelected(String(date).prettyDateReverse())
                }
              />
              <SimpleSelect
                label={t("order.common.select-branch")}
                onSelect={setBranchSelected}
                optionSelected={branchSelected}
                options={listBranch?.getAllBranch?.results || []}
              />
            </TwoSideLayout.ButtonBox>
            <OrderInfo
              data={[
                {
                  content: totalRevenue.toFixed().toString(),
                  title: t("order.delivery.revenue"),
                },
                {
                  content: totalCount?.toString() as string,
                  title: t("order.delivery.total"),
                },
              ]}
            />
            <TwoSideLayout.ListBox>
              {listOrder?.length == 0 ? (
                <h1>{t("order.common.empty-order")}</h1>
              ) : (
                ""
              )}
              {listOrder?.map((item, index) =>
                item._id === orderSelected?._id ? (
                  <OrderItem
                    key={index}
                    orderItem={item}
                    active
                    onClick={() => {
                      setOrderSelected(item);
                    }}
                  />
                ) : (
                  <OrderItem
                    key={index}
                    orderItem={item}
                    onClick={() => {
                      setOrderSelected(item);
                    }}
                  />
                ),
              )}
            </TwoSideLayout.ListBox>
            <Pagination
              sizePerPage={SIZE_PER_PAGE}
              onPageChange={onPageChange}
              totalSize={totalCount as number}
            />
          </TwoSideLayout.LeftSide>
          <TwoSideLayout.RightSide>
            {orderSelected && (
              <RightSideWrapper>
                <OrderProgressBar
                  status={orderSelected?.status as OrderStatusEnum}
                  className="ml-auto mr-0 desktop:mb-4 laptop:mb-3 mb-2"
                />
                <OrderDetail order={orderSelected as Order} />
              </RightSideWrapper>
            )}
            <DeleteOrderDialog
              order={orderSelected as Order}
              onSuccess={() => {
                invokeGetAllData();
                setOrderSelected(null);
              }}
            >
              {orderSelected && (
                <DeleteButton outline>
                  {t("order.common.delete-order")}
                </DeleteButton>
              )}
            </DeleteOrderDialog>
          </TwoSideLayout.RightSide>
        </TwoSideLayout>
      )}
    </DeliveryOrderContainer>
  );
};

export default ShippingOrder;
