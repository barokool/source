import { useEffect, useState } from "react";
import { Formik } from "formik";
import * as Yup from "yup";
import Input from "designs/InputV2";
import Select from "designs/Select";
import TextArea from "designs/TextArea";
import SingleImageUploaderV2 from "components/SingleImageUploaderV2";
import MultipleImageUploaderV2 from "components/MultiImageUploaderV2";
import Dialog from "components/Dialog";
import MultipleSelect from "designs/MultipleSelect";
import { DialogTitle } from "common/styles/Title";
import { CUSTOM_SIZE_CATEGORY_LV_2 } from "common/constants/category";
import { toast } from "react-toastify";
import AddTopping from "../../components/AddTopping";

//hooks
import { useProductCategory } from "hooks/useProductCategory";
import { useCurrentBranch } from "hooks/useCurrentBranch";
import { useTags } from "hooks/useTags";

//typings
import { IImage } from "typings";

//locals
import {
  ElementWrapper,
  UserDialogContainer,
  Form,
  ButtonWrapper,
  Button,
  FormGroup,
} from "./styles";

//api
import {
  Product,
  ProductCategory,
  Tag,
  ProductStatus,
  ImageInput,
  useCreateProduct,
  useUpdateProduct,
  ToppingInput,
  Topping,
} from "apiCaller";
import { fragmentUpdateProduct, fragmentCreateProduct } from "services/product";

//languages
import { t } from "language";

interface ProductDialogProps {
  ButtonMenu?: React.ReactElement;
  editField?: Product;
  title?: string;
  onClose?: () => void;
  onSuccess?: () => void;
  open?: boolean;
}

interface IFormValue {
  name: string;
  category: string;
  price: number;
  tag?: string[];
  description?: string;
  avatar?: string;
  images?: string[];
  topping?: ToppingInput[];
}
export interface IGroup {
  _id: string;
  name: string;
  product: Product[];
}
export interface IGroupInput {
  name: string;
  toppings: Product[];
}

const validationSchema = Yup.object().shape<{ [key in keyof IFormValue]: any }>(
  {
    name: Yup.string().required(
      t("product.product-list.please-enter-product-name"),
    ),
    category: Yup.string().required(
      t("product.product-list.please-select-product-category"),
    ),
    price: Yup.string().required(
      t("product.product-list.please-enter-product-price"),
    ),
  },
);

const ProductDialog: React.FC<ProductDialogProps> = ({
  ButtonMenu,
  editField,
  title,
  onSuccess,
  onClose,
  open = false,
}) => {
  //hooks
  const { branchSelected } = useCurrentBranch();
  const { listTag } = useTags();
  const { listProductCategory } = useProductCategory();

  //local states
  const [isOpen, setIsOpen] = useState(open);
  const [initialValues, setInitialValues] = useState<IFormValue>({
    name: "",
    category: "",
    price: 0,
  });
  const [tagSelected, setTagSelected] = useState<Tag[]>([]);
  const [categorySelected, setCategorySelected] =
    useState<ProductCategory | null>(null);
  const [avatarSelected, setAvatarSelected] = useState<IImage | null>(null);
  const [imagesSelected, setImagesSelected] = useState<IImage[]>([]);

  //api
  const [
    updateProduct,
    {
      data: updateProductData,
      error: updateProductErrors,
      loading: updateProductLoading,
    },
  ] = useUpdateProduct(fragmentUpdateProduct);
  const [
    createProduct,
    {
      data: createProductData,
      error: createProductErrors,
      loading: createProductLoading,
    },
  ] = useCreateProduct(fragmentCreateProduct);

  useEffect(() => {
    if (editField) {
      setInitialValues({
        name: editField.name as string,
        category: categorySelected?._id as string,
        price: editField.price as number,
        description: editField.description as string,
        images: imagesSelected.map(
          image => image.default || image.medium || (image.small as string),
        ) as string[],
        tag: editField?.tags?.map(item => item.name) as string[],
        avatar: editField.thumbnail?.small as string,
      });
      setCategorySelected(editField.productCategory as ProductCategory);
      setTagSelected(editField.tags as Tag[]);
      setCategorySelected(editField.productCategory as ProductCategory);
      setAvatarSelected(editField.thumbnail as IImage);
      setImagesSelected(editField.images as IImage[]);
    }
  }, [editField]);

  useEffect(() => {
    createProductErrors && toast.error(createProductErrors.message);
    updateProductErrors && toast.error(updateProductErrors);
  }, [createProductErrors, updateProductErrors]);

  useEffect(() => {
    if (createProductData && !createProductErrors) {
      toast.success(t("product.product-list.create-success"));
      onSuccess?.();
      onHandleClose?.();
    }
  }, [createProductData]);

  useEffect(() => {
    if (updateProductData && !updateProductErrors) {
      toast.success(t("product.product-list.update-success"));
      onSuccess?.();
      onHandleClose?.();
    }
  }, [updateProductData]);

  const handleSubmit = (values: IFormValue) => {
    const idBranch = branchSelected && branchSelected._id;
    if (editField) {
      updateProduct({
        variables: {
          id: editField._id!,
          productUpdateInput: {
            price: values.price as number,
            productCategory: values.category,
            description: values.description,
            name: values.name,
            thumbnail: {
              medium: avatarSelected?.medium as string,
              small: avatarSelected?.small as string,
            },
            images: imagesSelected as ImageInput[],
            tags: tagSelected.map(tag => tag._id) as string[],
            toppings: values.topping as ToppingInput[],
          },
        },
      });
      return;
    }
    createProduct({
      variables: {
        productCreateInput: {
          price: values.price as number,
          productCategory: values.category,
          description: values.description,
          name: values.name,
          thumbnail: {
            medium: avatarSelected?.medium as string,
            small: avatarSelected?.small as string,
          },
          images: imagesSelected as ImageInput[],
          status: ProductStatus.Instock,
          tags: tagSelected.map(tag => tag._id) as string[],
          branch: idBranch as string,
          toppings: values.topping as ToppingInput[],
        },
      },
    });
  };

  const onHandleClose = () => {
    setIsOpen(false);
    onClose && onClose();
  };

  return (
    <>
      <ElementWrapper
        onClick={() => setIsOpen(true)}
        className="flex justify-end w-full"
      >
        {ButtonMenu}
      </ElementWrapper>
      <Dialog
        size="lg"
        open={isOpen}
        onClose={onHandleClose}
        className="overflow-auto"
      >
        <UserDialogContainer>
          <DialogTitle>{title}</DialogTitle>
          <Formik
            enableReinitialize
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={handleSubmit}
          >
            <Form>
              <FormGroup>
                <ElementWrapper className="flex flex-col gap-y-4">
                  <SingleImageUploaderV2
                    label={t("product.product-list.avatar")}
                    name="avatar"
                    image={avatarSelected}
                    onChange={value => setAvatarSelected(value)}
                    required
                    customSize={CUSTOM_SIZE_CATEGORY_LV_2}
                  />
                  <MultipleImageUploaderV2
                    label={t("product.product-list.other-images")}
                    images={imagesSelected}
                    name="images"
                    onChange={value => setImagesSelected(value)}
                    required
                    customSize={CUSTOM_SIZE_CATEGORY_LV_2}
                  />
                  <Input
                    name="name"
                    label={t("product.product-list.name")}
                    required
                    placeholder={t("product.product-list.enter-product-name")}
                  />
                  <Select
                    name="category"
                    label={t("product.product-list.category")}
                    placeholder={t(
                      "product.product-list.please-select-category",
                    )}
                    optionSelected={categorySelected}
                    options={
                      listProductCategory?.getAllProductCategory?.results || []
                    }
                    onSelect={value => setCategorySelected(value)}
                    required
                  />
                </ElementWrapper>
                <ElementWrapper className="flex flex-col gap-y-4 pt-3">
                  <Input
                    name="price"
                    label={t("product.product-list.price")}
                    placeholder={t("product.product-list.enter-price")}
                    required
                    type="number"
                  />
                  <MultipleSelect
                    name="tags"
                    label={t("product.product-list.tags")}
                    required
                    placeholder={t("product.product-list.select-tags")}
                    options={listTag}
                    onSelect={tags => setTagSelected(tags)}
                    listOptionsSelected={tagSelected}
                  />
                  <TextArea
                    name="description"
                    value={initialValues.description}
                    label={t("product.product-list.description")}
                  />
                  <AddTopping
                    name="topping"
                    existedToppings={editField?.toppings as Topping[]}
                  />
                </ElementWrapper>
              </FormGroup>
              <ButtonWrapper>
                <Button outline type="button" onClick={onHandleClose}>
                  {t("common.cancel")}
                </Button>
                <Button
                  primary
                  type="submit"
                  loading={updateProductLoading || createProductLoading}
                >
                  {t("common.accept")}
                </Button>
              </ButtonWrapper>
            </Form>
          </Formik>
        </UserDialogContainer>
      </Dialog>
    </>
  );
};

export default ProductDialog;
