import React, { useEffect, useState } from "react";
import GroupTab from "designs/TabsAction";
import { toast } from "react-toastify";

//api
import { ProductCategory, useGetAllProductCategory } from "apiCaller";
import { fragmentGetAllProductCategory } from "services/product";

//locals
import MenuTab from "./Tab";
import { MenuLeftContainer } from "./styles";

interface IMenuLeftProps {}

const MenuLeft: React.FC<IMenuLeftProps> = props => {
  const [listProductCategory, setListProductCategory] = useState<
    ProductCategory[]
  >([]);
  const [
    getAllProductCategories,
    { data, error: getAllProductCategoriesErrors },
  ] = useGetAllProductCategory(fragmentGetAllProductCategory);

  useEffect(() => {
    invokeGetAllDataAPI();
  }, []);

  useEffect(() => {
    setListProductCategory(
      data?.getAllProductCategory.results as ProductCategory[],
    );
  }, [data]);

  useEffect(() => {
    if (getAllProductCategoriesErrors) {
      toast.error(getAllProductCategoriesErrors.message);
    }
  }, []);

  const invokeGetAllDataAPI = () => {
    getAllProductCategories({
      variables: {
        filterProductCategory: {},
      },
    });
  };

  return (
    <>
      {listProductCategory ? (
        <MenuLeftContainer>
          <GroupTab
            titles={
              listProductCategory.map(category => category.name) as string[]
            }
            content={listProductCategory.map(item => (
              <MenuTab _id={item._id as string} />
            ))}
          />
        </MenuLeftContainer>
      ) : (
        <></>
      )}
    </>
  );
};

export default MenuLeft;
