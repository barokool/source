import styled from "styled-components/macro";
import tw from "twin.macro";

export const OrderItemContainer = styled.div<{ active?: boolean }>`
  ${tw`grid grid-cols-2 px-2 border-2 rounded border-transparent py-1.5 cursor-pointer h-15`}
  ${({ active }) => active && tw` border-neutral-3 border-dashed`}
`;

export const Box = styled.div`
  ${tw`  `}
`;
export const LeftSide = styled.div`
  ${tw`  `}
`;
export const RightSide = styled.div`
  ${tw`flex flex-col-reverse justify-between items-end`}
`;
export const Code = styled.p`
  ${tw`text-13`}
`;
export const Name = styled.p`
  ${tw`text-16 font-medium`}
`;
export const Time = styled.p`
  ${tw`text-14 text-body-color`}
`;
export const Phone = styled.p`
  ${tw`text-14`}
`;
export const Title = styled.p`
  ${tw`text-13 text-body-color text-right`}
`;
export const Content = styled.p`
  ${tw`text-14  font-medium text-right`}
`;
