import React, { useState, useEffect } from "react";
import RadioGroup from "components/RadioGroup";
import { Formik } from "formik";
import * as Yup from "yup";
import Input from "designs/InputV2";
import { toast } from "react-toastify";
import { forceTextInputEnterNumber } from "common/functions/condition/forceTextInputEnterNumber";

//redux
import { useSelector, useDispatch } from "react-redux";
import { setActionSuccess } from "redux/slices/common";
import { setOrderSelected } from "redux/slices/order";

//api
import {
  OrderStatusEnum,
  useUpdateOrder,
  useGetOrderById,
  Order,
  PaymentTypeEnum,
} from "apiCaller";
import { fragmentUpdateOrder, fragmentGetOrderById } from "services/order";

import { IRootState } from "typings";

//languages
import { t } from "language";

//locals
import {
  RightPaymentContainer,
  Bottom,
  BoxPoint,
  CustomRadio,
  Middle,
  Point,
  RadioItem,
  Top,
  ItemWrapper,
  PhoneNumberFieldWrapper,
  CustomerPayFieldWrapper,
  ItemLeft,
  ItemRight,
} from "./styles";

interface IRightPaymentProps {}

interface IFormValue {
  phoneNumber: string;
}

const validationSchema = Yup.object().shape<{
  [key in keyof IFormValue]: any;
}>({
  phoneNumber: Yup.string().required(t("selling.please-enter-phone-number")),
});

const RightPayment: React.FC<IRightPaymentProps> = props => {
  //redux
  const dispatch = useDispatch();
  const { orderSelected } = useSelector((state: IRootState) => state.order);

  //local state
  const [initialValues] = useState<IFormValue>({
    phoneNumber: "",
  });
  const [customerPhoneNumber, setCustomerPhoneNumber] = useState<string>();
  const [paymentTypeSelected, setPaymentTypeSelected] = useState<{
    id: string;
    name: string;
  } | null>(null);
  const [listPayment] = useState<{ id: string; name: string }[]>([
    {
      id: "1",
      name: t("selling.cash"),
    },
    {
      id: "2",
      name: t("selling.credit-card"),
    },
  ]);
  const [order, setOrder] = useState<Order | null>(null);

  //api
  const [updateOrder, { data: orderUpdated, error: updateOrderErrors }] =
    useUpdateOrder(fragmentUpdateOrder);
  const [getOrderById, { data: orderReturned, error: getOrderByIdErrors }] =
    useGetOrderById(fragmentGetOrderById);

  //catch returned data from api
  useEffect(() => {
    if (orderReturned) {
      setOrder(orderReturned?.getOrderById as Order);
    }
  }, [orderReturned]);

  useEffect(() => {
    if (order) {
      dispatch(setOrderSelected(order));
    }
  }, [order]);

  useEffect(() => {
    if (orderUpdated) {
      dispatch(setActionSuccess());
      toast.success(t("selling.order-in-processing"));
      handleRefreshOrder();
    }
  }, [orderUpdated]);

  //toast errors when they occur
  useEffect(() => {
    if (updateOrderErrors) {
      toast.error(updateOrderErrors.message);
    }
  }, [updateOrderErrors]);

  useEffect(() => {
    if (getOrderByIdErrors) {
      toast.error(getOrderByIdErrors.message);
    }
  }, [getOrderByIdErrors]);

  //functions
  const handleRefreshOrder = async () => {
    try {
      await getOrderById({
        variables: { id: orderSelected?._id as string },
      });
    } catch (error) {
      console.log(error);
    }
  };

  const handleCancel = async () => {
    try {
      dispatch(setOrderSelected(null));
      dispatch(setActionSuccess());
      toast.success(t("selling.cancel-checkout-success"));
      handleRefreshOrder();
    } catch (error) {
      console.log(error);
    }
  };

  const handleProcessOrder = async () => {
    if (!initialValues.phoneNumber) {
      toast.error(t("selling.please-enter-phone-number"));
    } else {
      try {
        await updateOrder({
          variables: {
            id: orderSelected?._id as string,
            input: {
              status: "processing" as OrderStatusEnum,
              phoneNumber: customerPhoneNumber,
              paymentType: paymentTypeSelected?.id as PaymentTypeEnum,
            },
          },
        });
      } catch (error) {
        console.log(error);
      }
    }
  };

  return (
    <Formik
      enableReinitialize
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={handleProcessOrder}
    >
      <RightPaymentContainer>
        {/*Top*/}
        <Top.Container>
          <ItemWrapper>
            <ItemRight className="text-black">
              {orderSelected?.createdAt?.toString() || "12:11AM Wed 06 23 2022"}
            </ItemRight>
          </ItemWrapper>
          <PhoneNumberFieldWrapper>
            <ItemLeft>{t("selling.phone-number")}</ItemLeft>
            <Input
              name="phoneNumber"
              label={t("selling.phone-number")}
              placeholder={t("selling.enter-phone-number")}
              onChangeValue={phoneNumber =>
                setCustomerPhoneNumber(phoneNumber as string)
              }
              onKeyDown={forceTextInputEnterNumber}
            />
          </PhoneNumberFieldWrapper>
          <ItemWrapper>
            <ItemLeft>{t("selling.discount")}</ItemLeft>
            <ItemRight>
              {orderSelected?.coupon?.value?.toString().prettyMoney ||
                t("selling.no-info")}
            </ItemRight>
          </ItemWrapper>
          <ItemWrapper>
            <ItemLeft>{t("selling.final-price")}</ItemLeft>
            <ItemRight className="font-semibold">
              {orderSelected?.price?.finalPrice}
            </ItemRight>
          </ItemWrapper>
        </Top.Container>

        {/*Middle*/}
        <Middle.Container>
          <CustomerPayFieldWrapper>
            <ItemLeft>{t("selling.customer-pay")}</ItemLeft>
          </CustomerPayFieldWrapper>
          <ItemWrapper>
            <ItemLeft>{t("selling.payment-method")}</ItemLeft>
            <RadioGroup
              options={listPayment}
              onChange={option => setPaymentTypeSelected(option)}
              optionSelected={paymentTypeSelected}
              renderOption={({ option, checked }) => (
                <RadioItem.Container>
                  <RadioItem.LeftWrapper>
                    <BoxPoint>
                      <Point>
                        <CustomRadio active={checked} />
                      </Point>
                    </BoxPoint>
                    <RadioItem.Label>{option?.name}</RadioItem.Label>
                  </RadioItem.LeftWrapper>
                </RadioItem.Container>
              )}
            />
          </ItemWrapper>
        </Middle.Container>

        {/*Bottom*/}
        <Bottom.Container>
          <ItemWrapper className="mt-2">
            <Bottom.ButtonWrapper>
              <Bottom.Button
                outline
                onClick={() => {
                  dispatch(setOrderSelected(null));
                  handleCancel();
                }}
              >
                {t("common.cancel")}
              </Bottom.Button>
              <Bottom.Button primary type="submit">
                {t("common.accept")}
              </Bottom.Button>
            </Bottom.ButtonWrapper>
          </ItemWrapper>
        </Bottom.Container>
      </RightPaymentContainer>
    </Formik>
  );
};

export default RightPayment;
