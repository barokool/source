import styled from "styled-components/macro";
import tw from "twin.macro";

export const SectionContainer = styled.div`
  ${tw`flex flex-col gap-1`}
`;

export const Title = styled.p`
  ${tw`text-16 font-semibold text-black`}
`;

export const Body = styled.div`
  ${tw`p-2 shadow flex flex-col gap-2 `}
`;

export const Item = {
  Wrapper: styled.div`
    ${tw`flex flex-col gap-0.5`}
  `,
  ElementWrapper: styled.div`
    ${tw`flex justify-between`}
  `,
  TextWrapper: styled.div`
    ${tw`flex flex-col `}
  `,
  TextSecondary: styled.p`
    ${tw`font-regular text-secondary text-13 blur-sm`}
  `,
  TextPrimary: styled.p`
    ${tw`text-16 text-secondary font-semibold`}
  `,
};
