import React, { useState } from "react";
import {
  HeaderContainer,
  HeaderLabel,
  MenuPageContainer,
  ScrollItem,
  ScrollMenuContainer,
  AddButtonWrapper,
  DishQuantity,
  LeftButtonContent,
  RightButtonContent,
  QuantityContent,
} from "./styles";
import Item1 from "assets/images/qr-order/food-image-1.png";
import Item2 from "assets/images/qr-order/food-image-2.png";
import Item3 from "assets/images/qr-order/food-image-3.png";
import Item4 from "assets/images/qr-order/food-image-4.png";
import Item5 from "assets/images/qr-order/food-image-5.png";
import Item6 from "assets/images/qr-order/food-image-6.png";

import DishCard from "components/DishCard";
import CheckOutButton from "components/CheckOutButton";
import SVG from "designs/SVG";
import Prototype from "assets/images/qr-order/food-image.png";

interface IMenuPageProps {}

export interface IDish {
  name?: string;
  price?: number;
  thumbnail?: typeof Item1;
  quantity?: number;
}

const MenuPage: React.FC<IMenuPageProps> = props => {
  const [datas, setData] = useState<IDish[]>([
    {
      name: "Mực chiên nước mắm",
      price: 155000,
      thumbnail: Item1,
      quantity: 0,
    },
    {
      name: "Tàu hũ nhồi thịt",
      price: 155000,
      thumbnail: Item2,
      quantity: 0,
    },
    {
      name: "Gà chiên nước mắm",
      price: 200000,
      thumbnail: Item3,
      quantity: 0,
    },
    {
      name: "Gà chiên nước mắm",
      price: 200000,
      thumbnail: Item4,
      quantity: 0,
    },
    {
      name: "Gà chiên nước mắm",
      price: 200000,
      thumbnail: Item5,
      quantity: 0,
    },
    {
      name: "Gà chiên nước mắm",
      price: 200000,
      thumbnail: Item6,
      quantity: 0,
    },
  ]);

  const [listCategory, setListCategory] = useState<
    { id: string; name: string }[]
  >([
    {
      id: "1",
      name: "Điểm tâm",
    },
    {
      id: "2",
      name: "Cơm Quảng Nam",
    },
    {
      id: "3",
      name: "Cơm Mười Khó",
    },
    {
      id: "4",
      name: "Cơm Tàu",
    },
  ]);

  const [categorySelected, setCategorySelected] =
    useState<{ id: string; name: string }>();
  const [quantity, setQuantity] = useState<number>(0);

  return (
    <MenuPageContainer>
      <HeaderContainer>
        <HeaderLabel>Menu</HeaderLabel>
        <SVG name="common/search" />
      </HeaderContainer>

      <ScrollMenuContainer>
        {listCategory.map(item => (
          <ScrollItem
            onClick={() => setCategorySelected(item)}
            isSelected={item.id === categorySelected?.id}
          >
            {item.name}
          </ScrollItem>
        ))}
      </ScrollMenuContainer>

      <img src={Prototype} alt="/" width={600} className="mb-2" />

      {datas.map((item, index) => (
        <DishCard
          dish={item}
          RightButton={
            <AddButtonWrapper>
              <DishQuantity>{quantity > 0 ? quantity : ""}</DishQuantity>
              <SVG name="qrOrder/add-dish" onClick={() => {}} />
            </AddButtonWrapper>
          }
        />
      ))}

      <CheckOutButton
        LeftElements={
          <>
            {" "}
            <LeftButtonContent>Xem giỏ hàng</LeftButtonContent>
            <QuantityContent>1</QuantityContent>{" "}
          </>
        }
        RightElements={<RightButtonContent>đ 155,000</RightButtonContent>}
      />
    </MenuPageContainer>
  );
};

export default MenuPage;
